# Python Program for quick gmsh grid gen

import string,sys
import scipy.interpolate as intp
import numpy as np

def interpol60SymmetricAirfoilpts (x,y):
    
    xNLE = np.linspace(0,0.08,15)
    xNRest = np.linspace(0.09,1.0,16)
    xN = np.zeros(31)
    xN[:15] = xNLE
    xN[15:] = xNRest
#    Symmetric length
    n = len(x)/2
    interpol = intp.interp1d(x[0:n],y[0:n], bounds_error = False)
    yN = interpol(xN)
    xnp = np.concatenate((xN,xN))
    ynp = np.concatenate((yN,-yN))
    xnp[:31] = np.flipud(xnp[:31])
    ynp[:31] = np.flipud(ynp[:31])
    for i in range(len(xnp)):
	if np.isnan(ynp[i]):
		ynp[i] = 0
		ynp = np.delete(ynp, i+1)
		xnp = np.delete(xnp, i+1)
		break	
    return[xnp,ynp]

def interpol60Airfoilpts (x,y):
    
    xNLE = np.linspace(0,0.08,15)
    xNRest = np.linspace(0.09,1.0,16)
    xN = np.zeros(31)
    xN[:15] = xNLE
    xN[15:] = xNRest
    xNLE2 = np.linspace(0,0.08,15)
    xNRest2 = np.linspace(0.09,1.0,16)
    xN2 = np.zeros(31)
    xN2[:15] = xNLE2
    xN2[15:] = xNRest2
#    Non-Symmetric length
    n = len(x)/2
    interpol = intp.interp1d(x[0:n],y[0:n], bounds_error = False)
    yN = interpol(xN)
    interpol2 = intp.interp1d(x[n:],y[n:], bounds_error = False)
    yN2 = interpol2(xN2)
    xnp = np.concatenate((xN,xN2))
    ynp = np.concatenate((yN,yN2))
    xnp[:31] = np.flipud(xnp[:31])
    ynp[:31] = np.flipud(ynp[:31])
    for i in range(len(xnp)):
	if np.isnan(ynp[i]):
		ynp[i] = 0
		ynp = np.delete(ynp, i+1)
		xnp = np.delete(xnp, i+1)
		break
    ynp[0] = 0
    xnp[0] = 1
    ynp[-1] = 0
    xnp[-1] = 1
    print 'x'
    print xnp
    print 'y'
    print ynp
    print len(xnp)
    return[xnp,ynp]

def readDatAirfoil(filename):
    fin=open(filename, 'r')
    i=0
    x = []; y = [];
    lines = fin.readlines()

    for line in lines:
		# comments indicated by #
		if line[0] != '#' or line[0] != ' ':
			i=i+1
			data = string.split(line)
			x.append(float(data[0]))
			y.append(float(data[1]))
    return [x,y]

def writeDatAirfoil(filename,x,y):
    	# Write data out with this;

	fout = open(filename,'w')
	for i in range(len(x)):
		outputline  = " %8.8f %8.8f \n" \
					      % (x[i],y[i] )
		fout.write(outputline)
	fout.close

def convert_pts_togmsh(filename,outputname,startpoint):

	# Read in data using this bit
	fin=open(filename, 'r')
	i=0
	x = []; y = []; z = [];

	lines = fin.readlines()

	for line in lines:
		# comments indicated by #
		if line[0] != '#' or line[0] != ' ':
			i=i+1
			data = string.split(line)
			x.append(float(data[0]))
			y.append(float(data[1]))
                # Identify 2D geometries
                if len(data) == 2:
                    z.append(float(0))
                else:
                    z.append(float(data[2]))
                n_lines = int(i)

	# Write data out with this;

	fout = ''

	lc_name = "lc"
	# Format
	# Point(1) = {0, 0, 0, lc};
	fout += lc_name + ' = 0.005 ;\n'
	j = startpoint
	for i in range(n_lines):
		outputline  = "Point(%i) = { %8.8f, %8.8f, %8.8f, %s};\n " \
					      % (j,x[i],y[i],z[i],lc_name )
		j = j + 1
		fout += outputline

	# gmsh bspline format
	# Write out splinefit line
	# fout += "Spline(%d) = {%d :%d};\n" \
	# % ((startpoint,startpoint,startpoint+n_lines-1))
	fout = fout.rstrip('\n')
	finalContent = []
	finalContent.append(fout)
	with open('../gmsh/SD7003/SD7003toModWOPoints.geo','r') as f:
		content = f.readlines()
	for item in content:
		finalContent.append(str(item))
	finalContent = [finalC.replace('#xPoint1023',str(x[23])) for finalC in finalContent]
	finalContent = [finalC.replace('#xPoint1038',str(x[38])) for finalC in finalContent]
	finalContent = [finalC.replace('#yPoint1023',str(y[23])) for finalC in finalContent]
	finalContent = [finalC.replace('#yPoint1038',str(y[38])) for finalC in finalContent]
     	with open('../gmsh/SD7003/SD7003toModWPoints.geo','w') as f:
		for item in finalContent:
			f.write(str(item))
	fin.close







inputfile = 'profile.dat'
# inputfile = 'SD7003/data'
[x,y] = readDatAirfoil(inputfile)
[x,y] = interpol60Airfoilpts(x,y)
writeDatAirfoil(inputfile+'mod',x,y)
convert_pts_togmsh(inputfile+'mod',inputfile+".geo",1000)

