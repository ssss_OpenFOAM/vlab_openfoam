// NACA5012 Aerofoil Shape and Spline fit

//Suction Side Points define a different characteristic length for suction side and pressure side
 nac_lc = 0.005;
 Point(1000) = { 0.00000000, 0.00000000, 0.00000000, nac_lc};
 Point(1001) = { 0.03448276, 0.03017095, 0.00000000, nac_lc};
 Point(1002) = { 0.06896552, 0.04061835, 0.00000000, nac_lc};
 Point(1003) = { 0.10344828, 0.04735474, 0.00000000, nac_lc};
 Point(1004) = { 0.13793103, 0.05211300, 0.00000000, nac_lc};
 Point(1005) = { 0.17241379, 0.05545321, 0.00000000, nac_lc};
 Point(1006) = { 0.20689655, 0.05772185, 0.00000000, nac_lc};
 Point(1007) = { 0.24137931, 0.05914974, 0.00000000, nac_lc};
 Point(1008) = { 0.27586207, 0.05987043, 0.00000000, nac_lc};
 Point(1009) = { 0.31034483, 0.05996594, 0.00000000, nac_lc};
 Point(1010) = { 0.34482759, 0.05958426, 0.00000000, nac_lc};
 Point(1011) = { 0.37931034, 0.05871127, 0.00000000, nac_lc};
 Point(1012) = { 0.41379310, 0.05748124, 0.00000000, nac_lc};
 Point(1013) = { 0.44827586, 0.05587838, 0.00000000, nac_lc};
 Point(1014) = { 0.48275862, 0.05398381, 0.00000000, nac_lc};
 Point(1015) = { 0.51724138, 0.05181015, 0.00000000, nac_lc};
 Point(1016) = { 0.55172414, 0.04938509, 0.00000000, nac_lc};
 Point(1017) = { 0.58620690, 0.04674713, 0.00000000, nac_lc};
 Point(1018) = { 0.62068966, 0.04388795, 0.00000000, nac_lc};
 Point(1019) = { 0.65517241, 0.04085604, 0.00000000, nac_lc};
 Point(1020) = { 0.68965517, 0.03762922, 0.00000000, nac_lc};
 Point(1021) = { 0.72413793, 0.03424721, 0.00000000, nac_lc};
 Point(1022) = { 0.75862069, 0.03069518, 0.00000000, nac_lc};
 Point(1023) = { 0.79310345, 0.02698558, 0.00000000, nac_lc};
 Point(1024) = { 0.82758621, 0.02312035, 0.00000000, nac_lc};
 Point(1025) = { 0.86206897, 0.01909404, 0.00000000, nac_lc};
 Point(1026) = { 0.89655172, 0.01490179, 0.00000000, nac_lc};
 Point(1027) = { 0.93103448, 0.01054205, 0.00000000, nac_lc};
 Point(1028) = { 0.96551724, 0.00599617, 0.00000000, nac_lc};
 Point(1029) = { 1.00000000, 0.00126000, 0.00000000, nac_lc};
 Point(1030) = { 0.00000000, -0.00000000, 0.00000000, nac_lc};
 Point(1031) = { 0.03448276, -0.03017095, 0.00000000, nac_lc};
 Point(1032) = { 0.06896552, -0.04061835, 0.00000000, nac_lc};
 Point(1033) = { 0.10344828, -0.04735474, 0.00000000, nac_lc};
 Point(1034) = { 0.13793103, -0.05211300, 0.00000000, nac_lc};
 Point(1035) = { 0.17241379, -0.05545321, 0.00000000, nac_lc};
 Point(1036) = { 0.20689655, -0.05772185, 0.00000000, nac_lc};
 Point(1037) = { 0.24137931, -0.05914974, 0.00000000, nac_lc};
 Point(1038) = { 0.27586207, -0.05987043, 0.00000000, nac_lc};
 Point(1039) = { 0.31034483, -0.05996594, 0.00000000, nac_lc};
 Point(1040) = { 0.34482759, -0.05958426, 0.00000000, nac_lc};
 Point(1041) = { 0.37931034, -0.05871127, 0.00000000, nac_lc};
 Point(1042) = { 0.41379310, -0.05748124, 0.00000000, nac_lc};
 Point(1043) = { 0.44827586, -0.05587838, 0.00000000, nac_lc};
 Point(1044) = { 0.48275862, -0.05398381, 0.00000000, nac_lc};
 Point(1045) = { 0.51724138, -0.05181015, 0.00000000, nac_lc};
 Point(1046) = { 0.55172414, -0.04938509, 0.00000000, nac_lc};
 Point(1047) = { 0.58620690, -0.04674713, 0.00000000, nac_lc};
 Point(1048) = { 0.62068966, -0.04388795, 0.00000000, nac_lc};
 Point(1049) = { 0.65517241, -0.04085604, 0.00000000, nac_lc};
 Point(1050) = { 0.68965517, -0.03762922, 0.00000000, nac_lc};
 Point(1051) = { 0.72413793, -0.03424721, 0.00000000, nac_lc};
 Point(1052) = { 0.75862069, -0.03069518, 0.00000000, nac_lc};
 Point(1053) = { 0.79310345, -0.02698558, 0.00000000, nac_lc};
 Point(1054) = { 0.82758621, -0.02312035, 0.00000000, nac_lc};
 Point(1055) = { 0.86206897, -0.01909404, 0.00000000, nac_lc};
 Point(1056) = { 0.89655172, -0.01490179, 0.00000000, nac_lc};
 Point(1057) = { 0.93103448, -0.01054205, 0.00000000, nac_lc};
 Point(1058) = { 0.96551724, -0.00599617, 0.00000000, nac_lc};
 Point(1059) = { 1.00000000, -0.00126000, 0.00000000, nac_lc};


// Put a Spline fit through the appropriate points - note that we could 
// also use BSpline but you end up with a different shape 

Spline(2000) = {1023:1031,1031};
//Spline(2001) = {1000:1023,1023};
Spline(2001) = {1023,1022,1021,1020,1019,1018,1017,1016,1015,1014,1013,1012,1011,1010,1009,1008,1007,1006,1005,1004,1003,1002,1001};
Spline(2002) = {1059,1058,1057,1056,1055,1054,1053,1052,1051,1050,1049,1048,1047,1046,1045,1044,1043,1042,1041,1040,1039,1038};
Spline(2003) = {1038:1031,1031};

edge_lc = 0.2;


Line Loop (2) = {1000};



// We need to extrude the surface so that the mesh is 3D for OpenFOAM
R = 20;
xAfterProfile = 20;
pointIntrados = 1040;
heightBoxAirfoil = 0.2;
angleWake = 75;
firstPointXBox = -0.15;
thetaLeftBoxUp = 70*3.14159/180;
thetaLeftBoxDown = 75*3.14159/180;
thetaRightBox = 75*3.14159/180;
xPoint1004 = 0.13793103;
yPoint1004 = 0.05211300;
xBoxLeftUp = xPoint1004 - (heightBoxAirfoil-yPoint1004)*Cos(thetaLeftBoxUp)/Sin(thetaLeftBoxUp);
xPoint1034 = 0.13793103;
yPoint1034 = -0.05211300;
xBoxLeftDown = xPoint1034 - (heightBoxAirfoil+yPoint1034)*Cos(thetaLeftBoxDown)/Sin(thetaLeftBoxDown);
xBoxRight = 1.0+heightBoxAirfoil/Tan(thetaRightBox);
pi = 3.14159;
Point(100) = {firstPointXBox,0.0,0.0,1.0};
Point(101) = {xBoxLeftUp,heightBoxAirfoil,0.0,1.0};
Point(102) = {xBoxLeftDown,-heightBoxAirfoil,0.0,1.0};
Point(103) = {xBoxRight,heightBoxAirfoil,0.0,1.0};
Point(104) = {xBoxRight,-heightBoxAirfoil,0.0,1.0};

angleSpline = 70;
angleSpline2 = 30;
firstPointXBox = firstPointXBox*1.00;
Point(105) = {firstPointXBox*Cos(angleSpline*pi/180),-firstPointXBox*Sin(angleSpline*pi/180),0.0,1.0};
Point(106) = {firstPointXBox*Cos(angleSpline*pi/180),firstPointXBox*Sin(angleSpline*pi/180),0.0,1.0};
Point(107) = {firstPointXBox*Cos(angleSpline2*pi/180),-firstPointXBox*Sin(angleSpline2*pi/180),0.0,1.0};
Point(108) = {firstPointXBox*Cos(angleSpline2*pi/180),firstPointXBox*Sin(angleSpline2*pi/180),0.0,1.0};
Line(1) = {1038,102};
Line(2) = {1023,101};
Line(3) = {1031,100};

Line(1001) = {101, 103};
Line(1002) = {1059, 103};
Line(1003) = {1059, 104};
Line(1004) = {104, 102};

afterWakeAngle = 5*pi/180;

Point(109) = {xAfterProfile, heightBoxAirfoil+xAfterProfile*Tan(afterWakeAngle),0.0,1.0};
Point(110) = {xAfterProfile, -heightBoxAirfoil-xAfterProfile*Tan(afterWakeAngle),0.0,1.0};
Point(111) = {-R,0.0,0.0,1.0};
Point(112) = {-R*Cos(thetaLeftBoxUp),R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(113) = {-R*Cos(thetaLeftBoxDown),-R*Sin(thetaLeftBoxDown),0.0,1.0};
Point(114) = {xAfterProfile,R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(115) = {xAfterProfile,-R*Sin(thetaLeftBoxDown),0.0,1.0};

angleSpline = 20;
angleSpline2 = 45;
Point(116) = {-R*Cos(angleSpline2*pi/180),R*Sin(angleSpline2*pi/180),0.0,1.0};
Point(117) = {-R*Cos(angleSpline2*pi/180),-R*Sin(angleSpline2*pi/180),0.0,1.0};

Point(118) = {-R*Cos(angleSpline*pi/180),R*Sin(angleSpline*pi/180),0.0,1.0};
Point(119) = {-R*Cos(angleSpline*pi/180),-R*Sin(angleSpline*pi/180),0.0,1.0};

Point(120) = {xAfterProfile,0.0,0.0,1.0};
Point(121) = {1.0+R*Sin(thetaLeftBoxUp)/Tan(thetaRightBox),R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(122) = {1.0+R*Sin(thetaLeftBoxDown)/Tan(thetaRightBox),-R*Sin(thetaLeftBoxDown),0.0,1.0};

BSpline(1007) = {102,106,108,100};
BSpline(1022) = {100,107,105,101};


Line(1008) = {101, 112};
Line(1010) = {114, 109};
Line(1011) = {109, 103};
Line(1013) = {104, 110};
Line(1014) = {110, 115};

Spline(3016) = {112, 116, 118, 111};
Spline(3017) = {111, 119, 117, 113};
Line(1017) = {102, 113};
Line(1018) = {111, 100};
Line(1019) = {1059, 120};
Line(1020) = {104, 122};
Line(1021) = {103, 121};

Line Loop(2003) = {2002, 1, -1004, -1003};
Plane Surface(2004) = {2003};
Line(2005) = {109, 120};
Line(2006) = {110, 120};
Line Loop(2007) = {1011, -1002, 1019, -2005};
Plane Surface(2008) = {2007};
Line Loop(2009) = {1019, -2006, -1013, -1003};
Plane Surface(2010) = {2009};
Line Loop(2011) = {2001, 1002, -1001, -2};
Plane Surface(2012) = {2011};
Line Loop(2013) = {2, -1022, -3, -2000};
Plane Surface(2014) = {2013};
Line Loop(2015) = {3, -1007, -1, 2003};
Plane Surface(2016) = {2015};
Line(2017) = {112, 121};
Line(2018) = {121, 114};
Line Loop(2019) = {2017, -1021, -1001, 1008};
Plane Surface(2020) = {2019};
Line Loop(2021) = {2018, 1010, 1011, 1021};
Plane Surface(2022) = {2021};
Line(2023) = {113, 122};
Line(2024) = {122, 115};
Line Loop(2025) = {1004, 1017, 2023, -1020};
Plane Surface(2026) = {2025};
Line Loop(2027) = {1013, 1014, -2024, -1020};
Plane Surface(2028) = {2027};
Line Loop(3018) = {3016, 1018, 1022, 1008};
Plane Surface(3019) = {3018};
Line Loop(3020) = {1007, -1018, 3017, -1017};
Plane Surface(3021) = {3020};



// Divisions in the mesh

XTopDiv = 70;
XBottomDiv = 70;
YAirfoilBlockDiv = 124;
YAirfoilBlockGrading = 1.05;
YBigBlockGrading = 1.05;
XGradingTop = 1.02;
YBigBlockDiv = 96;
XBottomLEDiv = 60;
XTopLEDiv = 60;
XWakeDiv = 100;
XWakeGrading = 1.05;
BumpAirfoil = 4;
Transfinite Line{2,1002} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{1,1003} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{3,2} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{3,1} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2003,1007} = XBottomLEDiv;
Transfinite Line{1007,3017} = XBottomLEDiv;
Transfinite Line{2000,1022} = XTopLEDiv;
Transfinite Line{1022,3016} = XTopLEDiv;
Transfinite Line{1001,2017} = XTopDiv Using Bump 1/BumpAirfoil;
Transfinite Line{1001,2001} = XTopDiv Using Bump 1/BumpAirfoil;
Transfinite Line{2002,1004} = XBottomDiv Using Bump 1/BumpAirfoil;
Transfinite Line{1004,2023} = XBottomDiv Using Bump 1/BumpAirfoil;

Transfinite Line{1014} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1020} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1020,1017} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1017,1018} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1018} = YBigBlockDiv Using Progression 1/YBigBlockGrading;
Transfinite Line{1008} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1021} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1021} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1010} = YBigBlockDiv Using Progression 1/YBigBlockGrading;
Transfinite Line{2024} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1013} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1019} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1011} = XWakeDiv Using Progression 1/XWakeGrading;
Transfinite Line{2018} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1002} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2005} = YAirfoilBlockDiv Using Progression 1/YAirfoilBlockGrading;
Transfinite Line{1003} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2006} = YAirfoilBlockDiv Using Progression 1/YAirfoilBlockGrading;


Transfinite Surface {2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};
Recombine Surface{2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};

Extrude {0, 0, 1} {
  Surface{2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};
  Layers{1};
  Recombine;
}



Physical Volume("fluid") = {1,2,3,4,5,6,7,8,9,10,11,12};

Physical Surface("inlet") = {3250,3280};
Physical Surface("top") = {3184,3162};
Physical Surface("bottom") = {3214,3236};
Physical Surface("outlet") = { 3232,3078,3064,3188};
Physical Surface("airfoilWall") = { 3096,3130,3152,3030};
Physical Surface("back") = { 2022,2020,3019,3021,2026,2028,2010,2008,2012,2014,2016,2004};
Physical Surface("front") = { 3197,3175,3263,3285,3219,3241,3087,3065,3109,3131,3153,3043};



