// NACA5012 Aerofoil Shape and Spline fit

//Suction Side Points define a different characteristic length for suction side and pressure side
SD7_lc = 0.005;
 Point(1000) = { 1.00000000, 0.00000000, 0.00000000, SD7_lc};
 Point(1001) = { 0.99681000, 0.00031000, 0.00000000, SD7_lc};
 Point(1002) = { 0.98745000, 0.00132000, 0.00000000, SD7_lc};
 Point(1003) = { 0.97235000, 0.00310000, 0.00000000, SD7_lc};
 Point(1004) = { 0.95193000, 0.00547000, 0.00000000, SD7_lc};
 Point(1005) = { 0.92639000, 0.00824000, 0.00000000, SD7_lc};
 Point(1006) = { 0.89600000, 0.01139000, 0.00000000, SD7_lc};
 Point(1007) = { 0.86112000, 0.01494000, 0.00000000, SD7_lc};
 Point(1008) = { 0.82224000, 0.01884000, 0.00000000, SD7_lc};
 Point(1009) = { 0.77985000, 0.02304000, 0.00000000, SD7_lc};
 Point(1010) = { 0.73449000, 0.02744000, 0.00000000, SD7_lc};
 Point(1011) = { 0.68673000, 0.03197000, 0.00000000, SD7_lc};
 Point(1012) = { 0.63717000, 0.03649000, 0.00000000, SD7_lc};
 Point(1013) = { 0.58641000, 0.04086000, 0.00000000, SD7_lc};
 Point(1014) = { 0.53499000, 0.04494000, 0.00000000, SD7_lc};
 Point(1015) = { 0.48350000, 0.04859000, 0.00000000, SD7_lc};
 Point(1016) = { 0.43249000, 0.05171000, 0.00000000, SD7_lc};
 Point(1017) = { 0.38250000, 0.05415000, 0.00000000, SD7_lc};
 Point(1018) = { 0.33405000, 0.05581000, 0.00000000, SD7_lc};
 Point(1019) = { 0.28760000, 0.05658000, 0.00000000, SD7_lc};
 Point(1020) = { 0.24358000, 0.05639000, 0.00000000, SD7_lc};
 Point(1021) = { 0.20240000, 0.05518000, 0.00000000, SD7_lc};
 Point(1022) = { 0.16442000, 0.05292000, 0.00000000, SD7_lc};
 Point(1023) = { 0.12993000, 0.04961000, 0.00000000, SD7_lc};
 Point(1024) = { 0.09921000, 0.04526000, 0.00000000, SD7_lc};
 Point(1025) = { 0.07244000, 0.03993000, 0.00000000, SD7_lc};
 Point(1026) = { 0.04978000, 0.03372000, 0.00000000, SD7_lc};
 Point(1027) = { 0.03130000, 0.02677000, 0.00000000, SD7_lc};
 Point(1028) = { 0.01702000, 0.01932000, 0.00000000, SD7_lc};
 Point(1029) = { 0.00697000, 0.01172000, 0.00000000, SD7_lc};
 Point(1030) = { 0.00127000, 0.00438000, 0.00000000, SD7_lc};
 Point(1031) = { 0.00025000, -0.00186000, 0.00000000, SD7_lc};
 Point(1032) = { 0.00457000, -0.00741000, 0.00000000, SD7_lc};
 Point(1033) = { 0.01408000, -0.01285000, 0.00000000, SD7_lc};
 Point(1034) = { 0.02839000, -0.01759000, 0.00000000, SD7_lc};
 Point(1035) = { 0.04763000, -0.02141000, 0.00000000, SD7_lc};
 Point(1036) = { 0.07182000, -0.02438000, 0.00000000, SD7_lc};
 Point(1037) = { 0.10073000, -0.02660000, 0.00000000, SD7_lc};
 Point(1038) = { 0.13407000, -0.02809000, 0.00000000, SD7_lc};
 Point(1039) = { 0.17150000, -0.02888000, 0.00000000, SD7_lc};
 Point(1040) = { 0.21268000, -0.02900000, 0.00000000, SD7_lc};
 Point(1041) = { 0.25719000, -0.02852000, 0.00000000, SD7_lc};
 Point(1042) = { 0.30456000, -0.02752000, 0.00000000, SD7_lc};
 Point(1043) = { 0.35426000, -0.02608000, 0.00000000, SD7_lc};
 Point(1044) = { 0.40572000, -0.02428000, 0.00000000, SD7_lc};
 Point(1045) = { 0.45837000, -0.02217000, 0.00000000, SD7_lc};
 Point(1046) = { 0.51161000, -0.01980000, 0.00000000, SD7_lc};
 Point(1047) = { 0.56484000, -0.01723000, 0.00000000, SD7_lc};
 Point(1048) = { 0.61748000, -0.01450000, 0.00000000, SD7_lc};
 Point(1049) = { 0.66898000, -0.01167000, 0.00000000, SD7_lc};
 Point(1050) = { 0.71883000, -0.00887000, 0.00000000, SD7_lc};
 Point(1051) = { 0.76644000, -0.00628000, 0.00000000, SD7_lc};
 Point(1052) = { 0.81118000, -0.00403000, 0.00000000, SD7_lc};
 Point(1053) = { 0.85241000, -0.00220000, 0.00000000, SD7_lc};
 Point(1054) = { 0.88957000, -0.00082000, 0.00000000, SD7_lc};
 Point(1055) = { 0.92210000, 0.00008000, 0.00000000, SD7_lc};
 Point(1056) = { 0.94952000, 0.00052000, 0.00000000, SD7_lc};
 Point(1057) = { 0.97134000, 0.00057000, 0.00000000, SD7_lc};
 Point(1058) = { 0.98718000, 0.00037000, 0.00000000, SD7_lc};
 Point(1059) = { 0.99679000, 0.00011000, 0.00000000, SD7_lc};
 Point(1060) = { 1.0000000, -0.00000000, 0.00000000, SD7_lc};

// Put a Spline fit through the appropriate points - note that we could 
// also use BSpline but you end up with a different shape 
//Spline(1000) = {1000:1060,1000};
Spline(2000) = {1023:1031,1031};
//Spline(2001) = {1000:1023,1023};
Spline(2001) = {1023,1022,1021,1020,1019,1018,1017,1016,1015,1014,1013,1012,1011,1010,1009,1008,1007,1006,1005,1004,1003,1002,1001,1060};
Spline(2002) = {1060,1059,1058,1057,1056,1055,1054,1053,1052,1051,1050,1049,1048,1047,1046,1045,1044,1043,1042,1041,1040,1039,1038};
Spline(2003) = {1038:1031,1031};

edge_lc = 0.2;


Line Loop (2) = {1000};



// We need to extrude the surface so that the mesh is 3D for OpenFOAM
R = 20;
xAfterProfile = 20;
pointIntrados = 1040;
heightBoxAirfoil = 0.2;
angleWake = 75;
firstPointXBox = -0.15;
thetaLeftBoxUp = 70*3.14159/180;
thetaLeftBoxDown = 75*3.14159/180;
thetaRightBox = 75*3.14159/180;
xPoint1023 = 0.12993000;
yPoint1023 = 0.04961;
xBoxLeftUp = xPoint1023 - (heightBoxAirfoil-yPoint1023)*Cos(thetaLeftBoxUp)/Sin(thetaLeftBoxUp);
xPoint1038 = 0.13407;
yPoint1038 = -0.02809;
xBoxLeftDown = xPoint1038 - (heightBoxAirfoil+yPoint1038)*Cos(thetaLeftBoxDown)/Sin(thetaLeftBoxDown);
xBoxRight = 1.0+heightBoxAirfoil/Tan(thetaRightBox);
pi = 3.14159;
Point(100) = {firstPointXBox,0.0,0.0,1.0};
Point(101) = {xBoxLeftUp,heightBoxAirfoil,0.0,1.0};
Point(102) = {xBoxLeftDown,-heightBoxAirfoil,0.0,1.0};
Point(103) = {xBoxRight,heightBoxAirfoil,0.0,1.0};
Point(104) = {xBoxRight,-heightBoxAirfoil,0.0,1.0};

angleSpline = 70;
angleSpline2 = 30;
firstPointXBox = firstPointXBox*1.00;
Point(105) = {firstPointXBox*Cos(angleSpline*pi/180),-firstPointXBox*Sin(angleSpline*pi/180),0.0,1.0};
Point(106) = {firstPointXBox*Cos(angleSpline*pi/180),firstPointXBox*Sin(angleSpline*pi/180),0.0,1.0};
Point(107) = {firstPointXBox*Cos(angleSpline2*pi/180),-firstPointXBox*Sin(angleSpline2*pi/180),0.0,1.0};
Point(108) = {firstPointXBox*Cos(angleSpline2*pi/180),firstPointXBox*Sin(angleSpline2*pi/180),0.0,1.0};
Line(1) = {1038,102};
Line(2) = {1023,101};
Line(3) = {1031,100};

Line(1001) = {101, 103};
Line(1002) = {1060, 103};
Line(1003) = {1060, 104};
Line(1004) = {104, 102};

afterWakeAngle = 5*pi/180;

Point(109) = {xAfterProfile, heightBoxAirfoil+xAfterProfile*Tan(afterWakeAngle),0.0,1.0};
Point(110) = {xAfterProfile, -heightBoxAirfoil-xAfterProfile*Tan(afterWakeAngle),0.0,1.0};
Point(111) = {-R,0.0,0.0,1.0};
Point(112) = {-R*Cos(thetaLeftBoxUp),R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(113) = {-R*Cos(thetaLeftBoxDown),-R*Sin(thetaLeftBoxDown),0.0,1.0};
Point(114) = {xAfterProfile,R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(115) = {xAfterProfile,-R*Sin(thetaLeftBoxDown),0.0,1.0};

angleSpline = 20;
angleSpline2 = 45;
Point(116) = {-R*Cos(angleSpline2*pi/180),R*Sin(angleSpline2*pi/180),0.0,1.0};
Point(117) = {-R*Cos(angleSpline2*pi/180),-R*Sin(angleSpline2*pi/180),0.0,1.0};

Point(118) = {-R*Cos(angleSpline*pi/180),R*Sin(angleSpline*pi/180),0.0,1.0};
Point(119) = {-R*Cos(angleSpline*pi/180),-R*Sin(angleSpline*pi/180),0.0,1.0};

Point(120) = {xAfterProfile,0.0,0.0,1.0};
Point(121) = {1.0+R*Sin(thetaLeftBoxUp)/Tan(thetaRightBox),R*Sin(thetaLeftBoxUp),0.0,1.0};
Point(122) = {1.0+R*Sin(thetaLeftBoxDown)/Tan(thetaRightBox),-R*Sin(thetaLeftBoxDown),0.0,1.0};

BSpline(1007) = {102,106,108,100};
BSpline(1022) = {100,107,105,101};


Line(1008) = {101, 112};
Line(1010) = {114, 109};
Line(1011) = {109, 103};
Line(1013) = {104, 110};
Line(1014) = {110, 115};

Spline(3016) = {112, 116, 118, 111};
Spline(3017) = {111, 119, 117, 113};
Line(1017) = {102, 113};
Line(1018) = {111, 100};
Line(1019) = {1060, 120};
Line(1020) = {104, 122};
Line(1021) = {103, 121};

Line Loop(2003) = {2002, 1, -1004, -1003};
Plane Surface(2004) = {2003};
Line(2005) = {109, 120};
Line(2006) = {110, 120};
Line Loop(2007) = {1011, -1002, 1019, -2005};
Plane Surface(2008) = {2007};
Line Loop(2009) = {1019, -2006, -1013, -1003};
Plane Surface(2010) = {2009};
Line Loop(2011) = {2001, 1002, -1001, -2};
Plane Surface(2012) = {2011};
Line Loop(2013) = {2, -1022, -3, -2000};
Plane Surface(2014) = {2013};
Line Loop(2015) = {3, -1007, -1, 2003};
Plane Surface(2016) = {2015};
Line(2017) = {112, 121};
Line(2018) = {121, 114};
Line Loop(2019) = {2017, -1021, -1001, 1008};
Plane Surface(2020) = {2019};
Line Loop(2021) = {2018, 1010, 1011, 1021};
Plane Surface(2022) = {2021};
Line(2023) = {113, 122};
Line(2024) = {122, 115};
Line Loop(2025) = {1004, 1017, 2023, -1020};
Plane Surface(2026) = {2025};
Line Loop(2027) = {1013, 1014, -2024, -1020};
Plane Surface(2028) = {2027};
Line Loop(3018) = {3016, 1018, 1022, 1008};
Plane Surface(3019) = {3018};
Line Loop(3020) = {1007, -1018, 3017, -1017};
Plane Surface(3021) = {3020};



// Divisions in the mesh

XTopDiv = 70;
XBottomDiv = 70;
YAirfoilBlockDiv = 124;
YAirfoilBlockGrading = 1.05;
YBigBlockGrading = 1.05;
XGradingTop = 1.02;
YBigBlockDiv = 96;
XBottomLEDiv = 60;
XTopLEDiv = 60;
XWakeDiv = 100;
XWakeGrading = 1.05;
BumpAirfoil = 4;
Transfinite Line{2,1002} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{1,1003} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{3,2} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{3,1} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2003,1007} = XBottomLEDiv;
Transfinite Line{1007,3017} = XBottomLEDiv;
Transfinite Line{2000,1022} = XTopLEDiv;
Transfinite Line{1022,3016} = XTopLEDiv;
Transfinite Line{1001,2017} = XTopDiv Using Bump 1/BumpAirfoil;
Transfinite Line{1001,2001} = XTopDiv Using Bump 1/BumpAirfoil;
Transfinite Line{2002,1004} = XBottomDiv Using Bump 1/BumpAirfoil;
Transfinite Line{1004,2023} = XBottomDiv Using Bump 1/BumpAirfoil;

Transfinite Line{1014} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1020} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1020,1017} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1017,1018} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1018} = YBigBlockDiv Using Progression 1/YBigBlockGrading;
Transfinite Line{1008} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1021} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1021} = YBigBlockDiv Using Progression YBigBlockGrading;
Transfinite Line{1010} = YBigBlockDiv Using Progression 1/YBigBlockGrading;
Transfinite Line{2024} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1013} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1019} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1011} = XWakeDiv Using Progression 1/XWakeGrading;
Transfinite Line{2018} = XWakeDiv Using Progression XWakeGrading;
Transfinite Line{1002} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2005} = YAirfoilBlockDiv Using Progression 1/YAirfoilBlockGrading;
Transfinite Line{1003} = YAirfoilBlockDiv Using Progression YAirfoilBlockGrading;
Transfinite Line{2006} = YAirfoilBlockDiv Using Progression 1/YAirfoilBlockGrading;


Transfinite Surface {2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};
Recombine Surface{2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};

Extrude {0, 0, 1} {
  Surface{2004,2008,2010,2012,2014,2016,2020,2022,2026,2028,3019,3021};
  Layers{1};
  Recombine;
}



Physical Volume("fluid") = {1,2,3,4,5,6,7,8,9,10,11,12};

Physical Surface("inlet") = {3250,3280};
Physical Surface("top") = {3184,3162};
Physical Surface("bottom") = {3214,3236};
Physical Surface("outlet") = { 3232,3078,3064,3188};
Physical Surface("airfoilWall") = { 3096,3130,3152,3030};
Physical Surface("back") = { 2022,2020,3019,3021,2026,2028,2010,2008,2012,2014,2016,2004};
Physical Surface("front") = { 3197,3175,3263,3285,3219,3241,3087,3065,3109,3131,3153,3043};



