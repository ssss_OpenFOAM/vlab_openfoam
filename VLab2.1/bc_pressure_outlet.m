function  bc_pressure_outlet (param)
%
% boundary condition type : pressure_outlet

% param     : parameters of the function
% call      : fluent_jou_cube.m

Fluent_ver      = param.Fluent_ver;     % 6 for 6.2.16, 6.3.26, ..., 1 for 13.0.0, 14.0.0, 14.5.0, ...
regime          = param.regime;
Tu_model        = param.Tu_model;
dim             = param.dim;
fid             = param.fid;
zone_name       = param.surf_bc_name;   % zone id/name [pressure_outlet.3] 
I_turb          = param.I_turb;         % Turbulence Intensity (%) [1]
L_turb          = param.L_turb;         % Turbulence Length Scale (m) [0.005]

PGP             = 'no';                 % Use Profile for Gauge Pressure? [no]
GP              = 0;                    % Gauge Pressure (pascal) [0] 
REPD            = 'no';                 % Radial Equilibrium Pressure Distribution [no]
BFDSM1          = 'no';                 % Backflow Direction Specification Method: Direction Vector [yes]
BFDSM2          = 'yes';                % Backflow Direction Specification Method: Direction Vector [yes]
TSM1            = 'no';                 % Turbulence Specification Method: Modified Turbulent Viscosity [no]
TSM2            = 'yes';                % Turbulence Specification Method: Intensity and Length Scale [yes]
Int_prof        = 'no';                 % Intermittency profile
Int             = 1.0;                  % Intermittency factor
p_avg           = 'no';                 % Specify averaged pressure spec. [no]
STMFR           = 'no';                 % Specify targeted mass-flow rate [no]

%fprintf(fid,'outlet no 0 no yes no yes 1 0.005 no\n\n');   % FLUENT 6.2.16 : OK

fprintf(fid,'/define/boundary-conditions/pressure-outlet\n'); % bc "outlet"

switch dim
    case {'2D'}
        switch regime
            case 'laminar'
        fprintf(fid,'%s %s %g %s %s %s \n\n',zone_name, PGP, GP, BFDSM1, BFDSM2, STMFR);
            case 'turbulent'
                switch Tu_model
                    case {'SA','SST'}
                        switch Fluent_ver(1)
                            case {'6'} %case {'6.2.16','6.3.26'}
        fprintf(fid,'%s %s %g %s %s %s %s %s %g %g %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1, TSM2,I_turb,L_turb, STMFR);
                            case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
        fprintf(fid,'%s %s %g %s %s %s %s %s %g %g %s %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1, TSM2,I_turb,L_turb,p_avg,STMFR);
                            otherwise
                            fprintf(' bc_pressure_outlet.m : Not yet implemented. Do it. ');
                        end
                    case {'SSTtr'}
        fprintf(fid,'%s %s %g %s %s %s %s %s %s %G %g %g %s %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1,TSM2,Int_prof,Int,I_turb,L_turb,p_avg,STMFR);
                    otherwise
                    fprintf(' bc_pressure_outlet.m : Not yet implemented. Do it. ');
                end
        end
    case {'3D'}
        switch regime
            case 'laminar'
        fprintf(fid,'%s %s %g %s %s %s \n\n',zone_name, PGP, GP, BFDSM1, BFDSM2, STMFR);
            case 'turbulent'
                switch Tu_model
                    case {'SA','SST'}
                        switch Fluent_ver(1)
                            case {'6'} %case {'6.2.16','6.3.26'}
        fprintf(fid,'%s %s %g %s %s %s %s %s %g %g %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1, TSM2, I_turb, L_turb, STMFR);
                            case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
        fprintf(fid,'%s %s %g %s %s %s %s %s %g %g %s %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1, TSM2, I_turb, L_turb,p_avg,STMFR);
                            otherwise
                            fprintf(' bc_pressure_outlet.m : Not yet implemented. Do it. ');
                        end
                    case 'SSTtr'
        fprintf(fid,'%s %s %g %s %s %s %s %s % %g %g %g %s %s\n\n',zone_name, PGP, GP, REPD, BFDSM1, BFDSM2, TSM1, TSM2,Int_prof,Int,I_turb,L_turb,p_avg,STMFR);
                    otherwise
                    fprintf(' bc_pressure_outlet.m : Not yet implemented. Do it. ');
                end
        end
end


%Backflow Turbulence Intensity (%) [10] 1
%Backflow Turbulence Length Scale (m) [1] 0.005
%Specify targeted mass-flow rate [yes] n
