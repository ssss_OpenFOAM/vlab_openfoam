function [data_num] = mesh_launch (os,mesh_script_file,mesh_file,data_num)
%run GAMBIT to create mesh file
% input :
    % nom du fichier journal GAMBIT
    % nom du fichier maillage
    
% function : creation du fichier maillage � partir du fichier journal GAMBIT

% output local :
    % status : 0 si run successfully
    % result : resultat de la commande

%disp('mesh_launch : debut');

t_start_mesh = clock;

gambit_ver  = data_num.Gambit_ver;
gambit_dir  = data_num.Gambit_dir;
gmsh_dir  = data_num.GMSH_dir;
mesher      = data_num.mesher;
parallel    = data_num.parallel;
nproc       = data_num.nproc;


switch os
    
    case 'Linux'
        switch mesher
            case 'GAMBIT'
                if strcmp(data_num.Affichage,'yes') fprintf(' mesh_launch : Mesh generation with GAMBIT %s \n',gambit_ver);end;
                status = 5 ; compteur = 10*60;
                while((status~=0) && (compteur>0))
                s_launch = sprintf('%sgambit -r%s -inp %s',gambit_dir,gambit_ver,mesh_script_file)
                [status,result] = system (s_launch);           % GAMBIT en batch (Ok)
                compteur = compteur - 1;
                error_fatal = 1 - isempty(findstr(result,'FATAL ERROR'));
                if (status~=0 || error_fatal~=0) % bad execution or licence pb
                    if (status~=0)
                        fprintf('\n    Error during mesh generation');
                        fprintf('\n    Is GAMBIT version ok ?');
                        fprintf('\n    Please STOP !!!');
                        fprintf('\n    status = %s ',status);
                        pause(360);
                    else
                        fprintf('\n    Licence GAMBIT not available, still %g try',compteur);
                        fprintf('\n    result = %s ',result);
                    end
                    status = max(status,error_fatal);
                    pause(10);
                else
                    fprintf('\n    Mesh generation \n');
                end
                end % while((status~=0) && (compteur>0))
            case 'GMSH'
                if strcmp(data_num.Affichage,'yes') fprintf(' mesh_launch : Mesh generation with GMSH\n');end;
                systemCmd = sprintf('%s%s%s', gmsh_dir, 'gmsh -3 ', mesh_script_file);
                disp('Test')
                disp(systemCmd)
                [dummytosuppressoutput1 dummytosuppressoutput2] = system(systemCmd);          % start GMSH and create mesh
            case 'ICEM'
                if strcmp(data_num.Affichage,'yes') fprintf(' mesh_launch : Mesh generation with ICEM\n');end;
                systemCmd = sprintf('%s%s ', 'icem14 -script ', mesh_script_file);
                [dummytosuppressoutput1 dummytosuppressoutput2] = system(systemCmd);          % start ICEM and create mesh
            
            case 'STAR'
                star_ver = data_num.Star_ver;
                sim_file = data_num.sccm_sim_file;
                system  (sprintf('rm -f %s', sim_file));
                if strcmp(data_num.Affichage,'yes') fprintf(' mesh_launch : Mesh generation with StarCCM+\n');end;                
                if  (parallel == 1)
                    s_nproc = num2str(nproc);
                    [status,mesh_log] = system  (sprintf('starccm%s -np %s -new -batch %s > mesh.log',star_ver, s_nproc, mesh_script_file));
                else                        
                end    
        
        end
    case 'Windows'
        switch gambit_ver
            case '2.2.30'
                %gambit_dir  = 'c:\Fluent.Inc\ntbin\ntx86\'; % version crack sur diwan
                %gambit_dir  = 'd:\Fluent.Inc\ntbin\ntx86\'; % version crack sur diwan2
            otherwise
                %gambit_dir  = 'c:\Fluent.Inc\ntbin\ntx86\'; % version licence sur diwan2
        end
        s_launch = sprintf('%sgambit.exe -r%s -skipstartup -inp %s',gambit_dir,gambit_ver,mesh_script_file)
        [status,result] = system (s_launch);     % GAMBIT en batch (Ok)
       %[status,result] = system (sprintf('gambit -r%s -inp %s >&! &',gambit_ver,mesh_script_file));     % GAMBIT en batch (Ok)
       %[status,result] = system (sprintf('gambit -r2.2.30 -inp %s >&! &',mesh_script_file));            % GAMBIT en batch (Ok)
        
        error_fatal = 1 - isempty(findstr(result,'FATAL ERROR'));
        if (status~=0 || error_fatal~=0) % bad execution or licence pb 
            display (' Z! : wrong mesh file creation !!!');
            fprintf('\n    status = %s ',status);
            fprintf('\n    result = %s ',result);
        end

        % attendre fin GAMBIT pour continuer (sur Windows)
        fid = fopen(mesh_file);    % try to open mesh file if it exist ?
        while (fid==-1)
            pause(1);fprintf('waiting for GAMBIT mesh_file : %s \n',mesh_file);
            fid = fopen(mesh_file);         
        end
        fclose(fid);        % mesh file exist => je le ferme et je continue
end

if strcmp(data_num.Affichage,'yes') fprintf(' mesh_launch : end Ok \n');end;
%disp('mesh_launch : fin');
data_num.cpu_time_mesh   = etime(clock,t_start_mesh);
%data_num.cpu_time_mesh   = data_num.cpu_time_mesh + etime(clock,t_start_mesh);

