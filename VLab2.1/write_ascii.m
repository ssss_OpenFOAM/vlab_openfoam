function  write_ascii (fluent_ver,dim,fid,str_file,force_surface)
% Create *_field.txt file
% FLUENT    : /file/export/ascii ...
% output    : str_file_field.txt


%=================================
% Choice fields on walls to write
%=================================
filename = strcat(str_file,'_field.txt');if exist(filename,'file');delete(filename);end % warning permission denied
scalar_choice = 'CpCf';
switch scalar_choice
    case 'pVxVy'
        scalar              = 'pressure x-velocity y-velocity';             % x, y, p, Vx, Vy
    case 'CpCf'
        scalar              = 'pressure-coefficient skin-friction-coef';    % x, y, Cp, Cf
end

switch dim  
    case {'2D'}
        switch fluent_ver(1)
            case {'6'} %case '6.3.26'
                %fprintf(fid,'/file/export/ascii %s %s, no yes %s, \n',filename,force_surface,scalar);    % Z! version Fluent 6.2.16
                %fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar); % bug Windows : remplace , par quit
                fprintf(fid,'/file/export/ascii %s %s, no %s quit no \n',filename,force_surface,scalar); % in one line
            case {'1'} %case {'13.0.0','14.0.0','14.5.0', etc...}
                fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar);
            otherwise
                fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar);
        end        
    case {'3D'}     
                fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar);
end

%=================================
% Field in BL
%=================================
filename = strcat(str_file,'_BL.txt');if exist(filename,'file');delete(filename);end
x0=1; y0=0;x1=1;y1=0.2;
surface_name = 'x=1m';

switch dim
    case {'2D'}
        switch fluent_ver(1)
            case {'6'} %case '6.3.26'
                fprintf(fid,'/surface/line-surface %s %g %g %g %g \n',surface_name,x0,y0,x1,y1); % define a line in BL
                fprintf(fid,'/file/export/ascii %s %s, no %s quit no \n',filename,surface_name,'x-velocity'); % in one line
            case {'1'} %case {'13.0.0','14.0.0','14.5.0', etc...}
                fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar);
            otherwise
                fprintf(fid,'/file/export/ascii %s %s, no %s, no \n',filename,force_surface,scalar);
        end        
    case {'3D'}     
end

end

