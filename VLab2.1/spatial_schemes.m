function  spatial_schemes (fid,data_sim,data_num)
%
%   regime      = {'laminar', 'turbulent'}              2 Flow regimes      implemented in VLab
%   Tu_model    = {'SA','SST','SAS','SSTtr','kklw'}     5 Turbulence model  implemented in VLab
%   Fluid_model = {'incompressible', 'compressible'}    2 Fluid model       implemented in VLab

dim             = data_sim.dim;
regime          = data_sim.regime;
Tu_model        = data_sim.Tu_model;
scheme_order    = data_sim.scheme_order;
Fluid_model     = data_sim.Fluid_model    ; % Fluid model
solver          = data_num.solver;
%solver          = 'pressure-based';     % not yet implemented

% Affichage criteres de convergence

% Discretisations : solve/set/discretization-sheme

switch solver
    case 'pressure-based'

% p-v coupling : (20, 21, 22, 24)     = (simple, simplec, piso, coupled) coupled is better than simplec on deformed meshes 
% pressure     : (10, 14, 11, 12, 13) = (standard, PRESTO, linear, second-order, body-force weighted)
% mom          : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
% Tu model dependent
% var          : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
% SA    model     var = (nut)
% SST   model     var = (k,omega)
% SAS   model     var = (k,omega)
% SSTtr model     var = (k,omega,internit,retheta)
% kklw  model     var = (k,kl,omega)
% Fluid_model dependent
% density      : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
% energy       : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)

switch scheme_order
    case 'O1'
        fprintf(fid,'/solve/set/discretization-scheme/pressure 10\n');
        fprintf(fid,'/solve/set/discretization-scheme/mom       0\n');
    case 'O2'
        %fprintf(fid,'/solve/set/p-v-coupling 24\n'); % Fluent 13
        %fprintf(fid,'/solve/set/p-v-coupling 22\n'); % Fluent 6.3.26 (may be unstable)
        fprintf(fid,'/solve/set/p-v-coupling 20\n'); % Fluent 6.3.26
        fprintf(fid,'/solve/set/discretization-scheme/pressure 12\n');
        fprintf(fid,'/solve/set/discretization-scheme/mom       1\n');

        if strcmp(regime,'turbulent');  % Tu model dependent 
            switch Tu_model
                case 'SA'
                    fprintf(fid,'/solve/set/discretization-scheme/nut       1\n');
                case {'SST','SAS'}
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n');
                case 'SSTtr'
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/intermit  1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/retheta   1\n');
                case 'kklw'
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/kl        1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n\n');
            end
        end
        if strcmp(Fluid_model,'compressible');  % Fluid_model dependent 
            switch Fluid_model
                case 'incompressible'
                case 'compressible'
                    fprintf(fid,'/solve/set/discretization-scheme/density     1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/temperature 1\n');
            end
        end

    case 'O3'
        fprintf(fid,'/solve/set/discretization-scheme/pressure 12\n');
        fprintf(fid,'/solve/set/discretization-scheme/mom       6\n');

        if strcmp(regime,'turbulent');
            switch Tu_model
                case 'SA'
                    fprintf(fid,'/solve/set/discretization-scheme/nut       6\n');
                case {'SST','SAS'}
                    fprintf(fid,'/solve/set/discretization-scheme/k         6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     6\n');
                case 'SSTtr'
                    fprintf(fid,'/solve/set/discretization-scheme/k         6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/intermit  6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/retheta   6\n');
                case 'kklw'
                    fprintf(fid,'/solve/set/discretization-scheme/k         6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/kl        6\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     6\n\n');
            end
        end
end
    case 'density-based-implicit'   % default = second-order

% flow     = amg-c (0, 1, 6)

% Tu model dependent
% var          : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
% SA    model     var = (nut)
% SST   model     var = (k,omega)
% SAS   model     var = (k,omega)
% SSTtr model     var = (k,omega,internit,retheta)
% kklw  model     var = (k,kl,omega)
% Fluid_model dependent
% density      : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
% energy       : (0, 1, 2, 4, 6)      = (O1 upwind, O2 upwind, power-law, quick, third-order MUSCL)
CFL = 5; % default value
fprintf(fid,'/solve/set/courant-number  %g\n',CFL);
fprintf(fid,'/solve/set/gradient-scheme y \n');     % gradient green-gauss node based
switch scheme_order
    case 'O1'
        fprintf(fid,'/solve/set/discretization-scheme/amg-c     0\n');  % O1
        if strcmp(regime,'turbulent');  % Tu model dependent 
            switch Tu_model
                case 'SA'
                    fprintf(fid,'/solve/set/discretization-scheme/nut       0\n');  % O1
                case {'SST','SAS'}
                    fprintf(fid,'/solve/set/discretization-scheme/k         0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     0\n');
                case 'SSTtr'
                    fprintf(fid,'/solve/set/discretization-scheme/k         0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/intermit  0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/retheta   0\n');
                case 'kklw'
                    fprintf(fid,'/solve/set/discretization-scheme/k         0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/kl        0\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     0\n\n');
            end
        end
    case 'O2'
        if strcmp(regime,'turbulent');  % Tu model dependent 
            switch Tu_model
                case 'SA'
                    fprintf(fid,'/solve/set/discretization-scheme/nut       1\n');  % O2
                case {'SST','SAS'}
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n');
                case 'SSTtr'
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/intermit  1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/retheta   1\n');
                case 'kklw'
                    fprintf(fid,'/solve/set/discretization-scheme/k         1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/kl        1\n');
                    fprintf(fid,'/solve/set/discretization-scheme/omega     1\n\n');
            end
        end
    case 'O3'
end

end
