function [str_file] = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo)
%
% str_file = strcat(str_geo,str_sup)    % a string for a filename

%===================================
%  str_geo : part 1 of the filename
%===================================
str_geo     = data_geo.method.str_geo;  % part 1 of the filename

if strcmp(str_geo , '')
    str_geo = data_geo.filename{1};    % filename without extension (.txt or .dat)
end

%===================================
%  str_sup : part 2 of the filename
%===================================
Vinf        = data_sim.Vinf;
inc         = data_sim.inc;
str_URANS   = data_sim.str_URANS;
regime      = data_sim.regime;
str_dir     = data_sim.str_dir;
dim         = data_sim.dim;
Tu_model    = data_sim.Tu_model;

theta   = data_ac.Theta_deg;
pos     = data_ac.pos;
dia     = data_ac.dia;
Freq    = data_ac.Freq;
Vjfluct = data_ac.Vjfluct;
Vjmean  = data_ac.Vjmean;

switch regime
    case 'laminar'
        switch str_URANS
            case 'RANS'
                str_model = 'LS';
            case {'DNS','URANS'}
                str_model = 'DNS';
        end
    case 'turbulent'
        str_model = str_URANS;
end

switch data_ac.ac
    case 'no'
%    str_sup  = ['_v',num2str(Vinf),'_i',num2str(inc),'_',str_model,'_',num2str(i_cas)];
    str_sup  = ['_v',num2str(Vinf),'_i',num2str(round(inc)),'_',str_model,dim,'_',Tu_model,'_',num2str(i_cas)];
    case 'yes'
        switch data_ac.type
            case 'continuous jet'
                s_name = 'j';
            case 'synthetic jet'
                s_name = 'js';
            case 'pulsed jet'
                s_name = 'jp';
        end
    str_sup  = ['_v',num2str(Vinf),'_i',num2str(inc),'_',s_name,'_F',num2str(Freq),'Vjm',num2str(Vjmean),'Vjf',num2str(Vjfluct),'t',num2str(theta),'p',num2str(100*pos),'d',num2str(100*dia),'_',str_model,'_',num2str(i_cas)];
end

%str_optim = num2str(x);
%ind         = [first_intrados:last_intrados];  [intrados] = sprintf('"_x%g_%g" ',ind);     % string des 251 vertex intrados
%==================================================================================

str_file = strcat(str_geo,str_sup);         % filename = str_geo + str_sup
%str_file = strcat(str_dir,'/',str_file);
