function Results_dir(n_conf,data_num,data_sim,data_ac)
%
% Creation du repertoire Resultats
% Copy or Move data & results files
%

%========================================
% create directory for file copy or move
%========================================
code_directory  = 'D:\Computing\VLab\code\';
directory       = data_sim.str_dir;     % see in defaults.m or affect.m
mkdir(directory);

%===========================
% copy MATLAB data files
%===========================
file = strcat(code_directory,'defaults2.m');if exist(file,'file');copyfile (file,directory);end
file = strcat(code_directory,'affect',num2str(n_conf),'.m');if exist(file,'file');copyfile (file,directory);end
file = strcat(code_directory,'data'  ,num2str(n_conf),'.m');if exist(file,'file');copyfile (file,directory);end

%=====================================
% move MATLAB & FLUENT results files
%=====================================
%file = strcat('fluent.jou');           if exist(file,'file');movefile(file,directory);end
%file = strcat('fluent_j2.jou');        if exist(file,'file');movefile(file,directory);end
%file = strcat('fluent_jend.jou');      if exist(file,'file');movefile(file,directory);end
%file = strcat('fluent_journal.trn');   if exist(file,'file');movefile(file,directory);end
%file = strcat('fluent_j2.trn');        if exist(file,'file');movefile(file,directory);end
file = strcat('Cases.out');            if exist(file,'file');movefile(file,directory);end
file = strcat('stop.out');             if exist(file,'file');movefile(file,directory);end
%file = strcat('Cases_Coeff_mean.out'); if exist(file,'file');movefile(file,directory);end

movefile('*.jou'                ,directory);
movefile('*.msh'                ,directory);
%movefile('*.trn'                ,directory);
%movefile('*.out'                ,directory);
movefile('*.cas'                ,directory);
movefile('*.dat'                ,directory);
movefile('*_field.txt'          ,directory);
movefile('*_Coeff_mean.out'     ,directory);
movefile('*_drag_history.out'   ,directory);
movefile('*_lift_history.out'   ,directory);
movefile('*.tif'                ,directory);

if strcmp(data_ac.ac,'yes') && not(strcmp(data_ac.type,'continuous jet'))
    movefile('*_ac_history.out',directory);
    movefile('*.c',directory);
end

if data_sim.anim.film==1
    movefile('*_grid_z1.tif', directory);
    movefile('*_pressure.tif',directory);
    movefile('*.mpeg',        directory);
end

file = strcat(data_sim.str_file,'_Coeff_mean.out');  if exist(file,'file');movefile(file,directory);end
file = strcat(data_sim.str_file,'_drag_history.out');if exist(file,'file');movefile(file,directory);end
file = strcat(data_sim.str_file,'_lift_history.out');if exist(file,'file');movefile(file,directory);end
file = strcat(data_sim.str_file,'_ac_history.out');  if exist(file,'file');movefile(file,directory);end
file = strcat('topo.out');                           if exist(file,'file');movefile(file,directory);end

if strcmp(data_num.Affichage,'yes') 
    movefile('*.mat',directory);
%    file = data_num.mat_file;if exist(file,'file');movefile(file,directory);end
end

%file = strcat(data_sim.str_file,'_grid_z1.tif');if exist(file,'file');movefile(file,directory);end
%file = strcat(data_sim.str_file,'_pressure.tif');if exist(file,'file');movefile(file,directory);end
%file = strcat(data_sim.str_file,'_field.txt');if exist(file,'file');movefile(file,directory);end
%file = strcat('Fig2_conv.tif');if exist(file,'file');movefile(file,directory);end
%file = strcat('Fig3_results.tif');if exist(file,'file');movefile(file,directory);end

%===========================
% move CMAES results files
%===========================

if exist('variablescmaes.mat','file');movefile('variablescmaes.mat',directory);end



