function [xfoil_output_file,xfoil_input_file,xfoil_Cp_file] = jou_XFOIL (param)
% create xfoil-input.dat to run XFOIL
% param

xfoil_input_file  = 'xfoil-input.dat';
xfoil_output_file = 'xfoil-output.dat';
xfoil_Cp_file     = 'xfoil-Cp.dat';
%xfoil_dump_file = 'xfoil-dump.dat';

% default values

if nargin == 0
disp(' jou_XFOIL : default geometry : NACA 2414');
airfoil_name    = 'NACA2414';   % Airfoil geometry
Reynolds        = 5e6;          % Reynolds number
Mach            = 0.0;          % Mach number
Inc             = 5;            % Angle of attack
N_crit          = 9;            % Turbulence level Tu=0.07%
XTRu            = 1.0;          % Natural transition on upper side
XTRl            = 1.0;          % Natural transition on lower side
end

if nargin == 1
airfoil_name    = param.airfoil_name;
Reynolds        = param.Reynolds;
Mach            = param.Mach;
Inc             = param.Inc;
XTRu            = param.XTRu;
XTRl            = param.XTRl;
Ncrit           = param.Ncrit;
end

% Xfoil journal for 1 airfoil, 1 AoA, 1 transition mode

% line 1 : NACA 0012        % generate
% line 1 : LOAD geoJ.dat    % load

fid = fopen(xfoil_input_file, 'wt');

choice = 'load';    	% ok on windows & Linux
%choice = 'generate'; 	% ok on Linux
switch choice
    case 'load'     % for all airfoils loaded from geoJ.dat
        filename = 'geoJ.dat'; % not ok with geoJX.dat 
        fprintf(fid,'LOAD %s \n',filename);             % airfoil file name (to implement)
        fprintf(' jou_XFOIL : load airfoil in %s \n',filename);
    case 'generate' % only for NACA airfoils
        %fprintf(fid,'%s \n',airfoil_name);              % airfoil name for Naca 4,5 digits
        if param.t > 9
        fprintf(fid,'NACA %i%i%i \n',param.m,param.p,param.t); % airfoil name for Naca 4,5 digits
        fprintf(' jou_XFOIL : generate NACA %i%i%i \n',param.m,param.p,param.t);
        else
        fprintf(fid,'NACA %i%i0%i \n',param.m,param.p,param.t); % airfoil name for Naca 4,5 digits
        fprintf(' jou_XFOIL : generate NACA %i%i0%i \n',param.m,param.p,param.t);
        end
end

% line 2, ...
% added for smooth airfoil
zzz=1;
if zzz==1
fprintf(fid,'PANE \n');
fprintf(fid,'MDES \n');
fprintf(fid,'FILT 1.00 \n');
fprintf(fid,'EXEC \n');
fprintf(fid,'EXEC \n');
fprintf(fid,'EXEC \n');
fprintf(fid,'EXEC \n');
fprintf(fid,'EXEC \n');
fprintf(fid,' \n');                             % change menu XFOIL
fprintf(fid,'PANE \n');

end

% line n, ...
fprintf(fid,'OPER \n');                         % direct analysis mode
fprintf(fid,'VISC %12.4g \n',Reynolds);         % viscous analysis at Re
fprintf(fid,'M %12.4g \n', Mach);               % Mach number value
fprintf(fid,'VPAR \n');                         % Change Xfoil parameters
fprintf(fid,'XTR %12.4g %12.4g \n',XTRu,XTRl);  % Transition method
fprintf(fid,'N   %4.1g \n',Ncrit);              % Background Turbulence level
fprintf(fid,' \n');                             % change menu XFOIL
fprintf(fid,'PACC \n');                         % begin accumulation in polar file
fprintf(fid,'%s \n',xfoil_output_file);         % polar filename
fprintf(fid,' \n');                             % change menu XFOIL 
%fprintf(fid,'%s \n',xfoil_dump_file);          % dump file
fprintf(fid,'A %12.4g \n', Inc);                % analysis at i=Inc 
fprintf(fid,'CPWR %s \n', xfoil_Cp_file);       % write Cp file 
fprintf(fid,' \n');                             % change menu XFOIL 
fprintf(fid,'QUIT \n');                         % quit
fclose(fid);

% Xfoil run suggested by airfoiltools.com (http://airfoiltools.com/polar/index) - interesting to try
%
%LOAD dat/e1211-il.dat	Load the dat file
%MDES	Go to the MDES menu
%FILT	Smooth any variations in the dat file data
%EXEC	Execute the smoothing
%Back to main menu
%PANE	Set the number and location of the airfoil points for analysis
%OPER	Go to the OPER menu
%ITER 70	Max number of iterations set to 70 for convergence
%RE 50000	Set Reynolds number (required?)
%VISC 50000	Set viscous calculation with Reynolds number
%PACC	Start polar output file
%polar/e1211-il_50000.txt	The output polar file name
%No dump file
%ALFA 0	Calculate lift and drag at 0� angle of attack
%ALFA 0.25	... 0.25�
%ALFA 0.5	... 0.5� ...
%...	...more alpha calculations here ...
%ALFA 3.5	At 3.5� no convergence
%ALFA 3.5	... try again ...
%ALFA 3.5	... and again
%INIT	Run INIT to reinitialise
%ALFA 3.75	Skip to 3.75�
%...	...rest of alpha calculations here ...
%PACC	Close polar file
%VISC	Reinitialise viscous calculation (required?)
%Down to main menu
%QUIT	Exit Xfoil