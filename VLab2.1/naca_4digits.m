function [xu,yu,xl,yl] = naca_4digits (m,p,t,geo_file,h,L,r)
% Compute NACA 4 digits airfoil geometry
%
% input :
%
% m         : max camber value
% p         : location of the max camber
% t         : thickness
% geo-file  : begin of the filename
%
% example NACA 8412 soit (m,p,t)=(0.08,0.4,0.12)
%
% cambrure   8% de la corde
% position  40% de la corde
% �paisseur 12% de la corde
%
% output :
%           geo.dat         geometry vertex file
%           geo-bdf.dat     geometry vertex file with bdf
%           geoJ.dat        geometry vertex file in Javafoil format
%
%           xu(i), i=1,n  : abcisse  des points extrados du bda vers le bdf
%           yu(i), i=1,n  : ordinate des points extrados du bda vers le bdf
%           xl(i), i=1,n  : abcisse  des points intrados du bda vers le bdf
%           yl(i), i=1,n  : ordinate des points intrados du bda vers le bdf
%
% le bda est en (0,0)
% le bdf est en (1,0)
%
% Z! : en x(n)=1, xu(n) et xl(n) sont differents de 1 (bdf tronqu� h/c = 0.1% environ) car yt(n) <> 0
% Z! : il faudrait ajouter une portion de cercle pour fermer le bdf
% ==================================

w_bio_skin  = 0;    % biomimetic skin
w_plot      = 0;    % plot

%x=[0 0.0025 0.005 0.0075 0.010 0.0125 0.015 0.0185 0.025 0.0375 0.05 0.0625 0.075 0.1:0.025:1];    % abcisses of the NACA airfoil : n points
%x=[0 0.0025 0.005 0.0075 0.0125 0.0185 0.025 0.0375 0.05 0.0625 0.075 0.1:0.010:1];                % abcisses of the NACA airfoil : n points
x=[0 0.0025 0.005 0.0075 0.01:0.005:1];                                                             % 31/08/14 needed for n_conf=46

rt=1.1019*power(t,2);   % ne sert a rien !!!

% NASA geometry : y= +- 0.594689181*[0.298222773*sqrt(x) - 0.127125232*x - 0.357907906*x2 + 0.291984971*x3 - 0.105174696*x4].
% on http://turbmodels.larc.nasa.gov/naca0012_val.html is different than the following one !

for i=1:length(x)    
    yt(i)=t/0.2*(0.2969*sqrt(x(i))-0.126*x(i)-0.3516*power(x(i),2)+0.2843*power(x(i),3)-0.1015*power(x(i),4));  % thickness distribution     
    if m==0
        yc(i) = 0;
    else
        if x(i)<=p
            yc(i)=m/power(p,2)*(2*p*x(i)-power(x(i),2));            % ordinate of the mean line before x(i)=p 
        else
            yc(i)=m/power(1-p,2)*(1-2*p+2*p*x(i)-power(x(i),2));    % ordinate of the mean line after  x(i)=p
        end
    end
    if i>1                  
        theta(i) = atan((yc(i)-yc(i-1))/(x(i)-x(i-1)));    
        xu   (i) = (x (i)-yt(i)*sin(theta(i)));
        yu   (i) = (yc(i)+yt(i)*cos(theta(i)));
        xl   (i) = (x (i)+yt(i)*sin(theta(i)));
        yl   (i) = (yc(i)-yt(i)*cos(theta(i)));  
    end   
end

%============================================
% Compute bdf point from xu, yu, xl, yl
%============================================
bdf = 'new';
%bdf = 'culot';     % not useful for now
switch bdf
    case 'old';
x_bdf = 1;
y_bdf = 0;
    case 'new'
x1          = xu(length(xu)-1);
y1          = yu(length(xu)-1);
x2          = xu(length(xu));
y2          = yu(length(xu));
x3          = xl(length(xl)-1);
y3          = yl(length(xl)-1);
x4          = xl(length(xl));
y4          = yl(length(xl));
% droite 1 = droite extrados y=a1*x+b1
% droite 2 = droite intrados y=a2*x+b2
a1          = (y2-y1)/(x2-x1);
a2          = (y4-y3)/(x4-x3);
b1          = (x2*y1 - x1*y2)/(x2-x1);
b2          = (x4*y3 - x3*y4)/(x4-x3);
x_bdf               = (b2-b1)/(a1-a2);
y_bdf               = a1*x_bdf+b1;
    case 'culot'
x_bdf = xu(length(xu))+0.01;
y_bdf = 0;
%y_bdf = 0.5*(yu(length(xu))-yl(length(xu)));
end

%============================================
% NASA scaling (c=1 but t/c=11.9%)
%============================================
zzz=0;
if zzz == 1
xu      = xu/1.009;
yu      = yu/1.009;
xl      = xl/1.009;
yl      = yl/1.009;
x_bdf   = x_bdf/1.009;
y_bdf   = y_bdf/1.009;
end
%============================================
% Biomimetic skin
%============================================
if w_bio_skin == 1
    [xu,yu] = Bio_skin(xu,yu, h, L, r)
end
%============================================
% geo.dat   : airfoil without bdf
%============================================
fid         = fopen(geo_file,'w');
for i=1:length(xu)
    fprintf(fid,'%f %f \n', xu(i), yu(i));   % points extrados (0 < xu < 0.99?)
end
for i=2:length(xl)
    fprintf(fid,'%f %f \n', xl(i), yl(i));   % points intrados (0.0? < xl < 0.99?)
end
fclose(fid);
fprintf('    naca_4digits : create file %s \n',geo_file);
%============================================
% geo-bdf.dat : airfoil with bdf (x,y)
%============================================
file    = strcat(geo_file(1:size(geo_file,2)-4),'-bdf.dat');
fid     = fopen(file,'w');
for i=1:length(xu)
    fprintf(fid,'%f %f \n', xu(i), yu(i));   % points extrados (0 < xu < 0.99?)
end
fprintf(fid,'%f %f \n', x_bdf, y_bdf);      % bdf
for i=2:length(xl)
    fprintf(fid,'%f %f \n', xl(i), yl(i));   % points intrados (0.0? < xl < 0.99?)
end
fclose(fid);
fprintf('    naca_4digits : create file %s \n',file);
%============================================
% geo-bdf-G.dat : airfoil with bdf (x,y,z)
%============================================
file    = strcat(geo_file(1:size(geo_file,2)-4),'-bdf-G.dat');
fid     = fopen(file,'w');
for i=1:length(xu)
    fprintf(fid,'%f %f %f \n', xu(i), yu(i), 0);   % points extrados (0 < xu < 0.99?)
end
fprintf(fid,'%f %f %f \n', x_bdf, y_bdf, 0);      % bdf
for i=2:length(xl)
    fprintf(fid,'%f %f %f \n', xl(i), yl(i), 0);   % points intrados (0.0? < xl < 0.99?)
end
fclose(fid);
fprintf('    naca_4digits : create file %s \n',file);
%==================================================
% geoJ.dat : airfoil without bdf (format Javafoil)
%==================================================
file    = strcat(geo_file(1:size(geo_file,2)-4),'J.dat');
fid     = fopen(file,'w');
fprintf(fid,' airfoil in VLab format writed in Javafoil format \n');    % ligne de commentaires
for i=length(xu) : -1 : 1
    fprintf(fid,'%f %f \n', xu(i), yu(i));   % extrados : bdf > bda
end
for i=2:length(xl)
    fprintf(fid,'%f %f \n', xl(i), yl(i));   % intrados : bda > bdf
end
fclose(fid);
fprintf('    naca_4digits : create file %s \n',file);
%==================================================
% Plot : airfoil without bdf (format Javafoil)
%==================================================
if w_plot==1
    disp('    naca_4digits : figure 381')
    figure (381)
    plot(xu,yu,'-ob');hold on;
    plot(xl,yl,'-sr');
    plot([0;1],[ t/2, t/2]);
    plot([0;1],[-t/2,-t/2]);
    str = strcat('naca4digits : f/c=',num2str(100*m),' xf/c=',num2str(100*p),' e/c=',num2str(100*t));
    title(str);
    hold off;
end
%==================================================
% Plot for publication
%==================================================
w_plot=0;
if w_plot==1
    figure (567)
    plot(xu,yu,'-r');hold on;
    plot(xl,yl,'-r');
    axis equal;
    axis ([-0.05 1.05 -0.35 0.35]);
    str = strcat('naca4digits : f/c=',num2str(100*m),' xf/c=',num2str(100*p),' e/c=',num2str(100*t));
    title(str);
    
    plot([-0.02 1.02],[0.0 0.0]);       % axe X
    plot([1.02 1.01],[0.  0.01 ]);      % arrow
    plot([1.02 1.01],[0. -0.01 ]);      % arrow
    
    plot([0.0  0.0],[-0.02 0.15]);      % axe Y
    plot([0.0 -0.01],[0.15  0.14 ]);      % arrow
    plot([0.0  0.01],[0.15  0.14 ]);      % arrow
    
    plot([0.12 0.12],[0.03 0.05],'r');      % jet
    plot([0.13 0.13],[0.03 0.05],'r');      % jet
    
    plot([0.0   0.125],[-0.1 -0.1]);    % jet location Xj
    plot([0.00  0.01 ],[-0.1 -0.11 ]);    % arrow
    plot([0.00  0.01 ],[-0.1 -0.09 ]);    % arrow
    plot([0.125 0.115],[-0.1 -0.11 ]);    % arrow
    plot([0.125 0.115],[-0.1 -0.09 ]);    % arrow

    plot([0 1],[-0.15 -0.15]);         % chord
    plot([0 0.01],[-0.15 -0.16 ]);    % arrow
    plot([0 0.01],[-0.15 -0.14 ]);    % arrow
    plot([1 0.99],[-0.15 -0.16 ]);    % arrow
    plot([1 0.99],[-0.15 -0.14 ]);    % arrow
    %plot([1 1],[0.02 -0.17]);         % chord

    plot([0.125 0.225],[0.05 0.05 ]);    % tangent  vector
    %plot([0.125 0.125],[0.05 0.15 ]);    % normal   vector
    plot([0.125 0.225],[0.05 0.15 ],'LineWidth',1.5);    % velocity vector
    plot([0.225 0.205],[0.15 0.15 ],'LineWidth',1.5);    % velocity vector
    plot([0.225 0.225],[0.13 0.15 ],'LineWidth',1.5);    % velocity vector
    text(0.12 , 0.01,'dj','FontSize',14,'FontWeight','Bold');
    text(0.05 ,-0.08,'Xj','FontSize',14);
    text(0.13 , 0.13,'Vj','FontSize',14);
    text(0.50 ,-0.13,'C' ,'FontSize',14);
    text( 1.0  ,-0.03 ,'X' ,'FontSize',14);
    text(-0.03 , 0.15 ,'Y' ,'FontSize',14);
    
    AoA_rad     = 45*pi/180;
    step        = -AoA_rad/10;
    theta       = AoA_rad:step:0;
    xc_AoA      = 0.13 + 0.06*cos(theta);
    yc_AoA      = 0.05 + 0.06*sin(theta);
    plot(xc_AoA,yc_AoA,'b');                % arc      AoA angle
    text(0.20 , 0.08,'\theta_j','FontSize',14);

end
fprintf('    naca_4digits : end\n');

