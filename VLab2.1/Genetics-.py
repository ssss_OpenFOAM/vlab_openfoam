import random
from math import *
from Gen2Airfoil import *
#from xfoil_dat import *
import sys
import os
import subprocess
# remove all files from last run
filelist = [ f for f in os.listdir(".") if f.endswith(".dat") ]
for f in filelist:
    os.remove(f)

#    print f
filelist = [ f for f in os.listdir(".") if f.endswith(".log") ]
for f in filelist:
    os.remove(f)

filelist = [ f for f in os.listdir(".") if f.endswith(".datBezier") ]
for f in filelist:
    os.remove(f)


# Airfoil is defined by
#  LEU = Leading edge up            LED = Leading edge down
#  C25 = Camber at 25%              T25 = Camber at 25%
#  C50 = Camber at 50%              T50 = Camber at 50%
#  C75 = Camber at 75%              T75 = Camber at 75%

#              LEU   LED     C25   C50    C75      T25   T50   T75
#genmaxs = [    0.2,  0.2,    0.2,  0.2,   0.2,     0.2,  0.2,  0.1   ]
genmaxs = [    0.2,  0.2,    0.2,  0.2,   0.2,     0.28,  0.28,  0.18   ]
#genmaxs=  [i*0.5 for i in genmaxs]
genmaxs=  [i*0.3 for i in genmaxs]
#genmaxs=  [i*0.3 for i in genmaxs]

#genmins = [    0.0,  0.0,    0.0,  0.0,   0.0,     0.0,  0.0,  0.0   ]
genmins = [    0.0,  0.0,    0.0,  0.0,   0.0,     0.0,  0.0,  0.0   ]
genmins = [    0.07,  0.07,    0.0,  0.0,   0.0,     0.0,  0.0,  0.0   ]
genmins=  [i*0.7 for i in genmins]
ngen= len (genmaxs)

foilnum = 0

# ==============================================
# ============== BASIC FUNCTIONS ===============
# ==============================================

def newborn(gen):
    global foilnum
    foilnum+=1
    return [0,gen,'%06d' %foilnum]

def check_range(gen):
    for i in range(0,ngen):
        if(gen[i]>genmaxs[i]):
            gen[i]=genmaxs[i]
        if(gen[i]<genmins[i]):
            gen[i]=genmins[i]

def proximity(gen1,gen2):
    proximity = 0
    for i in range(0,ngen):
        d = gen1[i] - gen2[i]
        proximity+= d*d
    return sqrt(proximity)

def breed_random():
    child = [0]*ngen
    for i in range(0,ngen):
        child[i] = random.uniform(genmins[i],genmaxs[i])
        check_range(child)
    return newborn(child)

def breed_interpolate(mother,father,weight):
    child = [0]*ngen
    for i in range(0,ngen):
        child[i] = (1-weight)*mother[i] + weight*father[i]
    return newborn(child)

def breed_crossover(mother,father):
    child = [0]*ngen
    for i in range(0,ngen):
        if(random.random()>0.5):
            child[i] = mother[i]
        else:
            child[i] = father[i]
    return newborn(child)

def breed_mutate(mother, scale):
    child = [0]*ngen
    for i in range(0,ngen):
        child[i] = mother[i] * (1 + random.uniform(-scale,scale) )
        check_range(child)
    return newborn(child)

def gen2log(gen,logfile2):
    #logfile.write(" %3.2f          " %gen[0])
    logfile = open("logfile.log","a")
    logfile.write (gen[2])
    logfile.write (' {0:10.2f}       '.format(gen[0]))
    for i in range(0,ngen):
        logfile.write(" %1.4f " %gen[1][i])
    logfile.write("\n")
    logfile.close()

#              LEU   LED     C25   C50    C75      T25   T50   C75
#testgen = [    0.5,  0.1,    0.1,  0.1,   0.3,     0.5,  0.6,  0.7   ]
#testgen = [ 0.0,  0.,  0.0845, 0.1290,  0.0496,  0.0295,  0.1326,  0.0277   ]




#print testgen
#check_range(testgen)
#print
#print testgen
#print breed_random()



# ==============================================
# ============== Algorithm ===============
# ==============================================

nsubjects = 10
nbest     = 8

name = "testfoil"

niterations = 10
Re = 1e6
Ncrit = 14

logfile = open("logfile.log","w")
logfile.close()
#logfile=""



population = [[0,[],""] for i in range(0,nsubjects)]
print len(population)
fitness = [0]*nsubjects
plotFoilInXfoil=True
def populate():
    print "Population phase"
    logfile = open("logfile_verbose.log","a")
    for i in range (0,nsubjects):
        #foilnum++
        population[i] = breed_random()
        #name = '%06d' %foilnum
        gen2airfoil(population[i][1],population[i][2]) # generate Airfoil shape by Bezier interpolation
        writeFile_AirfoilGenerated(population[i])
        print "Evaluating fitness of individual "+ str(i)
        logfile.write("Evaluating fitness of individual" + str(i) +"\n")
        done = readFile_SimulationDone()
        logfile.write("Simulation done\n ")
        if done==True:
            population[i][0] = getParameter()      # get the value of the parameter to optimize
        else:
            population[i][0] = 0
        if (population[i][0] <= 0.0 or population[i][0] >=600):
            logfile.write("There seems to be an error in population id = " + str(population[i][2]) + "  paramter = "+str(population[i][0])+"\n")
        else:
            logfile.write("There seems to be no error in population id = " + str(population[i][2]) + "  paramter = "+str(population[i][0])+"\n")
        gen2log(population[i],logfile)
    logfile.close()

def writeFile_AirfoilGenerated(individual):
    # is it possible to get the number of the individual?
    with open("genetics_AirfoilCreated.txt",'w') as f:
        f.write("Created airfoil id = " + str(individual[2]))
def readFile_SimulationDone():
    import os.path
    while os.path.isfile("vlab_AirfoilSimulated.txt") == False:
        import time
        time.sleep(5)
    # remove the file so that when we re-enter this function the file does not exist

    os.remove('vlab_AirfoilSimulated.txt')
    # read results
    return  true
    
def eval_fitness():
    logfile = open("logfile_verbose.log","a")
    for i in range (nbest,nsubjects):          # evaluate fittens just for new ones
        gen2airfoil(population[i][1],population[i][2]) # generate Airfoil shape by Bezier interpolation
        writeFile_AirfoilGenerated(population[i])
        print "Evaluating fitness of individual "+ str(i)
        logfile.write("Evaluating fitness of individual" + str(i) +"\n")
        done = readFile_SimulationDone()
        logfile.write("Simulation done\n ")
        if done==True:
            population[i][0] = getParameter()      # get the value of the parameter to optimize
        else:
            population[i][0] = 0
        if (population[i][0] <= 0.0 or population[i][0] >=600):
            logfile.write("There seems to be an error in population id = " + str(population[i][2]) + "  paramter = "+str(population[i][0])+"\n")
        else:
            logfile.write("There seems to be no error in population id = " + str(population[i][2]) + "  paramter = "+str(population[i][0])+"\n")
        gen2log(population[i],logfile)
    logfile.close()
def getParameter():
    line = subprocess.check_output(['tail', '-1', 'Cases_Coeff_mean.out'])
    line = str(line)
    line.strip().split()
    cl = float(line[1])
    print "Cl " + str(cl)
    cd = float(line[2])
    print "Cd " + str(cd)
    param = cl/cd
    return param

def evolve():
    population.sort( reverse=True)
    for i in range (0,nbest):
        print "survive: ",population[i][2],"      ",population[i][0]
    population[nbest+ 0 ] =  breed_mutate(population[0][1],0.1)     # modify 1 best
    population[nbest+ 5 ] =  breed_random()     # generate new from scratch
    population[nbest+ 1 ] =  breed_crossover  ( population[0][1] ,population[1][1])      # crossover 2 best
    population[nbest+ 2 ] =  breed_interpolate( population[0][1] ,population[1][1],0.5)  # interpolation 2 best
    population[nbest+ 3 ] =  breed_crossover  ( population[0][1] ,population[random.randint(2,nbest-1)][1]    )  # crossover 1 and other
    population[nbest+ 4 ] =  breed_interpolate( population[0][1] ,population[random.randint(2,nbest-1)][1],0.5)  # interpolation 1 and other
    for i in range (nbest+ 6 ,nsubjects):
        a =  random.randint(0,nbest-1)   # choose any 2
        b =  random.randint(0,nbest-1)
        if(random.random()>0.5):   # interpolate or crossover, 1:1 chance
            population[i] = breed_crossover  ( population[a][1] ,population[b][1])
        else:
            population[i] = breed_interpolate  ( population[a][1] ,population[b][1], 0.5)

print " ===== iteration: 0"
populate()
for i in range (1,niterations):
    print " ===== iteration: ",i
    evolve()
    eval_fitness()
logfile.close()

#print "======================="
#population.sort()
#for i in range (0,nsubjects):
#    print population[i][0]
#    print population[i][1]
#    print
