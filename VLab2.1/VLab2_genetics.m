function [Fobj] = VLab2 (   x, ...          % vector of the parameters (optim)
                            n_conf, ...     % configuration (airfoil, wing, deflector, ...)
                            sim, ...        % sim=1 default full simulation, sim=0 only mesh no simulation
                            os, ...         % operation system ('Linux', Windows')
                            param)          % optimization problem definition
%
% VLab ( http://www.VLab.org/ ) - This file is part of VLab
% Copyright (C) - 2014 - Vincent G. CHAPIN
% 
% This file is released under the GPL license
%
% VLab : Virtual Laboratory version 3.0
%
% author     : V.G. CHAPIN                            date :   7 Juillet 2014
%
% co-authors : 2004 : G. Dulliand, S. Castanet
%              2005 : B. Cochet, G. Parra Hinchado, T. Ricou
%              2006 : B. Cochet, E. Luthman, G. Parra Hinchado, M. Pichon, P. Val Dominguez  
%              2007 : F. Mayerhoeffer, J. Comas, U. Gastaldi, J. Mosca
%              2008 : J. Peelman, S. Fougere, S. Guirand, E. Chebli
%              2009 : Y. Cao, E. Collado Morata, D. Gomez Ariza, B. Seve Chevaleyre
%              2010 : Y. Sadoudi, N. de Carlan, F. Gallard, T. Kervern, G. Zumbil
%              2011 : J. Mougel, L. Perrot-Minot, C. Li, A. Laenger
%              2012 : A. Merle, V. Ganapathi, J. Dinquel, A. Garavel, M. Stasia, D. Vialait, S. Le Bras
%              2013 : D. Dupatti, A. Ananthakrishnan, A. Monti, M. Ambrosio, N. Pirozzi
%              2013 : F. Manara, F. Sgarbossa, A. MeyerForsting
%              2014 : N. Verdin
%              2015 : 
% ________________________________________________________________________________________________________________
%                                                                                                                 |
%  Code coupling :                                                                                                |
%                                                                                                                 |
%  Meshers : GAMBIT, ICEMCFD                                                                                      |
%  HiFi    : FLUENT                                                                                               |
%  MeFi    : Tornado, Openprop                                                                                    |
%  LoFi    : Xfoil - M. Drela                                                                                     |
%  FSI     : RelaX - P. Heppel                                                                                    |
%  CAA     : NAFNoise                                                                                             |
% ________________________________________________________________________________________________________________
%                                                                                                                 |
% Implemented cases in VLab :                                                                                     |
%                                                                                                                 |
% 2D thick airfoil      : naca 4 or 5 digits,(x,y) file, airfoil + flap    VGC, T. Kervern, ...                   |
% 2D LowRe thin airfoil : 6-vertex, n-vertex thin airfoil                  VGC, Y. Sadoudi                        |
% 2D Paragliding        :                                                  J. Mougel, L. Perrot-Minot             |
%                                                                                                                 |
% 3D building + AtmBL   :                                                  VGC                                    |
% 3D interacting sails  :                                                  VGC, N. de Carlan                      |
% 3D parametric  sails  :                                                  VGC, N. de Carlan                      |
% 3D wing               :                                                  VGC, N. de Carlan                      |
% 3D wing bio-inspired  :                                                  VGC, A. Laenger, A. Merle              |
% 3D CROR bio-inspired  :                                                  VGC, C. Li, V. Ganapathi               |
% 3D deflector          :                                                  VGC                                    |
% 3D deflector + human  :                                                  VGC                                    |
% 3D wingsail           :                                                  N. Verdin                              |
% ________________________________________________________________________________________________________________|
%
% Cases         Type Dim     Case                                            Dev     Test  OS  M/G/S #1
% =================================================================================================================
% Airfoil === szone                                                                                             ===
% =================================================================================================================
% VLab2([],0)   RANS 2D      NACA xxxx i=8.3 Re=2.7e6 0.72 0.03      2.4/6.3     10/09   07/15 W+L- 28/??/13s
% VLab2([],1)   DNS  2D      NACA xxxx i= 2 Re=1000                  2.4/6.3     12/09   05/14 W+L+ 144/63/72s
% VLab2([],4)   RANS 2D V Ok NACA 0012 Re=6e6 NASA test case         2.4/6.3     10/10   05/14 W+L+ ?/?/8mins
% VLab2([],6)   RANS 2D O1P4 NACA xxxx max fobj = f(f/c,xf/c,e/c,i)              04/09   05/14 W+L? 50s/?s/?s
% VLab2([],10)  RANS 2D V    NACA 0012 i=2 Re=2.7e6                  valid_10    02/09   05/14 W+L?       
% VLab2([],11)  RANS 2D V    NACA 0012 + Transition SSTtr,k-kl-w     2.4/13/14   05/12   06/13 W?L+ ?/?/7mins
% VLab2([],12)  URANS2D      NACA xxxx + cg motion                               07/09   02/12 W?L? 2.6ms/it/pts   
% VLab2([],13)  RANS 2D      JAVAFOIL geo., film flap sep.           2.4/6.3/13  02/10   05/14 W+L+ 1/?/2mins
% VLab2([],14)  RANS 2D      NACA 6 digits                           2.4.6.3.13  02/10   11/12 W?L? ?/?/20s
% VLab2([],17)  RANS 2D V Ok RAE  2822 M0.73 compressible            2.4/6.3/13  05/12   12/14 W+L? ?/?/?mins
% VLab2([],20)  RANS 2D      NACA xxxx film Vortex Shedding          2.4/6.3/13  02/12   10/13 W+L? 5/?/20mins
% VLab2([],21)  RANS 2D      NACA xxxx Polar Max(L/D)   = f(f/c)                 02/12   11/12 W?L? 55/?/?mins
% VLab2([],22)  RANS 2D      NACA xxxx Polar Max(Cl,L/D)= f(f/c,xf/c,e/c)        02/12   11/12 W?L? 42/?/?s
% VLab2([],23)  RANS 2D V Ok NACA 0036 Massive Separation RANS/DES Alonso        02/12   11/12 W?L? 6/2/?mins
% VLab2([],38)  RANS 2D V    NACA 0012 BE2 Re=6e6 G mesh uns/hybrid 2.4/6.3/14 s 11/10   10/15 W+L+ 43/?/33s 
% VLab2([],39)  RANS 2D V    NACA 0012 BE2 Re=6e6 G mesh uns/hybrid 2.4/6.3/14 P 11/10   10/15 W+L+ ?/?/?s
% VLab2([],40)  RANS 2D V    NACA 0012 BE2 Re=6e6 G mesh hybrid      valid_40    11/10   11/12 W+L+ s 
% VLab2([],44)  RANS 2D      n objects           G mesh uns  to do      6.3/14   04/12   11/13 W+L+  ?/?/?mins  % 2013 VGC.
% VLab2([],45)  RANS 2D      2 airfoils AC45     G mesh uns             6.3/14   11/11   05/14 W+L+ 10/5/?mins 
% VLab2([],46)  RANS 2D O1Pn 2 airfoils AC45     G mesh uns             6.3      11/11   11/13 W+L? 46 s 
% VLab2([],47)  RANS 2D      3 airfoils surf     G mesh uns  mCFD       6.3      04/12   11/13 W+L? 26/?/?mins  % 2012 Dinquel, ...
% VLab2([],48)  RANS 2D      multi-elts Vialait  G mesh uns  mCFD       6.3/14   04/12   11/13 W+L+  ?/?/?mins  % 2012 Vialait, ...
% VLab2([],49)  RANS 2D      multi-elts 30p30n   G mesh uns             6.3/14   04/12   05/14 W+L+  ?/?/?mins  % 2013 VGC.
% VLab2([],54)  RANS 2D      SD 7003   LSB & Transition model        6.3/13/14   04/12   06/13 W?L+ 60/30/?s
% VLab2([],55)  RANS 2D      E374      LSB & Transition model        6.3/13/14   04/12   06/13 W?L+ 60/30/?s
% VLab2([],99)  RANS 2D      Mesh conv. study (struct. mesh) szone   6.3/??      12/09   11/12 W?L? 12/?/?mins
% VLab2([],100) RANS 2D      Mesh conv. study (wall y+)      szone   6.3/??      12/09   ??/?? W?L? s
% VLab2([],101) DNS  2D      NACA 0012 i=18 Re=1000 + film           6.3+14-     03/10   11/12 W?L? 2/?/?s
% VLab2([],102) DNS  2D      NACA 8504 i=18 Re=1000 + film           6.3/??      03/10   11/12 W?L? ?/?/?s
% VLab2([],103) DNS  2D      NACA 0012      Re=5000  PhD T. LeGarrec 2008        10/11   ??/?? W?L? s
% VLab2([],106) DNS  3D      NACA 0012      Re=1.6e5 PhD T. LeGarrec 2008        10/11   ??/?? W?L? s
% VLab2([],107) DNS  2D      NACA 0018      Re=2.0e4        Colonius 2011        10/11   11/12 W?L? ?/?/?s
% Airfoil === szone_bda                                                                                         ===
% VLab2([],9)   RANS 2D V Ok NACA 0012 Re=6e6 NASA test case wo bda              02/12   02/12 W?L? 7/?/3mins
% =================================================================================================================
% Airfoil + Flow control === szone_ac                                                                           ===
% =================================================================================================================
% VLab2([],2)   URANS2D      NACA xxxx HighRe w/wo 1 j,sj,pj                     10/09   05/14 W+L+ ?/?/8mins
% VLab2([],3)   all  2D      NACA xxxx        w/wo 1 j,sj,pj debug   2.4/6.3     10/09   05/14 W+L+ 3/3/1mins
% VLab2([],5)   URANS2D      NACA xxxx HighRe w/wo 1 j,sj by adapt.              04/09   02/12 W L? 8s
% VLab2([],7)   RANS 2D O1P4 NACA xxxx +jet max fobj = f(Vj,xj,tj,dj)            04/09   02/12 W?L? s
% VLab2([],15)  RANS 2D      NACA xxxx + flap + control ON/OFF         Gbug      02/10   02/12 W?L? 100s
% VLab2([],16)  RANS 2D      NACA xxxx + flap + fente + control ON/OFF Gbug      02/11   02/12 W?L? 100s
% VLab2([],30)  URANS2D      NACA xxxx Control Optimum position ?
% VLab2([],41)  URANS2D      NACA 0012 BE3 HighRe + control on/off               01/09   11/12 W+L+ s
% VLab2([],51)  DNS  2D      NACA 4418 BE6 LowRe i=18 R. Mittal                  01/09 s
% VLab2([],52)  RANS 2D      NACA 0015 i=16 Re=1e5-3e5 Exp Moreau 2007           04/09
% VLab2([],53)  RANS 2D      NACA 2415 i= 8 Re=2e5 Transition model      13      11/11
% VLab2([],60)  URANS2D      NACA xxxx +TSJ Re=1e6 Exp Duvigneau 2006, ...       11/09   02/12 W?L? s
% VLab2([],61)  URANS2D O1P7 NACA xxxx +sj  max fobj = f(f,xf,e,i,Vj,xj,tj)      11/09   02/12 W?L? 46s
% VLab2([],63)  URANS2D      NACA xxxx +NSJ Re=1e6                               11/09
% VLab2([],71)  URANS2D O1P3 NACA xxxx +sj  max fobj = f(tj,fj,xj)               07/10   02/12 W?L? s
% =================================================================================================================
% Parafoil               === szone_para                                                                         ===
% =================================================================================================================
% Airfoil === szone_para                                                                                        ===
% VLab2([],80)  RANS 2D      Paragliding airfoil Gmesh hyb mCFD      6.3+13?14?  02/11   11/13 W+L? 2/?/5mins
% Airfoil === szone_parafoil                                                                                    ===
% VLab2([],81)  RANS 2D      Paragliding airfoil Gmesh hyb PIP       6.3+13?14?  01/14   05/14 W+L? 2/?/5mins
% Airfoil === szone_parafoil2                                                                                   ===
% VLab2([],82)  RANS 2D      Paragliding airfoil Gmesh hyb PIP       6.3+13?14?  01/14   01/14 W+L? 2/?/5mins
% 
% VLab2([],50)  DNS  2D      Cylindre BE5 LowRe Re=30-300 from .msh              01/09   07/15 W+L? ?/?/?s
% =================================================================================================================
% Airfoil without szone                                                                                         ===
% =================================================================================================================
% VLab2([],60)  RANS 2D      airfoil ???            bspline I/G   Merle 6.3+     11/13   02/14 W+L+ 80/?/?s
% =================================================================================================================
% LowRe non-derivable shapes                                                                                    ===
% =================================================================================================================
% VLab2([],200) DNS  2D      ?                                                   03/10   02/12      ?s
% VLab2([],201) DNS  2D      ?                                                   03/10   02/12      ?s
% VLab2([],202) DNS  2D      Dragonfly, Flat Plate,.. read mesh                  04/10   05/13 W L- 42/?/? mins
% VLab2([],203) DNS  2D O1Pn Dragonfly    6-vertex  Gambit mesh   2.4/6.3/14-    04/10   09/15 W+L+ 21/?/? mins
% VLab2([],204) DNS  2D      Universal    n-vertex  Gambit mesh   2.4/6.3        04/10   11/12      ?/?/1 mins
% VLab2([],205) DNS  2D      Flat plate   n-vertex  Gambit mesh   2.4/6.3        04/12   11/12 W L? 7/?/2mins
% VLab2([],206) DNS  2D      Levy 2009    n-vertex  Gambit mesh   2.4/6.3        04/12   11/13 W+L? 2/?/?mins
% VLab2([],207) DNS  2D O1Pn Levy 2009    n-vertex  Gambit mesh   2.4/6.3        04/12   11/12 W?L? 2 mins
% =================================================================================================================
% Non-Derivable Shapes                                                                                          ===
% =================================================================================================================
% VLab2([],250) RANS 2D      Forme d'une tente      G or ?                        12/14   12/14      ?s
% VLab2([],255) RANS 2D      High Lift Airfoil      G or ?                        12/14   12/14      ?s
% =================================================================================================================
% Wind Engineering                                                                                              ===
% =================================================================================================================
% VLab2([],300) RANS 3D      cube     Re=6.8e3  cube.msh          2.4/6.3/13     03/10   05/13 W?L- ?/?/50s
% VLab2([],301) all  3D      building Re=6.8e5  hexcore           2.4/6.3/??     09/11   11/12 W?L? ?/?/?s
% VLab2([],302) DES  3D      building Re=?                  to do                09/11   ??/?? W?L? s
% =================================================================================================================
% Sailing Yacht Aerodynamics                                                                                    ===
% =================================================================================================================
% VLab2([],400) RANS 3D      gv+foc igs + j1_200k.jou > sail.msh                 05/10   ??/?? W?L? s
% VLab2([],401) RANS 3D O1P6 GV param (3 camber, 3 trim)    G 2.4/6.3/?/?        05/10   07/15 W+L? 1/?/? mins
% VLab2([],402) RANS 3D O1P8 GV param (4 camber, 4 trim)    G 2.4/6.3/13         07/10   07/15 W+L? 4/?/? mins
% VLab2([],403) RANS 3D O1P8 GV param + foc igs   - ?       G                    07/10   02/12 W?L? s
% VLab2([],404) RANS 3D O1P8 GV param + foc igs   - H  sail G                    07/10   02/12 W?L? s
% VLab2([],405) RANS 3D O1P8 GV param + foc igs   - MM sail G                    07/10   02/12 W?L? s
% VLab2([],406) RANS 3D O1P8 GV igs   + foc param - to do   G                    09/10   02/12 W?L? s
% VLab2([],407) RANS 3D      gv+foc igs param(relax(trim, design)) G 6.3/?/?     09/10   06/13 W+L? 2/?/? mins

% VLab2([],409)RANS 3D       wingsail - nWing 3D                 todo            10/10   ??/??   ? s
% VLab2([],410)RANS 3D       AC72 igs param(relax(trim, design)) todo            10/10   ??/??   ? s

% VLab2([],411) RANS 3D O?P? gv          param      Viola  2010                  03/11   07/15 W+L? 3/?/? mins
% VLab2([],412) RANS 3D O?P? gv+foc      param+igs  Viola  2010 todo             03/11   ??/??   ? s
% VLab2([],413) RANS 3D O?P? gv+mat+foc  param      Viola  2010 todo             03/11   ??/??   ? s
% VLab2([],414) RANS 3D      gv          igs        Viola  2010       6.3/?/?    03/11   06/13 W+L  1/?/? mins
% VLab2([],422) RANS 3D      gv+foc      igs+igs    Viola  2010       6.3/?/?    05/11   06/13 W+L  ?/?/? s
% VLab2([],423) RANS 3D      gv+mat+foc  igs        Viola  2010 todo             03/11 s
% VLab2([],424) RANS 3D      gv          igs BL     Viola  2010                  05/11   07/15 W-L?  ?s
% VLab2([],430) RANS 3D      spinnaker   igs        Lasher 2005                  04/13   07/15 W-L?  ?/?/? s
% =================================================================================================================
% Wing Aerodynamics                                                                                             ===
% =================================================================================================================
% VLab2([],447) RANS 3D      Wing 3D full span G uns tetra                       10/10 s
% VLab2([],448) RANS 3D      Wing 3D full span G uns prism  Annika               08/11 s
% VLab2([],449) RANS 3D      Wing 3D per. span G uns prism  Annika to do         10/11 s
% VLab2([],450) RANS 3D      Wing 3D 1/2  span G uns prism  Annika 2.4/6.3/13    10/11 11/12 W/L 8/?/3mins
% VLab2([],455) RANS 3D      Wing 3D 1/2  span G Miklo.  Re=5e5    2.4/6.3/13    10/11 11/12 W/L ?/3/?mins
% VLab2([],456) RANS 3D      Wing 3D 1/2  span G AR=2.54 Re=7e4    2.4/6.3/13    10/11 11/12 W/L ?/?/?mins

% VLab2([],472)URANS 3D      wingsail 3D 1/ span Starccm� 8.04                   07/14 ??/?? W/L ?mins

% VLab2([],499) RANS 2D      airfoil with ground effect                          02/11 ??/??  ?mins
% VLab2([],501) RANS 3D O1Pn deflector        - 2 nurbs surface    jouM          07/11 01/12  4mins
% VLab2([],502) RANS 3D      body             - volume              msh          07/11 ??/??  ?mins
% VLab2([],503) RANS 3D      body + deflector - volume + surface    msh          07/11 ??/??  ?mins

% VLab2([],505) RANS 3D      CROR design & optimization  ICEM       only 13      09/12 11/12 ??/?? ?/L ?/40/?mins

% VLab2([],601) RANS 2D      ADONF integration     to finish and debug           07/11 ??/??  ?mins

% VLab2([],919) RANS 2D      PIP ETSIA Airfoil Design                            05/12 05/12
% VLab2([],9194)RANS 2D      PIP ETSIA Airfoil Design (pb szone < szoneb)        05/12 05/12
% =================================================================================================================
% XFOIL / RANS comparisons =======================================================================================
% =================================================================================================================
% VLab2([],1001)XFOIL        XFOIL                                               03/12 07/15 W?/L- 2secs
% VLab2([],1002)XFOIL   V    XFOIL validation on NACA 0012                       03/12 07/15 W+/L- 2secs
% VLab2([],1003)XFOIL   O1P? XFOIL or RANS NACA 4 digits optimization            03/12 10/15 W+/L+ 2secs
% =================================================================================================================
% Airfoil & Wing shape optim with NURBS - Merle 2011    ===========================================================
% =================================================================================================================
% VLab2([],140) RANS 2D      airfoil                bspline I/G         6.3+     11/13   02/14 W+L+ 80/?/?s
% VLab2([],143) RANS 2D O1Pn airfoil                bspline I/G         6.3+     04/11   10/15 W?L+ 48s
% VLab2([],144) RANS 2D O1Pn airfoil                bspline I/G         6.3+     04/14   10/15 W+L? ?s
% VLab2([],145) RANS 2D O1Pn airfoil                bspline             XFOIL    04/14   04/14 W+L? ?s
% VLab2([],146) RANS 2D O1Pn 2 airfoils AC45 sym.   bspline I/G         6.3+     10/14   10/14 W+L? ?s
%
% VLab2([],141) RANS 3D      bioLEwing              bspline I           6.3+14-  04/11   06/13 W?L+ ?s 
% VLab2([],142) RANS 3D O?P? bioLEwing              bspline I           6.3+14-  04/11   06/13 W?L+ ?s
% =================================================================================================================
% Demo cases                =======================================================================================
% =================================================================================================================
% VLab2([], 700)XFOIL  O1P4  XFOIL Naca 4-digits optimization                    07/15 07/15 W+/L? 2secs
% =================================================================================================================
% Local variables & their initilization

% RANS, URANS, DNS Results current case
% Resultc = ['inc        ' 'Cl_mean    ' 'Cd_mean    ' 'Cl_rms     ' 'Cd_rms     ' 'Cm_mean    ' 'xcp        '...
%            'Cx         ' 'Cy         ' 'Cq         ' 'Cmu        ' 'q_jet      ' 'Err_Clmean ' 'Err_Cdmean '...
%            'Cl_mean2   ' 'Cd_mean2   ' 'Fx1        ' 'Fx2        ' 'Mx1        ' 'Mx2        ' 'eta1       ' 'eta2       '];
% Result  = [ inc           Cl_mean       Cd_mean       Cl_rms        Cd_rms        Cm_mean       xcp         ...
%             Cx            Cy            Cq            Cmu           q_jet         Err_Clmean    Err_Cdmean  ...
%             Cl_mean2      Cd_mean2      Fx1           Fx2           Mx1           Mx2           eta1          eta2];
% Result affected in post.m
% Result read/write : [Result] = rw_Result (str_cases, fid_cases, data_sim.str_file, Result, 'open_write');
Result    = zeros(1,22);

% XFOIL Results current case
% ResultX = [inc Cl Cd Cdp Cm Top_xtr Bot_xtr]
% Result(1:7) = ResultX; for optim with XFOIL suppose that size(Result) > size(ResultX)
ResultX   = zeros(1, 7);

% RANS / XFOIL for all cases
% ResultsR (i_cas,:) = Result;          % Result = [inc Cl Cd Clrms Cdrms ...]
% xscR     (i_cas,:) = zeros(1);        % (to implement) x/c
% CpR      (i_cas,:) = zeros(1);        % (to implement) Cp=f(x/c)
ResultsR = zeros(1,22);
% ResultsX (i_cas,:) = ResultX;         % ResultX= [inc Cl Cd Cdp Cm ...]
% xscX     (i_cas,:) = xscX1;
% CpX      (i_cas,:) = CpX1;
ResultsX = zeros(1, 7);
% ============================================================================================================================
% ResultsR is not initialized !
xscX    = [];           % vector for Xfoil
xscX1   = [];           % vector for Xfoil
CpX     = [];           % vector for Xfoil
CpX1    = [];           % vector for Xfoil

% Start the clock
t_start         = clock;

% Varargin
defaults_opt    = struct ('x',[],'n_conf',0,'sim',1,'os','Windows','param','');
if nargin == 0
    x       = defaults_ops.x;       % x     = []
    n_conf  = defaults_opt.n_conf;  % n_conf= 0
    sim     = defaults_opt.sim;     % sim   = 1
    os      = defaults_opt.os;      % os    = 'Windows'
    param   = defaults_opt.param;   % param = ''
end
if nargin == 1
    n_conf  = defaults_opt.n_conf;  % n_conf= 0
    sim     = defaults_opt.sim;     % sim   = 1
    os      = defaults_opt.os;      % os    = 'Windows'
    param   = defaults_opt.param;   % param = ''
end
if nargin == 2
    sim     = defaults_opt.sim;     % sim   = 1
    os      = defaults_opt.os;      % os    = 'Windows'
    param   = defaults_opt.param;   % param = ''
end
if nargin == 3
    os      = defaults_opt.os;      % os    = 'Windows'
    param   = defaults_opt.param;   % param = ''
end
if nargin == 4
    param   = defaults_opt.param;   % param = ''
end

% =========================================================================
%Fobj_best   = param.Fobj_best;
%last_Cl     = param.last_Cl;
%last_ClsCd  = param.last_ClsCd;
% =========================================================================
% if os not specified or wrong at the beginning, try to found it alone
% =========================================================================
str = computer;     % what is my computer ?
switch str
    case {'pcwin','pcwin64','PCWIN','PCWIN64'}
        os = 'Windows';
    case {'glnx86','glnxa64','GLNX86','GLNXA64','MACI64'}
        os = 'Linux';
    otherwise
        disp('not detected the operating system of your computer');
        disp('please specify it by doing: VLab(...,''Linux'') for example');
end

% =========================================================================
% defaults values fixed except param if defined before VLab call
% =========================================================================
[data_geo,data_topo,data_mesh,data_num,data_sim,data_ac,data_sup,data_phy,str_cases,fid_cases,param] = defaults2(n_conf,os,param);  % defaults values of all variables
if sim==0 data_sim.sim=0;end

%x
%n_conf
%sim
%os
%param
switch os
    case 'Linux'
        [sta,data_num.current_dir]=system('pwd');   % running directory
    case 'Windows'
        data_num.current_dir = pwd;   % running directory
end
% =========================================================================
% affect optim variables
% =========================================================================
if not(size(x,1))==0            % if optim mode
    x
    data_sim.cas                = param.cas;            % O1P2 or O1P4 or ... in opti_pb??.m & data??.m
    data_sim.vector             = param.vector;         % multi-point optimization
    
    data_sim.opti.objective     = param.objective;      % fitness function {'max lift','min drag','max L/D','min Cp-CP*','min drag clift'}
    data_sim.opti.Cl_target     = param.Cl_target;      % target    lift for fitness function
    data_sim.opti.Cl_ref        = param.Cl_ref;         % reference lift for fitness function
    data_sim.opti.Cd_target     = param.Cd_target;      % target    drag for fitness function
    data_sim.opti.Cd_ref        = param.Cd_ref;         % reference drag for fitness function

    data_sim.opti.Mxmax         = 0;                % x-axis moment limit for constraint objective
    data_sim.opti.F1            = 0;                % objectif F1
    data_sim.opti.C1_target     = 0;                % contrainte C1
    data_sim.opti.C2_target     = 0;                % contrainte C2

    data_sim.opti.Fx1_target    = param.Fx1_target;     % target thrust for objective function - Front rotor
    data_sim.opti.Fx1_ref       = param.Fx1_ref;        % ref.   thrust for objective function - Front rotor
    data_sim.opti.Fx2_target    = param.Fx2_target;     % target thrust for objective function - Rear  rotor
    data_sim.opti.Fx2_ref       = param.Fx2_ref;        % ref.   thrust for objective function - Rear  rotor
    data_sim.opti.Mx1_target    = param.Mx1_target;     % target moment for objective function - Front rotor
    data_sim.opti.Mx1_ref       = param.Mx1_ref;        % ref.   moment for objective function - Front rotor
    data_sim.opti.Mx2_target    = param.Mx2_target;     % target moment for objective function - Rear  rotor
    data_sim.opti.Mx2_ref       = param.Mx2_ref;        % ref.   moment for objective function - Rear  rotor
end
% =========================================================================
% logo options
% =========================================================================
str_logo = 'no';   % no logo
if (size(x,1)==0 && strcmp(str_logo,'no'));logo(x);end  % if analysis mode & str_logo='no' : run logo

% =========================================================================
%
% data : generic cas
%
% =========================================================================
str_data_function=strcat('data',num2str(n_conf));           % string of the function data_xxx.m
fh_data = str2func(str_data_function);                      % function name handle from the string
[cas,var,str_var] = fh_data(x,data_sim);                    % call to the function data_xxx.m

% ===========================
%
%  affect : current cas
%
% ===========================
t_start_cas (var.n_cas,1:6) = 0;
cpu_time_cas(var.n_cas,1:6) = 0;
firstIteration = true;
genetics = true;
firstIteration = true; 
i = 0
while genetics == true 
    if (firstIteration == true)
       i_cas = 1;
       disp('I enter')
       slaunch = sprintf('python Genetics-.py &');
       [sta,str] = system  (slaunch); 
       disp('Im out')
       firstIteration = false;
        t_start_cas(i_cas,1:6) = clock;    
        str_affect_function = strcat('affect',num2str(n_conf));             % string of the function data_xxx.m
        fh_affect           = str2func(str_affect_function);                % function name handle from the string
        [data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = fh_affect (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x);
        firstIteration = false;
    end
    %% We shouls change the name of the folder to be created but...
    
    
    % ==========================================
    % Intelligent
    % ==========================================
    if data_sim.Stop_Nit    == 0; data_sim.Stop_type    = 'Intelligent';end
    if data_sim.sim         == 0; data_sim.graphic      = 1;end
    if not(size(x,1)==0);                   % if optimization mode
        data_sim.sim        = 1;
        data_num.Affichage  = 'no';         % Verbose mode with a lot of text print
        data_num.debug      = 'no';         % Affichage in debug mode
        data_num.Rep_data   = '';           % Choix des variables to print : {'geo','mesh','topo','sim','ac','all'} 
        data_num.mat        = 'no';         % no mat files
        data_num.visu       ='opti';        % Fig4.m
    end
    % Cpu time estimation
    % input = (data_sim.Stop_Nit, data_sim.np_per, var.n_cas, Mesh kn, tpppdt_computer, data_num.parallel, data_num.nproc)
    % pb : Mesh kn ? A. Laenger / gambit results analysis ?
    % ================================================================
    % Figures VLab : results for comparisons (analysis, optimization)
    % ================================================================
    if strcmp(data_sim.comparison,'yes')
        figure(3);
        data_sim.data_type = 'num'; %?
        switch data_sim.data_type
            case 'num'  % ?
            case 'file' % load file to implement
        end
        for i = 1 : size(data_sim.Cl_res,1)     % loop on the number of results
            %symbol = ['sk' 'sr' 'sg'];          % 'sk' 'sr' 'sg' for square symbol without line 
            subplot(2,2,1);
            plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),'s');hold on;
            %plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),symbol(i));hold on;
            title('Polar Cd = f(Cl)');
            subplot(2,2,2);
            plot(data_sim.Cl_res(i,:),data_sim.Cl_res(i,:)./data_sim.Cd_res(i,:),'s');hold on; % new  point in red
            title('Lift to drag ratio Cl/Cd = f(Cl)');
            subplot(2,2,3);
            plot(data_sim.al_res(i,:),data_sim.Cl_res(i,:),'s');hold on;
            title('Lift versus angle of attack Cl = f(i)');
            subplot(2,2,4);
            plot(data_sim.al_res(i,:),data_sim.Cm_res(i,:),'s');hold on;
            title('Moment versus lift Cm(c/4) = f(i)');

        end
    end
    %=====================================================================

    % new parametrization (of the current cas) from old one : param 
    % [param] = parametrization (data_geo,data_mesh,data_sim,data_ac,data_num,data_sup);

    %=========================================================================
    %
    % GEOMETRY : create n_cut files of vertex for each sections of the 3D
    %            non dimensional geometry in geoJ.dat
    % input     data_geo, i
    % output    data_geo, xl, yl, xu, yu
    %
    %=========================================================================
    
    %% Know if Genetics has created the airfoil genetics_airfoilCreated.txt
    while exist('genetics_airfoilCreated.txt','file')~=2  
        fprintf('Waiting for genetic algorithm to create the airfoil data\n')
        pause(2)
    end
    fprintf('Airfoil data ready')
    %% Remove file created by genetics
    delete('genetics_airfoilCreated.txt')
    %% Call gmsh
    addToFilename = strcat(num2str(i),'ProfileGenetic_');
    data_sim.str_file = strcat(addToFilename,data_sim.str_file);
    [gmsh_journal_file, mesh_file] = journal_GMSH(data_geo,data_mesh,data_topo,data_sim,data_num);
    [openfoam_journal_file]           = openfoam_journal   (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  
    [data_num] = openfoam_launch(os,openfoam_journal_file,data_num,data_sim, data_geo, mesh_script_file);
    fprintf('post OpenFOAM : start \n');
    data_sim.Nit                    = data_sim.iter_trans+2*data_sim.iter_sup;
    [Resultc, Result, Err_Clmean]   = post_OpenFOAM(os,data_geo,data_num,data_sim,data_ac);
    fprintf('post OpenFOAM : end \n\n');
    %% write file to tell genetics that results are available
    fileID = fopen('vlab_AirfoilSimulated.txt.txt','w');
    fprintf(fileID,'w');
    fprintf(fileID,'Finished OpenFOAM\n');
    fclose(fileID);
end
for i_cas = 1 : var.n_cas
t_start_cas(i_cas,1:6) = clock;    
delete_gambit_files;                                                % delete GAMBIT files from previous cas
str_affect_function = strcat('affect',num2str(n_conf));             % string of the function data_xxx.m
fh_affect           = str2func(str_affect_function);                % function name handle from the string
[data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = fh_affect (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x);

var.value  % to verify that var affectation is well done by affect?.m

%===================================================
% multipoint optimization provisoire implementation
%===================================================
if not(size(x,1)==0)                % if optim mode
if size(param.vector,1)==0          % if param.vector = []
else                                % if param.vector is a vector (multipoint optimization)
%    data_sim.inc = param.scalar;    % NVLab.m : affectation of the multipoint vector - link with x ?
end
end

% ==========================================
% Intelligent
% ==========================================
if data_sim.Stop_Nit    == 0; data_sim.Stop_type    = 'Intelligent';end
if data_sim.sim         == 0; data_sim.graphic      = 1;end
if not(size(x,1)==0);                   % if optimization mode
    data_sim.sim        = 1;
    data_num.Affichage  = 'no';         % Verbose mode with a lot of text print
    data_num.debug      = 'no';         % Affichage in debug mode
    data_num.Rep_data   = '';           % Choix des variables to print : {'geo','mesh','topo','sim','ac','all'} 
    data_num.mat        = 'no';         % no mat files
    data_num.visu       ='opti';        % Fig4.m
end
% Cpu time estimation
% input = (data_sim.Stop_Nit, data_sim.np_per, var.n_cas, Mesh kn, tpppdt_computer, data_num.parallel, data_num.nproc)
% pb : Mesh kn ? A. Laenger / gambit results analysis ?
% ================================================================
% Figures VLab : results for comparisons (analysis, optimization)
% ================================================================
if strcmp(data_sim.comparison,'yes')
    figure(3);
    data_sim.data_type = 'num'; %?
    switch data_sim.data_type
        case 'num'  % ?
        case 'file' % load file to implement
    end
    for i = 1 : size(data_sim.Cl_res,1)     % loop on the number of results
        %symbol = ['sk' 'sr' 'sg'];          % 'sk' 'sr' 'sg' for square symbol without line 
        subplot(2,2,1);
        plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),'s');hold on;
        %plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),symbol(i));hold on;
        title('Polar Cd = f(Cl)');
        subplot(2,2,2);
        plot(data_sim.Cl_res(i,:),data_sim.Cl_res(i,:)./data_sim.Cd_res(i,:),'s');hold on; % new  point in red
        title('Lift to drag ratio Cl/Cd = f(Cl)');
        subplot(2,2,3);
        plot(data_sim.al_res(i,:),data_sim.Cl_res(i,:),'s');hold on;
        title('Lift versus angle of attack Cl = f(i)');
        subplot(2,2,4);
        plot(data_sim.al_res(i,:),data_sim.Cm_res(i,:),'s');hold on;
        title('Moment versus lift Cm(c/4) = f(i)');

    end
end
%=====================================================================

% new parametrization (of the current cas) from old one : param 
% [param] = parametrization (data_geo,data_mesh,data_sim,data_ac,data_num,data_sup);

%=========================================================================
%
% GEOMETRY : create n_cut files of vertex for each sections of the 3D
%            non dimensional geometry in geoJ.dat
% input     data_geo, i
% output    data_geo, xl, yl, xu, yu
%
%=========================================================================
if strcmp(data_geo.sequence.type(1:3),'geo')
fprintf('GEO : start \n');
n_cut = size(data_geo.cut.var,1);                                   % nmuber of cuts
for i = 1 : n_cut                                                   % loop   on cuts (default value 1)
    [xu,yu,xl,yl,data_geo]  = airfoil_geometry (data_geo,i);        % create or read 2D geometry (airfoil or sail cut)
end
%data_geo.cut.var(:,2)
%[data_sim.str_file]     = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
fprintf('GEO : end \n\n');
end
% geometry : write in Javafoil format (if necessary)
%[x_geo,y_geo] = airfoil_rw_Javafoil ('write','geoJ.txt',x_geo,y_geo)
% geometry : dimensional in SI units  (if necessary)
% geometry : translate, rotate, etc   (if necessary)

%=======================================================================
%
% DESIGN : design the geometry in a figure
%
%=======================================================================
%=======================================================================
%
% TOPOLOGY : geoJ.dat -> profile_2d.dat (adim -> dim)
%
%=======================================================================
if strcmp(data_geo.sequence.type(5:8),'topo')
fprintf('TOPO : start \n');
switch n_conf
    case {0,1,4,6,10,11,12,13,14,17,20,21,22,23,38,39,40,44,45,46,47,48,49,54,55,99,100,101,102,103,107,140,143,144,145,146,200,201,499,1001,1002,1003}
        [points_file,data_topo]     = szone             (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
    case {919,9194}
        %[points_file,data_topo]     = szoneb            (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
        [points_file,data_topo]     = szoneb            (data_geo,data_topo,data_mesh,data_num,data_ac)
    case {2,3,5,7,15,30,41,51,52,53,60,61,63,71,72,73}
        [points_file,data_topo]     = szone_ac          (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
    case {80}   % Parapente : Louis PERROT-MINOT (CFD 3A)
        [points_file,data_topo]     = szone_para        (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);
    case {81}   % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
        [points_file,data_topo]     = szone_parafoil    (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);  
    case {82}   % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
        [points_file,data_topo]     = szone_parafoil2    (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl); 
    case {9}    % airfoil withou bda surface
        [points_file,data_topo]     = szone_bda         (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);
    case {472}    % wingsail Starccm+
        szone_SCCM                                      (data_geo,data_num);
end
fprintf('TOPO : end \n\n');
end

%=====================================================================
%
% jouM : create journal GAMBIT / ICEM - profile_2d.dat -> ?.jou
%
%=====================================================================
if strcmp(data_geo.sequence.type(10:13),'jouM') || strcmp(data_geo.sequence.type(10:13),'jouG') || strcmp(data_geo.sequence.type(10:13),'jou2') % if RANS
fprintf('jouM : start \n');
switch n_conf
    case {0,1,4,6,10,11,12,13,14,17,20,21,22,23,38,39,40,54,55,99,100,101,102,103,107,499,919,1001,1002,1003,9194}
    [gambit_journal_file,mesh_file] = journal_GAMBIT      (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {9}
    [gambit_journal_file,mesh_file] = journal_GAMBIT_bda  (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {2,3,5,7,15,61,30,41,51,52,53,60,63,71,72,73}
    [gambit_journal_file,mesh_file] = journal_GAMBIT_ac   (data_geo,data_mesh,data_topo,data_sim,data_ac,data_num);   % (data -> *.jou)
    case {80}   % Parafoil PIP 
    [gambit_journal_file,mesh_file] = journal_GAMBIT_para (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)    
    case {81}   % Parafoil PIP F.Manara; F.Sgarbossa; A.MeyerForsting
    [gambit_journal_file,mesh_file] = journal_GAMBIT_parafoil (data_geo,data_mesh,data_topo,data_sim,data_num);       % (data -> *.jou)    
    case {82}   % Parafoil PIP F.Manara; F.Sgarbossa; A.MeyerForsting
    [gambit_journal_file,mesh_file] = journal_GAMBIT_parafoil2 (data_geo,data_mesh,data_topo,data_sim,data_num);      % (data -> *.jou)  
%   case {16}   % ?
%   [gambit_journal_file,mesh_file] = jouG_profilafente   (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {44}    % n objects
    [gambit_journal_file,mesh_file] = jou_G_objects       (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {45,46,146}    % 2 objects - VGC
    [gambit_journal_file,mesh_file] = jou_G_AC45          (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {47}       % 3 objects - Dinquel
    [gambit_journal_file,mesh_file] = jou_G_3_airfoils    (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {48}       % 2, 3 objects - Vialait
    [gambit_journal_file,mesh_file] = jou_G_AC45b         (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {49}       % 3 objects - 30P30N
    [gambit_journal_file,mesh_file] = jou_G_AC45c         (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {50}       % Cylinder
    [gambit_journal_file,mesh_file] = jou_G_Cylinder      (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {51}       % 2D Bluff Body
    [gambit_journal_file,mesh_file] = jou_G_Bluff_Body    (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {52}       % 2D Body Ground
    [gambit_journal_file,mesh_file] = jou_G_Body_Ground   (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {560}
    [gmsh_journal_file, mesh_file] = journal_GMSH(data_geo,data_mesh,data_topo,data_sim,data_num);
    case {140,143,144,145}
        switch data_num.mesher
            case 'GAMBIT'
                [gambit_journal_file,mesh_file] = journal_GAMBIT (data_geo,data_mesh,data_topo,data_sim,data_num);    % (data -> *.jou)
            case 'ICEM'
                [icem_replay_file,mesh_file]    = replay2dHybrid_ICEM (data_geo, data_mesh);                                                          % (data -> *.rpl)
        end
    case {141,142}
    [icem_replay_file,mesh_file]    = replay3d_ICEM       (data_geo, data_mesh); 
    case {200,201}
    [gambit_journal_file,mesh_file] = jou_GAMBIT_uns      (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {203,204,205,206,207}
    %[gambit_journal_file,mesh_file] = jouG_dragonfly      (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    [gambit_journal_file,mesh_file] = jouG_dragonflyB     (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    case {301}
    [gambit_journal_file,mesh_file] = jouG_Cube_3D        (data_geo,data_mesh,data_topo,data_sim,data_num);     % cube 3D (param)
    case {401,402,404,405,411}
    [gambit_journal_file,mesh_file] = jouG_sail_3D        (data_geo,data_mesh,data_topo,data_sim,data_num);     % 1 sail (param)
    case {403,406,412}
    [gambit_journal_file,mesh_file] = jouG_2sail_3D       (data_geo,data_mesh,data_topo,data_sim,data_num);     % 2 sails (param+iges)
    case {413}
    [gambit_journal_file,mesh_file] = jou_G_2sail_mast_3D (data_geo,data_mesh,data_topo,data_sim,data_num);     % 2 sails+mast (param)
    case {414,430}
    [gambit_journal_file,mesh_file] = jouG_sail_3D_igs    (data_geo,data_mesh,data_topo,data_sim,data_num);     % 1 sail (iges)
    case {424}
    [gambit_journal_file,mesh_file] = jouG_sail_3DBL_igs  (data_geo,data_mesh,data_topo,data_sim,data_num);     % 1 sail (iges)
    case {407,422}
    [gambit_journal_file,mesh_file] = jouG_2sail_3D_igs   (data_geo,data_mesh,data_topo,data_sim,data_num);     % 2 sails (iges+iges)
    case {432}
    [gambit_journal_file,mesh_file] = jouG_2sail_3D_par   (data_geo,data_mesh,data_topo,data_sim,data_num);     % 2 sails (param)
    case {447} % tetra mesh 
    [gambit_journal_file,mesh_file] = jou_G_Wing_3D       (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D
    case {448} % tetra extruded mesh
%   [gambit_journal_file,mesh_file] = jou_G_Wing_3D_Ak2   (data_geo,data_mesh,data_topo,data_sim,data_num);      % Wing 3D - Annika mesh - 2S
    [gambit_journal_file,mesh_file] = jou_G_Wing_3D_Ak3   (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D - Annika mesh - 4S
    case {450,455,456} % tetra extruded mesh
%    [gambit_journal_file,mesh_file] = jou_G_Wing_3D_Ak4   (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D - Annika mesh - 4S - 5 sections
%    [gambit_journal_file,mesh_file] = jou_G_Wing_3D_Ak4b  (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D - Annika mesh - 4S - n sections
    [gambit_journal_file,mesh_file] = jou_G_Wing_3D_Ak4c  (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D - Annika mesh - 4S - n sections
    case {409}
    [gambit_journal_file,mesh_file] = jou_G_nWing_3D      (data_geo,data_mesh,data_topo,data_sim,data_num);     % Wing 3D
    case {410}
    [gambit_journal_file,mesh_file] = jou_G_AC72          (data_geo,data_mesh,data_topo,data_sim,data_num);     % AC72 Design 3D
    case {501}
    [gambit_journal_file,mesh_file] = jou_G_Deflector_3D  (data_geo,data_mesh,data_topo,data_sim,data_num);     % Deflector 3D
    case {505}
    [icem_replay_file,mesh_file]    = replay_CROR_ICEM    (data_geo,data_mesh);                                 % CROR 3D
    
    case {472}       % Wingsail - StarCCM+
    [sccm_journal_file,mesh_file,data_geo] = jou_SCCM_AC72(data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
    data_num.sccm_file = mesh_file;
    
    case {601}
        switch data_geo.n_voile
            case 1
    [gambit_journal_file,mesh_file] = jou_G_ADONF_2D_1    (data_geo,data_mesh,data_topo,data_sim,data_num);           % ADONF 2D 1 voile
            case 2
    [gambit_journal_file,mesh_file] = jou_G_ADONF_2D_2    (data_geo,data_mesh,data_topo,data_sim,data_num);           % ADONF 2D 2 voiles
            case 3
    [gambit_journal_file,mesh_file] = jou_G_ADONF_2D_3    (data_geo,data_mesh,data_topo,data_sim,data_num);           % ADONF 2D 3 voiles
            case 4
    [gambit_journal_file,mesh_file] = jou_G_ADONF_2D_4    (data_geo,data_mesh,data_topo,data_sim,data_num);           % ADONF 2D 4 voiles
        end
end

%gambit_journal_file
%mesh_file

switch data_num.mesher
            case 'GAMBIT'
                data_geo.sequence.mesh_script_file  = gambit_journal_file;  % journal file computed in jouM
            case 'ICEM'
                data_geo.sequence.mesh_script_file  = icem_replay_file;     % replay  file computed in jouM
            case 'STAR'
                data_geo.sequence.mesh_script_file  = sccm_journal_file;    % replay  file computed in jouM
            case 'GMSH'
            	data_geo.sequence.mesh_script_file  = gmsh_journal_file; 
end

data_geo.sequence.mesh_file = mesh_file;            % mesh_file defined in jouG
%data_geo.sequence.jou_file  = gambit_journal_file;  % journal file computed in jouG 24/10/12 %
fprintf('jouM : end \n\n');
end

%=====================================================================
% Xfoil : create Xfoil journal file xfoil-input.dat
%=====================================================================
if strcmp(data_geo.sequence.type(10:13),'jouX') || strcmp(data_geo.sequence.type(10:13),'jou2') && strcmp(os,'Windows') % if XFOIL
    fprintf('jouX : start \n');
    XFOIL_param.airfoil_name    = data_sim.geo;                             % Airfoil geometry - from data?.m
    XFOIL_param.Inc             = data_sim.inc;                             % Angle of attack  - from data?.m
    XFOIL_param.Reynolds        = data_sim.Vinf*data_geo.c/data_phy.nu;     % Reynolds number  - from data?.m & affect?.m
    XFOIL_param.Mach            = 0.0;                                      % Mach number
    XFOIL_param.XTRu            = data_sim.XTRu;                            % (0 = forced transition at L.E., 1 = free transition) - from affect?.m
    XFOIL_param.XTRl            = data_sim.XTRl;                            % (0 = forced transition at L.E., 1 = free transition) - from affect?.m
    XFOIL_param.Ncrit           = data_sim.Ncrit;                           % Turbulence level (N=9 => Tu = 0.07%)                 - from affect?.m

zzz=0;  % may be discarded
if zzz==1
    if size(x,1)==0 % NACA analysis
        XFOIL_param.m 		= var.value(i_cas,1);	% airfoil camber value
        XFOIL_param.p		= var.value(i_cas,2);	% airfoil camber position value
        XFOIL_param.t 		= var.value(i_cas,3);	% airfoil thickness value
    else            % NACA optimization
    	XFOIL_param.m               = round(x(1));                              % airfoil camber value
    	XFOIL_param.p               = round(x(2));                              % airfoil camber position value
    	XFOIL_param.t               = round(x(3));                              % airfoil thickness value
    end
end

	[output_file,input_file,Cp_file] = jou_XFOIL (XFOIL_param);                % create XFOIL journal
end
%=====================================================================
%
% runM : run Mesh journal - ?.jou or ?.rpl -> ?.msh
%
%=====================================================================
if strcmp(data_geo.sequence.type(15:18),'runM') || strcmp(data_geo.sequence.type(15:18),'runG')
fprintf('runM : start \n');
%    gambit_journal_file = data_geo.sequence.jou_file;                               % name of the journal file for Gambit
    mesh_script_file = data_geo.sequence.mesh_script_file;                          % name of the script file for Mesher
    mesh_file           = data_geo.sequence.mesh_file;                              % name of the mesh file
    [data_num]          = mesh_launch (os,mesh_script_file,mesh_file,data_num);  % run Mesher (*.jou / *.rpl -> *.msh)
    if data_num.parallel == 1
        switch n_conf
            case {401,402,403,404,405,406,407,447,448,409,410,411,412,413}
            fprintf(' cas = %i',n_conf,' parallel => tpoly for hexcore mesh ');
            mesh_file_new = 'mesh_tpoly.msh';
            if strcmp(os,'Linux')
            [sta,str] = system(sprintf('/homes/daep-appli/chapin/Fluent/tpoly.1.0.0 %s %s',mesh_file,mesh_file_new)) % for hexcore mesh and parallel calculation
            else
            fluent_dir = 'c:\Fluent.Inc\utility\tpoly1.0\ntx86\';
            [sta,str] = system(sprintf('%stp100.exe %s %s',fluent_dir,mesh_file,mesh_file_new)) % for hexcore mesh and parallel calculation
            end
            mesh_file = mesh_file_new;
            data_geo.sequence.mesh_file = mesh_file;
        end
    end
fprintf('runM : end \n\n');
end

% put right value to c after mesh generation because scale <> 1
%if data_geo.scale~=1
%    data_geo.c = data_geo.scale;
%end
% turbulent scales
if strcmp(data_sim.regime,'turbulent')
    data_mesh.yplus  = eval_yplus       (data_mesh.y_wall,data_num.Affichage,data_geo,data_sim,data_phy);           % evaluate wall y+ (turbulent flat plate)
    [L,lk,tL,tlk]    = flow_scales      (data_sim.Vinf,data_geo.c,data_sim.I_turb,data_phy.nu,data_num.Affichage);  % turbulent scales (L, lk, tL, tlk)
    data_mesh.scales.L      = L;
    data_mesh.scales.lk     = lk;
    data_mesh.scales.tL     = tL;
    data_mesh.scales.tlk    = tlk;
end

%===================
% view workspace
%===================
voir_var = 'no';
if strcmp(data_num.Affichage,'yes') && strcmp(voir_var,'yes')
    data_geo
    disp('data_geo.sequence');      data_geo.sequence
    disp('data_geo.method');        data_geo.method
    data_topo
    disp('data_topo.surf_bc');      data_topo.surf_bc
    data_mesh
    data_sim
    if data_sim.anim.film==1;       disp('data_sim.anim');data_sim.anim; end
    disp('data_sim.save');          data_sim.save
    data_num
    if strcmp(data_ac.ac , 'yes');  data_ac; end
end
%===================
% save workspace
%===================
if (size(x,1)==0) && strcmp(data_num.debug,'yes');              % if analysis mode & debug mode
    data_num.mat_file = strcat(datestr(now),'b.mat');           % nom de fichier unique
    data_num.mat_file(12)    = '-';                             % remplace espace par tiret
    data_num.mat_file(15)    = '';
    data_num.mat_file(17)    = '';
    save(data_num.mat_file)                                     % save workspace in dateb.mat, save ('toto.txt','-ascii') is not ok because of data_geo, ... are not compatible with ascii
end
%====================================
% define filename name
%====================================
switch  n_conf                      % affectation des variables du cas courant
    case 80                         % Modif Parapente : Louis PERROT-MINOT (CFD 3A)
        [data_sim.str_file]     = file_name_para (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
    case {81,82}                       % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
        [data_sim.str_file]     = file_name_parapente (data_geo,data_num,data_sim,data_ac,i_cas,data_topo);
    otherwise
        [data_sim.str_file]     = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
end
% added 4/10/13 for lift history file with it(size) and Cl(size) differences
file_cl         = strcat(data_sim.str_file,'_','lift'  ,'_history.out');
file_cd         = strcat(data_sim.str_file,'_','drag'  ,'_history.out');
file_cm         = strcat(data_sim.str_file,'_','moment','_history.out');
file_ac         = strcat(data_sim.str_file,'_','ac'    ,'_history.out');
file_pressure   = strcat(data_sim.str_file,'_pressure.tif');
if exist(file_cl,'file')==2;delete(file_cl);end;
if exist(file_cd,'file')==2;delete(file_cd);end;
if exist(file_cm,'file')==2;delete(file_cm);end;
if exist(file_ac,'file')==2;delete(file_ac);end;
if exist(file_pressure,'file')==2;delete(file_pressure);end;
%====================================
% best practice implementation
%====================================

%======================================
%
% Simulation : iter_trans + 2*iter_sup
%
%======================================
if (data_sim.sim == 1 || data_sim.sim == 2)                 % 1:simulation, 2:?
if strcmp(data_geo.sequence.type(20:23),'jouF')
    fprintf('jouF : start \n');
    mesh_file = data_geo.sequence.mesh_file;                   % The mesh : read in affect?.m or computed in journal_GAMBIT.m

% Time step, Simulation time, Time for mean
[data_sim.time_step, data_sim.time_sim, data_sim.iter_mean, data_sim.iter_sim] = The_time (data_geo.c, data_sim.Vinf, data_ac.Freq, data_sim.period_sim, data_sim.period_ndt, data_sim.period_mean);

switch data_geo.sequence.fluent_journal
    case 'fluent_journal'
    [fluent_journal_file]           = fluent_journal     (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'OpenFOAM_journal'
    [openfoam_journal_file]           = openfoam_journal   (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   OpenFOAM (data -> *.jou)
    case 'fluent_jou_cyl'
    [fluent_journal_file]           = fluent_jou_cyl     (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_cube'
    [fluent_journal_file]           = fluent_jou_cube    (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_cube2'
    [fluent_journal_file]           = fluent_jou_cube2   (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_18ft'
    [fluent_journal_file]           = fluent_jou_18ft    (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_sail_3D'
    [fluent_journal_file]           = fluent_jou_sail_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_Wing_3D'
    [fluent_journal_file]           = fluent_jou_Wing_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'fluent_jou_CROR_3D'
    [fluent_journal_file]           = fluent_jou_CROR_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
    case 'SCCM_AC72_Phys'
    [sccm_journal_file]             = jou_SCCM_AC72_Sim  (data_geo,data_mesh,data_topo,data_sim,data_num); % Journal SCCM (data > *.jou)
    fluent_journal_file = sccm_journal_file;
end
    fprintf('jouF : end \n\n');
end

%====================================================
% Launch FLUENT for iter_trans+2*iter_sup iterations
%====================================================
if strcmp(data_geo.sequence.fluent_journal, 'OpenFOAM_journal')
    [data_num] = openfoam_launch(os,openfoam_journal_file,data_num,data_sim, data_geo, mesh_script_file);
    fprintf('post OpenFOAM : start \n');
    data_sim.Nit                    = data_sim.iter_trans+2*data_sim.iter_sup;
    [Resultc, Result, Err_Clmean]   = post_OpenFOAM(os,data_geo,data_num,data_sim,data_ac);
    fprintf('post OpenFOAM : end \n\n');
else
if strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2') % if RANS
    fprintf('runF : start \n');
    [data_num]                      = fluent_launch    (os,fluent_journal_file,data_num,data_sim);              % Lancement FLUENT (*.msh -> *.cas)
    data_sim.Nit                    = data_sim.iter_trans+2*data_sim.iter_sup;
    [Resultc, Result, Err_Clmean]   = post(os,data_geo,data_num,data_sim,data_ac);
    fprintf('runF : end \n\n');
end
%============================================
%
% Launch XFOIL < input_file > output_file
%
%============================================
if strcmp(data_geo.sequence.type(10:13),'jouX') || strcmp(data_geo.sequence.type(10:13),'jou2') % if XFOIL
    fprintf('runX : start \n');last=0;
	
    xfoil_launch      (os,input_file,output_file); % run XFOIL(input_file) create xfoil-output.dat, xfoil-screen.dat, xfoil-Cp.dat
	
    if exist(output_file,'file') % if Xfoil converged and create result file, read it
    [alpha,Cl,Cd,Cdp,Cm,Top_xtr,Bot_xtr]= textread(output_file,'%f %f %f %f %f %f %f','headerlines',12); % alpha,... may be vectors
    last = size(alpha,1);
    end
    
    [xscX1,CpX1] = textread(Cp_file,'%f %f ','headerlines',1); % x, Cp may be vectors
    
    if last == 0    % if not converged (useful for optimization)
        disp (' Xfoil : convergence problem : please STOP by CTRL C')
        pause (2);last=1;alpha(1)=XFOIL_param.Inc;Cl(1)=0.1*XFOIL_param.Inc;Cd(1)=0.0005+0.05*Cl(1)^2;Cdp(1)=0.01;Cm(1)=0;Top_xtr(1)=1;Bot_xtr(1)=1;
    end
    
    ResultX(1) = alpha   (last);
    ResultX(2) = Cl      (last);
    ResultX(3) = Cd      (last);
    ResultX(4) = Cdp     (last);
    ResultX(5) = Cm      (last);
    ResultX(6) = Top_xtr (last);
    ResultX(7) = Bot_xtr (last);
    ResultXc   = ['inc    ' 'Cl     ' 'Cd     ' 'Cdp    ' 'Cm     ' 'Top_xtr' 'Bot_xtr'];
    fprintf('runX : (alpha,Cl,Cd,Cdp,Cm,xtru,xtrl) = %f %f %f %f %f %f %f \n\n',ResultX);
end
if strcmp(data_geo.sequence.type(25:28),'runX'); % for f_Fobj (Result, ...)
    Result(1:7) = ResultX;
end;
%================================================================
% Change fluid model to compressible flow => in fluent_journal.m
%================================================================
if strcmp(data_sim.Fluid_model,'compressible') 
    %if strcmp(data_sim.Fluid_model,'compressible') && (data_sim.Mach_number > 0.7)
    fprintf('Change Fluid Model to %s \n',data_sim.Fluid_model);
    [fluent_journal_file]           = fluent_jou_comp   (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal FLUENT    (material ideal-gas + bc pressure-far-field)
    [data_num]                      = fluent_launch     (os,fluent_journal_file,data_num,data_sim);              % Lancement FLUENT  (*.msh -> *.cas)
end
%======================================================
% Convergence loop : n*iter_sup < Stop_Nit
%======================================================

if strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2') % if RANS
    soft = data_num.soft;
%    if strcmp(soft,'Fluent')
switch data_sim.Stop_type
    case 'Intelligent'
        i_mean=1;i_err = 0;
        while ( Err_Clmean > data_sim.Stop_Err &&  data_sim.Nit < data_sim.Stop_Nit && data_sim.Nit < data_sim.Max_Nit )
            %if i_err < 10
            %    i_err = i_err + 1;
            %else
            %    data_sim.iter_sup = 2*data_sim.iter_sup;i_err=0;
            %end
            data_sim.Nit = data_sim.Nit + data_sim.iter_sup;
            [fluent_journal_file]           = fluent_j2     (mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,n_conf,data_num);  % Journal   FLUENT (data -> *.jou)
            [data_num]                      = fluent_launch (os,fluent_journal_file,data_num,data_sim);                             % Lancement FLUENT (*.msh -> *.cas)
            [Resultc, Result, Err_Clmean]   = post          (os,data_geo,data_num,data_sim,data_ac);
            [data_ac]                       = closed_loop   (os,data_geo,data_num,data_sim,data_ac);
            i_mean=i_mean+1;i_err=i_err+1;
        end     % while
    case 'Definite'
        data_sim.Nit    = data_sim.iter_trans+2*data_sim.iter_sup;
        counter         = 0;
        while (data_sim.Nit < data_sim.Stop_Nit && 1==1)
            data_sim.Nit = data_sim.Nit + data_sim.iter_sup;
            [fluent_journal_file]           = fluent_j2     (mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,n_conf,data_num);  % Journal   FLUENT (data -> *.jou)
            [data_num]                      = fluent_launch (os,fluent_journal_file,data_num,data_sim);                             % Lancement FLUENT (*.msh -> *.cas)
            [Resultc, Result, Err_Clmean]   = post          (os,data_geo,data_num,data_sim,data_ac);
            [data_ac]                       = closed_loop   (os,data_geo,data_num,data_sim,data_ac);
            if exist('stop.dat','file')                             % stop.dat exist ?
                disp(' VLab2 : stop forced by user ');              % display message
                %toto = textread('stop.dat','%f','headerlines',0)   % read an action to do
                data_sim.Nit = data_sim.Stop_Nit;                   % stop the loop
                delete 'stop.dat';                                  % reinit the process for next case
            end
            if counter == 0
                estimated_cpu_time          = round(data_sim.Stop_Nit / data_sim.iter_sup) * data_num.ellapsed_time;    % for   1 case
                cpu_time_sec                = round(estimated_cpu_time);data_sim.cpu_time_sec=cpu_time_sec;
                [cpu_time_order,time_unity] = time_order (cpu_time_sec); 
                fprintf(' ======================================================== \n');
                e_cpu_time = sprintf(' VLab2 : Estimated CPU time for this case = %6.0f %s',round(cpu_time_order),time_unity);disp(e_cpu_time);
                fprintf(' ======================================================== \n');
            end
            counter = counter + 1;
        end
    case 'Interactive'
        reply = '';reply = input('Lift history converged ? Y/N [N]: ', 's');
        while isempty(reply)
            [fluent_journal_file]           = fluent_j2     (mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,n_conf,data_num);  % Journal   FLUENT (data -> *.jou)
            [data_num]                      = fluent_launch (os,fluent_journal_file,data_num,data_sim);                             % Lancement FLUENT (*.msh -> *.cas)
            [Resultc, Result, Err_Clmean]   = post          (os,data_geo,data_num,data_sim,data_ac);
            [data_ac]                       = closed_loop   (os,data_geo,data_num,data_sim,data_ac);
        reply = input('Lift history converged ? Y/N [N]: ', 's');
        end
end     % switch data_sim.Stop_type
end

%clf;    % clear current figure(6) when convergence is ok
%=============================
% End of the convergence loop
%=============================

%return % sccm ?

%===================================================
% Launch FLUENT for end post-processing
%   Write  forces               -> VLab2_F?.out
%   if data_sim.graphic == 1    -> Create Graphics
%===================================================
if size(x,1)==0;                % if analysis mode
    if strcmp(data_num.fluent_jloads,'yes')
    [fluent_journal_file] = fluent_jloads (data_topo,data_sim,data_num);            % VLab_Drag.txt, VLab_Lift.txt
               [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
    end
    if strcmp(data_num.fluent_jend,'yes')
    [fluent_journal_file] = fluent_jend   (data_geo,data_topo,data_sim,data_num);   % VLab_Cl.txt, ..., *_field.txt, *_BL.txt, *_Model.txt, fluent-interpolation.ip
               [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
    end
    if strcmp(data_num.fluent_jimg,'yes')
    [fluent_journal_file] = fluent_jimg   (data_geo,data_topo,data_sim,data_num);   % *.tif
               [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
    end
end

%===================================================
% sccm
%    elseif strcmp (soft,'STAR')
%        switch data_sim.Stop_type
%            case 'Definite'
%                [fluent_journal_file] = jou_SCCM_AC72_Sim_Fin (data_geo,data_mesh,data_topo,data_sim,data_num); % Journal SCCM (data > *.java)
%                [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);      % Lancement Starccm+
%                [Resultc,Result,Err_Clmean] = post(os,data_geo,data_num,data_sim,data_ac);  % Post-processing
%        end
%    end
%
%end
%===================================================
end

%====================================
%
% Fobj - if optimization mode
%
%====================================
if not(size(x,1)==0);
    [Fobj] = f_Fobj(Result,data_sim,data_num,x); %f_Fobj;     % Compute objective function from Result

    %==========================================
    % figure 5 : Fobj=f(xi), i=1,nv
    %==========================================
    figure(5);
    nl = round(sqrt(size(x,1)));
    nc = round(size(x,1)/nl);
    if nc*nl < size(x,1)
        nc = nc + 1;
    end
    for nv = 1 : size(x,1)
        subplot(nl,nc,nv);
        plot(x(nv),Fobj,'+b');hold on;
        str_title = sprintf('Fobj = f(x%g)',nv);
        title(str_title);
    end
end

%==========================================
% Test add actuator in FLUENT
%==========================================
%[fluent_journal_file] = fluent_ac (mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,n_conf)

%=====================================================
%
% Plot current case on 4 figures
%
% 4 figures : Cd=f(Cl), Cl/Cd=f(Cl), Cl=f(i), Cmc/4=f(Cl) or image
%
% input :
% VLab      Result(2), Result(3), Result(6), x, i_cas, data_geo.sequence.type,
%           data_sim.cas, data_sim.comparison, data_sim.comparison, data_sim.str_file
%           file_image, xl, yl, xu, yu, data_sim.opti.objective, Cr, Ch
% RANS      ?
% Xfoil     xscX1, CpX1
%
%=====================================================
fprintf('VLab : post-processing \n\n');

% input : Result, ResultX, i_cas, 
% Result  = [ inc           Cl_mean       Cd_mean       Cl_rms        Cd_rms        Cm_mean       xcp         ...
%             Cx            Cy            Cq            Cmu           q_jet         Err_Clmean    Err_Cdmean  ...
%             Cl_mean2      Cd_mean2      Fx1           Fx2           Mx1           Mx2           eta1          eta2];
% ResultX = [inc Cl Cd Cdp Cm Top_xtr Bot_xtr]
figure(3);
%==========================================
% figure 3.1 : Cd=f(Cl)
%==========================================
xR      = Result (2); % Cd RANS
yR      = Result (3); % Cl RANS
xX      = ResultX(2); % Cd XFOIL
yX      = ResultX(3); % Cl XFOIL
Fig3p(1,i_cas,xR,yR,xX,yX,data_sim.title,size(x,1),data_geo.sequence.type(25:28),data_sim.comparison);
%===========================================
% figure 3.2 : Cl/Cd=f(Cl) - Exp/RANS/XFOIL
%===========================================
xR      = Result (2);
yR      = Result (2)/Result (3);
xX      = ResultX(2);
yX      = ResultX(2)/ResultX(3);
Fig3p(2,i_cas,xR,yR,xX,yX,data_sim.title,size(x,1),data_geo.sequence.type(25:28),data_sim.comparison);
%==========================================
% figure 3.3 : Cl=f(i)
%==========================================
xR      = Result (1);
yR      = Result (2);
xX      = ResultX(1);
yX      = ResultX(2);
Fig3p(3,i_cas,xR,yR,xX,yX,data_sim.title,size(x,1),data_geo.sequence.type(25:28),data_sim.comparison);
%==========================================
% Figure 3.4 Polar data Cl, Cd, Cm
% Analysis + RANS       Cm = f(Cl)
% Analysis + Xfoil      Cp=f(x/c)
% Optim    + airfoil    shape(x) + Cp(x)
% Optim    + sail3D     ?
%==========================================
%Fig3p4
xR      = Result (1);
yR      = Result (6)-0.25*Result (2); % Cm(c/4) = CmLE - Cl/4
xX      = ResultX(1);
yX      = ResultX(5)-0.25*ResultX(2); % Cm(c/4) = CmLE - Cl/4
Fig3p(4,i_cas,xR,yR,xX,yX,data_sim.title,size(x,1),data_geo.sequence.type(25:28),data_sim.comparison);
%===============================================
% Figure 4 & 7 Cp=f(x/c) & Cf=f(x/c) & geometry
%
% May be replaced by Polar.m
%===============================================
Fig4
%==========================================
% Figure 6 Image or Mesh
%==========================================
%Fig6
%==============================================
% file Cases_Coeff_mean.out
%==============================================
%if ~strcmp(data_geo.sequence.type(25:28),'runX')
[Result] = rw_Result (str_cases, fid_cases, data_sim.str_file, Result, 'open_write');
%save ('Result.mat',Result); disp(' VLab2 : Result.mat created');
%end
%==================================================
% file (format EXCEL) (bug incomprehensible !!!)
%==================================================
%cellule   = strcat( '''',char(64+i_cas),'1','''' )  
%status = xlswrite('cases.xls', Result, 'cases', cellule)

%============================================================
% add experimental data on figures plot data  (format ASCII)
%============================================================
%valid_0()
%=================================
% ASCII file visu from t_post.m
%=================================
if size(x,1)==0;                   % if analysis mode
end
%=================================
% Animation file from anim.m
%=================================
if ( size(x,1)==0 && data_sim.anim.film==1 );          % if analysis mode & anim mode
    anim
end     % if ( size(x,1)==0 && data_sim.anim.film==1 );

%=============================================================================
% save VLab_ResultsR.mat & VLab_ResultsX.mat (Fluent or/and XFOIL results)
%=============================================================================
if (strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2'))% && size(x,1)==0 % if XFOIL
    ResultsR (i_cas,:) = Result;        % Result = [inc Cl Cd Clrms Cdrms ...]
    xscR     (i_cas,:) = zeros(1);      % (to implement) x/c
    CpR      (i_cas,:) = zeros(1);      % (to implement) Cp=f(x/c)
end
if (strcmp(data_geo.sequence.type(25:28),'runX') || strcmp(data_geo.sequence.type(25:28),'run2'))% && size(x,1)==0 % if XFOIL
    ResultsX (i_cas,:) = ResultX;       % ResultX= [inc Cl Cd Cdp Cm ]
    xscX     (i_cas,:) = xscX1;
    CpX      (i_cas,:) = CpX1;
end

t_end_cas(i_cas,1: 6) = clock;

end     % if data_sim.sim == 1
end     % boucle i_cas
%=================================
% end loop i_cas
%=================================

%fclose(fid_cases);                  % close file Cases_Coeff_mean.out

%=============================================
% Writing Fig2_conv.tif, ...
%         VLab_Cases.out
%=============================================
if data_sim.sim==1;              % if sim mode
if size(x,1)==0;                 % if analysis mode
    print -f2 -dtiff 'Fig2_conv.tif'                % save file Convergence (fig2)
    print -f3 -dtiff 'Fig3_results.tif'             % save file Results     (fig3)
else                            % if optim mode
    print -f2 -dtiff 'Fig2_conv.tif'                % save file Convergence (fig2)
    print -f3 -dtiff 'Fig3_results.tif'             % save file Results     (fig3)
    print -f5 -dtiff 'Fig5_optim.tif'               % save file Optim       (fig5)
end
Results_file (x,data_geo,data_mesh,data_sim);   % save file VLab_Cases.out

%=====================================================================
% save VLab_ResultsR.mat & VLab_ResultsX.mat with Cp=f(x/c) - Polar.m
%=====================================================================
%if size(x,1)==0;                % if analysis mode
if strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2') % if RANS or RANS/XFOIL
save('VLab_ResultsR.mat','ResultsR','xscR','CpR');  % FLUENT for Polar.m
fprintf(' save VLab_ResultsR.mat for Polar.m\n');
end
if strcmp(data_geo.sequence.type(25:28),'runX') || strcmp(data_geo.sequence.type(25:28),'run2') % if XFOIL or RANS/XFOIL
save('VLab_ResultsX.mat','ResultsX','xscX','CpX');  % XFOIL  for Polar.m
fprintf(' save VLab_ResultsX.mat for Polar.m\n');
end
%end
end

%=================================
% Polar write
%=================================
ResultsR        % RANS results 
%====================================
% Move results files in a directory
%====================================
% copy defaults2.m, affect.m, data.m, *.out
% move *.cas, *.dat, *.mat, *.c, *.jpg
%
if data_sim.sim == 1 && size(x,1)==0 && 1==0            % if analysis mode and data_sim.sim=1
    Results_dir(n_conf,data_num,data_sim,data_ac);      % create a directory : data  files + results files
end
%====================================
% Grid only infos
%====================================
if data_sim.sim == 0 && 1==1
    [fluent_journal_file]           = fluent_grid   (os,mesh_file,data_geo,data_sim,data_num);          % Journal   FLUENT (data -> *.jou)
    [data_num]                      = fluent_launch (os,fluent_journal_file,data_num,data_sim);         % Lancement FLUENT (*.msh -> *.cas)
end
%===================
% save workspace
%===================
if size(x,1)==0;                % if analysis mode
    data_num.mat_file = strcat(datestr(now),'.mat');            % nom de fichier unique
    data_num.mat_file(12)    = '-';                             % remplace espace par tiret
    data_num.mat_file(15)    = '';
    data_num.mat_file(17)    = '';
    save(data_num.mat_file)                                     % save workspace in matlab.mat, save ('toto.txt','-ascii') is not ok because of data_geo, ... are not compatible with ascii
end
%=========================
% Resume simulation ai.m
%=========================
if size(x,1)==0;                % if analysis mode
[data_sim.ai_file] = ai (data_geo,data_mesh,data_num,data_sim,data_ac,i_cas,data_topo,data_phy,Result);
end
%====================================
% CPU time elapsed
%====================================
cpu_time_sec = etime(clock    ,t_start    );    % a scalar = cpu_time
cpu_time_cas = etime(t_end_cas,t_start_cas);    % a vector = cpu_time for each cas
cpu_timepcpi = cpu_time_sec/var.n_cas/data_num.mesh_cells/data_sim.Stop_Nit;       % a scalar = mean cpu_time / cas / cells / it
[cpu_time_order    ,time_unity     ] = time_order (cpu_time_sec);    % a scalar
[cpu_time_order_cas,time_unity_cas ] = time_order (cpu_time_cas);    % a vector  
[cpu_timepcpi_order,timepcpi_unity ] = time_order (cpu_timepcpi);    % a scalar
ratio_mesh   = round(100*data_num.cpu_time_mesh /cpu_time_sec);
ratio_solve  = round(100*data_num.cpu_time_solve/cpu_time_sec);
ratio_MATLAB = 100 - ratio_mesh - ratio_solve;

fprintf ('\n=============================================================================\n');
for i_cas = 1:var.n_cas 
s_cpu_time = sprintf(' CPU time cas(%g)   = %6.1f %s ',i_cas,cpu_time_order_cas(i_cas),time_unity_cas);disp(s_cpu_time);
end
%    fprintf ('=============================================================================\n');
s_cpu_time = sprintf(' Total CPU time    = %6.1f %s, Solver (Fluent/OpenFOAM) %3.0f %%, Mesher (Gambit/GMSH) %3.0f%%, MATLAB %3.0f %%',cpu_time_order,time_unity,ratio_solve,ratio_mesh,ratio_MATLAB);disp(s_cpu_time);
%    fprintf ('=============================================================================\n');
tpp        = sprintf(' CPU time/nodes/it = %6.1f %s for %i nodes and %i it.',cpu_timepcpi_order,timepcpi_unity,data_num.mesh_cells,data_sim.Stop_Nit);disp(tpp);
fprintf ('=============================================================================\n');

%====================================
% Best one, last one
%====================================
%param.Fobj_best     = Fobj_best;
%param.last_Cl       = last_Cl;
%param.last_ClsCd    = last_ClsCd;
%====================================
% Achievements
%====================================
Ok_file = 'Ok.txt';
fid     = fopen(Ok_file,'a+');
fprintf(fid,'%s VLab(%4i) %s %s %s Tcpu=%6g %s : Ok\n',datestr(now),n_conf,cas,data_sim.str_URANS,data_num.Fluent_ver,cpu_time_order,time_unity);
fclose(fid);
fclose('all');

% TO DO:
% 1003          x or x' in data1003.m / str2var.m when using optim with gaV2
%               bug init by interpolation because fluent-interpolation.ip exist before
%               normalized filename to facilitate debug .out .jou .msh .cas .dat .trn
%               film is ok in Fluent 6.3 but not in Fluent 14 or later .... a parameter is missing probably
%               add affichage nom mesh journal file and mesh file in VLab
%               Design of the wingsail config in VLab (case 45)
%               Optim x(param) y(multipoint) in optim with NVLab
%               Optim add Clrms, ... in VLab-Fobj...txt
% 142           bug interface def. FLUENT 14 (ok fluent6.3)
% ?             subject : add propeller/wind turbine/hydrogenerator blade design (project ENSIACET biomimetism/aero, PIP, dr�nes, ...)
% ?             test case RANS highRe RAE  2822
% ?             RANS highRe NACA 0012 / NACA 0020 polar comparison related to a different behavior of the suction separation seen on the lift slope (Fish NACA 020)
% ?             3D Appendage-Body Junction for horseshow vortex flow : http://www.stanford.edu/group/ctr/gallery.html
% ?             routine resolution maillage auto pour DNS, RANS, ...
% ?             VLab([],13) DNS 2D & 3D E387 Re=60 000 Sahin paper to validate
% ?             replace os by ispc and isunix => VLab2([],302) without the 'Linux or 'Windows' word
% ?      		cut post.m, add post V-V to see net effect of actuator Moreau p166
% ?             : add function Closed_Loop_Control.m
                %   1- V(t) adapt Cl through mesure of Cl and action on airfoil shape (e/c, f/c, xf/c)
                %   2- V(t) adapt Cl through mesure of Cl and action on actuator (Vj, Xj, Tj)
% ?             : add function Visu_Mesh.m 
% ?             : pb data1, data2, szone, szone_ac, ... variables pas tjs necessaires, perte de temps debug ...
% ?             : passage 3D pour traiter cas Menter Naca 0021 i=60
% ?             : calcul du debit reel du jet
% ?             : cas Airplane Tutorial (Fluorem)
% ?             : fichier txt des nom des parametres (Fluorem)
% ?             : parametric sensibility (Fluorem)
% ?             : maillage structurer uniforme + adapt region (jet + extrados )in Fluent
% ?             : sonde v(t) pour FFT
% ?             : 2 actionneurs (Amitay concept)
% ?             : add optim fmincon F. Gallard from 3A_cfd/G7/SX120/.
% ?             : add optim heat exchanger from M. Hermann award for module CFD
% ?             : add lam/tur/sep detection with hair in a BL
% ?             : separate fluent_journal.m en fluent_jou_def.m fluent_jou_init.m fluent_jou_iter.m

% Changes:
%  5 Avr  2015  : do not use -t2 in fluent command in fluent_jend.jou for /file/export/ascii                    : ok w/l
% 30 Mar  2015  : Fluent_Ver : force.m                                                                          : ok w/l
% 09 Dec  2014  : Fluent_Ver : bc_velocity_inlet.m, bc_pressure_outlet.m, bc_pressure_far_field.m,              : ok w/l
%                   write_forces.m, write_ascii.m, fluent_jloads.m
% 21 Mai  2014  : change scale and c use : scale is unused, mesh is generated in adim,   (c is used to scale)   : ok w/?
% 03 Avr  2014  : change scale and c use : non-dimensional units in affect.m         (scale is used to scale)   : ok w/?
% 03 Avr  2014  : in fact, c may diseappear because it should not be used now (mesh is always non-dimensional)  : ok w/?
% 15 Fev  2014  : add data_num.mesh_cells in fluent_launch.m (valid : 2D yes 3D no)                             : ok w/?
% 27 Oct  2013  : add x input in affect???.m                                                                    : ok w/l
% 30 Avr  2013  : add 430 spinnaker igs file from Lasher 2005                                                   : ok w/l
% 22 Nov  2012  : reconditionnement affect501.m & data501.m for deflector is to finish                          : ok w/l
% 23 Sept 2012  : replace finish.cas by finish.txt a smaller file for best perfo                                : ok w/l
% 30 Mai  2012  : VLab([],448) Ak4c ?                                                                           : ?? w/l
% 30 Mai  2012  : VLab([],11) NACA 0012 + transition prediction with SSTtr, k-kl-w                              : ok w/l
% 02 Dec  2011  : VLab([],448) Ak4b op�rationel                                                                 : ok w/l
% 02 Oct  2011  : VLab([],448) Ak3  op�rationel                                                                 : ok w/l
% 26 Aout 2011  : risque de manquer certains fichiers *.jou et *.msh car delete par code Annika !!!
% 19 Juil 2011  : fluent_journal.m, fluent_jend.m, szone.m, szoneb.m szone_ac.m szone_para.m / surf_bc          : ok w
% 11 Mai  2011  : affect422.m, fluent_jend.m / interpolation                                                    : ok w
% 12 Avr  2011  : data_geo.airfoil => data_geo.method
% 10 Avr  2011  : add choice of turbulence model SA, SST                                                        : ok windows
% 21 Mar  2011  : 411, 412, 413 : 3D param sails (412, 413 not finished)
% 17 Nov  2010  : I_turb, L_turb added in fluent_journal.m for BC turbulent scales influence study for BE2
% 18 Oct  2010  : data_mesh.y_wall for y+ evaluation for structured or unstructured meshes
% 17 Oct  2010  : NACA 0012 test case NASA 2D, valid_4.m, M1, M2, M3, M4 Ok but y+<24 ? (to finish)
%  2 Oct  2010  : data_geo.b the envergure used also for Sref=b*c, b=1 in 2D
%  2 Oct  2010  : data_geo.c is in meter not in millimeter => think to change data_mesh.h1 in meter also !!!
% 24 Sept 2010  : Read_FLUENT_Loads.m ok to extract (Fx,Fy,Fz,Mx,My,Mz) and calculate CP                        : ok Linux, need Fluent 6.3.26
% 23 Juil 2010  : The_time.m                                                                                    : ok windows
%  7 Juil 2010  : gv param + foc igs pour optimisation gv en interaction avec foc - PFE Nolwenn                 : ok windows
% 28 Juin 2010  : gv param 3D        pour optimisation gv                         - PFE Nolwenn                 : ok windows
% 19 Mars 2010  : read mesh : naca0012_uns.msh                                                                  : ok windows
% 19 Mars 2010  : read geometry data_geo.airfoil.type, etc...                                                   : ok windows
% 10 Fev  2010  : Flapped airfoil through airfoil read geometry from Javafoil                                   : ok windows
%  2 Fev  2010  : 3D run from a given mesh file : read mesh file cube_32k.msh                                   : ok windows
% 21 Jan  2010  : expansion for structured mesh toward TE (1+c)*h_mesh szone.m, szone_ac.m                      : ok windows
% 15 Oct  2009  : fluent_anim.m                                                                                 : ok windows
% 15 Avr  2009  : naca 4 digits geometry creation in szone.m, szone_ac.m, ...                                   : ok windows
% 15 Avr  2009  : jet_start, jet_stop in data_topo and not in data_ac                                           : ok windows
% 10 Avr  2009  : adapt_region.m for mesh adaptation around an actuator                                         : ok windows
%  9 Avr  2009  : modif valid_0.m, rw_coeff.m to adapt the new Cases_Coeff_mean.out                             : ok windows
% 27 Mar  2009  : build_udf.m, velocity_inlet.m : udf_jet interpreted or compiled
% 23 Jan  2009  : Results_dir.m : move results files in a unique case directory
% 21 Jan  2009  : fluent_jNit.m :
% 20 Jan  2009  : set_monitor_surface.m for fluent 6.2.16, 6.3.26, 6.4.11                                       : ok (windows,gambit2.2.30, fluent6.2.16)
% 20 Jan  2009  : variable domain size : N_Corde_Amont, N_Corde_Aval, N_Corde_Up, N_Corde_Down                  : ok (windows,gambit2.2.30,fluent 6.2.16)
% 19 Jan  2009  : repertoire unique sauvegarde fichiers de donnees defaults.m, affect.m, data.m                 : ok windows & linux
%  8 Jan  2009  : upgrade partiel Gambit 2.3.16 et 2.4.6 et Fluent 6.3.26 et 6.4.1                              : ok windows & linux
%  8 Jan  2009  : fluent_ver et gambit_ver                                                                      : ok windows & linux
% 28 Oct  2008  : pb file_ac VLab(2) cas=2
% 14 Oct  2008  : test case : 10, 11, 20
% 10 Oct  2008  : sauvegarde workspace in matlab.mat, Run_file
%  3 Oct  2008  : mode 'Definite'    : data_sim.Stop_Err, data_sim.Stop_type  
% 29 Sep  2008  : mode 'Intelligent' : arret auto URANS suivant convergence Cl(t)
% 13 Dec  2007  : choix du schema, du modele de turbulence
% 12 Nov  2007  : fichier global cases ASCII et EXCEL, logo, Err_Cd_mean, VLab(3) validation sans convergence
%  4 Juin 2007  : fichier vitesse moyenne extrados / localisation point de decollement (OK)
%  1 Juin 2007  : fichier vitesse actionneur (validation actionneur)
% 25 Mai  2007  : URANS convergence on periodic solution {intelligent,interactive}
% Remarks:
% 24 Sept 2010  : n_conf=401 tourne sur unix mais pb si parallel=1 et maillage hexcore
% ==========================================================================
