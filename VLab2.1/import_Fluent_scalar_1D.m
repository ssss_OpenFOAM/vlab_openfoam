function [X,Y,Cp,Cf] = import_Fluent_scalar_1D (filename)
%
fid     = fopen     (filename, 'r');
%field   = textscan  (fid, '%f %f %f %f', 9, 'headerlines', 1);  % read 9 lines of 4 column, except 1 header line
field   = textscan  (fid, '%f %f %f %f %f', 'headerlines', 1);  % read all lines, except 1 header line

%field{:}    % print all the data

nodenumber      = field{:,1};   % column 1
X               = field{:,2};   % column 2
Y               = field{:,3};   % column 3
Cf              = field{:,4};   % column 4
Cp              = field{:,5};   % column 5

xmin    = min(X);
xmax    = max(X);

end