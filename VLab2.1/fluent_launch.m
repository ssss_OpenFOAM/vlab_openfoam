function [data_num] = fluent_launch (os,fluent_jou,data_num,data_sim)
%
% input data :
%   num     : os, parallel, nproc, fluent_jou
% FLUENT options :
% -r    version
% -ssh  ?
% -p    parallel
% -t    number of processors
% -g    without graphic
% -i    journal file

t_start_solve = clock;

parallel    = data_num.parallel;
nproc       = data_num.nproc;
fluent_dir  = data_num.Fluent_dir;
fluent_ver  = data_num.Fluent_ver;
fluent_sn   = data_num.Fluent_sn;   % may be changed by '/usr/local/ansys13/v130/fluent' on sleipnir
str_file    = data_sim.str_file;
graphic     = data_sim.graphic;

if strcmp(data_sim.dim,'2D'); fluent_dim = '2ddp';end;
if strcmp(data_sim.dim,'3D'); fluent_dim = '3ddp';end;

%if strcmp(data_num.Affichage,'yes');fprintf(' fluent_launch : %s, FLUENT %s %s %s\n',os,fluent_ver, fluent_dim, fluent_jou);end;
fprintf(' fluent_launch : %s, FLUENT %s %s %s\n',os,fluent_ver, fluent_dim, fluent_jou);

%======================
% Run Fluent sous Linux
%======================
if strcmp(os,'Linux')
    
    file    = 'finish.txt';
    if exist(file,'file')==2 ; system (sprintf('rm -f %s',file)); end
    file    = ['fluent_journal.trn'];
    if exist(file,'file')==2 ; system (sprintf('rm -f %s',file)); end

    sta = 5; compteur = 3600;    % wait license during 10 hours
    while ((sta~=0) && (compteur>0))    % cond_1 AND cond_2
        if  (parallel == 1)
            s_nproc = num2str(nproc);
            if graphic == 0
            slaunch = sprintf('%s%s %s -r%s -ssh -p -t%s -g -i%s',fluent_dir,fluent_sn,fluent_dim,fluent_ver,s_nproc,fluent_jou) % batch without graphic (Ok)
            [sta,str] = system (slaunch);
            else
            slaunch = sprintf('%s%s %s -r%s -ssh -p -t%s    -i%s',fluent_dir,fluent_sn,fluent_dim,fluent_ver,s_nproc,fluent_jou) % batch with    graphic (Ok)
            [sta,str] = system (slaunch);
            end
        else
            if graphic == 0
            slaunch = sprintf('%s%s %s -r%s -g -i%s',fluent_dir,fluent_sn,fluent_dim,fluent_ver,fluent_jou) % batch without graphic (OK)
            [sta,str] = system  (slaunch);
            else
            slaunch = sprintf('%s%s %s -r%s    -i%s',fluent_dir,fluent_sn,fluent_dim,fluent_ver,fluent_jou) % batch with    graphic (OK)
            [sta,str] = system  (slaunch);
            end
        end        
        compteur = compteur - 1;
        error_fatal = 1 - isempty(findstr(str,'license problem'));
        if (sta~=0 || error_fatal~=0)   % cond_1 OR cond_2
            fprintf(' no available FLUENT license, try it again %g times\n',compteur);
            sta = max(sta,error_fatal);
            pause(10);
        end
    end     % while
        
end         % if Linux
%=========================
% Run Fluent sous Windows
%=========================
if strcmp(os,'Windows')
    
    switch fluent_ver
        case {'6.2.16','6.3.26'}
            fluent_dir  = 'c:\Fluent.Inc\ntbin\ntx86\';    % diwan
            %fluent_dir  = 'e:\Fluent.Inc\ntbin\ntx86\';     % diwan2
            %fluent_dir  = 'f:\Fluent.Inc\ntbin\ntx86\';     % diwan2
        otherwise
            fluent_dir  = 'c:\Fluent.Inc\ntbin\ntx86\'; % version licence diwan2
    end
    
    file = 'finish.txt'; if exist(file,'file'); system (sprintf('del %s',file)); end
    file = 'fluent_journal.trn'; if exist(file,'file'); system (sprintf('del %s',file)); end
    file = strcat(str_file,'_field.txt');if exist(file,'file');delete(file);end % warning permission denied
    
    if strcmp(fluent_jou,'fluent_jend.jou'); parallel = 0; nproc=1;end % solve bug in /file/export/ascii str_file_field.txt but create bug on PC_SLC because it run only in parallel !!!

    if  (parallel == 1)
        s_nproc = num2str(nproc);
        if graphic == 0
            slaunch = sprintf('%sfluent.exe %s -r%s -t%s -antx86 -hidden -i%s',fluent_dir,fluent_dim,fluent_ver,s_nproc,fluent_jou)    % string of launch
            [sta,str] = system  (slaunch);% in batch without graphic (OK)
            if strcmp(sta,0); str
            end
        else
            slaunch = sprintf('%sfluent.exe %s -r%s -t%s -antx86         -i%s',fluent_dir,fluent_dim,fluent_ver,s_nproc,fluent_jou)    % string of launch
            [sta,str] = system  (slaunch); % batch with    graphic (OK)
        end
    else % (parallel==0)
        if graphic == 0
            slaunch = sprintf('%sfluent.exe %s -r%s -antx86 -hidden -i%s',fluent_dir,fluent_dim,fluent_ver,fluent_jou)    % string of launch
            [sta,str] = system  (slaunch); % batch without graphic (OK)
        else
            slaunch = sprintf('%sfluent.exe %s -r%s -antx86         -i%s',fluent_dir,fluent_dim,fluent_ver,fluent_jou)    % string of launch
            [sta,str] = system  (slaunch);          % batch with    graphic (OK)
        end
    end
end
%=============================================
% Mesh cells number = ?
%=============================================
advanced = 1;
if advanced==1
    sta; % defaults sta = 0
    str; % if data_sim.graphic=1 str = ''
    n_cells = 0;    % init
    string  = 'node flags';
    str_end = findstr(str,string);   % search for string in str
    if size(str_end,1) ~=0
        n_cells = str2num(str(1,str_end-9:str_end-1));
        if n_cells==0
            fprintf(' fluent_launch : Mesh cells = %i because data_sim.graphic=1 \n',n_cells);
        else
            fprintf(' fluent_launch : Mesh cells = %i \n',n_cells);
        end
    end
    if ~isnan(n_cells) || n_cells ~=0
        data_num.mesh_cells = n_cells;
    else
        data_num.mesh_cells = 1;
    end
end
%=============================================
% Detect Error in Fluent - Windows/Linux
%=============================================
error_fatal = 1 - isempty(findstr(str,'ERROR'));
if (sta~=0 || error_fatal~=0) % bad execution or licence pb
    if (sta~=0)
        fprintf('    Error during Fluent run \n');
        fprintf('    Is FLUENT version ok ? \n');
        fprintf('    Please STOP !!! \n');
        fprintf('    status = %i \n',sta);
        pause(360);
    else
        fprintf('    Licence FLUENT not available, still %g try \n',compteur);
        fprintf('    result = %s \n',str);
    end
    sta = max(sta,error_fatal);
    pause(10);
else
    fprintf(' fluent_launch : FLUENT run OK \n');
end
%===========================================================
%
% MATLAB script wait end of Fluent - Windows/Linux
%
%===========================================================
waiting_time        = 0;
waiting_time_tot    = 0;
step                = 10;
max_cputime         = 24*3600;      % wait for 24 hours
delay               = 10;           % print delay
delay_sub           = delay/step;   % file test delay
coeff               = 2;            % coeff (may be estimated)
file                = 'finish.txt';
fid                 = fopen(file);

while (fid==-1) && (waiting_time < max_cputime)
    pause(delay_sub);
    waiting_time = waiting_time + delay_sub;
    fprintf(' %i ',waiting_time);
    fid   = fopen(file);    % file id number (-1 file does not exist)
    
    if waiting_time == delay
        waiting_time_tot = waiting_time_tot + waiting_time;
        [waiting_time_order,time_unity] = time_order (waiting_time_tot); % convert to relevant unit
        fprintf(' waited solver for %6.1f %s \n',waiting_time_order,time_unity);
        delay           = delay*coeff;  % new print delay
        delay_sub       = delay/step;   % new file test delay
        waiting_time    = 0;
    end
    
end
if fid==-1  % exit 1 : no finish.txt but cpu_time > 10 hours
    disp ('    Z! : finish.txt not created. Probably your FLUENT simulation was longer than 10 hours.');
    system(sprintf('cp Ok.txt finish.txt'));    % create finish.txt to continue (bug or too long run)
%else        % exit 2 : finish.txt exist and cpu_time < 10 hours
%    switch  os
%        case 'Linux'
%            system (sprintf('rm -f %s',file)); fprintf('    delete finish.txt after \n');
%        case 'Windows'
%            system (sprintf('del   %s',file)); fprintf('    delete finish.txt after \n');
%    end
end

fclose(fid);    % close finish.cas

%=============================================

if strcmp(data_num.Affichage,'yes');fprintf(' fluent_launch : end Ok \n');end;
data_num.ellapsed_time   = etime(clock,t_start_solve);
data_num.cpu_time_solve  = data_num.cpu_time_solve + etime(clock,t_start_solve);
    
    
    


