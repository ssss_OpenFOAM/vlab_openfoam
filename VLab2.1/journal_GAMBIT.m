function [gambit_journal_file,mesh_file] = journal_GAMBIT (data_geo,data_mesh,data_topo,data_sim,data_num)
% Create GAMBIT journal
%
% input : extrados, intrados, extrados_int, intrados_int
% input : s_bda, s_extra, s_intra, y_cl, n_cl, sill_num, npts_edge_domain,
% input : m,p,t
%
% unity : airfoil of chord c in meters
%
% liste des surfaces :
% bda               : surface bda du profil
% extrados          : surface extrados du profil
% intrados          : surface intrados du profil
% sillage           : surface sillage  du profil
%
% bda_int           : surface bda du profil
% extrados_int      : surface extrados du profil
% intrados_int      : surface intrados du profil
% sillage_int_up    : surface sillage  du profil
% sillage_int_down  : surface sillage  du profil
%
% CL_bda_up         : CL bda_up
% CL_bda_down       : CL bda_down
% CL_bdf_up         : CL bdf_up
% CL_bdf_down       : CL bdf_down
% CL_sillage_up     : CL sillage_up
% CL_sillage_down   : CL sillage_down
%
%disp('journal_gambit : debut');
gambit_ver  = data_num.Gambit_ver;
if strcmp(data_num.Affichage,'yes') fprintf(' Journal_GAMBIT : GAMBIT %s \n',gambit_ver);end;
% affectations 
str_geo     = data_geo.method.str_geo;
%m   = data_geo.m;
%p   = data_geo.p;
%t   = data_geo.t;

mesh_algo               = data_mesh.mesh_algo;              % 

% both mesh type (non-dimensional)
npts_edge_domain        = data_mesh.npts_edge_domain;
npts_edge_domain_down   = data_mesh.npts_edge_domain_down;
N_Corde_Amont           = data_mesh.N_Corde_Amont;
N_Corde_Aval            = data_mesh.N_Corde_Aval;
N_Corde_Up              = data_mesh.N_Corde_ymax;
N_Corde_Down            = data_mesh.N_Corde_ymin;

% strutured mesh inside (prisms layer)
h1                      = data_mesh.h1;                     % first cell size on wall
n_BL                    = data_mesh.n_cl;                   % maximum number of cells in BL
g_BL                    = data_mesh.growthrate_inside;      % growthrate in BL

% strutured mesh inside (non-dimensional)
bda_size            = data_mesh.size_bda;               % cell size in chord percent
y_cl                = data_mesh.h1;                     % first cell height in chord percent
n_cl                = data_mesh.n_cl;
%bda_size            = c*data_mesh.size_bda;
%y_cl                = c*data_mesh.h1;                     % first cell height in m
%n_cl                = c*data_mesh.n_cl;
% unstructured mesh inside
growthrate_inside   = data_mesh.growthrate_inside;
growthrate_outside  = data_mesh.growthrate_outside;
size_limit          = data_mesh.size_limit;

i_bda               = data_topo.i_bda;
bda_start           = data_topo.bda_start;              % indice premier vertex extrados
bda_stop            = data_topo.bda_stop;               % indice dernier vertex extrados
extrados_start      = data_topo.extrados_start;         % indice premier vertex extrados
extrados_stop       = data_topo.extrados_stop;          % indice dernier vertex extrados
intrados_start      = data_topo.intrados_start;         % indice premier vertex intrados
intrados_stop       = data_topo.intrados_stop;          % indice avant dernier vertex intrados
extrados_int_start  = data_topo.extrados_int_start;     % indice premier vertex extrados_int
extrados_int_stop   = data_topo.extrados_int_stop;      % indice dernier vertex extrados_int
intrados_int_start  = data_topo.intrados_int_start;     % indice premier vertex intrados_int
intrados_int_stop   = data_topo.intrados_int_stop;      % indice dernier vertex intrados_int

bda_num             = data_topo.n_bda;                  % nombre de points de maillage surface bda
extra_num           = data_topo.n_ext;                  % nombre de points de maillage extrados
intra_num           = data_topo.n_int;                  % nombre de points de maillage intrados
sill_num            = data_topo.n_sill;                 % nombre de points de maillage sillage

% vertex lists of bda, extrados, intrados, bda_int, extrados_int, intrados_int
first       = intrados_start;
last        = intrados_start-i_bda+2;
ind         = [first:-1:last];
[bda1]      = sprintf('"vertex.%g" ',ind);          % string des 3 vertex bda intrados

first       = bda_start;
last        = bda_stop;
ind         = [first:last];
[bda2]      = sprintf('"vertex.%g" ',ind);          % string 4 vertex bda extrados

first       = intrados_int_start;
last        = intrados_int_start-i_bda+2;
ind         = [first:-1:last];
[bda_int1]  = sprintf('"vertex.%g" ',ind);          % string 3 vertex sup intrados

first       = extrados_int_start-i_bda+1;
last        = extrados_int_start;
ind         = [first:last];
[bda_int2]  = sprintf('"vertex.%g" ',ind);          % string 4 vertex sup extrados

first       = extrados_start;
last        = extrados_stop;
ind         = [first:last];
[extra]     = sprintf('"vertex.%g" ',ind);          % string vertex extrados

first       = extrados_int_start;
last        = extrados_int_stop;
ind         = [first:last];
[extra_int] = sprintf('"vertex.%g" ',ind);          % string vertex extrados_int

first       = intrados_start;
last        = intrados_stop;
ind         = [first:last extrados_stop];
[intra] = sprintf('"vertex.%g" ',ind);     % string vertex intrados (bdf added at the end!!!)

first       = intrados_int_start;
last        = intrados_int_stop;
ind         = [first:last];
[intra_int] = sprintf('"vertex.%g" ',ind);          % string vertex intrados_int

%gambit_journal_file = 'naca.jou';
%geo                 = ['naca',num2str(100*m),num2str(10*p),num2str(100*t)];
gambit_journal_file = [str_geo,'.jou'];
mesh_file           = [str_geo,'.msh'];

fid=fopen(gambit_journal_file,'w');

fprintf(fid,'import vertexdata "profile_2d.dat"\n\n');    %importation des points de construction

% airfoil nurbs
fprintf(fid,'edge create "bda"      nurbs %s%s interpolate\n',   bda1, bda2);   % nurbs bda
fprintf(fid,'edge create "extrados" nurbs %s   interpolate\n',   extra);        % nurbs extrados
fprintf(fid,'edge create "intrados" nurbs %s   interpolate\n\n', intra);        % nurbs intrados

% inside domain nurbs
fprintf(fid,'edge create "bda_int"      nurbs %s%sinterpolate\n', bda_int1, bda_int2);                                      % nurbs bda_int
fprintf(fid,'edge create "extrados_int" nurbs %sinterpolate\n', extra_int);                                                 % nurbs extrados_int
fprintf(fid,'edge create "intrados_int" nurbs %sinterpolate\n\n', intra_int);                                               % nurbs intrados_int

fprintf(fid,'edge create "CL_bda_up"   nurbs "vertex.%g" "vertex.%g" interpolate\n',   extrados_start, extrados_int_start);           % nurbs CL_bda_up
fprintf(fid,'edge create "CL_bdf_up"   nurbs "vertex.%g" "vertex.%g" interpolate\n',   extrados_stop , extrados_int_stop );           % nurbs CL_bdf_up
fprintf(fid,'edge create "CL_bda_down" nurbs "vertex.%g" "vertex.%g" interpolate\n',   intrados_start, intrados_int_start);           % nurbs CL_bda_down
fprintf(fid,'edge create "CL_bdf_down" nurbs "vertex.%g" "vertex.%g" interpolate\n\n', extrados_stop , intrados_int_stop );           % nurbs CL_bdf_down

fprintf(fid,'edge create "sillage" nurbs "vertex.%g" "vertex.%g" interpolate\n', extrados_stop, intrados_int_stop+1);                 % nurbs sillage
fprintf(fid,'edge create "sillage_int_up" nurbs "vertex.%g" "vertex.%g" interpolate\n', extrados_int_stop, intrados_int_stop+2);      % nurbs sillage_int_up
fprintf(fid,'edge create "sillage_int_down" nurbs "vertex.%g" "vertex.%g" interpolate\n', intrados_int_stop, intrados_int_stop+3);    % nurbs sillage_int_down
fprintf(fid,'edge create "CL_sillage_up" nurbs "vertex.%g" "vertex.%g" interpolate\n', intrados_int_stop+1, intrados_int_stop+2);     % nurbs CL_sillage_up
fprintf(fid,'edge create "CL_sillage_down" nurbs "vertex.%g" "vertex.%g" interpolate\n\n', intrados_int_stop+1, intrados_int_stop+3); % nurbs CL_sillage_down

% 4 edges of the outside domain : size = NxN airfoil chord

fprintf(fid,'vertex create "v_SW" coordinates %g %g 0\n'  , -N_Corde_Amont, -N_Corde_Down);                    % vertex SW domaine exterieur
fprintf(fid,'vertex create "v_NW" coordinates %g %g 0\n'  , -N_Corde_Amont,  N_Corde_Up  );                    % vertex NW domaine exterieur
fprintf(fid,'vertex create "v_SE" coordinates %g %g 0\n'  ,  N_Corde_Aval , -N_Corde_Down);                    % vextex SE domaine exterieur
fprintf(fid,'vertex create "v_NE" coordinates %g %g 0\n\n',  N_Corde_Aval ,  N_Corde_Up  );                    % vertex NE domaine exterieur
%fprintf(fid,'vertex create "v_SW" coordinates %g %g 0\n'  , -N_Corde_Amont*c, -N_Corde_Down*c);                    % vertex SW domaine exterieur
%fprintf(fid,'vertex create "v_NW" coordinates %g %g 0\n'  , -N_Corde_Amont*c,  N_Corde_Up  *c);                    % vertex NW domaine exterieur
%fprintf(fid,'vertex create "v_SE" coordinates %g %g 0\n'  ,  N_Corde_Aval *c, -N_Corde_Down*c);                    % vextex SE domaine exterieur
%fprintf(fid,'vertex create "v_NE" coordinates %g %g 0\n\n',  N_Corde_Aval *c,  N_Corde_Up  *c);                    % vertex NE domaine exterieur

fprintf(fid,'edge create "inlet"  straight "v_SW" "v_NW"\n');
fprintf(fid,'edge create "outlet" straight "v_SE" "v_NE"\n');
fprintf(fid,'edge create "down"   straight "v_SW" "v_SE"\n');
fprintf(fid,'edge create "up"     straight "v_NW" "v_NE"\n\n');

fprintf(fid,'edge create "e_NW" straight "vertex.%g" "v_NW"\n',   extrados_int_start);
fprintf(fid,'edge create "e_NE" straight "vertex.%g" "v_NE"\n',   intrados_int_stop+2);
fprintf(fid,'edge create "e_SE" straight "vertex.%g" "v_SE"\n',   intrados_int_stop+3);
fprintf(fid,'edge create "e_SW" straight "vertex.%g" "v_SW"\n\n', intrados_int_start);

% faces of the inside domain (structured zone)
fprintf(fid,'face create wireframe "bda" "CL_bda_up" "CL_bda_down" "bda_int" real\n');                    % face 1 = face bda
fprintf(fid,'face create wireframe "extrados" "CL_bda_up" "extrados_int" "CL_bdf_up" real\n');               % face 2 = face extrados
fprintf(fid,'face create wireframe "intrados" "CL_bda_down" "intrados_int" "CL_bdf_down" real\n');           % face 3 = face intrados
fprintf(fid,'face create wireframe "sillage" "CL_bdf_up" "sillage_int_up" "CL_sillage_up" real\n');             % face 4 = face sillage sup
fprintf(fid,'face create wireframe "sillage" "CL_bdf_down" "sillage_int_down" "CL_sillage_down" real\n\n');     % face 5 = face sillage inf

% faces of the outside domain (unstructured zone)
fprintf(fid,'face create wireframe "extrados_int" "sillage_int_up" "e_NE" "up" "e_NW" real\n');           % face 6 = uns extrados
fprintf(fid,'face create wireframe "e_NE" "outlet" "e_SE" "CL_sillage_down" "CL_sillage_up" real\n');     % face 7 = uns outlet
fprintf(fid,'face create wireframe "intrados_int" "sillage_int_down" "e_SE" "down" "e_SW" real\n');       % face 8 = uns intrados
fprintf(fid,'face create wireframe "e_NW" "bda_int" "e_SW" "inlet" real\n\n');                            % face 9 = uns inlet

fprintf(fid,'face merge "face.6" "face.7" "face.8" "face.9" mergelower\n\n');                       % merge 4 faces uns

% Inside zone : longitudinal mesh - airfoil + wake
%    remark : bda_size is the only mesh size given to mesh the profil

switch gambit_ver
    case '2.2.30'
        name = 'lastlength';
        outside_face_name = '"v_face.10"';  % struct, hybrid mesh
    otherwise
        name = 'firstlength';
        outside_face_name = '"face.6"';     % unstructured mesh
end

switch mesh_algo
    case {1,2}      % struct, hybrid
fprintf(fid,'edge mesh "bda"            successive ratio1 1 intervals %g\n', bda_num);
%fprintf(fid,'edge mesh "bda"            %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, bda_size, bda_num);
fprintf(fid,'edge mesh "extrados"       %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, 2*bda_size, extra_num);
fprintf(fid,'edge mesh "intrados"       %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, 2*bda_size, intra_num);
    case 0          % uns
fprintf(fid,'edge mesh "bda"            successive ratio1 1 intervals %g\n', bda_num);
%fprintf(fid,'edge mesh "bda"            %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, bda_size, bda_num);
%fprintf(fid,'edge mesh "extrados"       successive ratio1 1 intervals %g\n', extra_num);     % bda edge should diseappear to generate a uniform spacing % 
%fprintf(fid,'edge mesh "intrados"       successive ratio1 1 intervals %g\n', intra_num);     % bda edge should diseappear to generate a uniform spacing %
fprintf(fid,'edge mesh "extrados"       %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, 2*bda_size, extra_num);
fprintf(fid,'edge mesh "intrados"       %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, 2*bda_size, intra_num);
end

switch mesh_algo
    case {1,2}  % struct, hybrid
        % Inner box structured mesh
        %fprintf(fid,'edge mesh "bda"                successive ratio1 1 intervals %g\n', bda_num);    % repart2.m
        %fprintf(fid,'edge mesh "bda_int"      %s ratio1 %g ratio2 %g intervals %g\n', name, bda_size, bda_size, bda_num);
        fprintf(fid,'edge mesh "bda_int"      %s ratio1 %g ratio2 %g intervals %g\n', name, 2*bda_size, 2*bda_size, bda_num);
        fprintf(fid,'edge mesh "extrados_int" %s ratio1 %g ratio2 %g intervals %g\n', name, 1.3*bda_size, 2.2*bda_size, extra_num);
        fprintf(fid,'edge mesh "intrados_int" %s ratio1 %g ratio2 %g intervals %g\n', name, 1.3*bda_size, 2.2*bda_size, intra_num);
        fprintf(fid,'edge mesh "sillage"      %s ratio1 %g ratio2 %g intervals %g\n', name, 2*bda_size, 2*bda_size, sill_num);
        fprintf(fid,'edge mesh "sillage_int_up" "sillage_int_down" %s ratio1 %g ratio2 %g intervals %g\n\n', name, 2*bda_size, 2*bda_size, sill_num);
        % BL & wake mesh
        fprintf(fid,'edge mesh "CL_bda_up" "CL_bdf_up" "CL_bda_down" "CL_bdf_down"  firstlength ratio1 %g intervals %g\n\n', y_cl, n_cl);    % BL mesh (wall mesh size, mesh number)
        wake='uniform';
        switch wake
            case 'std'
        fprintf(fid,'edge mesh "CL_sillage_up" "CL_sillage_down"                    firstlength ratio1 %g intervals %g\n\n', y_cl, n_cl);    % BL mesh (wall mesh size, mesh number)
            case 'uniform'
        fprintf(fid,'edge mesh "CL_sillage_up" "CL_sillage_down"                    successive ratio1 1 intervals %g\n\n', n_cl);    % BL mesh (wall mesh size, mesh number)
        end
        
        % Outside unstructured mesh
        fprintf(fid,'default set "MESH.TRIMESH.MAX_FACES" numeric -1\n');
        fprintf(fid,'default set "MESH.TRIMESH.LSCALE_METHOD" numeric 3\n\n');
        fprintf(fid,'edge mesh "inlet" "outlet" successive ratio1 1 intervals %g\n\n', npts_edge_domain);
        fprintf(fid,'edge mesh "down" successive ratio1 1 intervals %g\n\n', npts_edge_domain_down);
        fprintf(fid,'edge mesh "up"   successive ratio1 1 intervals %g\n\n', npts_edge_domain);

    case 0  % unstructured mesh
        prism_layer = 'yes';
        switch prism_layer
            case 'yes'
                % Implement prism layer here as in jou_G_AC45.m
                h_BL = h1 * (1-g_BL.^n_BL)/(1-g_BL); % height of the BL mesh h=f(h1,g_BL,n)
                fprintf(fid,'blayer create first %g growth %g total %g rows %i transition 1 trows 0 wedge uniform \n', h1,g_BL,h_BL,n_BL);
                fprintf(fid,'blayer attach "b_layer.1" face "face.1" "face.2" "face.3" edge "bda" "extrados" "intrados" add \n');
            otherwise
        end
        % Inner box unstructured mesh
        fprintf(fid,'sfunction create "sf_int" sourceedges "bda" "extrados" "intrados" growthrate %g attachfaces "face.1" "face.2" "face.3" "face.4" "face.5" meshed\n',growthrate_inside);
        fprintf(fid,'sfunction bgrid attachfaces "face.1" "face.2" "face.3" "face.4" "face.5"\n');
        fprintf(fid,'face mesh "face.1" "face.2" "face.3" "face.4" "face.5" triangle\n\n');     % Inside  mesh
        % Outer box unstructured mesh
        fprintf(fid,'sfunction create "sf_ext" sourceedges "bda_int" "extrados_int" "sillage_int_up" "CL_sillage_up" "CL_sillage_down" "sillage_int_down" "intrados_int" growthrate %g sizelimit %g attachfaces %s meshed\n',growthrate_outside,size_limit,outside_face_name);
        fprintf(fid,'sfunction bgrid attachfaces %s\n',outside_face_name);
        fprintf(fid,'face mesh %s triangle\n\n',outside_face_name);                             % Outside mesh
end     % switch mesh_algo

%sizelimit_outside  = round(N_Corde_Amont+N_Corde_Aval)/npts_edge_domain;    % npts_edge_domain points on frontiers

switch mesh_algo
    case 0      % uns
        algorithm = 'triangle';
    case {1,2}  % struct or hybrid
        algorithm = 'map';
    otherwise
        algorithm = 'map';
end

switch mesh_algo
    case {1,2}  % struct or hybrid
fprintf(fid,'face mesh "face.1" "face.2" "face.3" "face.4" "face.5" %s\n\n',algorithm);     % Inside  mesh
fprintf(fid,'face mesh %s triangle\n\n',outside_face_name);                                 % Outside mesh
end

fprintf(fid,'physics create "live" ctype "FLUID" face "face.1" "face.2" "face.3" "face.4" "face.5" %s\n',outside_face_name);
fprintf(fid,'physics create "int"       btype "INTERIOR"        edge "sillage" "CL_bda_up" "CL_bdf_up" "CL_bda_down" "CL_bdf_down" "CL_sillage_up" "CL_sillage_down" "sillage_int_down" "sillage_int_up" "extrados_int" "intrados_int" "bda_int" \n');
fprintf(fid,'physics create "bda"       btype "WALL"            edge "bda"\n');
fprintf(fid,'physics create "extrados"  btype "WALL"            edge "extrados" \n');
fprintf(fid,'physics create "intrados"  btype "WALL"            edge "intrados"\n');
fprintf(fid,'physics create "inlet"     btype "VELOCITY_INLET"  edge "inlet"\n');
fprintf(fid,'physics create "up"        btype "VELOCITY_INLET"  edge "up"\n');
fprintf(fid,'physics create "down"      btype "VELOCITY_INLET"  edge "down"\n');
fprintf(fid,'physics create "outlet"    btype "PRESSURE_OUTLET" edge "outlet"\n\n');

if exist(mesh_file,'file')
    delete (mesh_file);
    fprintf ('    delete %s \n',mesh_file);
end

fprintf(fid,'export fluent5 "%s" nozval\n',mesh_file);                      % (Ok)
%fprintf(fid,'export fluent5 "naca%g%g%g.msh" nozval\n',100*m,10*p,100*t);  % (Ok)
fclose(fid);
if strcmp(data_num.Affichage,'yes') fprintf(' Journal_GAMBIT : end Ok \n');end;




