function [data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = affect1002 (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x)
% NACA 4 digits with XFOIL modeling
% ======================================================================
% Geometry XFOIL
% ======================================================================
data_geo.sequence.type                          = 'geo_1234_jouX_1234_1234_runX';   % XFOIL alone
%data_geo.sequence.type                          = 'geo_topo_jou2_runM_jouF_run2';   % XFOIL & RANS

data_sim.XTRu               = 0.0;              % (0 = forced transition at L.E., 1 = free transition)
data_sim.XTRl               = 0.0;              % (0 = forced transition at L.E., 1 = free transition)
data_sim.Ncrit              = 8.0;              % Turbulence level (Ncrit = -2.4 ln(Tu)-8.43)(Tu=1%, N=2.6)(Tu=0.1%, N=8)

% ======================================================================
% RANS
% ======================================================================
%============================
% Geometry definition
%============================
% defined in data??.m
data_geo.c                  = 1;                    % chord length (en m)
data_geo.method.type        = 'naca_4digits';       % defined in affect.m           used in airfoil_geometry.m
%============================
% GAMBIT Mesh
%============================
mesh ='M2';             % M2 for Validation test case
switch mesh
    case 'M0'           % 2kc - y+~660 - 35x10 - RANS 50s 0.66 0.1444 (not good)
data_mesh.h1            = 0.00300;          % mesh size on wall in m
data_mesh.n_cl          = 10;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.05;             % size of the structured zone (percent of chord)
data_mesh.size_bda      = data_geo.c/  50;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       = 50;               % iterations of the sliding window for Cl_mean
data_sim.reporting      = 1;                % reporting of lift & drag coeff.
data_sim.Stop_Nit       = 100;              % Iterations of STOP in RANS/URANS
    case 'M1'           % 14kc - y+~66 - 350x20 - RANS 4min 1.07 0.0158 (as good as XFOIL)
data_mesh.h1            = 0.00030;          % mesh size on wall in m
data_mesh.n_cl          = 20;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.05;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.reporting      = 1;                % reporting of lift & drag coeff.
data_sim.Stop_Nit       = 1500;             % Iterations of STOP in RANS/URANS
    case 'M2'           % 22kc - y+~33 - 350x40 - RANS 5min 1.07 0.0152 (nearly as good as XFOIL)
data_mesh.h1            = 0.00015;          % mesh size on wall in m
data_mesh.n_cl          = 40;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.reporting      =  1;                % reporting of lift & drag coeff.
data_sim.Stop_Nit       = 2000;             % Iterations of STOP in RANS/URANS
    case 'M3'           % 55kc - y+~22 - 690x60
data_mesh.h1            = 0.00010;          % mesh size on wall in m
data_mesh.n_cl          = 60;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/1000;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 3000;             % Iterations of STOP in RANS/URANS
    case 'M4'           % 164kc - y+~11 - 1379x100
data_mesh.h1            = 0.00005;          % mesh size on wall in m
data_mesh.n_cl          = 100;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/2000;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 6000;            % Iterations of STOP in RANS/URANS
    case 'M5'           % 76kc - y+~11
data_mesh.h1            = 0.00005;          % mesh size on wall in m
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M6'           % 76kc - y+~6
data_mesh.h1            = 0.00003;          % mesh size on wall in m
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M7'           % 76kc - y+~4
data_mesh.h1            = 0.00002;          % mesh size on wall in m
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/1000;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M8'           % 76kc - y+~11
data_mesh.h1            = 0.00005;          % mesh size on wall in m
data_mesh.n_cl          =  60;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M9'           % 76kc - y+~11
data_mesh.h1            = 0.00005;          % mesh size on wall in m
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.size_bda      = data_geo.c/ 500;  % mesh size at bda in chord fraction
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_mesh.npts_edge_domain_up       = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_down     = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_inlet    = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_outlet   = 20;                   % nombre de points edge domaine de calcul
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
end
data_mesh.y_wall    = data_mesh.h1;         % y+ evaluation
%============================
% Physical Modelization
%============================
data_sim.str_URANS          = 'RANS';           % choix RANS / URANS (utile uniquement si pas d'actionneur unsteady)
data_sim.regime             = 'turbulent';      % regime d'ecoulement (laminar, turbulent)
data_sim.Tu_model           = 'SA';             % Turbulence model
data_sim.I_turb             = 0.1;              % Turbulence Intensity (%) [1]
data_sim.L_turb             = 0.005;            % Turbulence Length Scale (m) [0.005]
%============================
% Numerical Modelization
%============================
data_sim.sim                = 1;                % simulation (0: non, 1: oui)
data_sim.graphic            = 1;                % Fluent graphic (0: non, 1: oui)
data_sim.scheme_order       = 'O2';             % ordre du scheme numerique spatial (O1, O2, O3)
data_sim.np_per             = 20;               % nombre de pas de temps par periode actionneur
data_sim.Stop_type          = 'Intelligent';    % Stop Type : {'Intelligent','Interactive','Definite'} 
data_sim.Stop_Err           = 0.0025;           % x% on lift coeff.
%============================
% Data numerique
%============================
data_num.parallel   = 1;
data_num.nproc      = 4;
data_num.Affichage          = 'no';           % Affichage des variables data_*
data_num.Rep_data           = 'all';          % Variables to print : {'geo','mesh','topo','sim','ac','all'} 

%==================================
% Comparison data NACA 0012 Re=3e6
%==================================
data_sim.comparison     = 'yes';
data_sim.data_type      = 'num';
data_sim.title          = 'TofWS Re=3e6'; % CFD_NACA0012.xls
data_sim.al_res         = [ 0       2       4       6       8       10      12    14    16    18  ];    % angle of attack
data_sim.Cl_res         = [ 0.0     0.24    0.45    0.65    0.88    1.10    1.28  1.45  1.53  1.00];    % lift coeff.
data_sim.Cd_res         = [ 0.0058  0.0063  0.0073  0.0084  0.0105  0.0134  0.00  0.00  0.00  0.00];    % drag coeff.
data_sim.Cm_res         = [ 0.00    0.00    0.00    0.00    0.00    0.00    0.00  0.00 -0.20 -0.00];    % moment coeff. at LE
%data_sim.title          = 'Ladson Re=6e6 TF'; % CFD_NACA0012.xls
%data_sim.al_res         = [0     2     4     6     8     10    12    14    16     18    ];    % angle of attack
%data_sim.Cl_res         = [0.0   0.22  0.43  0.65  0.89  1.07  1.26  1.44  1.57   1.00  ];    % lift coeff.
%data_sim.Cd_res         = [0.081 0.082 0.083 0.089 0.105 0.120 0.133 0.163 0.0222 0.1879];    % drag coeff.
% A validation done
% ToWS          10  3e6 1.10 0.0134
% XFOIL         10  3e6 1.10 0.0125
% RANS-SA       10  3e6 1.07 0.0152 M2 23kc Y+=O(10)
% RANS-SSTtr    10  3e6 1.?? 0.0??? M2 23kc Y+=O(10)
% ======================================================================
% Ne rien toucher sous cette ligne sans une bonne connaissance de VLab
% ======================================================================
% ===============================================
% Affectation du cas courant from var from data1
% ===============================================
% 4512 -> m=0.04 p=0.5 t=0.12
m   =   fix(  var.value(i_cas,1)/1000 );
p   =   fix( (var.value(i_cas,1)-1000*m)/100 );
t   =         var.value(i_cas,1)-1000*m-100*p;

data_geo.naca4digits.m      =  m/100;
data_geo.naca4digits.p      =  p/10;
data_geo.naca4digits.t      =  t/100;
data_sim.geo    = [str_var(1,3:6) , str_var(i_cas+1,2:6)];      % 
data_sim.Vinf   = var.value(i_cas,2);
data_sim.inc    = var.value(i_cas,3);

