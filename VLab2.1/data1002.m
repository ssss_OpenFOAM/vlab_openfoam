function [cas,var,str_var] = data1002 (x,data_sim)
%
% data for XFOIL
%param.airfoil_name    = 'NACA2412';   % Airfoil geometry
%param.Reynolds        = 5e6;          % Reynolds number
%param.Mach            = 0.0;          % Mach number
%param.Inc             = 5;            % Angle of attack

%cas = data_sim.cas;        % 2 for Validation test case
%cas = 'analysis';
cas = 1;

switch cas
    case 1      % Ok 28/10/12 Re=3e6
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    45' '  10.0' '     1' ];
    case 2      % Ok 28/10/12 Re=3e6
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    45' '     2' '     1' ;
                '  0012' '    45' '     4' '     1' ;
                '  0012' '    45' '     6' '     1' ;
                '  0012' '    45' '     8' '     1' ;
                '  0012' '    45' '    10' '     1' ;
                '  0012' '    45' '    12' '     1' ];
    case 3      % Xfoil convergence problem i=14
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    45' '     1' '     1' ;
                '  0012' '    45' '     2' '     1' ;
                '  0012' '    45' '     4' '     1' ;
                '  0012' '    45' '     6' '     1' ;
                '  0012' '    45' '     8' '     1' ;
                '  0012' '    45' '    10' '     1' ;
                '  0012' '    45' '    12' '     1' ;
                '  0012' '    45' '    14' '     1' ;
                '  0012' '    45' '    16' '     1' ;
                '  0012' '    45' '    18' '     1' ;
                '  0012' '    45' '    20' '     1' ];
    case 4
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    45' '     0' '     1' ;
                '  0012' '    45' '     2' '     1' ;
                '  0012' '    45' '     4' '     1' ;
                '  0012' '    45' '     6' '     1' ;
                '  0012' '    45' '     8' '     1' ;
                '  0012' '    45' '    10' '     1' ;
                '  0012' '    45' '    12' '     1' ];
    case 5
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    45' '    12' '     1' ;
                '  0012' '    45' '    14' '     1' ;
                '  0012' '    45' '    16' '     1' ;
                '  0012' '    45' '    18' '     1' ;
                '  0012' '    45' '    20' '     1' ];
end

            
var = str2var (str_var, x', 6);