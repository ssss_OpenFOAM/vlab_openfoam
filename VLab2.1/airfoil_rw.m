function [xu,yu,xl,yl] = airfoil_rw (string,filename)
%
% ==================================
% Read  airfoil geometry in Javafoil format
% Write airfoil in xu,yu,xl,yl
%
% input :   string      ('read', 'write') 
%           filename   
%
% output :
%
% xu(i), i=1,n  : abcisse  des points extrados du bda vers le bdf
% yu(i), i=1,n  : ordinate des points extrados du bda vers le bdf
% xl(i), i=1,n  : abcisse  des points intrados du bda vers le bdf
% yl(i), i=1,n  : ordinate des points intrados du bda vers le bdf
%
% bda en (0,0)
% bdf en (1,0)
% ==================================

% read airfoil in filename (format Javafoil : bdf, extrados, bda, intrados, bdf, default number of points = 61)
fid     = fopen    (filename,'r');
[x,y]   = textread (filename,'%f %f','headerlines',1);  % begin on line 2


switch mod(length(x),2)         % nombre de point pair / impair ?
    case 1              % impair
N       = length(x);    % 61
NS2     = (N-1)/2;      % 30
start_l = NS2+1;        % 31
end_l   = N;            % 61
end_u   = NS2+1;        % 31
start_u = 1;            % 1
    case 0              % pair
N       = length(x);    % 60
NS2     = (N-2)/2;      % 29
start_l = NS2+1;        % 30
end_l   = N;            % 60
end_u   = NS2+1;        % 30
start_u = 1;            % 1
end

%====================================
% lower surface from bda to bdf
%====================================
ind = 0;
for i=start_l:end_l     % from 31 to 61 = from bda to bdf on lower surface
    ind = ind + 1;
    xl   (ind) = x (i);
    yl   (ind) = y (i);
end
%====================================
% upper surface from bdf to bda
%====================================
ind = 0;
for i=start_u:end_u     % from  1 to 31 = from bdf to bda on upper surface
    ind = ind + 1;
    xu   (ind) = x (end_u-i+1);
    yu   (ind) = y (end_u-i+1);  
end
fclose(fid);

%====================================
% fichier profil (format ASCII)
% hyp : bda en (0,0) et bdf en (1,0)
%====================================
geo_file    = 'geo.dat';
fid         = fopen(geo_file,'w');

for i=1:length(xu)-1
    fprintf(fid,'%f %f %f \n', xu(i), yu(i), 0);   % upper surface from bda to bdf-1    (0 < xu < 0.99?)
end
for i=2:length(xl)-1
    fprintf(fid,'%f %f %f \n', xl(i), yl(i), 0);   % lower surface from bda+1 to bdf-1  (0.0? < xl < 0.99?)
end
fclose(fid);

%fprintf(' airfoil_read : file %s created \n',geo_file);

