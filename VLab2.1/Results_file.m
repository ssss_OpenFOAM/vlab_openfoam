function Results_file (x,data_geo,data_mesh,data_sim)
% Create VLab_Cases.out file
% ===========================
% Affichage complet
% ===========================
% VLab2 : Optimization mode
%
% O1P3 : Min(lift coeff) = f(p1, p2, ..., pn)
%
% Algorithme : cmaes
% Function   : VLab2
%
% Flow model : RANS turbulent
%
% Mesh 200x30 = 6000 nodes, y+=1 (hyp: pp turbulent)
% ===========================
%var.n_cas
%var.name
%var.value

fid_1 = fopen('VLab_Cases.out','w');
fprintf(fid_1,' VLab2 : Results_file.m \n\n');

if size(x,1)==0
    fprintf(fid_1,' VLab2 : Analysis mode \n');
else
    fprintf(fid_1,' VLab2 : Optimization mode \n');
    fprintf(fid_1,' O1P%i : %s = f(p1, p2, ...) \n',size(x,1),data_sim.opti.objective);
    fprintf(fid_1,' Algorithme : cmaes \n');
    fprintf(fid_1,' Function   : VLab2 \n\n');
end
fprintf(fid_1,' Flow model : %s %s %s %s \n',data_sim.str_URANS, data_sim.regime, data_sim.Tu_model, data_sim.Fluid_model);
fprintf(fid_1,' Geometry   : %s %s \n',data_sim.geo, data_geo.method.type);
fprintf(fid_1,' Mesh       : %ix%i = %i, y+=%g\n\n',data_mesh.n_ext,data_mesh.n_cl,data_mesh.n_ext*data_mesh.n_cl,data_mesh.yplus);
fclose (fid_1);
