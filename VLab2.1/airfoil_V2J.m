function airfoil_V2J (filename,xu,yu,xl,yl)
%
% ==================================
% Read  airfoil geometry in VLab     format xu,yu,xl,yl
% Write airfoil geometry in Javafoil format x,y
%
% input :   filename 
%
% ==================================

fid         = fopen(filename,'w');

fprintf(fid,' airfoil in VLab format writed in Javafoil format \n');
for i=length(xu) : -1 : 1
    fprintf(fid,'%f %f \n', xu(i), yu(i));   % upper surface from bda to bdf-1    (0 < xu < 0.99?)
end
for i=2 : length(xl)
    fprintf(fid,'%f %f \n', xl(i), yl(i));   % lower surface from bda+1 to bdf-1  (0.0? < xl < 0.99?)
end
fclose(fid);

%fprintf(' airfoil_V2J : file %s created \n',filename);

