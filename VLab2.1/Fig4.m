%
% function [] = Fig4(n_conf,data_geo,data_sim,data_num,xscX1,CpX1,xl,yl,xu,yu,x,Cr,Ch,n_cut)
%==========================================
% Fig4 Cp & Cf = f(x/c)
%
% Optimize
%  n_conf = 6,7,71,144,145,1001,1003    airfoil     Cp(x)      + geometry(x)
%  n_conf = 203,204,205,206,207         n-vertex    design box + geometry(x)
%  n_conf = 401...413                   sail_3D     Ch = f(Cr)
%
% input :
%           n_conf, data_geo, data_sim, data_num, xscX1, CpX1, xl, yl, xu,
%           yu,x, Cr, Ch, n_cut
% local :
%           X,Y,Cp,Cf
%==========================================
figure(4); % all n_conf
%==========================================

% Fluent_jend.m  (only Linux, bug on Windows) : 1D Scalar ascii file (x,y,Cp,Cf) created by import_Fluent_scalar_1D.m
filename = strcat(data_sim.str_file,'_field.txt'); % exist if (analysis mode, data_sim.grapic = 1, data_num.Affichage = 'yes')
if exist(filename,'file')==2;
    [X,Y,Cp,Cf] = import_Fluent_scalar_1D (filename);
    subplot(2,1,1);plot(X,-Cp,'.b');hold on;legend('RANS');title('-Cp=f(x/c)');
    subplot(2,1,2);plot(X,Cf,'.b');hold on;legend('RANS');title('Cf=f(x/c)');
end;
% XFOIL Cp plot from (xl,yl,xu,yu,xscX1,CpX1)
if strcmp(data_geo.sequence.type(25:28),'runX') || strcmp(data_geo.sequence.type(25:28),'run2') % XFOIL alone or XFOIL + RANS
    subplot(2,1,1);
    plot(xscX1,-CpX1,'r');hold on; %
    plot(xl,-2+5*yl,xu,-2+5*yu); hold on;axis([0 1 -4 5]);
end
 
zzz=0;
if zzz==1
%==========================================
figure(7);  % n_conf specific
%==========================================

switch data_num.visu
    case 'analysis'     % geometry & Cp (xl,yl,xu,yu,xscX1,CpX1)
        switch n_conf
            case {6,7,13,38,39,45,49,71,144,145,1003} % airfoil conf.
                if strcmp(data_geo.sequence.type(25:28),'runX') || strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2') % XFOIL or RANS or RANS + XFOIL
                plot(xl,-2+5*yl,xu,-2+5*yu); hold on;axis([0 2 -4 1]); % plot airfoil only if created bu airfoil_geometry (not for NURBS A. Merle)
                plot(xscX1,-CpX1,'r');hold on; legend('Xfoil');title('-Cp=f(x/c)')
                end
            case {203,204,205,206,207} % n_vertex dragonfly conf.
                x1 = data_geo.dragonfly.geo(:,1);
                y1 = data_geo.dragonfly.geo(:,2);
                plot(x1,y1); hold on;axis([0 1 -4 1]);
            case {46} % AC45 wingsail conf.
        end

    case 'opti'
        switch n_conf
            case {6,7,13,46,71,144,145,1003}         % airfoil conf. optim
                img='airfoil'; % ?
                switch data_geo.sequence.type(25:28) % RANS alone or XFOIL alone
                    case 'runF'
                        plot(xl,yl,xu,yu); axis equal;  % geometry alone because Cp=f(x/c) not implemented for FLUENT
                    case {'runX'}
                        plot(xl,-2+5*yl,xu,-2+5*yu); hold on;axis([0 1 -4 4]); % plot airfoil only if created bu airfoil_geometry (not for NURBS A. Merle)
                        s_title=strcat(data_sim.cas,' ',data_sim.opti.objective,' ',data_sim.geo);title(s_title);
                        plot(xscX1,-CpX1,'r');hold off; %title('-Cp=f(x/c)')
                end
            case {45,46}               % AC45    optim
                img='AC45';
            case {203,204,205,206,207}      % dragonfly optim
                img='dragonfly';
                switch data_sim.cas
                    case 'O1P2'
                        y_plot = [0 ; 0.06 ; x ; 0.06 ; 0];
                    case 'O1P4'
                        y_plot = [0 ; x ; 0];
                    case 'O1P5'
                        y_plot = [0 ; x];
                    case 'O1P5i'
                        y_plot = [0 ; x(1:4) ; 0];  % only geometry
                    case 'O1P6'
                        y_plot = x;
                    case 'O1P6i'
                        y_plot = [0 ; x(1:5) ; 0];  % only geometry
                   otherwise
                        y_plot = x;
                end
                x_plot = data_geo.dragonfly.geo(:,1);
                plot([0 0 1 1 0],[-0.1 0.1 0.1 -0.1 -0.1],x_plot,y_plot,'-sr');axis equal;  % design box + geo (x,y)
                s_title=strcat(data_sim.cas,' ',data_sim.opti.objective);title(s_title);
            case {401, 402, 403, 404, 405, 406, 407, 410, 411, 412, 413}    % sail 3D optim
                img='sail_3D';
                plot(Cr,Ch,'-+b');axis equal;hold on;
                title('Driving & Heeling coeff. Ch = f(Cr)');
            otherwise
                img='';
                disp(' VLab2 : no image implemented for this optimization problem');
        end
        switch img
            case 'airfoil'      % geometry + Cp=f(x/c)  (best geometry should be better)
            case 'dragonfly'    % geometry              (best geometry should be better)
            case 'sail_3D'      % Ch=f(Cr)
            case 'sail_3D_1'    % x, Fobj
                x0 = 1:1:n_cut*2;                       % 2 param for each cut
                plot(x0,x,'-sr');axis equal;            % plot param vector x
                str=num2str(Fobj);text(0.5,-0.2,str);   % write Fobj value
            case 'sail_3D_2'    % grid view
                image_file = strcat(data_sim.str_file,'_grid_z1.tif');  % grid view
                [X_img,map] = imread(image_file);                       
                image(0,0,X_img);axis off;               % plot grid view
            otherwise
        end
end
end