function logo (x)
%
% ===========
% logo VLab
% ===========
% delete all figures (without conditions)
set(0,'ShowHiddenHandles','on')     % rendre visible toutes les figures
delete(get(0,'Children'))           % supprimer tous les enfants de Root (toutes les figures)

set(0,'Units','pixels')             % Ensure root units are pixels and get the size of the screen
scnsize = get(0,'ScreenSize');

% lire fichier image 628x552
[X_img,map] = imread('Logo.tif',1);
x_size      = size(X_img,2);y_size      = size(X_img,1); % image size
%x_size      = 950;         y_size      = 565;          % not the right method for beautiful field

% afficher image
left    = 10;
bottom  = y_size-310;
width   = x_size;        % x_size of image
height  = y_size;        % y_size of image
figure('Position',[left bottom width height],'Name','Virtual Laboratory','NumberTitle','off');
image(X_img); set(gca,'Position',[0 0 1 1])             % gca : graphic current axes
% incrustation texte
x_text = 0.5*size(X_img,2); y_text = 25;
text(x_text,y_text,'\itVLab 2.0^{\copyright}','FontName','Broadway','FontSize',36,'FontWeight','bold',...
    'HorizontalAlignment','center','Margin',4,'Color',[0.3 0. 1.],'BackgroundColor',[1. 1. 1.]);
x_text = 300; y_text = 60;
text(x_text,y_text,'\itAnalysis Design & Optimization of Navier-Stokes Flows','FontName','Arial','FontSize',8,...
    'FontWeight','bold','HorizontalAlignment','center','Margin',4,'Color',[0.3 0.3 0.3],'BackgroundColor',[1. 1. 1.]);

% ================
% Figures VLab
% ================ 
left    = x_size+6;
bottom  = y_size+6;
left    = scnsize(1);
bottom  = scnsize(2)+400;
width   = 560;
height  = 420;
% figure 2
figure('Position',[left bottom width height],'Name','Simulation Convergence');
% figure 3
figure('Position',[left+25 bottom-25 width height],'Name','Simulation Results');

% ===========================
% Figures VLab if optim mode
% =========================== 
if size(x,1) == 0
        % analysis mode
else    % optimization mode
% figure 4
figure('Position',[left+25*2 bottom-25*2 width height],'Name','Visualization Results');
% figure 5
figure('Position',[left+25*3 bottom-25*3 width height],'Name','Optimization Results');
% figure 6
%figure('Position',[left+25*4 bottom-25*4 width height],'Name','Optimization Results');
end

