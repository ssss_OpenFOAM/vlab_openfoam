function  bc_pressure_far_field (param)
%
% boundary condition type : velocity_inlet

% param     : parameters of the function
% call      : fluent_jou_comp.m

fluent_ver      = param.Fluent_ver;     % 6 for 6.2.16, 6.3.26, ..., 1 for 13.0.0, 14.0.0, 14.5.0, ...
regime          = param.regime;
dim             = param.dim;
fid             = param.fid;
zone_name       = param.surf_bc_name;
Vinf            = param.Vinf;           % Velocity magnitude
Vx              = param.Vx;             % X-Component of Flow Direction [0.98952579]
Vy              = param.Vy;             % Y-Component of Flow Direction [0.1443562]
Vz              = param.Vz;             % Z-Component of Flow Direction [1]
I_turb          = param.I_turb;         % Turbulence Intensity (%) [1]
L_turb          = param.L_turb;         % Turbulence Length Scale (m) [0.005]
Fluid_model     = param.Fluid_model;    % 'incompressible', 'compressible'

P_prof          = 'no';                 % Use Profile for Gauge Pressure? [no]   % fluent v13
P_val           = 0.0;                  % Gauge pressure;
T_prof          = 'no';                 % Use Profile for Temperature? [no]   % fluent v13
T_val           = 300.;                 % Temperature value
a_inf           = sqrt(1.4*287*T_val);  % Sound velocity
M_prof          = 'no';                 % Use Profile for Mach number? [no]   % fluent v13
M_val           = Vinf/a_inf;           % Mach number;
X_prof          = 'no';                 % Use Profile for X-Component of Flow Direction? [no]
Y_prof          = 'no';                 % Use Profile for Y-Component of Flow Direction? [no]
Z_prof          = 'no';                 % Use Profile for Z-Component of Flow Direction? [no]
TSM1            = 'no';                 % Turbulence Specification Method: Modified Turbulent Viscosity [no]
TSM2            = 'yes';                % Turbulence Specification Method: Intensity and Length Scale [yes]

fprintf(fid,'/define/boundary-conditions/pressure-far-field\n');

switch dim
    case {'2D'}
        switch regime
            case 'laminar'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g \n',      zone_name,P_prof,P_val,M_prof,M_val,X_profile,Vx,Y_profile,Vy);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g \n',zone_name,P_prof,P_val,M_prof,M_val,X_profile,Vx,Y_profile,Vy);
                otherwise
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g \n',zone_name,P_prof,P_val,M_prof,M_val,X_profile,Vx,Y_profile,Vy);
                end
            case 'turbulent'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                        switch Fluid_model
                            case 'incompressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %s %g %g\n',      zone_name,P_prof,P_val,M_prof,M_val,             X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                            case 'compressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %g %s %s %g %g\n',zone_name,P_prof,P_val,M_prof,M_val,T_prof,T_val,X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                        end
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                        switch Fluid_model
                            case 'incompressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %s %g %g\n',      zone_name,P_prof,P_val,M_prof,M_val,X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                            case 'compressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %g %s %s %g %g\n',zone_name,P_prof,P_val,M_prof,M_val,T_prof,T_val,X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                        end
                    otherwise
                        switch Fluid_model
                            case 'incompressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %s %g %g\n',      zone_name,P_prof,P_val,M_prof,M_val,X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                            case 'compressible'
fprintf(fid,'%s %s %g %s %g %s %g %s %g %s %g %s %s %g %g\n',zone_name,P_prof,P_val,M_prof,M_val,T_prof,T_val,X_prof,Vx,Y_prof,Vy,TSM1,TSM2,I_turb,L_turb);
                        end
                end
        end
    case {'3D'}
        switch regime
            case 'laminar'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                fprintf(fid,'%s %s %s %s %g %s %s %g %s %g %s %g \n',   zone_name, VSM, RF, VM_profile, Vinf, CSC, X_profile, Vx, Y_profile, Vy, Z_profile, Vz);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g %s %g \n',zone_name,VSM,RF,VM_profile,Vinf,P_profile,P_gauge,X_profile,Vx,Y_profile,Vy,Z_profile,Vz);
                end
            case 'turbulent'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                fprintf(fid,'%s %s %s %s %g %s %s %g %s %g %s %g %s %s %g %g\n',zone_name,VSM,RF,VM_profile,Vinf,CSC,X_profile,Vx,Y_profile,Vy,Z_profile,Vz,TSM1,TSM2,I_turb,L_turb);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g %s %g %s %s %g %g\n',zone_name,VSM,RF,VM_profile,Vinf,P_profile,P_gauge,X_profile,Vx,Y_profile,Vy,Z_profile,Vz,TSM1,TSM2,I_turb,L_turb);
                    otherwise
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g %s %g %s %s %g %g\n',zone_name,VSM,RF,VM_profile,Vinf,P_profile,P_gauge,X_profile,Vx,Y_profile,Vy,Z_profile,Vz,TSM1,TSM2,I_turb,L_turb);
                end
        end
end


