function [fluent_journal_file] = fluent_j2 (mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,n_conf,data_num)
%
% Lancement iterations sous FLUENT
%
% Affectation des variables locales
% Lecture des fichiers *.cas & *.dat
% Iterations RANS/URANS
% Mean velocity magnitude on wall surface
% Ecriture des fichiers *.cas et *.dat

Fluent_ver  = data_num.Fluent_ver;

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_j2 : FLUENT %s, ',Fluent_ver);end;

c = data_geo.c;                % chord in meter

FSI         = data_sim.FSI;

Vinf        = data_sim.Vinf;
inc         = data_sim.inc;
scheme_order= data_sim.scheme_order;
iter        = data_sim.iter;
iter_sup    = data_sim.iter_sup;
str_URANS   = data_sim.str_URANS;
str_file    = data_sim.str_file;
np_per      = data_sim.np_per;

force_surface   = data_topo.force_surface;
surf_bc         = data_topo.surf_bc;

theta   = data_ac.Theta_deg;
pos     = data_ac.pos;
dia     = data_ac.dia;
Freq    = data_ac.Freq;
Vjfluct = data_ac.Vjfluct;
Vjmean  = data_ac.Vjmean;

%======================================
% Lecture des fichiers *.cas & *.dat
%======================================
fluent_journal_file = 'fluent_j2.jou';
fid=fopen(fluent_journal_file,'w+');

%if exist('fluent_j2.trn'     ,'file');delete 'fluent_j2.trn';end
%filename = 'fluent_j2.trn';if exist(filename,'file')
%    delete (filename);
%end
%fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ? - not useful

fprintf(fid,'rc %s.cas\n', str_file); % Lecture du fichier *.cas
fprintf(fid,'rd %s.dat\n', str_file); % Lecture du fichier *.dat
%fprintf(fid,'rc %s.cas\n', 'fluent_journal'); % Lecture du fichier *.cas
%fprintf(fid,'rd %s.dat\n', 'fluent_journal'); % Lecture du fichier *.dat

%============================================
% Change actuator bc for closed-loop control
%============================================
if strcmp(data_ac.closed_loop,'yes')
% bc jet = velocity-inlet
switch data_ac.ac
    case'no'
    case'yes'
        switch data_ac.type
            case 'continuous jet'
                jet_continuous (fid,data_ac)            % velocity-inlet standard
            case 'synthetic jet'
                [udf_name] = jet_synthetic (data_ac);   % create udf file - language C
                udf_build(os,fid,udf_name)              % build & load udf file
                velocity_inlet (fid,udf_name,data_ac)   % velocity-inlet = synthetic_jet with udf

            case 'pulsed jet'
                [udf_name] = jet_pulsed (data_ac);      % bc jet : create udf - language C
                udf_build(os,fid,udf_name)              % build & load udf file
                velocity_inlet (fid,udf_name,data_ac)   % velocity-inlet = synthetic_jet with udf
        end
end
end
%======================================
% Iterations RANS/URANS
%======================================
switch data_ac.ac
    case {'no'}
        switch str_URANS
            case 'RANS'
                fprintf(fid,'/solve/iter %g\n\n',iter_sup);    % iterations steady
            case {'URANS','DES','LES','DNS'}
                time_step   =   c / Vinf / np_per;                                  % time step en secondes
                fprintf(fid,'/define/models/unsteady-2nd-order yes\n');             % choix du solveur : O2 unsteady
                fprintf(fid,'/solve/set/time-step %g\n',time_step);                 % choix du pas de temps
                data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',iter_sup,20);    % iterations unsteady
        end
    case 'yes'
        switch data_ac.type
            case 'continuous jet'
                switch str_URANS
                    case 'RANS'
                        fprintf(fid,'/solve/iter %g \n\n',iter_sup);                        % iterations steady
                    case {'URANS','DES','LES','DNS'}
                        time_step   =   c / Vinf / np_per;                                  % time step en secondes
                        fprintf(fid,'/define/models/unsteady-2nd-order yes\n');             % choix du solveur : O2 unsteady
                        fprintf(fid,'/solve/set/time-step %g\n',time_step);                 % choix du pas de temps
                        data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                        fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',iter_sup,20);    % iterations unsteady
                end
            case {'synthetic jet','pulsed jet'}
                time_step_1  = c / Vinf / np_per;                                   % time step en secondes - physic
                time_step_2  = 1 / Freq / np_per;                                   % time step en secondes - actuator
                time_step    = min  (time_step_1 , time_step_2);                    % time step       based on the smallest time step 
                ratio        = max  (time_step_1 , time_step_2) / time_step;        % simulation time based on the largest  time step
                ratio        = round (ratio);
                fprintf(fid,'/define/models/unsteady-2nd-order yes \n');            % choix du solveur : O2 unsteady
                fprintf(fid,'/solve/set/time-step %g \n',time_step);                % choix du pas de temps
                data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',iter_sup*ratio,20);    % iterations unsteady
        end
end

%======================================
% Ecriture des fichiers *.cas et *.dat
%======================================
%data_sim.save.cas.j2 = '1';
%if (data_sim.save.cas.j2 == 1)
    fprintf(fid,'wc %s.cas\n', str_file); % Ecriture du fichier *.cas
    fprintf(fid,'yes\n');
    fprintf(fid,'wd %s.dat\n', str_file); % Ecriture du fichier *.dat
    fprintf(fid,'yes\n');
%end

%===========================================
% surface pressure files for FSI -> file.txt 
%===========================================
if FSI==1
    if findstr(force_surface,'foc')
filename        = strcat('nbxyzp_foc_extrados.txt');
if exist(filename,'file');delete (filename);end
filename        = strcat('"nbxyzp_foc_extrados.txt"');
force_surface   = 'foc';
scalar          = 'pressure';            % x, y, z, p
fprintf(fid,'/file/export/ascii %s %s, no yes %s, \n',filename,force_surface,scalar);   % Z! version Fluent 6.2.16
%fprintf(fid,'/file/export/ascii %s %s, yes %s, no \n',filename,force_surface,scalar);   % Z! version Fluent 6.3.26
fprintf(fid,'yes \n\n');
filename        = strcat('nbxyzp_foc_intrados.txt');
if exist(filename,'file');delete (filename);end
filename        = strcat('"nbxyzp_foc_intrados.txt"');
force_surface   = 'foc-shadow';
scalar          = 'pressure';            % x, y, z, p
fprintf(fid,'/file/export/ascii %s %s, no yes %s, \n',filename,force_surface,scalar);   % Z! version Fluent 6.2.16
%fprintf(fid,'/file/export/ascii %s %s, yes %s, no \n',filename,force_surface,scalar);   % Z! version Fluent 6.3.26
fprintf(fid,'yes \n\n');
    end
end     % end FSI==1


%===========================================
%
% Partie graphique : Not useable if "fluent -g" is used
%
%===========================================
if (data_sim.graphic == 1)
%===========================================
% Grid 3D, Contour, ... -> file.tif
%===========================================
if strcmp(data_num.Affichage,'yes') && strcmp(data_sim.dim,'3D')
    visu = 'forme en plan';
    switch visu
        case 'perspective'
            right_angle = 30;
            up_angle    =-45;
            x_field_size=30;
            y_field_size=30;
            fprintf(fid,'/display/grid-outline \n');
            fprintf(fid,'/display/view/camera/orbit-camera %g %g \n',right_angle,up_angle);       % position camera
            fprintf(fid,'/display/view/auto-scale \n');
            fprintf(fid,'/display/view/camera/field %g %g \n',x_field_size,y_field_size);
        case 'forme en plan'
            fprintf(fid,'/display/grid-outline \n');
            fprintf(fid,'/display/view/camera/position %g %g %g \n',0,50,0);       % position camera
            fprintf(fid,'/display/view/camera/zoom-camera %g \n',9);
            fprintf(fid,'/display/view/camera/dolly-camera %g %g %g \n',0,-5,0);
    end
    filename = strcat(str_file,'_grid_z1.tif');
    if exist(filename,'file'); delete (filename);end
    filename = strcat('"',str_file,'_grid_z1.tif"');
    fprintf(fid,'/display/set/hardcopy/x-resolution 560 \n');
    fprintf(fid,'/display/set/hardcopy/y-resolution 420 \n');
    fprintf(fid,'/display/set/hardcopy/driver/tiff \n'); % file format choice
    fprintf(fid,'/display/hardcopy %s \n\n',filename);
end
%===========================================
% Grid 2D, Contour, ... -> file.tif
%===========================================
if strcmp(data_num.Affichage,'yes') && strcmp(data_sim.dim,'2D')
camera_target_x     = c/2;
camera_target_y     = 0;
camera_target_z     = 0;
width               = c;
height              = c;
fprintf(fid,'/display/grid \n');
fprintf(fid,'/display/view/camera/target %g %g %g \n',camera_target_x,camera_target_y,camera_target_z);       % position camera
fprintf(fid,'/display/view/camera/field  %g %g \n',width,height);                                             % zoom camera
%================================================
% grid file *.tif
%================================================
filename = strcat(str_file,'_grid_z1.tif');
if exist(filename,'file');delete (filename);end
filename = strcat('"',str_file,'_grid_z1.tif"');
fprintf(fid,'/display/set/hardcopy/driver/tiff \n'); % file format choice
fprintf(fid,'/display/hardcopy %s \n\n',filename);
%================================================
% pressure file *.tif
%================================================
filename = strcat(str_file,'_pressure.tif');
if exist(filename,'file');delete (filename);end
filename = strcat('"',str_file,'_pressure.tif"');
fprintf(fid,'/display/contour/pressure ,, \n');
fprintf(fid,'/display/set/hardcopy/color/color \n'); % color display
fprintf(fid,'/display/set/hardcopy/driver/tiff \n'); % file format choice
fprintf(fid,'/display/hardcopy %s \n',filename);
fprintf(fid,'yes \n\n'); % Ok to overwrite
end % if (data_sim.anim.film == 1)

end % if data_sim.graphic == 1

%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete ('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');

%if exist('finish.cas','file'); delete ('finish.cas');end
%fprintf(fid,'wc finish.cas yes\n');         % write file finish.cas

fprintf(fid,'exit yes \n');
fclose(fid);

if strcmp(data_num.Affichage,'yes') fprintf(' end Ok \n');end;

