function [fluent_journal_file] = fluent_jend (data_geo,data_topo,data_sim,data_num)
%
% Post-processing FLUENT
%
% Lecture des fichiers *.cas & *.dat                Read FLUENT Files
% Interpolation file for initialization             fluent-interpolation.ip
% Aerodynamic loads                                 VLab_Fx.txt, VLab_Fy.txt, ... 
% Graphic 2D/3D
%   tiff : Grid, contours                           str_file_grid_z1.tif
%   tiff : Pressure file                            str_file_pressure.tif
%   tiff : Vmag     file                            str_file_Vmag.tif
%   tiff : Friction lines on selected surfaces      ?
%   tiff : Mean velocity magnitude on wall surface  ?
%   txt  : 1D Scalar ascii file (x,y,Cp,Cf)         str_file_field.txt      import_Fluent_scalar_1D.m
% Report summary in file_Model.txt                  str_file_Model.txt
%
Fluent_ver  = data_num.Fluent_ver;
dim         = data_sim.dim;             % dimension  (2D, 3D)

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jend : FLUENT %s \n',Fluent_ver); end;

c           = data_geo.c;                % chord in meter
inc         = data_sim.inc;
str_file    = data_sim.str_file;

force_surface   = data_topo.force_surface;

%======================================
% Read files *.cas & *.dat
%======================================
fluent_journal_file = 'fluent_jend.jou';
fid=fopen(fluent_journal_file,'w+');

filename = 'fluent_jend.trn';if exist(filename,'file');delete (filename);end
fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ?

fprintf(fid,'rc %s.cas\n', str_file); % Lecture du fichier *.cas
fprintf(fid,'rd %s.dat\n', str_file); % Lecture du fichier *.dat

%===================================================================
% Write interpolation file for initialization of another simulation
%===================================================================
% /file/interpolate/write-data filename yes
if strcmp(data_sim.interpolation,'yes')
    filename = 'fluent-interpolation.ip';if exist(filename,'file');delete (filename);end
    fprintf(fid,'/file/interpolate/write-data %s yes \n',filename);        % write interpolation file
    fprintf(' fluent_jend : write interpolation file %s',filename);
end
%===============================================
% Write forces -> VLab_Fx.txt, VLab_Fy.txt, ...
%===============================================
param.Fluent_ver    = Fluent_ver;
param.fid           = fid;
param.dim           = dim;
param.inc           = inc;
write_forces (param)

%=====================================================
% str_file_field.txt : scalar(x,y,f) on wall surfaces 
%=====================================================
write_ascii(Fluent_ver,dim,fid,str_file,force_surface);     % /file/export/ascii
filename  = strcat(str_file,'_field.txt');if exist(filename,'file');delete(filename);end % warning permission denied
filename  = strcat(str_file,'_BL.txt'   );if exist(filename,'file');delete(filename);end
%===============================================
% str_file_Model.txt : FLUENT Model description
%===============================================
filename  = strcat(str_file,'_Model.txt');if exist(filename,'file');delete(filename);end
fprintf(fid,'/report/summary yes %s \n',filename);    % /report/summary
fprintf(' fluent_jend : file %s writed \n',filename);
%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');
fprintf(fid,'exit yes \n');
fclose(fid);

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jend : end Ok \n');end;

