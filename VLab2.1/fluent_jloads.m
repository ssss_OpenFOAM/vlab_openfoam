function [fluent_journal_file] = fluent_jloads (data_topo,data_sim,data_num)
%
% Lancement iterations sous FLUENT
%
% Affect local variables
% Lecture des fichiers *.cas & *.dat
% Extract wall forces
% Write   wall forces in VLab_loads.txt
%
fluent_ver      = data_num.Fluent_ver;
if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jloads : FLUENT %s, ',fluent_ver);end;
inc             = data_sim.inc;
str_file        = data_sim.str_file;

%======================================
% Lecture des fichiers *.cas & *.dat
%======================================
fluent_journal_file = 'fluent_jloads.jou';
fid=fopen(fluent_journal_file,'w+');

if exist('VLab_Lift.txt'    ,'file');delete 'VLab_Lift.txt'    ;end
if exist('VLab_Drag.txt'    ,'file');delete 'VLab_Drag.txt'    ;end
if exist('fluent_jloads.trn','file');delete 'fluent_jloads.trn';end
fprintf(fid,'/file/start-transcript %s \n','fluent_jloads.trn');

fprintf(fid,'rc %s.cas\n', str_file); % Lecture du fichier *.cas
fprintf(fid,'rd %s.dat\n', str_file); % Lecture du fichier *.dat

all_wall_zone   = 'yes';
in_file         = 'yes';

%======================================
% Loads in file VLab_Drag.txt, VLab-Lift.txt to be validated
%======================================
% Lift vector
file            = 'VLab_Drag.txt';
x_vector        = cos(inc*pi/180);
y_vector        = sin(inc*pi/180);

switch fluent_ver(1)
    case {'6'} %case {'6.2.16','6.3.26'}
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
    case {'1'} %case {'13.0.0','14.0.0'}
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
    otherwise
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
end
% Drag vector
file            = 'VLab_Lift.txt';
x_vector        =-sin(inc*pi/180);
y_vector        = cos(inc*pi/180);
switch fluent_ver(1)
    case {'6'} %case {'6.2.16','6.3.26'}
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
    case {'1'} %case {'13.0.0','14.0.0'}
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
    otherwise
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s\n',all_wall_zone,x_vector,y_vector,in_file,file);   % drag
end

%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete ('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');
fprintf(fid,'exit yes \n');
fclose(fid);

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jloads : end Ok \n');end;

