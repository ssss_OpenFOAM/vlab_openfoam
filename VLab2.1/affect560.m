function [data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = affect560 (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x);
% BE2 Naca 4 digits High Reynolds RANS modeling
%=================================================================
% Problem definition   (preemption on defaults2.m)
%=================================================================
%======================================================
% Geometry definition (by default defined in data*.m)
%======================================================
data_geo.c              = 1;                            % chord length (m)
% default airfoil is a NACA 4 digits                    (choose it in data*.m)
%data_geo.method.type        = 'read_Javafoil';         % choice=(naca_4digits, naca_6digits, read)
%data_geo.filename{1}        = 'naca0012_flap2010.txt'; % airfoil filename needed only if data_geo.airfoil = 'read'
%======================================================
% Flow conditions     (by default defined in data*.m)
%======================================================
%======================================================
% Physical Modeling
%======================================================
data_sim.str_URANS          = 'RANS';           % physical model    (RANS, URANS)
data_sim.regime             = 'turbulent';      % flow regime       (laminar, turbulent)
data_sim.Tu_model           = 'SSTtr';             % Turbulence model  (SA, SST, SSTtr)
%data_sim.str_URANS          = 'URANS';          % URANS + laminar = DNS !!!
%data_sim.regime             = 'laminar';        % flow regime (laminar, turbulent)
%======================================================
% Numerical Modeling
%======================================================
% see in data??.m
%======================================================
% GAMBIT Mesh          (other parameters in data*.m)
%======================================================
data_mesh.h_mesh                = 0.10;             % size of the inner zone (percent of chord)                     (0.05)
data_mesh.coeff_sill            = 0.2;              % size of the wake  zone (percent of chord)                     (0.10)
data_mesh.coeff_zone            = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh    (0)
data_mesh.coeff_surf            = 4;                % npts_surface = length_surface / bda_size / coeff_surf         (3)
%======================================================
% FLUENT simulation conditions
%======================================================
% Sim & graphics ON/OFF
data_sim.sim                = 1;                % 1 = Run the simulation
data_sim.graphic            = 1;                % Fluent graphic (0: no, 1: yes) ZZZ should be zero for cell number capture
% Initialization
data_sim.interpolation      = 'yes';            % create interpolation file ?
data_sim.init_method        = 'read';           % {'init','read'}
% Convergence criteria
data_sim.Stop_type          = 'Intelligent';    % Stop Type : {'Intelligent','Interactive','Definite'} 
data_sim.Stop_type          = 'Definite';       % Stop Type : {'Intelligent','Interactive','Definite'} 
data_sim.Stop_Err           = 0.01;            % Lift coeff. Error to stop RANS/URANS
data_sim.Max_Nit            = 4000;             % Iterations max
data_sim.iter_sup           = 200;              % iterations fenetre glissante moyennes sur Cl_mean

data_num.parallel           = 1;        % 1 = parallel calculation
data_num.nproc              = 2;        % number of processors
data_num.Affichage          = 'yes';    % Affichage des variables data_*
data_num.Rep_data           = 'all';    % Choix des variables � afficher : {'geo','mesh','topo','sim','ac','all'} 
%================================================================================================
% Results for comparisons - Naca 0012 Re=6e6 - http://turbmodels.larc.nasa.gov/naca0012_val.html
%================================================================================================
data_sim.comparison         = 'yes';            % 'yes' or 'no'
data_sim.title              = ['Exp.' ;     ];  % Z! strings should have same length (4 characters)
data_sim.al_res             = [0      10     15];       % angle of attack vector
data_sim.Cl_res             = [0      1.09   1.55];     % lift coeff. vector
data_sim.Cd_res             = [0.0082 0.0124 0.0212];   % drag coeff. vector
data_sim.Cm_res             = [0      0      0];        % moment coeff. vector
% ======================================================================
% Ne rien toucher sous cette ligne sans une bonne connaissance de VLab
% ======================================================================
data_sim.str_dir        = strcat('BE1_',datestr(now));      % nom repertoire resultats
data_sim.str_dir(16)    = '-';                              % remplace espace par tiret
data_sim.str_dir(19)    = '';
data_sim.str_dir(21)    = '';
% ======================================
% Affectation du cas courant from data1
% ======================================
% 4512 -> m=0.04 p=0.5 t=0.12
m   =   fix(  var.value(i_cas,1)/1000 );
p   =   fix( (var.value(i_cas,1)-1000*m)/100 );
t   =         var.value(i_cas,1)-1000*m-100*p;
data_geo.naca4digits.m      =  m/100;
data_geo.naca4digits.p      =  p/10;
data_geo.naca4digits.t      =  t/100;
data_sim.geo                    = [str_var(1,3:6) , str_var(i_cas+1,2:6)];      % NACA12817
data_sim.Vinf                   = var.value(i_cas,2);
data_sim.inc                    = var.value(i_cas,3);
data_mesh.mesh_algo             = var.value(i_cas,4);               % mesh choice (0:uns, 2:hybrid)
data_mesh.h1                    = var.value(i_cas,5);               % mesh choice (0:uns, 2:hybrid)
data_mesh.n_cl                  = var.value(i_cas,6);               % mesh choice (0:uns, 2:hybrid)
data_mesh.size_bda              = var.value(i_cas,7);
data_mesh.growthrate_inside     = var.value(i_cas,8);               % unstructured mesh inside
data_mesh.growthrate_outside    = var.value(i_cas,9);               % unstructured mesh outside
data_sim.Stop_Nit               = var.value(i_cas,10);              % Iterations of STOP in RANS/URANS

data_sim.I_turb                 = var.value(i_cas,12);              % Turbulence Intensity (%)          [1]
data_sim.L_turb                 = var.value(i_cas,13);              % Turbulence Length Scale (m)       [0.005]
switch var.value(i_cas,14)
    case 1
        data_sim.scheme_order = 'O1';
    case 2
        data_sim.scheme_order = 'O2';
    case 3
        data_sim.scheme_order = 'O3';
end
data_mesh.N_Corde_Amont         = var.value(i_cas,15);               % nombre de corde amont profil
data_mesh.N_Corde_Aval          = var.value(i_cas,15);               % nombre de corde aval  profil
data_mesh.N_Corde_ymin          = var.value(i_cas,15);               % nombre de corde au-dessous du profil
data_mesh.N_Corde_ymax          = var.value(i_cas,15);               % nombre de corde au-dessus  du profil 

switch var.value(i_cas,16)
    case 0
        data_sim.Fluid_model = 'incompressible';
    case 1
        data_sim.Fluid_model = 'compressible';
end

switch var.value(i_cas,17)
    case 0
        data_num.solver = 'pressure-based';
    case 1
        data_num.solver = 'density-based-implicit';
    case 2
        data_num.solver = 'density-based-explicit';
end

switch data_mesh.mesh_algo
    case 0;
        data_mesh.y_wall    = data_mesh.size_bda;   % y+ evaluation
    case {1,2};
        data_mesh.y_wall    = data_mesh.h1;         % y+ evaluation
end


