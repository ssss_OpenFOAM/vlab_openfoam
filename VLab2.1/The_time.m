function [time_step, time_sim, iter_mean,iter_sim] = The_time (L_ref, V_ref, Freq, period_sim, period_ndt, period_mean)
%
% L_ref : reference length in meters
% V_ref : reference velocity
time_scale_1    = L_ref / V_ref;
time_scale_2    = 1 / Freq;
t_min           = min (time_scale_1 , time_scale_2);
t_max           = max (time_scale_1 , time_scale_2);
time_step       = t_min / period_ndt;
time_sim        = t_max * period_sim;
iter_mean       = round (period_mean * t_max / time_step);
iter_sim        = round (time_sim / time_step);
end