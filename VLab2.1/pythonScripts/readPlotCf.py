# -*- coding: utf-8 -*-
"""
Script used to plot results from CSV file 
"""

import csv
import numpy as np

def ReadCSV(fname,posX,posTauX,posTauY,posTauZ):
    TauX = []
    TauY = []
    TauZ = []
    X = []
    U = []
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        next(f)
        for row in reader:
            TauX.append(float(row[posTauX]))
            TauY.append(float(row[posTauY]))
            TauZ.append(float(row[posTauZ]))
            X.append(float(row[posX]))
    f.close()
    X = np.array(X)
    TauX = np.array(TauX)
    TauY = np.array(TauY)
    TauZ = np.array(TauZ)
    Tau = []
    for i in range(len(TauX)):
        Tau.append(TauX[i]*TauX[i] + TauY[i]*TauY[i] + TauZ[i]*TauZ[i])
        if TauX[i] > 0:
		Tau[i] = -np.sqrt(Tau[i])
	else:
		Tau[i] = np.sqrt(Tau[i])
    return [X[311:],Tau[311:]]

def ReadCSV_Catalano(fname,posX,posData):
    data = []
    X = []
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            data.append(float(row[posData]))
            X.append(float(row[posX]))
    f.close()
    X = np.array(X)
    data = np.array(data)
    return [X,data]

[X,Y] = ReadCSV('pressure1.csv',11,7,8,9)
XSST = np.array(X)
TauSST = np.array(Y)

[X,Y] = ReadCSV('pressurekkl1.csv',12,8,9,10)
Xkklomega = np.array(X)
Taukklomega = np.array(Y)

[X,Y] = ReadCSV('pressureGamma1.csv',13,9,10,11)
XGamma = np.array(X)
TauGamma = np.array(Y)


[X,Y] = ReadCSV_Catalano('../../PDF/Data/csv/dataCatalano_Cf_alpha4_kwsstLR.csv',0,1)
XSSTLR_Catalano = np.array(X)
TauSSTLR_Catalano= np.array(Y)

import matplotlib.pyplot as plt

### General control of plot graphics

plt.rc('text')  
plt.rc('font', family='serif')  
plt.rc('font', size=15)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 40
plt.rcParams['axes.labelsize'] = 40
plt.rcParams['xtick.labelsize'] = 30
plt.rcParams['ytick.labelsize'] = 30
plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5

plt.plot(XSST,2*TauSST, color='0', ls='solid', marker = 'o', markerfacecolor='white',\
         label="SST-LR",markevery=40)

plt.plot(XSSTLR_Catalano,(TauSSTLR_Catalano), ls='solid', marker = '^', markerfacecolor='white',\
         label="SST-LR-Catalano")

plt.plot(Xkklomega,2*Taukklomega, color='red', ls='dashed', marker = 's', markerfacecolor='white',\
         label="$k-k_L-\\omega$",markevery=40)

plt.plot(XGamma,2*TauGamma, color='0.5', ls='-.', marker = '^', markerfacecolor='white',\
         label=r"$\gamma - Re_{\theta_t}$",markevery=40)

plt.legend(loc=0)
plt.xlabel(r'x/c', fontsize=16)
plt.ylabel(r'$C_f$', fontsize=16)

plt.ylim(-0.03,0.03)
plt.xlim(0.0,1.0)
plt.locator_params(axis='y',nbins=5)
plt.locator_params(axis='x',nbins=5)
plt.savefig("TauDistribution.pdf", dpi = 600, bbox_inches='tight')
