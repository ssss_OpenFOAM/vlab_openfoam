import sys,os
from operator import itemgetter
try: paraview.simple
except: import paraview.simple as pv
import matplotlib as mpl
mpl.use('Agg')
from paraview import numpy_support as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from scipy.interpolate  import griddata

def read(fileName, time = None, decomposed = False, saveTime = None):
    print 'Reading '+ fileName + " state decomposed = " + str(decomposed)
    extension = os.path.splitext(fileName)[-1]
    if extension == '.foam':
        reader = pv.OpenFOAMReader(FileName = fileName)
        if decomposed == False:
            if time == "latestTime":
                reader.UpdatePipelineInformation()
		reader.CaseType = 'Reconstructed Case'
                times = reader.TimestepValues
                print "Loading Reconstructed case time = " + str(times[-1])
                if saveTime != None:
                    saveTime.append(times[-1])
                reader.UpdatePipeline(time=times[-1])
            else:
                print "Loading Reconstructed case time = " + str(time)
                reader.UpdatePipeline(time=float(time))
        if decomposed == True:
            reader.UpdatePipelineInformation()
            reader.CaseType = 'Decomposed Case'
            if time == "latestTime":
                reader.UpdatePipelineInformation()
                times = reader.TimestepValues
                print "Loading Decomposed case time = " + str(times[-1])
                if saveTime != None:
                    saveTime.append(times[-1])
                reader.UpdatePipeline(time=times[-1])
            else:
                print "Loading Decomposed case time = " + str(time)
                reader.UpdatePipeline(time=float(time))
        reader = pv.MergeBlocks(reader)
    elif extension == '.cas':
        reader = pv.FluentCaseReader(FileName = fileName)
        #reader = pv.Tetrahedralize(Input = reader)
        reader = pv.CellDatatoPointData(Input = reader)
        reader = pv.MergeBlocks(reader)
    elif extension == '.vtu':
        reader = pv.XMLUnstructuredGridReader(FileName = [fileName])
    elif extension == '.pvtu':
        reader = pv.XMLPartitionedUnstructuredGridReader(FileName = [fileName])
    elif extension == '.vtp':
        reader = pv.XMLPolyDataReader(FileName = [fileName])
    elif extension == '.plt':
        reader = pv.VisItTecplotBinaryReader(FileName = fileName)
        reader.PointArrayStatus = ['X', 'Y', 'Z', 'Vx_U0', 'Vy_U0', 'Vz_U0', 'Conc']
        reader = pv.MergeBlocks(reader)
    elif extension == '.vtm':
        reader = pv.XMLMultiBlockDataReader(FileName = [fileName])
        reader = pv.MergeBlocks(reader)
    else:
	sys.exit( "Error: unknown extension of file "+fileName)
    return reader

def write(data, fileName, overwrite = False):
    print 'Writing', fileName
    if os.path.isfile(fileName) and not overwrite:
        print fileName,' already exists.'
        return False
    extension = os.path.splitext(fileName)[-1]
    if extension == '.vtu':
        writer = pv.XMLUnstructuredGridWriter(FileName = [fileName])
        #writer.HeaderType = 'UInt64'
    elif extension == '.vtp':
        writer = pv.XMLPolyDataWriter(FileName = [fileName])
        #writer.HeaderType = 'UInt64'
    elif extension == '.csv':
        writer = pv.DataSetCSVWriter(FileName = [fileName])
    writer.UpdatePipeline()
    pv.Delete(writer)


def celltoPoint(data):
    grid = pv.CleantoGrid(data)
    pointData = pv.CellDatatoPointData(grid)
    return pointData

def clip(data, point=None, norm=None, box=None, radius=None):
    clip = pv.Clip()
    clip.Input = data  
    if norm != None:
        print 'Clipping', point, norm
        clip.ClipType = "Plane"
        clip.ClipType.Origin = point
        clip.ClipType.Normal = norm
    elif box != None:
        box = list(box)
        if len(box) == 4:
            box = get3rdLimit(data,box)
        print 'Clipping', box
        clip.ClipType = "Box"
        clip.ClipType.Bounds = box
        clip.InsideOut = 1
    elif radius != None:
        print 'Clipping Sphere', point, radius
        clip.ClipType = "Sphere"
        clip.ClipType.Center = point
        clip.ClipType.Radius = radius
        clip.InsideOut = 1
    return clip
    
def clipClosedSurface(data, point,norm):
    clip = pv.Clip()
    clip.Input = data      
    print 'Clipping', point, norm
    clip.ClipType = "Plane"
    clip.ClipType.Origin = point
    clip.ClipType.Normal = norm
    return clip

def slice(data, point=None, norm=None, box=None):
    slice = pv.Slice(data)
    if box == None:
        if len(point) < 3:
            point = list(point)
            norm = list(norm)
            point = get3rdLimit(data,point)
            norm = get3rdLimit(data,norm)
        print 'Slicing', point, norm
        slice.SliceType = "Plane"
        slice.SliceType.Origin = point
        slice.SliceType.Normal = norm
    else:
        box = list(box)
        if len(box) == 4:
            box = get3rdLimit(data,box)
        print 'Clipping', box
        slice.SliceType = "Box"
        slice.SliceType.Bounds = box
    return slice

def streamLine(data,point,var):
    sline = pv.StreamTracer(data,SeedType = "Point Source")
    sline.SeedType.Center = point
    sline.SeedType.Radius = 0
    sline.SeedType.NumberOfPoints = 1
    sline.Vectors = ['POINTS',var]
    sline.MaximumStreamlineLength = 1000
    return sline

def get3rdLimit(data, vec):
    data.UpdatePipeline()
    bbox = data.GetDataInformation().GetBounds()
    vec = list(vec)
    print 'Bounding box', bbox
    for i in (0,len(bbox),2):
        if bbox[i]-bbox[i+1] < 1e-10:
            if len(vec) < 4:
                vec.insert(i,bbox[i]) 
                return vec
            else:
                vec[i:i] = bbox[i:i+2]
                return vec

def transform(data, translate = None, rotate = None, scale = None):
    trans = pv.Transform(data)
    trans.Transform = 'Transform'
    if translate != None:
        trans.Transform.Translate = translate
    if rotate != None:
        trans.Transform.Rotate = rotate
    if scale != None:
        trans.Transform.Scale = scale
    return trans

def calculator(data, expr, name=None):
    print 'Calculating', expr
    if name == None:
        name = 'expr'
    calc = pv.Calculator(data)
    calc.Function = expr
    calc.ResultArrayName = name
    return calc

def pyCalculator(data,expr,name=None):
    print 'PyCalculating', expr, name
    if name == None:
        name = 'expr'
    calc = pv.PythonCalculator(data)
    calc.Expression = expr
    calc.ArrayName = name
    return calc

def probe(data,point):
    probe = pv.ProbeLocation(data,ProbeType = "Fixed Radius Point Source")
    probe.ProbeType.Center = point
    return probe

def lineProbe(data,p1,p2,samples=100):
    data = pv.PlotOverLine(data,Source="High Resolution Line Source")
    data.Source.Point1 = p1
    data.Source.Point2 = p2
    data.Source.Resolution = samples
    return data

def refine(data, numberofSubdivisions = 1):
    print 'Refining'
    sdiv = pv.Subdivide(data)
    sdiv.NumberofSubdivisions = numberofSubdivisions
    smooth = pv.Smooth(sdiv)
    smooth.NumberofIterations = 2000
    return smooth

def gradient(data, var):
    gradient = pv.GradientOfUnstructuredDataSet(data)
    gradient.ScalarArray = ['POINTS', var]
    gradient.ResultArrayName = 'd'+var
    return gradient

def unstructuredSurfacetoPolyData(data):
    es = pv.ExtractSurface(data)
    es.NonlinearSubdivisionLevel =0
    es.PieceInvariant = 1
    es.UpdatePipeline()
    return es

def mask(data,var,xmin,xmax):
    mask = pv.Threshold(data)
    mask.Scalars = ['POINTS',var]
    mask.ThresholdRange = [xmin,xmax]
    return mask

def deleteFilters():
    deletedKeys = []
    for key,value in pv.GetSources().items():
        deletedKeys.append(key[0])
        pv.Delete(value)
    print 'Deleted', len(deletedKeys), 'Filters:', ', '.join(deletedKeys)
    

# This tool is designed to extract data for 2D plots.
def getBBox(data):
    data = pv.servermanager.Fetch(data)
    return data.GetBounds()

def getCoordinates(data, delete = True):
    data = pv.servermanager.Fetch(data)        
    if data == None:
        data = data.data
    nPoints = data.GetNumberOfPoints()
    coordinates = []
    for i in xrange(nPoints):
        coordinates.append(data.GetPoint(i)[:3])
    coordinates = np.array(coordinates).transpose()
    bounds = data.GetBounds()
    toDelete = []
    for b in range(0,len(bounds),2):
        if bounds[b+1]-bounds[b] < 1e-4:
            toDelete.append(b/2)
    coordinates = np.delete(coordinates,toDelete,0)
        #x = coordinates[0]#coordinates[allaxis.find(axis[0])]
        #y = coordinates[1]#coordinates[allaxis.find(axis[1])] 
        #coordinates = np.array([x,y])
    #print x,y
    return coordinates

def getTriangles(data, box=None, swapAxis = False):
    x,y = getCoordinates(data)    
    data = pv.servermanager.Fetch(data)
    triangles = []
    polys = data.GetPolys().GetData()
    nVertices = polys.GetSize()
    for i in range(0, nVertices, 4):
        triangles.append([polys.GetTuple1(i+1),polys.GetTuple1(i+2),polys.GetTuple1(i+3)])
    triangles = np.array(triangles)
    if swapAxis:
        x,y = y,x
        if box:
            box = [box[2],box[3],box[0],box[1]]
    else:    
        x,y = x,y
    
    triangles = tri.Triangulation(x,y,triangles)
    #if refine != None:
    #    refiner = tri.UniformTriRefiner(triangles)
    #    triangles, values = refiner.refine_field(values, subdiv = refine)
    if box:
        xmid =  x[triangles.triangles].mean(axis=1)
        ymid =  y[triangles.triangles].mean(axis=1)
        mask = np.where( (xmid > box[0]) & (xmid < box[1]) & (ymid > box[2]) & (ymid < box[3]) ,0,1 )
        triangles.set_mask(mask)
    return triangles

def getArrayData(data, var,component=None):
    data = pv.servermanager.Fetch(data)
    try:
        arrayData = ns.vtk_to_numpy(data.GetPointData().GetArray(var))
    except AttributeError:
        print data.GetPointData()
        print var, 'not found'
        assert 0
    if component != None:
        arrayData = np.array(arrayData)[:,component]
    return arrayData

def contour(data, plot=None, var=None, component = None, box = None, swapAxis = False, levels = None,cmap = None, linestyles = None, clabels=None,
            labels = True,contourf = False, interpolate = None, xLim=None, yLim=None):
#    data = pv.servermanager.Fetch(data)        
    print 'Contourplot'
    #triangles = data.getTriangles(data,box,swapAxis)
    
    #values = data.getArrayData(data,var,component)
    triangles = getTriangles(data,box,swapAxis)
    values = getArrayData(data,var,component)
#    values = np.where(np.abs(values) > 0.0001,values,0)
    if interpolate != None:
        interp = tri.CubicTriInterpolator(triangles,values)
        values =  interp(triangles.x,triangles.y)
        
    if plot == None:
        plot = plt
        plot.axis('equal', adjustable='box-forced')
        if not swapAxis and box != None:
            plot.xlim(box[0],box[1])
            plot.ylim(box[2],box[3])
        elif box != None:
            plot.xlim(box[2],box[3])
            plot.ylim(box[0],box[1])
        if xLim != None and yLim != None:
            print "Setting my axis"
            plot.xlim(xLim[0],xLim[1])
            plot.ylim(yLim[0],yLim[1])
    else:
        plot.set_aspect('equal', adjustable='box-forced')
    
        if not swapAxis and box != None:
            plot.set_xlim(box[0],box[1])
            plot.set_ylim(box[2],box[3])
        elif box != None:
            plot.set_xlim(box[2],box[3])
            plot.set_ylim(box[0],box[1])
        if xLim != None and yLim != None:
            print "Setting my axis"
            plot.set_xlim(xLim[0],xLim[1])
            plot.set_ylim(yLim[0],yLim[1])
    plot.tick_params(direction = 'out')
    if contourf:
        CS = plot.tricontourf(triangles, values, levels = levels, linestyles = linestyles, cmap=cmap, extend='both')
    else:
        CS = plot.tricontour(triangles, values, levels = levels, linestyles = linestyles)
    
    if not contourf and labels:
        plot.clabel(CS, manual = clabels, inline=1, fontsize=10)
    
    return CS
def quiver(data,plot=None,var = None, varUx = None,varUy = None,swapAxis = None,componentUx = None, componentUy = None, box = None, each = 3):
    print 'Quiverplot'
    #triangles = data.getTriangles(data,box,swapAxis)
    nx, ny = 300 ,300
    #values = data.getArrayData(data,var,component)

    triangles = getTriangles(data,box,swapAxis)
    if varUx == None:
        valuesUx = getArrayData(data,var,componentUx)
        valuesUy = getArrayData(data,var,componentUy)
    else:
        valuesUx = getArrayData(data,varUx,None)
        valuesUy = getArrayData(data,varUy,None)

    if plot == None:
        plot = plt
    
    plot.quiver(triangles.x[::each],triangles.y[::each],valuesUx[::each],valuesUy[::each])
    #plot.streamplot(triangles.x,triangles.y,valuesUx,valuesUy)

def contourf(data, plot=None, var=None, component = None,values=None, box = None, swapAxis = False,levels = None, cmap = None):
    return contour(data,plot,var,component,box,swapAxis,levels, contourf = True, cmap = cmap)


def integrate(data, var=None, box=None, expr=None, average = False):
    if box:
        data = clip(data, box=box)
        data = pv.ExtractSurface(data)
    if expr:
        var = 'expr'
        data = pyCalculator(data, expr)

    intData = pv.IntegrateVariables(data)
    intData = pv.servermanager.Fetch(intData)
    integral = ns.vtk_to_numpy(intData.GetPointData().GetArray(var))[0]
    area = ns.vtk_to_numpy(intData.GetCellData().GetArray('Area'))[0]
    print 'Integral(' + var +') = ',integral, ' Average = ', integral/area, ' Area = ', area
     
    if average:
        integral = integral/area
    return integral

def average(data, var=None, box = None, expr = None):
    average = integrate(data,var, box, expr, average = True)
    return average
   
# Extracts defined amount of lines from the plane and takes an average of them.
def avgLine(data, var=None, box=None, dir=0, samples = 10, expr = None):
    print box    
    data = clip(data, box = box)
    if expr:
        var = 'expr'
        data = calculator(data, expr)
    data.UpdatePipeline()
    box = data.GetDataInformation().GetBounds()
    x1,x2 = box[dir*2:dir*2+2]
    x1 = x1+0.01 # No data at the boundaries so take point just inside. TODO 1.01?
    x2 = x2-0.01
    norm = np.zeros(3)
    norm[dir] = 1
    values = []
    x = None
    for i in range(samples):
        orig = np.zeros(3)
        orig[dir]=(x2-x1)/(samples-1)*i+x1
        sliceData = slice(data,orig,norm)
        x = getCoordinates(sliceData)[0]        
        sliceData = pv.servermanager.Fetch(sliceData)
#            print sliceData.GetPointData()
        line = ns.vtk_to_numpy(sliceData.GetPointData().GetArray(var)).transpose()
        
        values.append([(a,b) for a,b in zip(x,line)])
        values[-1] = np.array(sorted(values[-1], key = itemgetter(0)))
    x = values[0][:,0] # Points from the first line
    newvalues = []
    for value in values:
        newvalues.append(np.interp(x,value[:,0],value[:,1])) # Interpolate other lines to the points
    avgline = np.trapz( newvalues, axis=0)/(len(newvalues)-1)
    return (x,avgline)

# All the points between p1 and p2
def line(data,var,p1,p2,coord = None):
    p1 = np.array(get3rdLimit(data,p1))
    p2 = np.array(get3rdLimit(data,p2))
    p3 = np.zeros(3)
    n = np.cross((p2-p1),(p3-p1))
    data = slice(data, point= p1, norm = n)
    coordinates = getCoordinates(data)
    data.UpdatePipeline()
    data = pv.servermanager.Fetch(data)
            
    line = ns.vtk_to_numpy(data.GetPointData().GetArray(var)).transpose()
    if coord != None:
        line = line[coord]
    
    if len(coordinates) > 1 or len(coordinates) == 0:
        print 'Wrong number of coordinates', len(coordinates)
        assert 0
    
    data = [(coordinates[0][i], line[i]) for i in xrange(len(line))]
    data = np.array(data,dtype = [('x',float),('var',float)])
    data = np.sort(data, order = 'x')
    #print data['x'][0:8]
    #plt.plot(data['x'],data['var'])
    return (data['x'],data['var'])

# Defined number of points between p1 and p2
def line2(data,p1,p2,vars=None,samples = 100):
    data = lineProbe(data,p1,p2,samples)    
    coordinates = getCoordinates(data, delete = False)
    data = pv.servermanager.Fetch(data)
        
#    if vars == None: TODO get all variables
    varData = []
    for var in vars:
        varData.append(ns.vtk_to_numpy(data.GetPointData().GetArray(var)))
    return coordinates,varData
def line3(data,p1,p2,vars=None,samples = 100):
    data = lineProbe(data,p1,p2,samples)    
    data = pv.servermanager.Fetch(data)
#    if vars == None: TODO get all variables
    varData = []
    for var in vars:
        varData.append(ns.vtk_to_numpy(data.GetPointData().GetArray(var)))
    return varData
            
def point(data,vars,point):
    data = probe(data.pvdata,point)
    data = pv.servermanager.Fetch(data)
    varData = dict()
    for var in vars:
        varData[var] = ns.vtk_to_numpy(data.GetPointData().GetArray(var))
    return varData

    
def getTimePlots(folderSlices):
    target = open(folderSlices+'timeSlices.txt', 'r')
    line = target.readline().split('\n')
    target.close()
    #return str(int(float(line[0])))
    time = str(float(line[0]))
    return time.replace(".","_")
