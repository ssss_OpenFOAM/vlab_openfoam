# -*- coding: utf-8 -*-
"""
Script used to plot results from CSV file
"""

import csv
import numpy as np

def ReadCSV(fname,posX,posY):
    X = []
    Y = []
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        next(f)
        for row in reader:
            X.append(float(row[posX]))
            Y.append(float(row[posY]))
    f.close()
    return [X,Y]

[X,Y] = ReadCSV('PressureData.csv',0,1)
XData = np.array(X)
YData = np.array(Y)

[X,Y] = ReadCSV('Pressure/pressure1.csv',11,6)
XSST = np.array(X)
YSST = np.array(Y)

[X,Y] = ReadCSV('Pressure/pressurekkl1.csv',12,7)
Xkklomega = np.array(X)
Ykklomega = np.array(Y)

[X,Y] = ReadCSV('Pressure/pressureGamma1.csv',13,8)
XGamma = np.array(X)
YGamma = np.array(Y)

import matplotlib.pyplot as plt

### General control of plot graphics

plt.rc('text')
plt.rc('font', family='serif')
plt.rc('font', size=15)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 40
plt.rcParams['axes.labelsize'] = 40
plt.rcParams['xtick.labelsize'] = 30
plt.rcParams['ytick.labelsize'] = 30
plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5

plt.plot(XData,YData, color='0', ls='', marker = 'o', markerfacecolor='black',\
         label="LES (Catalano et. al.)",markevery=1)

plt.plot(XSST,-2*YSST, color='0', ls='solid', marker = 'x', markerfacecolor='white',\
         label="SST-LR",markevery=40)

plt.plot(Xkklomega,-2*Ykklomega, color='red', ls='dashed', marker = 's', markerfacecolor='white',\
         label="$k-k_L-\\omega$",markevery=40)

plt.plot(XGamma,-2*YGamma, color='0.6', ls='-.', marker = '^', markerfacecolor='white',\
         label="$\\gamma-Re_{\\theta t}$",markevery=40)

plt.legend(loc=0)
plt.xlabel(r'x/c', fontsize=32)
plt.ylabel(r'$C_p$', fontsize=32)

plt.xlim(-0.05,1.05)
plt.ylim(-1.05,1.5)
major_ticks = np.arange(-1, 2, 31)
minor_ticks = np.arange(-1, 2, 31)

plt.locator_params(axis='y', nticks=10)
plt.locator_params(axis='x', nticks=10)

plt.grid(which='both')
plt.grid(which='minor', alpha=0.2)
plt.grid(which='major', alpha=0.5)

plt.savefig("PressureDistr.pdf", dpi = 600, bbox_inches='tight')
