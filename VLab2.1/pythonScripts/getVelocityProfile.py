import mftools as mf
import numpy as np
import matplotlib.pyplot as plt
import paraview.simple as pv
from paraview import numpy_support as ns
import csv as csv
import gmshData

#path = "/home/jsagg/OpenFOAM/jsagg-3.0.1/run/ProjectNumericalFluidMechanics/testCases/Re6e4/Re6e4_alpha0_SST"
path = "/home/jsagg/OpenFOAM/jsagg-3.0.1/run/ProjectNumericalFluidMechanics/testCases/Re6e4/deg0/SST"
#path = "../../OpenFOAM/testcases/validation/turbulenceModels/Catalano2010/GammaReTheta/Re6e4_alpha0_GammaReTheta"
name = "Re6e4.foam"
#name = "Re6e4_alpha0_GammaReTheta.foam"

fileNames = path+"/"+name
#data = mf.read(path+"/"+name,time = "latestTime", decomposed = True)
data = mf.read(path+"/"+name,time = "latestTime")

data.add_attribute('MeshRegions', 'airfoilWall')

plotOnIntersectionCurves1 = pv.PlotOnIntersectionCurves(Input=data)
plotOnIntersectionCurves1.SliceType = 'Plane'
# init the 'Plane' selected for 'SliceType'
plotOnIntersectionCurves1.SliceType.Origin = [0.5001250000059372, 0.013798349536955357, 0.5]
# Properties modified on plotOnIntersectionCurves1.SliceType
plotOnIntersectionCurves1.SliceType.Normal = [0.0, 0.0, 1.0]
pv.SaveData('./tau.csv', proxy=plotOnIntersectionCurves1)

def getSlopeAirfoil(filename,xPosition):
    [x,y] = gmshData.readDatAirfoil(filename) #leer puntos
    [x,y] = gmshData.interpol60SymmetricAirfoilpts(x,y) #interpolarlos, quiza haya que meter mas puntos?
    slope = 0
    for i in range(len(x)-1):
        if x[i]<=xPosition and x[i+1]>=xPosition and y[i]>0: # estamos en el punto donde queremos calcular la pendiente?
            slope = (y[i+1]-y[i])/(x[i+1]-x[i]) #calculamos la pendiente de la forma del perfil con una derivada forward
    theta = np.arctan(abs(slope)) #calculamos el angulo relacionado con esta pendiente (que esta ahora en valor absoluto)
    if slope < 0:
        theta = theta*(-1.0)
    trueTheta = np.pi/2. + theta #cogemos el angulo de la normal a la superficie
    slope = np.tan(trueTheta) #calculamos la pendiente de la normal a la superficie
    return slope
def getLastPointsOfProfiles(x0,y0,slope,length):
    x1 = x0 + length/slope
    y1 = y0 + slope*(x1-x0)
    return [x1,y1]
def ReadCSV(fname,posX,posTauX,posTauY,posTauZ):
    TauX = []
    TauY = []
    TauZ = []
    X = []
    Y = []
    U = []
    with open(fname, 'rb') as f:
        reader = csv.reader(f)
        next(f)
        for row in reader:
            TauX.append(float(row[posTauX]))
            TauY.append(float(row[posTauY]))
            TauZ.append(float(row[posTauZ]))
            X.append(float(row[posX]))
            Y.append(float(row[posX+1]))
    f.close()
    X = np.array(X)
    Y = np.array(Y)
    TauX = np.array(TauX)
    TauY = np.array(TauY)
    TauZ = np.array(TauZ)
    Tau = []
    for i in range(len(TauX)):
        Tau.append(TauX[i]*TauX[i] + TauY[i]*TauY[i] + TauZ[i]*TauZ[i])
        Tau[i] = np.sqrt(Tau[i])
    return [X,Y,Tau]


[X,y,Tau] = ReadCSV('tau1.csv',11,7,8,9)
#[X,y,Tau] = ReadCSV('tau1.csv',12,8,9,10)
#[X,y,Tau] = ReadCSV('tau1.csv',13,9,10,11)

x = np.linspace(0.0,1.0,11)
vU = np.zeros((2001,11))
uProjected = np.zeros((2001,11))
yPlus = np.zeros((2001,11))
uPlus = np.zeros((2001,11))
utau = np.zeros(11)
# Viscous scales
nuviscous = 1.667e-05
k = 0
x1array = []
y1array = []
for i in x:

     lineData = pv.PlotOverLine(data,Source="High Resolution Line Source")
     j = np.searchsorted(X+1e-7, i)
     slope = getSlopeAirfoil("data",i)
     lengthProfile = 1.0
     [x1,y1] = getLastPointsOfProfiles(i,y[j],slope,0.1)
     lineData.Source.Point1 = [i,y[j],0.0]
     lineData.Source.Point2 = [x1,y1,0.0]
     x1array.append(x1)
     y1array.append(y1)
     print "x0 = " + str(i) + " y0 = " + str(y[j])+ " x1 = " + str(x1) + " y1 = " + str(y1) + " angle slope = " + str(np.arctan(slope)*180/3.14159)
     lineData.Source.Resolution = 2000
     lineData.UpdatePipeline()
     lineDataFetch = pv.servermanager.Fetch(lineData)
     arc_length =  ns.vtk_to_numpy(lineDataFetch.GetPointData().GetArray("arc_length"))
     velocity = ns.vtk_to_numpy(lineDataFetch.GetPointData().GetArray("U"))

     vU[:,k] = velocity[:,0]

     uProjected[:,k] = velocity[:,0]*np.cos(np.pi/2.-np.tan(slope))-velocity[:,1]*np.sin(np.pi/2.-np.tan(slope))
     utau[k] = np.sqrt(Tau[j])
     uPlus[:,k] = vU[:,k]/utau[k]
     uProjected[:,k] = uProjected[:,k]/utau[k]
     yPlus[:,k] = arc_length*utau[k]/nuviscous
     k = k + 1



import matplotlib.pyplot as plt

### General control of plot graphics

plt.rc('text')
plt.rc('font', family='serif')
plt.rc('font', size=15)
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.titlesize'] = 40
plt.rcParams['axes.labelsize'] = 40
plt.rcParams['xtick.labelsize'] = 30
plt.rcParams['ytick.labelsize'] = 30
plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5

plt.semilogx(yPlus[:,5],uPlus[:,5], color='green', ls=':', marker = '^', markerfacecolor='white',\
         label="$SST LRe$",markevery=40)


plt.legend(loc=0)
plt.xlabel(r'$y^+$', fontsize=16)
plt.ylabel(r'$U^+$', fontsize=16)
plt.xlim([0.1,200])

#plt.locator_params(axis='y',nbins=5)
#plt.locator_params(axis='x',nbins=5)
plt.savefig("VelocityProfile.pdf", dpi = 600, bbox_inches='tight')




