import mftools as mf
import paraview.simple as pv

path = "/home/jsagg/OpenFOAM/jsagg-3.0.1/run/ProjectNumericalFluidMechanics/testCases/Re6e4/Re6e4_alpha4_SST"
name = "Re6e4_alpha4_SST.foam"
case = mf.read(path+"/"+name,time = "latestTime")
#case.Meshregions = 'airfoilWall'
case.add_attribute('MeshRegions', 'airfoilWall')

plotOnIntersectionCurves1 = pv.PlotOnIntersectionCurves(Input=case)
plotOnIntersectionCurves1.SliceType = 'Plane'

# init the 'Plane' selected for 'SliceType'
plotOnIntersectionCurves1.SliceType.Origin = [0.5001250000059372, 0.013798349536955357, 0.5]

# Properties modified on plotOnIntersectionCurves1.SliceType
plotOnIntersectionCurves1.SliceType.Normal = [0.0, 0.0, 1.0]

pv.SaveData('/home/jsagg/Studies/Aerospace/MAE/2ndYear/NumericalFluidMechanics/FinalProject/srcBitBucket/src_dev/VLab2.1/pythonScripts/pressure.csv', proxy=plotOnIntersectionCurves1)
