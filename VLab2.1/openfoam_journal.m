function [openfoam_journal_file] = openfoam_journal (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num)
%
% Define simulation for FLUENT
%
% Affect local variables
% Read mesh file                = f(mesh_file)
% Mesh scale                    = f(dim,scale)
% Turbulence model              = f(regime, Tu_model)
% BC type & values              = f(surf_bc, ...)  hyp: inlet, outlet, up, down
% Dynamic mesh
% Check convergence
% Read or initialize flowfield  = f(...)
% Change spatial scheme         = f(...)
% Grandeurs de reference
% Loads                                         hyp: bda, extrados, intrados
% Actuation                                     hyp: jet
% Iterations RANS/URANS
% Mean velocity magnitude on wall surface (old)
% Creations des fichiers resultats FLUENT *.cas et *.dat

Fluent_ver  = data_num.Fluent_ver;
OpenFOAM_ver = '3.0.1'



scale               = data_geo.scale;
c                   = data_geo.c;
b                   = data_geo.b;
Lref                = c;  fprintf(' Lref = %g   ',Lref);
Sref                = b*c;fprintf(' Sref = %g \n',Sref);

Vinf                = data_sim.Vinf;
nu                  = data_sim.nu;
inc                 = data_sim.inc;
I_turb              = data_sim.I_turb;
L_turb              = data_sim.L_turb;
scheme_order        = data_sim.scheme_order;
iter_trans          = data_sim.iter_trans;
iter_sup            = data_sim.iter_sup;
str_URANS           = data_sim.str_URANS;
str_file            = data_sim.str_file;
np_per              = data_sim.np_per;
regime              = data_sim.regime;
Tu_model            = data_sim.Tu_model;
Fluid_model         = data_sim.Fluid_model;
dim                 = data_sim.dim;
init_method         = data_sim.init_method;
frame               = data_sim.frame;
reporting           = data_sim.reporting;
ndt                 = data_sim.period_ndt;

solver              = data_num.solver;          % numerical scheme : {'pressure-based','density-based-explicit','density-based-implicit'} 

force_surface       = data_topo.force_surface;
surf_bc             = data_topo.surf_bc;

theta               = data_ac.Theta_deg;
pos                 = data_ac.pos;
dia                 = data_ac.dia;
Freq                = data_ac.Freq;
Vjfluct             = data_ac.Vjfluct;
Vjmean              = data_ac.Vjmean;

filename_U = strcat(str_file,'/0/U');
filename_k = strcat(str_file,'/0/k');
filename_kt = strcat(str_file,'/0/kt');
filename_kl = strcat(str_file,'/0/kl');
filename_intermittency = strcat(str_file,'/0/intermittency');
filename_omega = strcat(str_file,'/0/omega');
filename_ReTheta = strcat(str_file,'/0/Ret');
filename_ControlDict = strcat(str_file,'/system/controlDict');
filename_FvSchemes = strcat(str_file,'/system/fvSchemes');
filename_TurbulenceProperties = strcat(str_file,'/constant/turbulenceProperties');
filename_TransportProperties = strcat(str_file,'/constant/transportProperties');

slaunch = sprintf('pyFoamCloneCase.py --force %s %s','../OpenFOAM/testcases/template/baseTestcase/',str_file) % clone template
[sta,str] = system (slaunch);
if strcmp(data_num.Affichage,'yes'); fprintf(' Result from pyFoamCloneCase %s \n',str);end;

UString = fileread(filename_U);
kString = fileread(filename_k);
ktString = fileread(filename_kt);
klString = fileread(filename_kl);
intermittencyString = fileread(filename_intermittency);
omegaString = fileread(filename_omega);
ReThetaString = fileread(filename_ReTheta);
controlDictString = fileread(filename_ControlDict);
fvSchemesString = fileread(filename_FvSchemes);
turbulencePropertiesString = fileread(filename_TurbulenceProperties);
transportPropertiesString = fileread(filename_TransportProperties);


if strcmp(data_num.Affichage,'yes'); fprintf(' OpenFOAM %s \n',OpenFOAM_ver);end;

fluent_journal_file = strcat('fluent_',num2str(i_cas),'.jou');
fid=fopen(fluent_journal_file,'w+');

filename = 'fluent_journal.trn';
if exist(filename,'file')==2;delete (filename);end
fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ? - not useful

fprintf(fid,'rc %s\n\n',mesh_file);                         % read mesh file *.msh
switch dim
    case '2D'
        fprintf(fid,'/grid/scale %g %g\n',c,c);                   % scale mesh
    case '3D'
        fprintf(fid,'/grid/scale %g %g %g\n',c,c,c);          % scale mesh
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for msh file where bc type is not defined VLab([],50)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
zzz = 0;
if zzz == 1
for i = 1 : size(surf_bc,1)
    switch surf_bc{i,1} % name
        case 'inlet'
            Type = 'velocity-inlet';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
        case 'walldo'
            Type = 'velocity-inlet';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
        case 'wallup'
            Type = 'velocity-inlet';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
        case 'outlet'
            Type = 'pressure-outlet';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
        otherwise
    end
end
end



%============================================
% Define boundary conditions values - new
%============================================
for i = 1 : size(surf_bc,1) % BC name
switch surf_bc{i,2}         % BC type
    case 'velocity_inlet'   % all vi except jet actuator
        switch surf_bc{i,1}         % BC name
            case 'jet'
            otherwise
                param.Fluent_ver    = Fluent_ver;
                param.regime       	= regime;
                param.Tu_model   	= Tu_model;
                param.dim       	= data_sim.dim;
                param.fid       	= fid;
                param.Vinf          = Vinf;
                param.Vx            = cos(inc*pi/180);      % X-Component of Flow Direction [0.98952579]
                param.Vy            = sin(inc*pi/180);      % Y-Component of Flow Direction [0.1443562]
                param.Vz            = 0;                    % Z-Component of Flow Direction [0]
                param.I_turb        = I_turb;               % Turbulence Intensity (%) [1]

                %%%
                %%%   Calculate here turbulence quantities and write them to the correspondant string
                %%%
                
                param.L_turb        = L_turb;               % Turbulence Length Scale (m) [0.005]
                UString = strrep(UString, '#Ux', num2str(param.Vx*param.Vinf)); 
                UString = strrep(UString, '#Uy', num2str(param.Vy*param.Vinf));
                controlDictString = strrep(controlDictString, '#cos', num2str(param.Vx));
                controlDictString = strrep(controlDictString, '#sin', num2str(param.Vy));
                controlDictString = strrep(controlDictString, '#minusSin', num2str(-1.*param.Vy));
                controlDictString = strrep(controlDictString, '#Uinf', num2str(param.Vinf));
                kInlet = 1.5*(param.Vinf*0.01*param.I_turb)*(param.Vinf*0.01*param.I_turb);
                kString = strrep(kString, '#kInlet', num2str(kInlet));
                ktString = strrep(ktString, '#kInlet', num2str(kInlet)); 
                ReThetaString = strrep(ReThetaString, '#kInlet', num2str(kInlet));
                ReThetaString = strrep(ReThetaString, '#Uinf', num2str(param.Vinf));
                omegaInlet = sqrt(kInlet)/param.L_turb;
                omegaString = strrep(omegaString, '#omegaInlet', num2str(omegaInlet));
                omegaInlet = sqrt(kInlet)/param.L_turb;
                
                omegaString = strrep(omegaString, '#omegaInlet', num2str(omegaInlet));
                transportPropertiesString = strrep(transportPropertiesString, '#nu', num2str(1e-5));
                param.surf_bc_name  = surf_bc{i,1};
                bc_velocity_inlet (param);
        end
    case 'pressure_outlet'
        param.regime       	= data_sim.regime;
        param.dim       	= data_sim.dim;
        param.fid       	= fid;
        param.I_turb        = I_turb;               % Turbulence Intensity (%) [1]
        param.L_turb        = L_turb;               % Turbulence Length Scale (m) [0.005]
        param.surf_bc_name  = surf_bc{i,1};
        bc_pressure_outlet (param)
    case 'wall'
    case 'symmetry'
    otherwise
end
end

% bc jet = velocity-inlet
switch data_ac.ac
    case'no'
    case'yes'
        switch data_ac.type
            case 'continuous jet'
                jet_continuous (fid,data_ac)            % velocity-inlet standard
            case 'synthetic jet'
                [udf_name] = jet_synthetic (data_ac);   % create udf file - language C
                udf_build(os,fid,udf_name)              % build & load udf file
                velocity_inlet (fid,udf_name,data_ac)   % velocity-inlet = synthetic_jet with udf
            case 'pulsed jet'
                [udf_name] = jet_pulsed (data_ac);      % bc jet : create udf - language C
                udf_build(os,fid,udf_name)              % build & load udf file
                velocity_inlet (fid,udf_name,data_ac)   % velocity-inlet = synthetic_jet with udf
        end
end

zzz=1;
if zzz==1
%============================================
% Change Fluid Model to ideal-gas
%============================================
switch Fluid_model
    case 'compressible'
    case 'incompressible'
        Density = 'yes';
        Method  = 'ideal-gas';
        fprintf(fid,'/define/materials/change-create/air, %s %s no no no no no no no no no \n\n',Density,Method); % ideal-gas
%==========================================================
% Change BC type from velocity-inlet to pressure-far-field
%==========================================================
for i = 1 : size(surf_bc,1)
    switch surf_bc{i,2}
        case 'velocity_inlet'
            Type = 'pressure-far-field';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
            param.Fluent_ver    = Fluent_ver;
            param.regime       	= regime;               % 'laminar', 'turbulent'
            param.dim       	= data_sim.dim;         % '2D', '3D'
            param.fid       	= fid;
            param.Vinf          = Vinf;
            param.Vx            = cos(inc*pi/180);      % X-Component of Flow Direction [0.98952579]
            param.Vy            = sin(inc*pi/180);      % Y-Component of Flow Direction [0.1443562]
            param.Vz            = 0;                    % Z-Component of Flow Direction [0]
            param.I_turb        = I_turb;               % Turbulence Intensity (%) [1]
            param.L_turb        = L_turb;               % Turbulence Length Scale (m) [0.005]
            param.Fluid_model   = Fluid_model;          % 'incompressible' 'compressible'
            param.surf_bc_name  = surf_bc{i,1};
            bc_pressure_far_field (param);
        case 'pressure-outlet'
        case 'wall'
        case 'symmetry'
        otherwise
    end
end


%==========================================================
% Change boundary conditions and turbulence model OpenFOAM
%==========================================================

switch regime
    case 'laminar'
        turbulencePropertiesString = strrep(turbulencePropertiesString, '#model', 'laminar');
    case 'turbulent'
    switch Tu_model
        
        case 'SA'
            turbulencePropertiesString = strrep(turbulencePropertiesString, '#model', 'SpalartAllmaras'); % Spalart-Allmaras  (1 eqn)
            omegaString = strrep(omegaString, '#type','type           omegaWallFunction;\n value          $internalField;\n');
        case 'SST'
           turbulencePropertiesString = strrep(turbulencePropertiesString, '#model', 'kOmegaSSTLowRe'); % Spalart-Allmaras  (1 eqn)
            omegaString = strrep(omegaString, '#type','type           omegaWallFunction;\n value          $internalField;\n');
        case 'SSTtr'
            turbulencePropertiesString = strrep(turbulencePropertiesString, '#model', 'kOmegaSSTgammaReTheta');   % kw-SST transition (4 eqn)
            omegaString = strrep(omegaString, '#type','type           omegaWallFunction;\n value          $internalField;\n');

        case 'kklw'
            turbulencePropertiesString = strrep(turbulencePropertiesString, '#model', 'kkLOmega');           % k-kl-w transition (3 eqn)
            omegaString = strrep(omegaString, '#type','type           zeroGradient;\n');
        
         %case 'SAS'
        %    fprintf(fid,'/define/models/viscous/sas yes\n\n');              % SAS               (2 eqn)
        otherwise
            fprintf(fid,'/define/models/viscous/spalart-allmaras yes\n\n'); % default : Spalart-Allmaras
    end
end



%===========================================
% Change nu OpenFOAM
%===========================================
transportPropertiesString = strrep(transportPropertiesString, '#nu', num2str(nu)); 

%===========================
% Solver : pressure-based / density-based-implicit
%===========================
if strcmp(solver,'density-based-implicit');
    fprintf(fid,'/define/models/solver %s y \n\n',solver); % density-based-implicit scheme
end
end
end


%====================
% Check convergence
%====================
check_conv (fid, data_sim);

%===========================================
% Read or Initialize flowfield + iter_trans
%===========================================
str=strcat(data_num.current_dir,'/fluent-interpolation.ip');
if strcmp(init_method,'read') && exist(str,'file')==2
    % compatibility of the fluent-interpolation.ip file is not done => pb
    init_method = 'read';
else
    init_method = 'init';
end
switch init_method
    case 'init'
        switch Fluid_model
            case 'incompressible'
        fprintf(fid,'/solve/initialize/compute-defaults/velocity-inlet inlet\n');       % Choose inlet conditions
            case 'compressible'
        fprintf(fid,'/solve/initialize/compute-defaults/pressure-far-field inlet\n');   % Ok 6.3, 13
        end
        fprintf(fid,'/solve/initialize/initialize-flow\n');                             % Initialize 
        fprintf(fid,'/solve/iterate %g\n\n', iter_trans);                               % Run iter_trans iterations
        if strcmp(data_num.Affichage,'yes'); fprintf(' fluent_journal : initialize flowfield with inlet BC \n'); end;
    case 'read'
        fprintf(fid,'/file/interpolate/read-data %s\n\n', 'fluent-interpolation.ip');   % Read file.ip
        if strcmp(data_num.Affichage,'yes'); fprintf(' initialize flowfield with fluent-interpolation.ip \n'); end;
end

%===========================================
% Change spatial schemes
%===========================================
spatial_schemes (fid,data_sim,data_num);     % O1, O2, O3

fprintf(fid,'/solve/set/reporting-interval %g \n\n', reporting);
fprintf(fid,'/solve/iterate %g \n\n', 10);  % 10 iterations with high-order scheme before adaption

% Grandeurs de reference
fprintf(fid,'/report/reference-values/area %g\n', Sref);
fprintf(fid,'/report/reference-values/length %g\n', Lref);
fprintf(fid,'/report/reference-values/velocity %g\n\n', Vinf);

%================================================
% File of lift and drag history on force_surface
%================================================
%fprintf(fid,'/solve/monitor/force/clear-lift-monitor-data y y \n');      %bug
force (fid, force_surface, data_num,data_sim, 'drag'  , 'write');
force (fid, force_surface, data_num,data_sim, 'lift'  , 'write');
force (fid, force_surface, data_num,data_sim, 'moment', 'write');

%================================
% Adapt mesh around the actuator
%================================
switch data_ac.ac
    case 'yes' 
        if strcmp(data_ac.adapt_mesh,'yes')
            [xu,yu,xl,yl,data_geo]  = airfoil_geometry (data_geo,1);    % 
            %[xu,yu,xl,yl]       = naca_4digits (m,p,t);     % geometry
            x_actuator_center   = c*(pos+0.5*dia);          % x coordinate of the circle center
            y_actuator_center   = c*interp1(xu,yu,pos);     % y coordinate of the circle center
            actuator_radius     = c*dia;          % radius of the circle for mesh adaptation around the actuator
            
            param.fluent_version    = Fluent_ver; 
            param.fid               = fid;
            param.adapt_type        = 'region';
            param.x_center          = x_actuator_center;
            param.y_center          = y_actuator_center;
            param.radius            = actuator_radius;
            adapt_region (param);   % 2.00 * dia
            param.radius            = 2.*actuator_radius;
            adapt_region (param);   % 4.00 * dia   
        end
    case 'no'
    otherwise
end
%===================================================
% Adapt mesh around slot for multi-element airfoils
%===================================================
ZZZ=0;
if ZZZ == 1
%switch data_ac.ac
%    case 'yes' 
%        if strcmp(data_sim.adapt_mesh,'yes')
            x_center   = 0.5*c*(data_geo.v1);           % x coordinate of the circle center
            y_center   = 0;                         % y coordinate of the circle center
            radius     = 0.05*c;                    % radius of the circle for mesh adaptation around the actuator
            
            param.fluent_version    = Fluent_ver; 
            param.fid               = fid;
            param.adapt_type        = 'region';
            param.x_center          = x_center;
            param.y_center          = y_center;
            param.radius            = radius;
            adapt_region (param);   % 2.00 * dia
%        end
%    case 'no'
%    otherwise
%end
end
%================================
% Iterations RANS/URANS
%================================
switch data_ac.ac
    case {'no'}
        switch str_URANS
            case 'RANS'
                fprintf(fid,'/solve/iter %g\n\n',2*iter_sup);    % iterations steady
            case {'URANS','DES','LES','DNS'}
                time_step   =   c / Vinf / np_per;                                  % time step en secondes
                fprintf(fid,'/define/models/unsteady-2nd-order yes\n');             % choix du solveur : O2 unsteady
                fprintf(fid,'/solve/set/time-step %g\n',time_step);                 % choix du pas de temps
                data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',2*iter_sup,ndt);    % iterations unsteady
        end
    case 'yes'
        switch data_ac.type
            case 'continuous jet'
                switch str_URANS
                    case 'RANS'
                        fprintf(fid,'/solve/iter %g\n\n',2*iter_sup);    % iterations steady
                    case {'URANS','DES','LES','DNS'}
                        time_step   =   c / Vinf / np_per;                                  % time step en secondes
                        fprintf(fid,'/define/models/unsteady-2nd-order yes\n');             % choix du solveur : O2 unsteady
                        fprintf(fid,'/solve/set/time-step %g\n',time_step);                 % choix du pas de temps
                        data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                        fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',2*iter_sup,ndt);    % iterations unsteady
                end
            case {'synthetic jet','pulsed jet'}
                time_step_1  = c / Vinf / np_per;                                   % time step en secondes - physic
                time_step_2  = 1 / Freq / np_per;                                   % time step en secondes - actuator
                time_step    = min  (time_step_1 , time_step_2);                    % time step       based on the smallest time step 
                ratio        = max  (time_step_1 , time_step_2) / time_step;        % simulation time based on the largest  time step
                ratio        = round (ratio)
                fprintf(fid,'/define/models/unsteady-2nd-order yes \n');            % choix du solveur : O2 unsteady
                fprintf(fid,'/solve/set/time-step %g \n',time_step);                % choix du pas de temps
                data_sampling (fid, Fluent_ver);                                    % data sampling for unsteady statistics (Fluent 6.2.16 & 6.3.26)
                fprintf(fid,'/solve/dual-time-iterate %g %g\n\n',2*iter_sup*ratio,ndt);    % iterations unsteady
        end
end

%================================
% File of Actuation history
%================================
switch data_ac.ac
    case 'yes' 
    param.fid               = fid;
    param.name              = 'Vj';
    param.quantity          = 'velocity-magnitude';
    param.surface           = 'jet';
    param.filename          = strcat('"',str_file,'_ac_history.out"');
    param.fluent_version    = Fluent_ver; 
    switch str_URANS
        case 'RANS'
        param.steady            = 'yes';
        set_monitor_surface (param);
        case {'URANS','DES','LES','DNS'}
        param.steady            = 'no';
        set_monitor_surface (param);
    end
    case 'no'
    otherwise
end

% /report/wall-forces enlever le 5/04/15

%========================================================
% creations des fichiers resultats FLUENT *.cas et *.dat
%========================================================
fprintf(fid,'wc %s.cas yes\n', str_file);   % write file *.cas
fprintf(fid,'wd %s.dat yes\n', str_file);   % write file *.dat
%fprintf(fid,'wc %s.cas yes\n', 'fluent_journal');   % write file *.cas
%fprintf(fid,'wd %s.dat yes\n', 'fluent_journal');   % write file *.dat
%===========================================
% Grid, Contour, ... -> file.jpg
%===========================================
% Not useable if "fluent -g" is used
if (data_sim.anim.film == 1)
    camera_target_x     = c/2;
    camera_target_y     = 0;
    camera_target_z     = 0;
    width               = c;
    height              = c;
    fprintf(fid,'/display/grid \n\n');
    fprintf(fid,'/display/view/camera/target %g %g %g \n',camera_target_x,camera_target_y,camera_target_z);       % position camera
    fprintf(fid,'/display/view/camera/field  %g %g \n',width,height);                                             % zoom camera
    fprintf(fid,'/display/set/hardcopy/driver/tiff \n');                                                          % fichier *.jpg

    filename            = strcat('"',str_file,'_grid_z1.tif"');
    if exist(filename,'file') delete (filename);end
    fprintf(fid,'/display/hardcopy %s \n\n',filename);
    fprintf(fid,'yes \n\n'); % Ok to overwrite
    %===========================================
    % Pressure Contour, ... -> file.jpg
    %===========================================
    fprintf(fid,'/display/contour/pressure ,, \n');
    fprintf(fid,'/display/set/hardcopy/color/color \n');
    fprintf(fid,'/display/set/hardcopy/x-resolution 628 \n');
    fprintf(fid,'/display/set/hardcopy/y-resolution 522 \n');
    filename = strcat('"',str_file,'_pressure.tif"');
    if exist(filename,'file') delete (filename);end
    fprintf(fid,'/display/hardcopy %s \n\n',filename);
    fprintf(fid,'yes \n\n'); % Ok to overwrite : useless because the file has been deleted just before
end % if (data_sim.anim.film == 1)

%===========================================
% scalar(x,y) on surfaces -> file.txt 
%===========================================
zzz=0;
if zzz==1
    filename            = strcat('"',str_file,'_field.txt"');
    if exist(filename,'file')
        delete (filename);
    end
    %scalar              = 'y-velocity x-velocity pressure';            % x, y, p, Vx, Vy
    scalar              = 'skin-friction-coef pressure-coefficient';    % x, y, Cp, Cf
    fprintf(fid,'/file/export/ascii %s %s, no no %s, \n',filename,force_surface,scalar);
    fprintf(fid,'yes \n\n');
end
%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete ('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');

%if exist('finish.cas','file'); delete ('finish.cas');end
%fprintf(fid,'wc finish.cas yes\n');         % write file finish.cas

fprintf(fid,'exit yes \n');
fclose(fid);

fprintf(' %s created \n',fluent_journal_file);
if strcmp(data_num.Affichage,'yes'); fprintf(' openfoam_journal : end Ok \n'); end;

%===========================================
% Write files OpenFOAM
%===========================================

disp(filename_U);
fid = fopen(filename_U,'w')
fprintf(fid,UString);
fclose(fid);

fid = fopen(filename_k,'w')
fprintf(fid,kString);
fclose(fid);

fid = fopen(filename_kt,'w')
fprintf(fid,ktString);
fclose(fid);

fid = fopen(filename_kl,'w')
fprintf(fid,klString);
fclose(fid);

fid = fopen(filename_omega,'w')
fprintf(fid,omegaString);
fclose(fid);

fid = fopen(filename_intermittency,'w')
fprintf(fid,intermittencyString);
fclose(fid);

fid = fopen(filename_ReTheta,'w')
fprintf(fid,ReThetaString);
fclose(fid);

fid = fopen(filename_ControlDict,'w')
fprintf(fid,controlDictString);
fclose(fid);

fid = fopen(filename_FvSchemes,'w')
fprintf(fid,fvSchemesString);
fclose(fid);


fid = fopen(filename_TurbulenceProperties,'w')
fprintf(fid,turbulencePropertiesString);
fclose(fid);


fid = fopen(filename_TransportProperties,'w')
fprintf(fid,transportPropertiesString);
fclose(fid);


openfoam_journal_file = str_file;







