function [xu,yu,xl,yl,data_geo] = airfoil_geometry (data_geo,i)
%
%======================================================
%
% non dimensional airfoil geometry  (created or readed in filename)
%
% input  :  data_geo.method.type    method (naca4digits, naca6digits, ..., read_Javafoil, ...')
%           data_geo.filename{1}    filename
%           i                       (indice of the current cut for 3D sails or wings)
%
% output :  2D
%           geo_file                vertex file of the airfoil section
%           xu,yu,xl,yl             airfoil section geometry vectors
% output :  3D
%           geo_file                vertex file of the airfoil section
%           xu,yu,zu,xl,yl,zl       airfoil section geometry vectors
%
% Methods : naca4digits             (xu,yu,xl,yl,geo_file) = f(data_geo.naca4digits)
%           naca6digits             (xu,yu,xl,yl,geo_file) = f(data_geo.naca6digits)
%           AC45                    (xu,yu,xl,yl,geo_file) = f(data_geo.naca4digits)
%           multi-elts airfoil      (xu,yu,xl,yl,geo_file) = f()
%           read_Javafoil           (xu,yu,xl,yl)          = f(geo_file)  
%           read_UIUC_4_Gambit      (xu,yu,xl,yl)          = f(geo_file)
%           read_UIUC_4_ICEM        (xu,yu,xl,yl)          = f(geo_file)
%           bspline                 data_geo               = f(data_geo)
%           wingbio                 data_geo               = f(data_geo)
%           dragonfly               data_geo               = f(data_geo)
%           deflector
%           read_n_vertex
%           sail_2D
%           sail_3D
%           wing_3D
%           blade_3D
%           NURBS_Airfoil_4
%           NURBS_Airfoil
%======================================================
xu=[];yu=[];xl=[];yl=[];
xu=0;yu=0;xl=0;yl=0;

%if strcmp(data_num.debug,'yes');fprintf(' airfoil_geometry : start \n');end

switch data_geo.method.type        % choice geometry method
    
    case 'naca_4digits'             % create naca -> geo.dat, geo=bdf.dat, geoJ.dat
        m = data_geo.naca4digits.m(1);
        p = data_geo.naca4digits.p(1);
        t = data_geo.naca4digits.t(1);
        geo_file                    = data_geo.filename{1};                                    % 'geo.dat'
       %data_geo.method.str_geo     = ['naca',num2str(100*m),num2str(10*p),num2str(100*t)]; % 'nacaxxxx' % 27/01/12 %
        data_geo.method.str_geo     = ['naca',num2str(round(100*m)),num2str(round(10*p)),num2str(round(100*t))]; % 'nacaxxxx'
        h=0.003; L=0.1; r=0.1;
        h=0.000; L=0.1; r=0.1;
        [xu,yu,xl,yl]               = naca_4digits      (m,p,t,geo_file,h,L,r);             % create geo.dat, geo-bdf.dat, geoJ.dat
        fileName = 'profile.dat';
        file = fopen(fileName, 'w');
        xu_mod = fliplr(xu);
        yu_mod = fliplr(yu);
        yu_mod(1) = 0
        xl_mod = xl
        yl_mod = yl
        yl_mod(length(yl_mod)) = 0
        for i=1:length(xu_mod)
            fprintf(file, '%s %s \n', num2str(xu_mod(i)), num2str(yu_mod(i)));
        end
        for i=2:length(xl_mod)
            fprintf(file, '%s %s \n', num2str(xl_mod(i)), num2str(yl_mod(i)));
        end

        fclose(file);
        
        
    
        %       [xu,yu,xl,yl]               = naca_4digits      (m,p,t,geo_file);                   % create geo.dat, geo-bdf.dat, geoJ.dat
%       [xu,yu,xl,yl]               = airfoil_rw_VLab   ('write','geoV.txt',xu,yu,xl,yl)    % ?.
%       airfoil_V2J                 ('geoJ.txt',xu,yu,xl,yl)                                % ?.
        
    case 'naca_5digits'             % create naca -> geo.dat
        disp(' NACA 5 digits is not yet implemented');
        
    case 'naca_6digits'             % create naca -> geo.dat
        S   = data_geo.naca6digits.S;
        A   = data_geo.naca6digits.A;
        CIR = data_geo.naca6digits.CIR;
        CI  = data_geo.naca6digits.CI;
        t   = data_geo.naca6digits.t;
        geo_file                    = data_geo.filename{1};                                    % 'geo.dat'
        data_geo.method.str_geo     = ['naca',num2str(S),num2str(10*A),num2str(10*CIR),num2str(10*CI),num2str(100*t)];
        [xu,yu,xl,yl]               = naca_6digits      (S,A,CIR,CI,t,geo_file);    % NACA 6 digits geometry creation
        %disp('data_geo.naca6digits');data_geo.naca6digits
        
    case 'read_Javafoil'            % read airfoil in Javafoil format and create a vertex file
        geo_file                    = data_geo.filename{1};                                     % 'naca0012_flap2010.txt'
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   % filename without extension (.txt or .dat)
        geo_file_J                  = strcat(geo_file(1:size(geo_file,2)-4),'.dat');            % ?
        [xu,yu,xl,yl]               = airfoil_read      (geo_file_J,'rw');                      % read airfoil in Javafoil format and write it in VLab format
        %fprintf(' airfoil_geometry : read_Javafoil : end\n');
        
    case 'read_UIUC_4_Gambit'       % read airfoil in Javafoil format and create a vertex file for GAMBIT
        geo_file                    = data_geo.filename{1};                                     % 'naca0012_flap2010.txt'
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   % filename without extension (.txt or .dat)
        uiuc_to_javafoil(geo_file); % transform UIUC.dat to Javafoil format and check if bdf is available (if yes: Javafoil format includes bdf!)
        geo_file_J                  = strcat(geo_file(1:size(geo_file,2)-4),'J.dat');
        [xu,yu,xl,yl]               = airfoil_read      (geo_file_J,'rw');                      % read airfoil in Javafoil format and write it in VLab format for Gambit
        
    case 'read_UIUC_4_ICEM'          % read airfoil in UIUC format and create a vertex file for ICEM
        geo_file                    = data_geo.filename{1};                                     % 'rae2822.dat'
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   % filename without extension (.txt or .dat)
        data_geo                    = sort_UIUC(geo_file,data_geo);                             % read and sort UIUC airfol data
        geo_ICEM(data_geo);         % create airfoil file in ICEM, Javafoil, XFOIL format : data_geo.Points => files
        [~, q]                      = min(data_geo.Points(:,1));                                                                                                           
        data_geo.nosePoint          = q-1;
        data_geo.maxLeft            = abs(min(data_geo.Points(:,1)));
        data_geo.maxRight           = abs(max(data_geo.Points(:,1)));
        data_geo.maxUp              = abs(max(data_geo.Points(:,2)));
        data_geo.maxDown            = abs(min(data_geo.Points(:,2)));
        
    case 'b-spline'                 % create geometry based on UIUC airfoil and bspline parametrization
        geo_file                    = data_geo.filename{1};                                     
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   
        data_geo                    = bspline(geo_file,data_geo);           % create 2 bsplines to approximate airfoil geometry

    case 'b-spline-2'               % create geometry based on UIUC airfoil and bspline parametrization
        geo_file                    = data_geo.filename{1};                                     
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   
        data_geo                    = bspline(geo_file,data_geo);           % create 2 bsplines to approximate airfoil geometry
        geo_file                    = data_geo.filename{2};                                     
        data_geo.method.str_geo     = data_geo.filename{2}(1:size(data_geo.filename{2},2)-4);   
        data_geo                    = bspline(geo_file,data_geo);           % create 2 bsplines to approximate airfoil geometry
        
    case 'wingBio'                  % create 3d geometry based on UIUC airfoil sections and bspline parametrization
        geo_file                    = [];                                     
        data_geo.method.str_geo     = data_geo.wingName;   
        data_geo.method.section     = [];
        data_geo                    = geoWing(data_geo); % 3D wing creation
        
    case 'dragonfly'                % create dragonfly vertex file -> geo.dat
        geo                         = data_geo.dragonfly.geo;                                   % vector of the geometry coordinates (x1,y1, ..., x6,y6)
        geo_file                    = data_geo.filename{1};                                     % geometry filename
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);   % filename without extension (.txt or .dat)
                                    dragonfly (geo,geo_file);                                   % create dragonfly vertex file
        
    case 'AC45'                     % create naca -> geo.dat
        % create airfoil 1
        m = data_geo.naca4digits.m(1);
        p = data_geo.naca4digits.p(1);
        t1= data_geo.naca4digits.t(1);
        geo_file                    = data_geo.filename{1};                                 % 'geo1.dat'
        [xu,yu,xl,yl]               = naca_4digits      (m,p,t1,geo_file,0,0,0);            % create geo1.dat, geo1-bdf.dat, geo1J.dat
        % create airfoil 2
        m = data_geo.naca4digits.m(2);
        p = data_geo.naca4digits.p(2);
        t2= data_geo.naca4digits.t(2);
        geo_file                    = data_geo.filename{2};                                 % 'geo2.dat'
        [xu,yu,xl,yl]               = naca_4digits      (m,p,t2,geo_file,0,0,0);            % create geo2.dat, geo2-bdf.dat, geo2J.dat
        % geometry name
        data_geo.method.str_geo     = [data_geo.method.str_geo,num2str(100*t1),num2str(100*t2)];        % 'nacaxxxx'
        
    case 'AC72'                     % create naca -> geo.dat
        % create airfoil 1
        m = data_geo.naca4digits.m(1);
        p = data_geo.naca4digits.p(1);
        t1= data_geo.naca4digits.t(1);
        geo_file                    = data_geo.filename{1};                                 % 'geo1.dat'
        [xu,yu,xl,yl,x_bdf]               = naca_4digits_SCCM  (m,p,t1,geo_file,0,0,0);            % create geo1.dat, geo1-bdf.dat, geo1J.dat
        data_geo.bdf_1 = x_bdf;
        % create airfoil 2
        m = data_geo.naca4digits.m(2);
        p = data_geo.naca4digits.p(2);
        t2= data_geo.naca4digits.t(2);
        geo_file                    = data_geo.filename{2};                                 % 'geo2.dat'
        [xu,yu,xl,yl,x_bdf]               = naca_4digits_SCCM  (m,p,t2,geo_file,0,0,0);            % create geo2.dat, geo2-bdf.dat, geo2J.dat
        data_geo.bdf_2 = x_bdf;
        % geometry name
        data_geo.method.str_geo     = [data_geo.method.str_geo,num2str(100*t1),num2str(100*t2)];        % 'nacaxxxx'
        
    case 'deflector'                % create deflector vertex file -> geo.dat
        geo                         = data_geo.dragonfly.geo;                           % vector of the geometry coordinates (x1,y1, ..., x6,y6)
        geo_file                    = data_geo.filename{1};                             % geometry filename
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4); % filename without extension (.txt or .dat)
        deflector (geo,geo_file);                                                       % create vertex file

    case 'CROR_3D'                     % CROR (x1,y1, ...,x6,y6)
        geo_file                    = data_geo.filename{1};                                % geometry filename
        data_geo.method.str_geo     = data_geo.bladeName;
        data_geo = geoCROR(data_geo);                                                      % create CROR
        
    case 'read_n_vertex'            % create n-vertex file from his geometrical definition (x1,y1, ...,xn,yn)
        geo_file                    = data_geo.filename{1};                                    % data_geo.filename{1} read directly in the journal GAMBIT
        data_geo.method.str_geo     = data_geo.filename{1}(1:size(data_geo.filename{1},2)-4);  % filename without extension (.txt or .dat)
        
    case 'sail_2D'                  % create sail 2D vertex file from geometrical definition (f,Xf,...) - to finish
        z_cut                       = data_geo.cut.var(i,1);                                % z cut      value
        fsc                         = data_geo.cut.var(i,2);                                % cut camber value
        chord                       = data_geo.cut.var(i,3);                                % cut chord  value
        theta                       = data_geo.cut.var(i,4)*pi/180;                         % cut trim in radian
        xfsc                        = data_geo.cut.var(i,5);                                % cut draft  value
        [xu,yu,r1,r2,s_sail,alpha_i,alpha_o] = dac(fsc,xfsc,75);                            % create sail section in z cut
        geo_file                    = strcat(data_geo.filename{1},'_z',num2str(i),'.txt');     % filename of the geometry in z cut
        data_geo.method.str_geo     = data_geo.filename{1};                                    % filename without extension (.txt or .dat)
        zu                          = ones(length(xu),1)*z_cut;                             % z-cut value
        [xu,yu]                     = rot       (xu,yu,theta);                              % rotation
        xu                          = xu*chord;                                             % scale
        yu                          = yu*chord;                                             % scale
        write_sail_2D               (xu,yu,zu,geo_file);                                    % create a vertex file in data_geo.filename{1} from data_geo.cut.var with dac.m
        xl=0;yl=0;
        data_geo.x_max              = max(data_geo.x_max,max(xu));                          % real size in x of the sail
        data_geo.y_max              = max(data_geo.y_max,max(yu));                          % real size in y of the sail
        data_geo.z_max              = max(data_geo.z_max,max(zu));                          % real size in z of the sail
        data_geo.x_min              = min(data_geo.x_min,min(xu));                          % real size in x of the sail
        data_geo.y_min              = min(data_geo.y_min,min(yu));                          % real size in y of the sail
        data_geo.z_min              = min(data_geo.z_min,min(zu));                          % real size in z of the sail

    case 'sail_3D'                  % create sail 2D vertex file from geometrical definition (z1,camber1, ...,z6,camber6)
        z_cut                       = data_geo.cut.var(i,1)                                % z cut      value
        fsc                         = data_geo.cut.var(i,2);                                % cut camber value
        chord                       = data_geo.cut.var(i,3);                                % cut chord  value
        theta                       = data_geo.cut.var(i,4)*pi/180;                         % cut trim in radian
        xfsc                        = data_geo.cut.var(i,5);                                % cut draft  value
%        vector                      = [data_geo.cut.var(i,5) ; 0];                         % cut translation vector for jib
%        str_cut                     = num2str(data_geo.cut.var (1,i,1));
        [xu,yu,r1,r2,s_sail,alpha_i,alpha_o] = dac(fsc,xfsc,75);                            % create sail section in z cut
        geo_file                    = strcat(data_geo.filename{1},'_z',num2str(i),'.txt');  % filename of the geometry in z cut
        data_geo.method.str_geo     = data_geo.filename{1};                                 % filename without extension (.txt or .dat)
        zu                          = ones(length(xu),1)*z_cut;                             % z-cut value
        [xu,yu]                     = rot       (xu,yu,theta);                              % rotation
%        [xu,yu]                     = translate (xu,yu,vector);                             % translation for headstay
        xu                          = xu*chord;                                             % scale
        yu                          = yu*chord;                                             % scale
        write_sail_2D               (xu,yu,zu,geo_file);                                    % create vertex file in data_geo.filename{1} from data_geo.cut.var with dac.m
        xl=0;yl=0;
% sail volume
        data_geo.x_max              = max(data_geo.x_max,max(xu));                     % real size in x of the sail
        data_geo.y_max              = max(data_geo.y_max,max(yu));                     % real size in y of the sail
        data_geo.z_max              = max(data_geo.z_max,max(zu));                     % real size in z of the sail
        data_geo.x_min              = min(data_geo.x_min,min(xu));                     % real size in x of the sail
        data_geo.y_min              = min(data_geo.y_min,min(yu));                     % real size in y of the sail
        data_geo.z_min              = min(data_geo.z_min,min(zu));                     % real size in z of the sail

    case 'wing_3D'                  % create wing 2D vertex file from geometrical definition (z1,camber1, ...,z6,camber6)
        z_cut                       = data_geo.cut.var(i,1);                               % cut z      value
        fsc                         = data_geo.cut.var(i,2);                               % cut camber value
        chord                       = data_geo.cut.var(i,3);                               % cut chord  value
        theta                       = data_geo.cut.var(i,4)*pi/180;                        % cut trim   value in radian
        xfsc                        = data_geo.cut.var(i,5);                               % cut draft  value
        
    case 'blade_3D'                 % create wing 2D vertex file from geometrical definition (z1,camber1, ...,z6,camber6)
        z_cut                       = data_geo.cut.var(i,1);                               % z cut      value
        fsc                         = data_geo.cut.var(i,2);                               % cut camber value
        chord                       = data_geo.cut.var(i,3);                               % cut chord  value
        theta                       = data_geo.cut.var(i,4)*pi/180;                        % cut trim in radian
        xfsc                        = data_geo.cut.var(i,5);                               % cut draft  value

    case 'NURBS_Airfoil_4'
        
        x2 = data_geo.NURBS_Airfoil_4.x2;
        x6 = data_geo.NURBS_Airfoil_4.x6;
        y1 = data_geo.NURBS_Airfoil_4.y1;
        y2 = data_geo.NURBS_Airfoil_4.y2;
        y6 = data_geo.NURBS_Airfoil_4.y6;
        y7 = data_geo.NURBS_Airfoil_4.y7;
        k1 = data_geo.NURBS_Airfoil_4.k1;
        k4 = data_geo.NURBS_Airfoil_4.k4;
               
        geo_file                    = data_geo.filename{1};                                    % 'geo.dat'
        data_geo.method.str_geo     = ['NURBS_Airfoil_4', num2str(x2) , num2str(x6), num2str(y1), num2str(y2), num2str(y6),num2str(y7)];      % 'NURBS ( x2,x3...)'
        [xu,yu,xl,yl]               = NURBS_Airfoil_4 (x2,x6,y1,y2,y6,y7,k1,k4,geo_file);  % create geo.dat, geo-bdf.dat, geoJ.dat
        
    case 'NURBS_Airfoil'
                           
        x2 = data_geo.NURBS_airfoil.x2;
        x3 = data_geo.NURBS_airfoil.x3;
        x5 = data_geo.NURBS_airfoil.x5;
        x6 = data_geo.NURBS_airfoil.x6;
        y1 = data_geo.NURBS_airfoil.y1;
        y2 = data_geo.NURBS_airfoil.y2;
        y3 = data_geo.NURBS_airfoil.y3;
        y5 = data_geo.NURBS_airfoil.y5;
        y6 = data_geo.NURBS_airfoil.y6;
        y7 = data_geo.NURBS_airfoil.y7;
        k1 = data_geo.NURBS_airfoil.k1;
        k2 = data_geo.NURBS_airfoil.k2;
        k3 = data_geo.NURBS_airfoil.k3;
        k4 = data_geo.NURBS_airfoil.k4;
                    
        geo_file                    = data_geo.filename{1};                                    % 'geo.dat'
        data_geo.method.str_geo     = ['NURBS_Airfoil', num2str(k1) , num2str(k2) , num2str(k3) , num2str(k4)];      % 'NURBS ( x2,x3...)'
        [xu,yu,xl,yl]               = NURBS_Airfoil (x2,x3,x5,x6,y1,y2,y3,y5,y6,y7,k1,k2,k3,k4,geo_file);  % create geo.dat, geo-bdf.dat, geoJ.dat

    otherwise
        fprintf(' airfoil_geometry : no method available to create geometry for method : %s \n',data_geo.method.type);

end

fprintf(' airfoil_geometry : %s created with method %s \n',data_geo.method.str_geo,data_geo.method.type);

if exist(geo_file,'file')
    switch data_geo.method.type
        case {'naca_4digits','naca_6digits','dragonfly','sail_2D','sail_3D','deflector','CROR'}
            fprintf(' airfoil_geometry : %s created with method %s \n',geo_file,data_geo.method.type);   % Z! : not geo.dat but geoJ.dat
        case {'read_Javafoil','read_UIUC_4_Gambit','read_UIUC_4_ICEM','read_n_vertex'}
            fprintf(' airfoil_geometry : %s file %s readed \n',data_geo.method.type,geo_file);
    end
else
    fprintf(' airfoil_geometry : no file %s \n',data_geo.method.type,geo_file);
end
fprintf(' airfoil_geometry : end \n');



