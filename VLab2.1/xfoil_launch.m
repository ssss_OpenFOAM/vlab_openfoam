function [] = xfoil_launch (os,input_file,output_file)
% Run XFOIL
outputscreenfile = 'xfoil_screen.dat';

switch os
    case 'Linux'
        if exist('fort.5'  ,'file');  delete ('fort.5')  ;    end 	% Z! Linux only
        if exist('fort.6'  ,'file');  delete ('fort.6')  ;    end	% Z! Linux only
        if exist(output_file,'file');  delete (output_file);    end	% Z! Linux only
        %if exist(input_file,'file');  delete (input_file);    end	% Z! Linux only
    case 'Windows'
	system('del xfoil-output.dat'); 	% Z! Windows only
end

switch os
    case 'Linux'
        copyfile(input_file,'fort.5');          % necessary on Linux !
        command = sprintf('xfoil < %s > %s',input_file,outputscreenfile)
        system(command);
        %copyfile('fort.6',outputscreenfile);    % necessary on Linux !
        
    case 'Windows'
        runfile = 'run_test.bat';
        %- create run file (.bat):  ---------------------- 
        fid = fopen(runfile, 'wt'); 
        command = ['xfoilP4 < ',input_file,' > ',outputscreenfile]
        %command = ['xfoilP4 < ',input_file,' > ',output_file]; 
        fprintf(fid,'%s\n',command); 
        fclose(fid); 
        %- Execute run file ------------------------------ 
        %system('cd (directory where XFOIL is installed)'); 
        system(runfile);
end