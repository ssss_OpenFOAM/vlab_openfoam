function [fluent_journal_file] = fluent_jou_comp (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num)
%
% Change define/material properties to ideal-gas
% Change define/boundary conditions to pressure-far-field    hyp: inlet, outlet, up, down
% Change define/model/solveur   (pressure-based, density-based) (green-gauss cell-based, gree-gauss node-based)
% Change solve/control/solution scheme order    (O1, O2, O3)
% Change solve/control/solution p-v coupling    (simple, simplec, coupled) (AUSM, ...)
% Add energy equation   (automatic)

Fluent_ver  = data_num.Fluent_ver;

if strcmp(data_num.Affichage,'yes'); fprintf(' fluent_jou_comp : version FLUENT %s \n',Fluent_ver);end;

c                   = data_geo.c;
b                   = data_geo.b;
Lref                = c;
Sref                = b*c;

Vinf                = data_sim.Vinf;
inc                 = data_sim.inc;
I_turb              = data_sim.I_turb;
L_turb              = data_sim.L_turb;
scheme_order        = data_sim.scheme_order;
str_file            = data_sim.str_file;
regime              = data_sim.regime;
dim                 = data_sim.dim;
Fluid_model         = data_sim.Fluid_model;
Mach_number         = data_sim.Mach_number;
solver              = data_num.solver;          % numerical scheme : {'pressure-based','density-based-explicit','density-based-implicit'} 

force_surface       = data_topo.force_surface;
surf_bc             = data_topo.surf_bc;

fluent_journal_file = 'fluent_comp.jou';
fid=fopen(fluent_journal_file,'w+');

filename = 'fluent_jou_comp.trn';
if exist(filename,'file')==2;delete (filename);end
fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ? - not useful

fprintf(fid,'rc %s.cas\n', str_file); % Lecture du fichier *.cas
fprintf(fid,'rd %s.dat\n', str_file); % Lecture du fichier *.dat

%============================================
% Change Fluid Model to ideal-gas
%============================================
Density = 'yes';
Method  = 'ideal-gas';
switch Fluent_ver
    case '6.3.26'
        fprintf(fid,'/define/materials/change-create/air, %s %s no no no no no no no no no \n\n',Density,Method); % ideal-gas
    case {'13.0.0','14.0.0','14.5.0','14.5.7'}
        fprintf(fid,'/define/materials/change-create/air, %s %s no no no no no no \n\n',Density,Method); % ideal-gas
    otherwise
        disp(' fluent_jou_comp : not yet implemented. Do it !');
end
%==========================================================
% Change BC type from velocity-inlet to pressure-far-field
%==========================================================
for i = 1 : size(surf_bc,1)
    switch surf_bc{i,2}
        case 'velocity_inlet'
            Type = 'pressure-far-field';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
            param.Fluent_ver    = Fluent_ver;
            param.regime       	= regime;               % 'laminar', 'turbulent'
            param.dim       	= data_sim.dim;         % '2D', '3D'
            param.fid       	= fid;
            param.Vinf          = Vinf;
            param.Vx            = cos(inc*pi/180);      % X-Component of Flow Direction [0.98952579]
            param.Vy            = sin(inc*pi/180);      % Y-Component of Flow Direction [0.1443562]
            param.Vz            = 0;                    % Z-Component of Flow Direction [0]
            param.I_turb        = I_turb;               % Turbulence Intensity (%) [1]
            param.L_turb        = L_turb;               % Turbulence Length Scale (m) [0.005]
            param.Fluid_model   = Fluid_model;          % 'incompressible' 'compressible'
            param.surf_bc_name  = surf_bc{i,1};
            bc_pressure_far_field (param);
        case 'pressure_outlet-toto'     % not necessary => FLUENT plante !
            Type = 'pressure-far-field';
            fprintf(fid,'/define/boundary-conditions/zone-type %s %s \n\n',surf_bc{i,1},Type);
            param.Fluent_ver    = Fluent_ver;
            param.regime       	= regime;
            param.dim       	= data_sim.dim;
            param.fid       	= fid;
            param.Vinf          = Vinf;
            param.Vx            = cos(inc*pi/180);      % X-Component of Flow Direction [0.98952579]
            param.Vy            = sin(inc*pi/180);      % Y-Component of Flow Direction [0.1443562]
            param.Vz            = 0;                    % Z-Component of Flow Direction [0]
            param.I_turb        = I_turb;               % Turbulence Intensity (%) [1]
            param.L_turb        = L_turb;               % Turbulence Length Scale (m) [0.005]
            param.surf_bc_name  = surf_bc{i,1};
            bc_pressure_far_field (param);
        case 'wall'
        case 'symmetry'
        otherwise
    end
end

%===========================
% Solver : pressure-based / density-based-implicit
%===========================
if strcmp(solver,'density-based-implicit');
    fprintf(fid,'/define/models/solver %s y \n\n',solver); % density-based-implicit scheme
    disp(' fluent_jou_comp : solver = density-based-implicit');
end

%===============================
% initialize fields
%===============================
fprintf(fid,'/solve/initialize/compute-defaults/pressure-far-field inlet \n\n');    % Ok 6.3, 13
fprintf(fid,'/solve/initialize/initialize-flow\n');                                 % Initialize 
fprintf(fid,'/solve/iterate %g\n\n', 10); % some iterations

%===============================
% Check conv, Spatial schemes
%===============================
check_conv      (fid,data_sim);
spatial_schemes (fid,data_sim,data_num);

%======================================
% Ecriture des fichiers *.cas et *.dat
%======================================
%if (data_sim.save.cas == 1)
    fprintf(fid,'wc %s.cas\n', str_file); % Ecriture du fichier *.cas
    fprintf(fid,'yes\n');
    fprintf(fid,'wd %s.dat\n', str_file); % Ecriture du fichier *.dat
    fprintf(fid,'yes\n');
%end

%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete ('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');

%fprintf(fid,'wc finish.cas yes \n'); % Write finish.cas

fprintf(fid,'exit yes \n');
fclose(fid);

if strcmp(data_num.Affichage,'yes'); fprintf(' fluent_jou_comp : end Ok \n\n'); end;

