function [xu,yu,xl,yl] = airfoil_read (filename,str_rw)
%
% ==================================
% Read  airfoil geometry in JAVAFOIL format (bdf, extrados, bda, intrados, bdf)
% Write airfoil in xu,yu,xl,yl
%
% input : filename
%
% output : xu,yu,xl,yl in geoV.dat & geoJ.dat
%
% ==================================

% =================================================================================================================
% read airfoil in filename (format Javafoil : bdf > extrados > bda > intrados > bdf, default number of points = 61)
% =================================================================================================================
fprintf(' airfoil_read : file %s readed (JAVAFOIL format) \n',filename);
fid     = fopen    (filename,'r');
[x,y]   = textread (filename,'%f %f','headerlines',1);  % begin on line 2

switch mod(length(x),2)         % nombre de point pair / impair ?
    case 1              % impair
N       = length(x);    % 61
NS2     = (N-1)/2;      % 30
start_l = NS2+1;        % 31
end_l   = N;            % 61
end_u   = NS2+1;        % 31
start_u = 1;            % 1
    case 0              % pair
N       = length(x);    % 60
NS2     = (N-2)/2;      % 29
start_l = NS2+1;        % 30
end_l   = N;            % 60
end_u   = NS2+1;        % 30
start_u = 1;            % 1
end

%====================================
% lower surface from bda to bdf
%====================================
ind = 0;
for i=start_l:end_l     % from 31 to 61 = from bda to bdf on lower surface
    ind = ind + 1;
    xl   (ind) = x (i);
    yl   (ind) = y (i);
end
%====================================
% upper surface from bdf to bda
%====================================
ind = 0;
for i=start_u:end_u     % from  1 to 31 = from bdf to bda on upper surface
    ind = ind + 1;
    xu   (ind) = x (end_u-i+1);
    yu   (ind) = y (end_u-i+1);  
end
fclose(fid);

% =====================
% read or read & write
% =====================
switch str_rw
    case 'r'
        fprintf(' airfoil_read : nothing done !!! \n');
    case 'rw'
        %==============================================
        % geoV.dat : airfoil without bdf (format VLab)
        %==============================================
        geo_file    = 'geoV.dat';
        fid         = fopen(geo_file,'w');
        fprintf(fid,' airfoil writed in VLab format in %s \n',geo_file);    % ligne de commentaires
        for i=1:length(xu)
            fprintf(fid,'%f %f \n', xu(i), yu(i));   % upper surface from bda to bdf-1    (0 < xu < 0.99?)
        end
        for i=2:length(xl)
            fprintf(fid,'%f %f \n', xl(i), yl(i));   % lower surface from bda+1 to bdf-1  (0.0? < xl < 0.99?)
        end
        fclose(fid);
        fprintf(' airfoil_read : file %s writed \n',geo_file);
        %==================================================
        % geoJ.dat : airfoil without bdf (format Javafoil)
        %==================================================
        file    = 'geoJ.dat';
        fid     = fopen(file,'w');
        fprintf(fid,' airfoil writed in Javafoil format in %s \n',file);    % ligne de commentaires
        for i=length(xu) : -1 : 1
            fprintf(fid,'%f %f \n', xu(i), yu(i));   % extrados : bdf > bda
        end
        for i=2:length(xl)
            fprintf(fid,'%f %f \n', xl(i), yl(i));   % intrados : bda > bdf
        end
        fclose(fid);
        fprintf(' airfoil_read : file %s writed \n',file);
end     % switch str_rw
%==================================================
% plot airfoil
%==================================================
zzz=1;
if zzz==1
    disp(' airfoil_read : figure 382')
    figure(382)
    plot(xu,yu,'-b'); hold on;
    plot(xl,yl,'-r');
    axis([0 1 -0.5 0.5]);
    str = strcat('airfoil-read.m');
    title(str);
    hold off;
end

