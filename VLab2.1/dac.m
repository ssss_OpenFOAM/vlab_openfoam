function [X_v,Y_v,r1,r2,s_sail,alpha_i,alpha_o] = dac(f,Xf,npts)
%
% last updated : 27/05/12
% [X_v,Y_v,r1,r2,s_sail,alpha_i,alpha_o] = dac(f,Xf,npts)
% profil double arc de cercle de corde 1 align� avec l'axe x
% data
% f     : airfoil camber
% Xf    : position of the max camber
% results
% r1    : rayon du cercle amont
% r2    : rayon du cercle aval
% yc1   : coordonn�e du centre du cercle amont
% yc2   : coordonn�e du centre du cercle amont

% Amont du profil dac

yc1 = - (Xf^2 - f^2)/(2*f);
r1  =   f - yc1;

% Aval du profil dac

yc2 = - (1-2*Xf+Xf^2-f^2)/(2*f);
r2  =   f - yc2;

% vecteur des abcisses X_v
%X_v     = [0:1/npts:1 1.05];
X_v     = [0:1/npts:1];
length  = size(X_v,2);

% vecteur des ordonnees Y_v du profil dac
for i=1 : length
    if X_v(i)<= Xf
        Y_v(i)=sqrt(r1^2-(X_v(i)-Xf)^2)+yc1;    % amont du creux
    else
        Y_v(i)=sqrt(r2^2-(X_v(i)-Xf)^2)+yc2;    % aval du creux
    end
end

% inlet & outlet angles
dy = Y_v(2)-Y_v(1);
dx = X_v(2)-X_v(1);
alpha_i = atan(dy/dx)*180/pi;
dy = Y_v(length)-Y_v(length-1);
dx = X_v(length)-X_v(length-1);
alpha_o = atan(dy/dx)*180/pi;
% sail_surface
s       = curve_length (X_v,Y_v);
s_sail  = s(length);                % sail surface