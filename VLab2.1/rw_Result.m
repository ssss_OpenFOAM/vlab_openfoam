function [Result] = rw_Result (filename, fid, str_case, Result, str_rw)
%
% filename  = Cases_Coeff_mean.out
% str_case  = Naca0012_v10_i4_RANS2D_SA_x (name of the case)
% Result    = [inc Cl_mean Cd_mean Cl_rms Cd_rms Cm_mean xcp Cx Cy Cq Cmu q_jet Err_Clmean Err_Cdmean Cl_mean2 Cd_mean2 Fx1 Fx2 Mx1 Mx2 eta1 eta2]
% str_rw    : what should be done in a 4 choices todo list (open_write, write, open_read, read)
%
% used by : VLab2.m, valid_0.m
%

fprintf(' rw_Result : %s\n',str_rw)
%size(Result)

switch str_rw
    case 'open_write'                           % open file in append mode and write one more line (1 case)
        fid = fopen(filename,'a');
        fprintf(fid,'%-10.5g ', Result  );      % write vector Result
        fprintf(fid,'%70s'    , str_case);      % write str_case string
        fprintf(fid,'\n');                      % go to next line
        fclose(fid);                            % close file
        fprintf('  open & write %s\n',filename);
    case 'write'
        fprintf(fid,'%-10.5g ', Result  );
        fprintf(fid,'%70s'    , str_case);
        fprintf(fid,'\n');
        fprintf('  write %s\n',filename);
    case 'open_read'
        fid = fopen(filename,'r');
        line1       = fgetl     (fid)          % read first line
        line2       = textscan  (fid,'%s',1)   % read str_case
        line3       = textscan  (fid,'%f',size(Result,2))  % read Result values
        line2{1};
        line3{1};
        str_case = line2{1};
        Result   = line3{1};
        fclose (fid);
        fprintf('  open & read %s\n',filename);
    case 'read'
        fprintf(' rw_Result.m : not yet implemented\n' );
end
fprintf(' rw_Result : end\n')
