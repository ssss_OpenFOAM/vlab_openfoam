function [cas,var,str_var] = data560 (x,data_sim)
% BE2 Naca 0012 High Reynolds RANS modeling
%============================
% Implemented cases
%============================
%cas = 0;    % unstructured mesh                                    > debug, demo
%cas = 1;    %   structured mesh                                    > debug, demo
%cas = 3;    % mesh convergence unstructured                        > Ok   5min PC_SLC
%cas = 4;    % mesh convergence   structured                        > Ok   5min PC_SLC
%=========================================
% Parameters definition
%=========================================
% NACA      select a NACA 4 digits airfoil
% Vinf      Freestream velocity (m/s)
% inc       Angle of attack (deg)
%=========================================
% structured mesh       unstructured mesh
%=========================================
% m_algo = 1            m_algo = 0              % mesh algo ( 1 = structured mesh, 0 = unstructured mesh )
% n_cl   = 4            unused                  % number of cells in the inner zone (for Boundary Layer)
% s_bda  = 1e-2         1e-2                    % streamwise cell size along the airfoil surface
% h1     = 2e-3         unused                  % normal     cell size of the first cell on the wall
% g_i    = unused       1.3                     % normal     cell size growthrate in the inner zone
% g_o    = 1.3          1.3                     % normal     cell size growthrate in the outer zone
%=========================================
% Physics and numerics
%=========================================
% Iterations            N_stop                  % Maximun number of iterations
% Turbulence modeling   Tu_mod = 0,1,2          % 0 = SA, 1 = k-w SST, 2 = k-w SSTtr
% Turbulence scales     I_turb, L_turb          % Turbulence level and turbulent length scale      
% Scheme Order          Oschem = 1, 2, 3        % 1 = O1, 2 = O2, 3 = O3
% Domain Size           Dsize = 5, 10, ...      % Number of chords in each directions
% Fluid Model           Fluid = 0, 1            % 0 = incompressible, 1 = compressible
% Solver                solver = 0, 1, 2        % 0 = pressure-based, 1 = density-base-implicit, 2 = density-base-explicit
%=========================================
cas = 0;

%Pedir Reynolds en lugar de Vinf

str_var     = [ '  NACA' '  Vinf' '   inc' 'm_algo' '    h1' '  n_cl' ' s_bda' '   g_i' '   g_o' 'N_stop' 'Tu_mod' 'I_turb' 'L_turb' 'Oschem' ' Dsize' ' Fluid' 'solver';
                '  4412' '     1' '     0' '     0' '  1e-3' '    10' '  1e-2' '   1.1' '   1.3' '    50' '     0' '   1.0' '  5e-3' '     2' '    20' '     0' '     0'];

%=========================================
% Nothing to change below
%=========================================
var = str2var (str_var, x, 6);
  
var.n_cas;
var.name
var.value
