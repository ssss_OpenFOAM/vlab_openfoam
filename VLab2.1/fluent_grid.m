function [fluent_journal_file] = fluent_grid (os,mesh_file,data_geo,data_sim,data_num)
%
% Lancement des calculs sous FLUENT au premier et second ordre
%
% Affectation des variables locales
% Define boundary conditions type and values    hyp: inlet, outlet, up, down
% Solveur
% Grandeurs de reference
% Efforts                                       hyp: bda, extrados, intrados
% Actuation                                     hyp: jet
% Iterations RANS/URANS
% Mean velocity magnitude on wall surface (old)
% Creations des fichiers resultats FLUENT *.cas et *.dat

Fluent_ver  = data_num.Fluent_ver;

if strcmp(data_num.Affichage,'yes'); fprintf(' fluent_grid : version FLUENT %s \n',Fluent_ver);end;

c                   = data_geo.c;
b                   = data_geo.b;
Lref                = c;Lref
Sref                = b*c;Sref

str_file            = data_sim.str_file;

fluent_journal_file = 'fluent_grid.jou';
fid=fopen(fluent_journal_file,'w+');

%filename = 'fluent_grid.trn';
%if exist(filename,'file')==2;delete (filename);end
%fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ? - not useful

fprintf(fid,'rc %s\n\n',mesh_file);                         % read mesh file *.msh

%===========================================
% end of journal file 
%===========================================
fprintf(fid,'wc finish.cas\n'); % Write finish.cas
fprintf(fid,'yes\n');           % Erase if exist
%fprintf(fid,'exit\n');
%fprintf(fid,'yes\n');
fclose(fid);

if strcmp(data_num.Affichage,'yes'); fprintf(' fluent_grid : end Ok \n'); end;

