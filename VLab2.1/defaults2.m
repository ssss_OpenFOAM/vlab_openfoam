function [data_geo,data_topo,data_mesh,data_num,data_sim,data_ac,data_sup,data_phy,str_cases,fid_cases,param] = defaults2(n_conf,os,param)
%
%function [data_geo,data_topo,data_mesh,data_num,data_sim,data_ac,data_sup,cas,var,str_var,str_cases,fid_cases] = defaults2 (x)
% =========================================================================
% File of the default values
% =========================================================================
%
% Do not change this file
% Copy/Paste needed variables in affect?.m
%
% =========================================================================
if strcmp(param,'')     % if analysis mode
%if size(x,1)==0         % if analysis mode
    if exist('stop.dat','file');delete stop.dat;end  % if optim should create signals.par
end
if exist('fluent_journal.trn','file');delete 'fluent_journal.trn';end
if exist('fluent_j2.trn'     ,'file');delete 'fluent_j2.trn';end
if exist('fluent_anim.trn'   ,'file');delete 'fluent_anim.trn';end
delete default_id.*;        % gambit files
%if exist('signals.par'       ,'file');system('mv signals.par signals--.par');disp(' mv signals.par signals--.par for optimization');end
%if n_conf ~= 400 || n_conf ~= 401;delete *.jou;end
delete *.cxa;
file='*.hmf';if exist(file,'file');delete *.hmf;end;
if exist('VLab-ai.txt','file');delete VLab-ai.txt;end;
%delete *.out;
%delete *.msh;  % now in journal_GAMBIT.m, journal_GAMBIT_ac.m
%switch os
%    case 'Linux'
%    system('\rm default_id.*'); % command Linux
%    case 'Windows'
%    system('del default_id.*'); % command Windows
%end
figure(2);
clf(2);
% =========================================================================
% file : Cases_Coeff_mean.out
%   Analysis mode       save old file and write 1 line in the new file
%   Optimization mode   do nothing
% =========================================================================
fid_cases = 0;
str_cases = 'Cases_Coeff_mean.out';
if strcmp(param,'')     % if analysis mode
%if size(x,1)==0     % if analysis mode
    if exist (str_cases,'file');
        system('mv -f Cases_Coeff_mean.out Cases_Coeff_mean_sav.out');
        disp(' save Cases_Coeff_mean_sav.out');
    end
    fid_cases = fopen(str_cases,'w');
    fprintf(fid_cases,'                                                                       ');
    fprintf(fid_cases,'inc       Cl_mean   Cd_mean   Cl_rms    Cd_rms    xcp       cq        cmu       q_jet     Err_Cl    Err_Cd   \n');
    fclose(fid_cases);
else                % if optimization mode
end
%==============================
% Sequence
%==============================
data_geo.sequence.type                          = 'geo_topo_jouM_runM_jouF_runF';   % create gambit_journal (geo, topo, jou, msh, jou, cas)
%data_geo.sequence.type                         = 'geo_topo_jouG_runG_jouF_runF';   % create gambit_journal (geo, topo, jou, msh, jou, cas)
data_geo.sequence.igs_file1.name                = '';                               % igs file1
data_geo.sequence.igs_file1.scale               = 1;                                % scale of the igs file
data_geo.sequence.igs_file1.symmetry.value      = 0;                                % symmetry
data_geo.sequence.igs_file1.symmetry.vector     = [0 0 0];                          % symmetry vector (x,y,z)
data_geo.sequence.igs_file1.symmetry.origin     = [0 0 0];                          % symmetry origin (x,y,z)
data_geo.sequence.igs_file1.rotation.value      = 0;                                % rotation
data_geo.sequence.igs_file1.rotation.dangle     = 0;                                % rotation dangle
data_geo.sequence.igs_file1.rotation.vector     = [0 0 0];                          % rotation vector (x,y,z)
data_geo.sequence.igs_file1.rotation.origin     = [0 0 0];                          % rotation origin (x,y,z)
data_geo.sequence.igs_file1.translation.value   = 0;                                % translation
data_geo.sequence.igs_file1.translation.vector  = [0 0 0];                          % translation vector (dx,dy,dz)
data_geo.sequence.igs_file2.name                = '';                               % igs file2
data_geo.sequence.igs_file2.scale               = 1;                                % scale of the igs file
data_geo.sequence.igs_file2.symmetry.value      = 0;                                % symmetry
data_geo.sequence.igs_file2.symmetry.vector     = [0 0 0];                          % symmetry vector (x,y,z)
data_geo.sequence.igs_file2.symmetry.origin     = [0 0 0];                          % symmetry origin (x,y,z)
data_geo.sequence.igs_file2.rotation.value      = 0;                                % rotation
data_geo.sequence.igs_file2.rotation.dangle     = 0;                                % rotation dangle
data_geo.sequence.igs_file2.rotation.vector     = [0 0 0];                          % rotation vector (x,y,z)
data_geo.sequence.igs_file2.rotation.origin     = [0 0 0];                          % rotation origin (x,y,z)
data_geo.sequence.igs_file2.translation.value   = 0;                                % translation
data_geo.sequence.igs_file2.translation.vector  = [0 0 0];                          % translation vector (dx,dy,dz)
data_geo.sequence.jou_file          = '';                               % journal file for mesh creation
data_geo.sequence.mesh_file         = '';                               % mesh file
data_geo.sequence.fluent_journal    = 'OpenFOAM_journal';                 % subroutine of the Fluent journal creation
%==============================
% Geometry Spline / Method
%==============================
data_geo.method.type                = 'naca_4digits';       % defined in affect.m           used in airfoil_geometry.m
data_geo.method.str_geo             = '';                   % defined in airfoil_geometry.m used in ?
data_geo.method.bspline             = 'approx';             % bspline.m 'approx' finding control points for a given airfoil 
                                                            %           'create' creating bsplines out of a set of known control points
%============================
% BSpline Control
%============================
data_geo.bsplineOrder   = 5;                                % bspline.m
data_geo.bsplineNumCP   = 10;                               % bspline.m
data_geo.bsplineIt      = 25;                               % bspline.m, bspline_estimate.m
%==============================
% Geometry cut
%==============================
data_geo.cut.var (1,1)        = 0.0;               % z_coordinate of sail cut 1
data_geo.cut.var (1,2)        = 0.0;               % camber of cut 1
data_geo.cut.var (1,3)        = 0.0;               % camber location of cut 1
data_geo.cut.var (1,4)        = 0.0;               % chord of cut 1
data_geo.cut.var (1,5)        = 0.0;               % draft of cut 1
%============================
% Geometry
%============================
data_geo.scale                      = 1;                % scale on all coordinates (applied on the *.msh after reading by FLUENT)
data_geo.c                          = 1;                % chord length (m)
data_geo.b                          = 1;                % wing width   (m)
data_geo.S                          = 1;                % wing surface (m^2)
data_geo.filename{1}                = 'geo.dat';        % geo filename
data_geo.filename{2}                = 'geo2.dat';       % geo filename
data_geo.airfoil.str_geo            = 'default';        % string for filename
data_geo.airfoil                    = {};
data_geo.x_min                      = 0.;               % x-size of the geometry
data_geo.y_min                      = 0.;               % y-size of the geometry
data_geo.z_min                      = 0.;               % z-size of the geometry
data_geo.x_max                      = 1.;               % x-size of the geometry
data_geo.y_max                      = 1.;               % y-size of the geometry
data_geo.z_max                      = 1.;               % z-size of the geometry
data_geo.v1                         = 0;                % variable
data_geo.v2                         = 0;                % variable
data_geo.v3                         = 0;                % variable
data_geo.v4                         = 0;                % variable
data_geo.bdf                        = 'no';             % geometry check for bdf (especialy for UIUC format)
data_geo.Points                     = [];               % vertices for ICEM 2d mesh (x,y)
data_geo.sectionStyle               = 'single';         % only one airfoil section as standard
data_geo.bdf                        = 'with';           % ('with', 'without')
data_geo.x                          = [];               % x-coord. vector
data_geo.y                          = [];               % y-coord. vector
data_geo.z                          = [];               % z-coord. vector
%============================
% Geometry NACA 4 digits
%============================
data_geo.naca4digits.m(1)          = 0.0;                      % maximum camber of the mean line in chord percentage
data_geo.naca4digits.p(1)          = 0.0;                      % chordwise position of m in tenth of a chord
data_geo.naca4digits.t(1)          = 0.12;                     % thickness in chord percentage
data_geo.naca4digits.m(2)          = 0.0;                      % maximum camber of the mean line in chord percentage
data_geo.naca4digits.p(2)          = 0.0;                      % chordwise position of m in tenth of a chord
data_geo.naca4digits.t(2)          = 0.12;                     % thickness in chord percentage
%============================
% Geometry NACA 6 digits : example NACA 65(1)012 (S,A,CIR,CI,t)=(6,0.5,0.1,0.0,0.12)
%============================
data_geo.naca6digits.S          = 6;            % serie
data_geo.naca6digits.A          = 0.5;          % chordwise position of the minimum pressure for the symmetric section at zero lift
data_geo.naca6digits.CIR        = 0.1;          % range of lift coefficient around the design lift coefficient in which favorable pressure gradients exist on both surfaces
data_geo.naca6digits.CI         = 0.0;          % design lift coefficient
data_geo.naca6digits.t          = 0.12;         % thickness of the wing section 
%============================
% Mesh ICEM
%============================
data_geo.nosePoint = 0;                     % construction point to separate extrados from intrados during mesh creation

%============================
% Mesh GAMBIT
%============================

% BL mesh (normal to airfoil)
data_mesh.h1            = 0.002;                % hauteur 1ere maille sur l'obstacle en mm
data_mesh.n_cl          = 4;                    % nombre de mailles dans la zone structuree du maillage
data_mesh.h_mesh        = 0.05;                 % hauteur zone structuree du maillage (% de corde)
data_mesh.y_wall        = data_mesh.h1;         % y+ evaluation
data_mesh.scales.L      = 1;                    % largest length scale      - turbulent regime
data_mesh.scales.lk     = 1;                    % Kolmogorov length scale   - turbulent regime
data_mesh.scales.tL     = 1;                    % largest time scale        - turbulent regime
data_mesh.scales.tlk    = 1;                    % Kolmogorov time scale     - turbulent regime

% BL mesh (along the airfoil)
% szone
data_mesh.coeff_sill    = 0.1;              % longueur de la zone sillage (nombre de corde)
data_mesh.size_bda      = 0.01;
data_mesh.coeff_zone    = 0.;               % expansion zone structuree du maillage vers bdf (1+coeff_zone)*h_mesh
data_mesh.symmetric     = 0;                % 0 = symmetric mesh, 1 = unsymmetric mesh on extrados / intrados
data_mesh.coeff_surf    = 3;                % npts_surface = length_surface / bda_size / geo_ext
% szone_ac
data_mesh.n_ext         = 30;               % nombre de mailles extrados
data_mesh.n_jet         =  3;               % nombre de mailles dans l'actionneur jet
data_mesh.coeff_ext1    = 1;

% maillage domaine de calcul
data_mesh.npts_edge_domain          = 10;               % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_up       = 10;               % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_down     = 10;               % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_inlet    = 10;               % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_outlet   = 10;               % nombre de points edge domaine de calcul
data_mesh.N_Corde_Amont             = 20;               % nombre de corde amont profil
data_mesh.N_Corde_Aval              = 20;               % nombre de corde aval  profil
data_mesh.N_Corde_ymin              = 20;               % nombre de corde au-dessous du profil
data_mesh.N_Corde_ymax              = 20;               % nombre de corde au-dessus  du profil 
data_mesh.N_Corde_zmin              = 20;               % nombre de corde au-dessous du profil
data_mesh.N_Corde_zmax              = 20;               % nombre de corde au-dessus  du profil 
data_mesh.z_sea                     =  1;               % location of the lower symmetry plane in meters (may be sea or deck plane for yachts) 

data_mesh.yplus                 = 99;                   % y+ default value

% unstructured mesh properties
data_mesh.mesh_algo             = 1;            % (0:'tetra'   ,1:'hexcore')            in jou_G_sail_3D.m (face_algo, volume_algo)
data_mesh.mesh_algo             = 2;            % (0:'triangle',1:'pave',   2:'map')    in jou_G_dragonfly.m
data_mesh.mesh_algo             = 1;            % (0:'triangle',1:'map')                in journal_GAMBIT.m, journal_GAMBIT_ac.m, jou_G_AC45.m
data_mesh.cell_size             = 0.100;
data_mesh.cell_size2            = 0.100;
data_mesh.growthrate            = 1.30;
data_mesh.growthrate2           = 1.30;
data_mesh.growthrate_inside     = 1.30;
data_mesh.growthrate_outside    = 1.30;
data_mesh.size_limit            = 5000;
data_mesh.normalangle           = 20;
data_mesh.cell_size2            = 0.100;
data_mesh.growthrate2           = 1.30;
data_mesh.n_span                = 20;
data_mesh.n_tip                 = 3;
data_mesh.cell_number1          = 10;
data_mesh.cell_number2          = 10;
data_mesh.cell_number3          = 10;
data_mesh.kind                  = 'ref';                % 'ref', 'bio'

%============================
% Simulation FLUENT
%============================
data_sim.cas            = 'analysis';           
data_sim.sim            = 1;        % simulation     (0: non, 1: oui)
data_sim.graphic        = 0;        % Fluent graphic (0: non, 1: oui)
data_sim.frame          = 'xy';     % default frame
data_sim.dim            = '2D';     % dimension  (2D, 3D)
data_sim.geo            = 'NACA0012'; % ?
data_sim.Vinf           = 40;       % Vitesse infini amont
data_sim.nu             = 1e-5;
data_sim.inc            = 8.3;      % incidence ecoulement / profil
data_sim.I_turb         = 1;        % Turbulence Intensity (%) [1]
data_sim.L_turb         = 0.005;    % Turbulence Length Scale (m) [0.005]

data_sim.Nit            = 0;                % current iteration number
data_sim.iter           = 30;               % iterations stationnaire (RANS) (pas utile)
data_sim.scheme_order   = 'O2';             % ordre du schema numerique spatial
data_sim.iter_trans     = 10;               % iterations transitoire
data_sim.iter_sup       = 20;               % iterations fenetre glissante moyennes sur Cl_mean
data_sim.reporting      = 1;                % reporting of lift & drag coeff.
data_sim.str_URANS      = 'RANS';           % choix RANS / URANS (utile uniquement si pas d'actionneur unsteady)
data_sim.np_per         =  20;              % nombre de pas de temps par periode actionneur
data_sim.regime         = 'turbulent';      % regime d'ecoulement (laminar, turbulent)
data_sim.Tu_model       = 'SSTtr';             % Turbulence model (SA, SST, SSTtr)
data_sim.Tr_model       = 'no';             % Transition model (SA, SST, SSTtr)
data_sim.Fluid_model    = 'incompressible'; % Fluid model
data_sim.Mach_number    = 0;                % Default Mach number value

data_sim.Stop_Err       = 0.1;              % error to stop RANS/URANS
data_sim.Stop_Nit       = 40;               % Iterations to stop
data_sim.Max_Nit        = 99999;            % Iterations max
data_sim.Stop_type       = 'Definite';      % Type d'arr�t : {'Intelligent','Interactive','Definite'} 

data_sim.str_dir        = strcat('R_',datestr(now));    % nom par defaut du repertoire resultats
data_sim.str_dir(14)    = '-';                          % remplace espace par tiret
data_sim.str_dir(17)    = '';
data_sim.str_dir(19)    = '';
data_sim.str_file       = 'default';                    % file_name.m : str_file = strcat(str_geo,str_sup); nom par defaut des fichiers *.cas & *.dat

data_sim.DynMesh        = 'no';

data_sim.anim.film      = 0;                        % film (0: non, 1: oui)
data_sim.anim.quantity  = 'velocity-magnitude';     % field quantity (velocity-magnitude, pressure-coefficient, vorticity-mag, ...)
data_sim.anim.images    = 30;                       % number of images
data_sim.anim.frequency =  1;                       % number of time step or iterations between images
data_sim.anim.min_value = 0;                        % min value of the field quantity
data_sim.anim.max_value = 0;                        % max value of the field quantity
%data_sim.anim.iter      = 4 * data_sim.np_per;     % iterations between images (not used now)

data_sim.save.cas       = 1;                        % save *.cas & *.dat

data_sim.time_step          = 0;
data_sim.time_sim           = 0;
data_sim.iter_mean          = 0;
data_sim.iter_sim           = 0;
data_sim.period_sim         = 12;
data_sim.period_ndt         = 20;               % number of sub-iterations in each timestep
data_sim.period_mean        = 3;

data_sim.interpolation      = 'no';             % create interpolation file ?
data_sim.init_method        = 'init';           % {'init','read'}
data_sim.ai_file            = 'VLab-ai.txt';    % default name
data_sim.vector             = 0;                % for multipoint optim
data_sim.cpu_time_sec       = 0;                % estimated time in sec for 1 run

%============================
% Optimization data
%============================
%data_sim.opti.dir           = strcat('optim',num2str(round(rand(1)*1000))); % optim results directory
%system(sprintf('mkdir %s',data_sim.opti.dir));  % create optim directory
data_sim.opti.objective     = 'no';             % fitness function {'max lift','min drag','max L/D','min Cp-CP*'}
data_sim.opti.Cl_target     = 0.6;              % target    lift for objective function
data_sim.opti.Cl_ref        = 0.6;              % reference lift for objective function
data_sim.opti.Cd_target     = 0.05;             % target    drag for objective function
data_sim.opti.Cd_ref        = 0.05;             % reference drag for objective function
data_sim.opti.Cm_target     =-0.15;             % target    Cm   for objective function

data_sim.opti.Mxmax         = 0;                % x-axis moment limit for constraint objective
data_sim.opti.F1            = 0;                % objectif F1
data_sim.opti.C1_target     = 0;                % contrainte C1
data_sim.opti.C2_target     = 0;                % contrainte C2

data_sim.opti.Fx1_target    = 0;                % target thrust for objective function - Front rotor
data_sim.opti.Fx1_ref       = 1;                % ref.   thrust for objective function - Front rotor
data_sim.opti.Fx2_target    = 0;                % target thrust for objective function - Rear  rotor
data_sim.opti.Fx2_ref       = 1;                % ref.   thrust for objective function - Rear  rotor
data_sim.opti.Mx1_target    = 0;                % target moment for objective function - Front rotor
data_sim.opti.Mx1_ref       = 1;                % ref.   moment for objective function - Front rotor
data_sim.opti.Mx2_target    = 0;                % target moment for objective function - Rear  rotor
data_sim.opti.Mx2_ref       = 1;                % ref.   moment for objective function - Rear  rotor

if strcmp(param,'')     % if analysis mode
    param.cas               = 'analysis';       % analysis, O1P2 or O1P4 or ...
    param.vector            = [];               % multipoint optimization vector
    param.scalar            = [];               % current multipoint optimization vector value
    param.objective         = '';               % fitness function {'max lift','min drag','max L/D','min Cp-CP*','min drag clift'}
    param.Cl_target         = 0;                % target    lift for objective function
    param.Cl_ref            = 0;                % reference lift for objective function
    param.Cd_target         = 0;                % target    drag for objective function
    param.Cd_ref            = 0;                % reference drag for objective function
    param.Fx1_target        = 0;                % target thrust for objective function - Front rotor
    param.Fx1_ref           = 1;                % ref.   thrust for objective function - Front rotor
    param.Fx2_target        = 0;                % target thrust for objective function - Rear  rotor
    param.Fx2_ref           = 1;                % ref.   thrust for objective function - Rear  rotor
    param.Mx1_target        = 0;                % target moment for objective function - Front rotor
    param.Mx1_ref           = 1;                % ref.   moment for objective function - Front rotor
    param.Mx2_target        = 0;                % target moment for objective function - Rear  rotor
    param.Mx2_ref           = 1;                % ref.   moment for objective function - Rear  rotor
else
    % if optimization mode let param as defined in opti_pb??.m
end
%============================
% Comparison data
%============================
data_sim.comparison      = 'no';
data_sim.data_type       = 'num';
data_sim.title           = '';
data_sim.al_res          = [0 0];
data_sim.Cl_res          = [0 0];
data_sim.Cd_res          = [0 0];
data_sim.Cm_res          = [0 0];
%data_sim.d1.title           = '';
%data_sim.d1.al_res          = [0 0];
%data_sim.d1.Cl_res          = [0 0];
%data_sim.d1.Cd_res          = [0 0];
%data_sim.d2.title           = '';
%data_sim.d2.al_res          = [0 0];
%data_sim.d2.Cl_res          = [0 0];
%data_sim.d2.Cd_res          = [0 0];
%============================
% FSI data
%============================
data_sim.FSI            = 0;                    % to create FSI pressure files in fluent_j2.m

%============================
% Actionneur
%============================

data_ac.ac          = 'no';
data_ac.type        = 'no';                     % type de l'actionneur                              (continuous jet, synthetic jet, pulsed jet)
data_ac.pos         = 0.15;                     % position de l'actionneur                          (en % de corde)
data_ac.dia         = 0.00;                     % actuator diameter                                 (en % de corde)
data_ac.Vjmean      = 80.;                      % vitesse d'�jection de l'actionneur                (en m/s)
data_ac.Theta_deg   = 90.;                      % angle   d'�jection de l'actionneur / horizontal   (en deg)
data_ac.Vjfluct     = 80.;                       % vitesse d'�jection de l'actionneur                (en m/s)
data_ac.Freq        = 100.;                     % Frequence actionneur                              (en Hz)
data_ac.duty_cycle  = 0.5;
data_ac.adapt_mesh  = 'no';                     % adapt mesh region around the actuator
data_ac.closed_loop = 'no';                    % closed-loop control with actuator

data_sup.v1         = 0;                        % var 12 : variable supplementaire 1
data_sup.v2         = 0;                        % var 13 : variable supplementaire 2
data_sup.v3         = 0;                        % var 14 : variable supplementaire 3

%============================
% Physical data
%============================
data_phy.rho            = 1.225;                % density of air
data_phy.mu             = 1.7894*10^-5;
data_phy.nu             = 1.5*10^-5;            % kinematic viscosity of air
data_phy.gam            = 1.4;                  % ratio of the specific heat of air
data_phy.Rg             = 287.04;               % gas constant of air
%============================
% Data numerique
%============================
data_num.visu               = 'analysis';       % 'analysis', 'opti'
data_num.parallel           = 0;                % 1 = calcul parallel
data_num.nproc              = 1;                % nombre de processeurs
data_num.Affichage          = 'yes';            % Verbose mode with a lot of text print
data_num.fluent_jloads      = 'yes';            % Create files VLab_loads.txt, ...
data_num.fluent_jend        = 'yes';             % Create files VLab_Cl.txt, ..., *_field.txt, *_BL.txt, *_Model.txt, fluent-interpolation.ip, pb windows / proc=1
data_num.fluent_jimg        = 'yes';            % Create files *.tiff
data_num.debug              = 'no';             % Affichage in debug mode
data_num.Rep_data           = 'all';            % Choix des variables to print : {'geo','mesh','topo','sim','ac','all'} 
data_num.solver             = 'pressure-based'; % numerical scheme : {'pressure-based','density-based-explicit','density-based-implicit'} 
data_num.mesher             = 'GMSH';
data_num.soft               = 'OpenFOAM';
data_num.stop               = 0;
data_num.compteur_star      = 0;

% OS dependent variables
switch os
    case 'Linux'
data_num.Gambit_dir         = '';                                       % GAMBIT directory OK in c-shell
data_num.Gambit_dir         = '/usr/local/Fluent6.3.26/bin/';           % GAMBIT directory OK in bash
data_num.Gambit_dir         = '/usr/local/Fluent-6.3_Gambit-2.4/Fluent.Inc/bin/';           % GAMBIT directory OK in bash
data_num.GMSH_dir           = '../gmsh/gmsh-2.16.0-Linux/bin/'
data_num.Gambit_ver         = '2.4.6';                                  % Gambit version {'2.2.30','2.3.16','2.4.6'} 
data_num.Fluent_dir         = '';                                       % FLUENT directory OK in c-shell
data_num.Fluent_dir         = '/usr/local/ansys145/v145/fluent/bin/';   % FLUENT directory OK in bash
%data_num.Fluent_ver         = '6.3.26';                                % FLUENT version {'6.2.16','6.3.26','6.4.11'} 
data_num.Fluent_ver         = '14.5.7';                                 % FLUENT version {'13.0.0','14.0.0','14.5.7'} 
%data_num.Fluent_sn          = 'fluent14';                              % FLUENT command {'14.0.0'} 
%data_num.Fluent_sn          = 'fluent145';                             % FLUENT command {'14.5.7'} 
data_num.Fluent_sn          = 'fluent';                                 % FLUENT command {'6.2.16','6.3.26','6.4.11','14.5.7'} 
data_num.Star_dir           = '';                                       % Starccm+ directory 
data_num.Star_ver           = '8.04';                                   % Starccm+ version {'8.04'} 
    case 'Windows'
data_num.Gambit_dir         = 'c:\Fluent.Inc\ntbin\ntx86\';
data_num.Gambit_ver         = '2.4.6';          % version Gambit : {'2.2.30','2.3.16','2.4.6'} 
data_num.Fluent_dir         = '';               % FLUENT directory
data_num.Fluent_ver         = '6.3.26';         % FLUENT version {'6.2.16','6.3.26','6.4.11'} 
data_num.Fluent_sn         = 'fluent';          % FLUENT command {'6.2.16','6.3.26','6.4.11'} 
data_num.fluent_jend        = 'no';             % Create files VLab_Cl.txt, ..., *_field.txt, *_BL.txt, *_Model.txt, fluent-interpolation.ip, pb windows / proc=1
end

data_num.cpu_time_mesh      = 0;              % cpu time mesh  part (GAMBIT)
data_num.cpu_time_solve     = 0;              % cpu time solve part (FLUENT)
data_num.ellapsed_time      = 0;              % ellapsed time in Fluent during iter_sup iterations
data_num.mat_file           ='matlab.mat';
data_num.mat                = 'yes';           % write mat file
data_num.mesh_cells         = 1;               % number of cells of the mesh
data_num.current_dir        = '';              % useful but where ?
% Graphic part (2D/3D)
%   tiff : Grid, contours
%   tiff : Pressure file
%   tiff : Friction lines on selected surfaces
%   tiff : Mean velocity magnitude on wall surface
%   txt  : Scalar ascii file (x,y,z, Cp, Cf)

%============================
% Mesh default topology
%============================

% =============================================================
% mesh surfaces and bc
% =============================================================
surf_bc = { 'inlet'                 'velocity_inlet';
            'outlet'                'pressure_outlet';
            'up'                    'velocity_inlet';
            'down'                  'velocity_inlet';
            'int'                   'interior';
            'bda'                   'wall';
            'extrados'              'wall';
            'intrados'              'wall';};
%surface_laterale= 'inlet';                 % surface pour aligner plans de post-traitement
%surface_bdf     = 'inlet';                 % surface pour aligner plans de post-traitement
% ======================================================================
% surfaces for coefficient evaluation (Cl, Cd, Cm, ...)
% ======================================================================
force_surface    = '';space = ' ';
for i = 1 : size(surf_bc,1)
    surf_bc_name = surf_bc{i,1};
	surf_bc_type = surf_bc{i,2};
    if surf_bc_type(1:4) == 'wall'
        force_surface = [force_surface space surf_bc_name]; % concatenation preservant les espaces
    end
end

data_topo.i_bda                 = 0;
%Modification Parapente : Louis PERROT-MINOT (CFD 3A)
data_topo.i_bda_extrados        = 0; % nombre de point sur l'extrados appartenant au BDA
data_topo.i_bda_intrados        = 0; % nombre de point sur l'intrados appartenant au BDA
data_topo.jet_start             = 0;
data_topo.jet_stop              = 0;
data_topo.bda_start             = 0;
data_topo.bda_stop              = 0;
data_topo.extrados_start        = 1;
data_topo.extrados_stop         = 204;
data_topo.intrados_start        = 205;
data_topo.intrados_stop         = 406;
data_topo.extrados_int_start    = 0;
data_topo.extrados_int_stop     = 0;
data_topo.intrados_int_start    = 0;
data_topo.intrados_int_stop     = 0;
data_topo.force_surface         = force_surface;
data_topo.surf_bc               = surf_bc;

data_topo.n_bda         = 0;
data_topo.n_ext         = 0;
data_topo.n_int         = 0;
data_topo.n_sill        = 0;
data_topo.n_ext1        = 0;
data_topo.n_jet         = 0;
data_topo.n_ext1        = 0;
data_topo.n_ext2        = 0;
data_topo.size_uni      = 0;     % not used
data_topo.size_mini     = 0;
data_topo.size_bdf      = 0;
data_topo.ratio_bda     = 1;
data_topo.ratio_ext1    = 1;
data_topo.ratio_ext2    = 1;
data_topo.ratio_jet     = 1;
data_topo.ratio_int     = 1;

% Modification Parapente : Louis PERROT-MINOT (CFD 3A)
data_topo.l_bda         = 0;
data_topo.s_bda         = 0;
data_topo.both_edges    = 0;

time_unity                  = 'secs';
img                         = '';

%=====================================================================
% XFOIL data
%=====================================================================
XFOIL_param.airfoil_name    = 'NACA2414';                   % Airfoil geometry - from data?.m
XFOIL_param.Inc             = 5;                            % Angle of attack - from data?.m
XFOIL_param.Reynolds        = 5e6;                          % Reynolds number
XFOIL_param.Mach            = 0.0;                          % Mach number
XFOIL_param.XTRu            = 0;                            % (0 = forced transition at L.E., 1 = free transition)
XFOIL_param.XTRl            = 0;                            % (0 = forced transition at L.E., 1 = free transition)
XFOIL_param.Ncrit           = 9;                            % Turbulence level (N=9 > Tu = 0.07%)

%=====================================================================
% Do not touch below this line without a good expertise of VLab !!!
%=====================================================================
%cas=1;
%x=[];

%str_var     = [ '  NACA' '  Vinf' '   inc' 'delta ';
%                '  0012' '    40' '     2' '     0'];
%var = str2var (str_var, x, 6);
