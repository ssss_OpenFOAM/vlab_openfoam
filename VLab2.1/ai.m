function [ai_file] = ai (data_geo,data_mesh,data_num,data_sim,data_ac,i_cas,data_topo,data_phy,Result)
%
% ai is a function to implement best practice in CFD

str_geo     = data_geo.method.str_geo;
if strcmp(str_geo , '') str_geo = data_geo.filename; end   % filename without extension (.txt or .dat)

c           = data_geo.c;

nu          = data_phy.nu;
gam         = data_phy.gam;
Rg          = data_phy.Rg;

Tref        = 300;                  % this may be wrong if initialized with a different value
a           = sqrt(gam*Rg*Tref);

Vinf        = data_sim.Vinf;
inc         = data_sim.inc;
str_URANS   = data_sim.str_URANS;
regime      = data_sim.regime;
str_dir     = data_sim.str_dir;
dim         = data_sim.dim;
Tu          = 0.01*data_sim.I_turb;   % Turbulence Intensity (%) [1]
Tu_Model    = data_sim.Tu_model;      % Turbulence model (SA, SST, SSTtr)
Tr_Model    = data_sim.Tr_model;      % Transition model (?)
Fluid_Model = data_sim.Fluid_model;   % Fluid model

%=============================
% Transition modeling issues
%=============================
LaminarFraction = Tr_model (Vinf,c,Tu);

% Which Transition model ?
switch Tu_Model
    case {'SA','SST'}
        Tr_Model = 'no';
    otherwise
        Tr_Model = Tu_Model;
end

% Turbulent modeling issues

%=====================
% Flow model issues
%=====================
switch regime
    case 'laminar'
        switch str_URANS
            case 'RANS'
                str_model = 'LS';
            case {'DNS','URANS'}
                str_model = 'DNS';
        end
    case 'turbulent'
        str_model = str_URANS;
end

%=====================
% Variables to print
%=====================
Re      = Vinf*c/nu;
Mach    = 0.01*round(100*Vinf/a);
AoA     = Result(1);
Cl      = Result(2);
Cd      = Result(3);
Cm      = Result(6)-0.25*Result(2); % Cm(c/4) = CmLE - Cl/4

%=====================
% on screen
%=====================
fprintf('==================================================\n');
fprintf(' Flow Model            : %s %s %s %s \n',str_model,' ',dim,Fluid_Model);
fprintf(' Geometry              : %s \n',str_geo);
fprintf(' Mesh                  : %i nodes, y+=O(%g) \n',data_num.mesh_cells,data_mesh.yplus);
fprintf(' Solver                : Spatial Order %s  \n',data_sim.scheme_order);
if strcmp(regime,'turbulent')
fprintf(' Turbulence Model      : %s \n',Tu_Model);
fprintf(' Transition Model      : %s \n',Tr_Model);
fprintf(' Laminar Fraction      : %g %% of the chord\n',round(100*LaminarFraction));
end
fprintf('==================================================\n');
fprintf(' Reynolds  Mach  AoA   Cl      Cd     Cm   \n');
fprintf(' %8g %5.2f %5.1f  %6.4f  %6.4f %6.4f \n',Re,Mach,AoA,Cl,Cd,Cm);
fprintf('==================================================\n');

%=====================
% in file
%=====================
ai_file = data_sim.ai_file;
fid=fopen(ai_file,'a+');
fprintf(fid,'==================================================\n');
fprintf(fid,' Flow Model            : %s %s %s %s \n',str_model,' ',dim,Fluid_Model);
fprintf(fid,' Geometry              : %s \n',str_geo);
fprintf(fid,' Mesh                  : %i nodes, y+=O(%g) \n',data_num.mesh_cells,data_mesh.yplus);
fprintf(fid,' Solver                : Spatial Order %s  \n',data_sim.scheme_order);
fprintf(fid,' Chord Reynolds number : %g \n',Re);
fprintf(fid,' Mach number           : %g \n',Mach);
if strcmp(regime,'turbulent')
fprintf(fid,' Turbulence Model      : %s \n',Tu_Model);
fprintf(fid,' Transition Model      : %s \n',Tr_Model);
fprintf(fid,' Laminar Fraction      : %g %% of the chord\n',round(100*LaminarFraction));
end
fprintf(fid,'==================================================\n');
fprintf(fid,' Reynolds  Mach  AoA   Cl      Cd     Cm   \n');
fprintf(fid,' %8g %5.2f %5.1f  %6.4f  %6.4f %6.4f \n',Re,Mach,AoA,Cl,Cd,Cm);
fprintf(fid,'==================================================\n');
fclose(fid);
end
