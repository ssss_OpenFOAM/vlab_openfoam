function [fluent_journal_file] = fluent_jimg (data_geo,data_topo,data_sim,data_num)
%
% Post-processing FLUENT
%
% Lecture des fichiers *.cas & *.dat                Read FLUENT Files
% Interpolation file for initialization             fluent-interpolation.ip
% Aerodynamic loads                                 VLab_Fx.txt, VLab_Fy.txt, ... 
% Graphic 2D/3D
%   tiff : Grid, contours                           str_file_grid_z1.tif
%   tiff : Pressure file                            str_file_pressure.tif
%   tiff : Vmag     file                            str_file_Vmag.tif
%   tiff : Friction lines on selected surfaces      ?
%   tiff : Mean velocity magnitude on wall surface  ?
%   txt  : 1D Scalar ascii file (x,y,Cp,Cf)         str_file_field.txt      import_Fluent_scalar_1D.m
% Report summary in file_Model.txt                  str_file_Model.txt
%
Fluent_ver  = data_num.Fluent_ver;
dim         = data_sim.dim;             % dimension  (2D, 3D)

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jimg : FLUENT %s \n',Fluent_ver); end;

c           = data_geo.c;                % chord in meter
inc         = data_sim.inc;
str_file    = data_sim.str_file;

force_surface   = data_topo.force_surface;

%======================================
% Read files *.cas & *.dat
%======================================
fluent_journal_file = 'fluent_jimg.jou';
fid=fopen(fluent_journal_file,'w+');

filename = 'fluent_jimg.trn';if exist(filename,'file');delete (filename);end
fprintf(fid,'/file/start-transcript %s \n',filename);        % start transcript file - bug ?

fprintf(fid,'rc %s.cas\n', str_file); % Lecture du fichier *.cas
fprintf(fid,'rd %s.dat\n', str_file); % Lecture du fichier *.dat

if (data_sim.graphic == 1)  % Not useable if "fluent -g" is used
%===========================================
% Graphique 2D
%===========================================
if strcmp(data_num.Affichage,'yes') && strcmp(data_sim.dim,'2D')
    %===========================================
    % Grid, ... -> str_file_grid_z1.tif
    %===========================================
    camera_target_x     = c/2;
    camera_target_y     = 0;
    camera_target_z     = 0;
    width               = c;
    height              = c;
    fprintf(fid,'/display/grid \n');
    fprintf(fid,'/display/view/camera/target %g %g %g \n',camera_target_x,camera_target_y,camera_target_z);       % position camera
    fprintf(fid,'/display/view/camera/field  %g %g \n',width,height);                                             % zoom camera
    filename = strcat(str_file,'_grid_z1.tif');if exist(filename,'file');delete(filename);end
    % delete files if they exist (prevent errors)
    fprintf(fid,'/display/set/hardcopy/driver/tiff \n'); % file format choice
    fprintf(fid,'/display/hardcopy %s \n\n',filename);
    %================================================
    % pressure file *.tif
    %================================================
    filename = strcat(str_file,'_pressure.tif');if exist(filename,'file');delete(filename);end
    fprintf(fid,'/display/contour/pressure ,, \n');         %
    fprintf(fid,'/display/set/hardcopy/color/color \n');    % color display
    fprintf(fid,'/display/set/hardcopy/driver/tiff \n');    % file format choice
    fprintf(fid,'/display/hardcopy %s \n',filename);    % Ok to overwrite
    %================================================
    % Vmag file *.tif
    %================================================
    filename = strcat(str_file,'_Vmag.tif');if exist(filename,'file');delete(filename);end
    fprintf(fid,'/display/contour/velocity-magnitude ,, \n');         %
    fprintf(fid,'/display/set/hardcopy/color/color \n');    % color display
    fprintf(fid,'/display/set/hardcopy/driver/tiff \n');    % file format choice
    fprintf(fid,'/display/hardcopy %s \n',filename);    % Ok to overwrite
end

%===========================================
% Graphique 3D
%===========================================
%===========================================
% iso-clip Cp=f(x) in sections z=Cte - Ok
%===========================================
if strcmp(data_num.Affichage,'yes') && strcmp(data_sim.dim,'3D')
    select_s1 = 'extrados';select_s2 = 'intrados'  ;  % To test ?
    %select_s1 = 'foc'     ;select_s2 = 'foc-shadow';  % Ok
    if ( strfind(force_surface,select_s1) )
        filename = strcat('"',str_file,'_Cpx_zcte.txt"');if exist(filename,'file');delete(filename);end
        z_cut = 1.5; dz = 0.001;
        s_z_cut = num2str(z_cut);cut = strrep(s_z_cut,'.','p'); % exeample 1.5 = 1p5
        lower_threshold     = z_cut - dz;
        upper_threshold     = z_cut + dz;
        s1                  = ['clip_',select_s1,'_z',cut,'m'];
        fprintf(fid,'/surface/iso-clip/z-coordinate %s %s %g %g \n',s1,select_s1,lower_threshold,upper_threshold);   % create iso-clip
        s2                  = ['clip_',select_s2,'s_z',cut,'m'];
        fprintf(fid,'/surface/iso-clip/z-coordinate %s %s %g %g \n',s2,select_s2,lower_threshold,upper_threshold);   % create iso-clip
        fprintf(fid,'/plot/solution-set/plot-to-file %s \n',filename);                           % set filename for plot
        zone_name           = '()';
        surface_name        = [s1,' ',s2];     % 2 surfaces concatenation
        fprintf(fid,'/plot/solution/pressure-coefficient %s %s, \n',zone_name,surface_name);    % plot iso-clip
        fprintf(' fluent_jend : create iso-clip in %s \n',filename); 
    end
end
%===========================================
end     % if data_sim.graphic == 1
%===========================================

%===========================================
% end of journal file 
%===========================================
if exist('finish.txt','file'); delete('finish.txt');end
fprintf(fid,'/file/write-bc finish.txt \n\n');
fprintf(fid,'exit yes \n');
fclose(fid);

if strcmp(data_num.Affichage,'yes') fprintf(' fluent_jimg : end Ok \n');end;

