function [ output_args ] = coefmean( input_args,periode,coeff_type )
%
% COEFMEAN Summary of this function goes here
%   Detailed explanation goes here


longueur=length(input_args);
fenetre=floor(longueur/(periode));
seuil=0;

%le seuil est adapt� au type de coefficient aero � traiter.

switch coeff_type
    case 'drag'
        seuil=0.001;        
    case 'lift'
        seuil=0.05;
end

%test si il y a des oscillations, si oui moyenne entre deux maximums, sinon le
%coefficient vaut le dernier indice.

temp=input_args(longueur-fenetre:longueur,:);
erreur=abs((max(temp)-min(temp))/min(temp));

output_args=input_args(longueur);

if erreur>seuil
    
sortV=sort(input_args(longueur-fenetre:longueur,:));
kmax=sortV(length(sortV));
indmax=find(input_args==kmax,1,'last');

sortV=sort(input_args(longueur-2*fenetre:longueur-1*fenetre-1,:));
kmin=sortV(length(sortV));
indmin=find(input_args==kmin,1,'last');
output_args=mean(input_args(indmin:indmax,:));
%plot(input_args(indmin:indmax,:));
end






    


