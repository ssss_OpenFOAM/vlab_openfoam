function [xu,yu,xl,yl] = airfoil_V2J (str_rw,filename,xu,yu,xl,yl)
%
% ==================================
% Read  airfoil geometry in VLab     format xu,yu,xl,yl
% Write airfoil geometry in Javafoil format x,y
%
% input :   str_rw      ('read', 'write') 
%           filename   
%
% output :
%
% xu(i), i=1,n  : abcisse  des points extrados du bda vers le bdf
% yu(i), i=1,n  : ordinate des points extrados du bda vers le bdf
% xl(i), i=1,n  : abcisse  des points intrados du bda vers le bdf
% yl(i), i=1,n  : ordinate des points intrados du bda vers le bdf
%
% bda en (0,0)
% bdf en (1,0)
% ==================================

fid         = fopen(filename,'w');

for i=length(xl)-1 : -1 : 2
    fprintf(fid,'%f %f %f \n', xl(i), yl(i), 0);   % lower surface from bda+1 to bdf-1  (0.0? < xl < 0.99?)
end
for i=1:length(xu)-1
    fprintf(fid,'%f %f %f \n', xu(i), yu(i), 0);   % upper surface from bda to bdf-1    (0 < xu < 0.99?)
end
fclose(fid);

%fprintf(' airfoil_V2J : file %s created \n',filename);

