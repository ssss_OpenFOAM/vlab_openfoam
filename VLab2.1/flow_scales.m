%[L,lk] = flow_scales (V,c,I_turb,L_turb,y_cl)
function [L,lk,tL,tlk] = flow_scales (V,c,Tu,nu,affichage)
%
% not yet validated
%
% input
% V         velocity scale
% c         length scale
% Tu        turbulence intensity
% nu        kinematic viscosity
% affichage print 'yes' or 'no'
%
% turbulent scales :
% L         Largest length scale
% lk        Kolmogorov length scale
% tL        Largest time scale
% tlk       Kolmogorov time scale
% epsilon   dissipation rate

AR = 4;

if nargin == 3
    nu          = 1.5e-5;
    affichage   = 'yes';
end

hypothesis  = 'isotropic';

switch hypothesis
    
    case 'isotropic'    % isotropic turbulence
        L       = c;
        tL      = L/V;
        epsilon = (V*Tu)^3/L;
        lk      = (nu^3/epsilon)^0.25;
        tlk     = lk/(V*Tu);
        ReL     = V*L/nu;
    case 'anisotropic'  % wall turbulence
        L       = c;
        
end

% Boundary Layer Thickness
deltaL = 4.92 * L * ReL^-0.5;         % from Blasius
deltaT = 0.38 * L * ReL^-0.2;         % from Falkner-Skan

% Mesh Requirements - RANS - Chapman 1979
nx =  1;    % average number of gridpoints in delta the boundary layer thickness
ny = 20;    % average number of gridpoints in delta the boundary layer thickness
nz =0.5;    % average number of gridpoints in delta the boundary layer thickness
Nx = 4.5*nx*ReL^0.2;
Ny = ny;
Nz = 4.5*AR*nz*ReL^0.2;
NBL = Nx*Ny*Nz;
Nwake = 0.5*NBL;
Ninviscid = 2e5;

% Printing

if strcmp(affichage,'yes')
    fprintf(' flow conditions \n');
    fprintf('     Reference velocity                    : %g  m/s\n',V);
    fprintf('     Reference length scale                : %g  m\n'  ,L);
    fprintf('     Taylor Micro length scale - lambda    : %g  m\n'  ,(L^2/ReL^0.5))^0.5;
    fprintf('     Chord  Reynolds number                : %g  \n'   ,ReL);
    fprintf('     Taylor Reynolds number                : %g  \n'   ,ReL^0.5);
    fprintf('     Boundary Layer thickness (laminar  )  : %g  m\n'  ,deltaL);
    fprintf('     Boundary Layer thickness (turbulent)  : %g  m\n\n',deltaT);
    fprintf(' flow_scales (hypothesis : %s turbulence) \n',hypothesis);
    fprintf('     Length scale ratio                    : %g  \n'   ,L/lk);
    fprintf('     Time   scale ratio                    : %g  \n\n' ,tL/tlk);
    fprintf(' mesh requirements (hypothesis : RANS, AR=%g \n',AR);
    fprintf('     Gridpoints in BL                      : %g  \n'   ,NBL);
    fprintf('     Gridpoints overall                    : %g  \n'   ,NBL+Nwake+Ninviscid);
end

