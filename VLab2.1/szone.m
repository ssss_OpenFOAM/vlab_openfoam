function [points_file,data_topo] = szone (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl)
%
% function : Create file of vertex for GAMBIT
% from airfoil   geometry in JAVAFOIL format
% from inner_box geometry
%
% [xu,yu,xl,yl] => profile_2d.dat
% geoJ.dat      => profile_2d.dat
%
% input :
%
% xu(i), yu(i) i=2,n : extrados coordinates
% xl(i), yl(i) i=2,n : intrados coordinates
%
% output :
%
% points_file   : file of vertex name
% data_topo     : mesh topology for journal GAMBIT

if strcmp(data_num.debug,'yes');fprintf(' szone : start\n');end

c           = data_geo.c;           % dimensional (in meters)
geo_file    = data_geo.filename{1};
bdf         = data_geo.bdf;

force_surface   = data_topo.force_surface;
surf_bc         = data_topo.surf_bc;

h1          = data_mesh.h1;           % dimensional
bda_size    = data_mesh.size_bda;     % dimensional
%h1          = c*data_mesh.h1;           % dimensional
%bda_size    = c*data_mesh.size_bda;     % dimensional

h           = data_mesh.h_mesh;         % adim
coeff_sill  = data_mesh.coeff_sill;     % adim
coeff       = data_mesh.coeff_zone;     % adim
symmetric   = data_mesh.symmetric;      % adim
coeff_surf  = data_mesh.coeff_surf;     % npts_surface = length_surface / bda_size / coeff_surf

i_jet   = 0;    % default value;
i_bda   = 2;    % choice of the cut point bda / extrados et bda / intrados (default value = 3)

step_int = 5;   % only 1 point every step_int from xu to xu_int (default value = 1)

%==========================================
% Read airfoil geometry in JAVAFOIL format
%==========================================
%[xu,yu,xl,yl] = airfoil_read ('geoJ.dat','r');  % airfoil geometry without bdf in JAVAFOIL format

switch  data_geo.method.type    % (naca_4digits, naca_6digits, read_Javafoil, dragonfly_5, read_251_vertex)
    case 'b-spline'
        file = 'optim_4_GAMBIT.dat';                                    % ICEM geo file but readable here ?
        file = strcat(geo_file(1:size(geo_file,2)-4),'_4_GAMBIT.dat');  % otherwise take the Javafoil format file
    case 'read_Javafoil'          
        file = geo_file;                                        % Javafoil format
    otherwise
        file = strcat(geo_file(1:size(geo_file,2)-4),'J.dat');  % otherwise take the Javafoil format file
end

[xu,yu,xl,yl]   = airfoil_read (file,'r'); % read airfoil geometry without bdf in JAVAFOIL format

%==============================================================================
% Ajout point bdf from (xu, yu, xl, yl) because Javafoil format is without bdf
%==============================================================================
switch bdf
    case 'without'
        x_bdf = 1;
        y_bdf = 0;
    case 'with'
        x1          = xu(length(xu)-1);
        y1          = yu(length(xu)-1);
        x2          = xu(length(xu));
        y2          = yu(length(xu));
        x3          = xl(length(xl)-1);
        y3          = yl(length(xl)-1);
        x4          = xl(length(xl));
        y4          = yl(length(xl));
        % droite 1 = droite extrados y=a1*x+b1
        % droite 2 = droite intrados y=a2*x+b2
        a1          = (y2-y1)/(x2-x1);
        a2          = (y4-y3)/(x4-x3);
        b1          = (x2*y1 - x1*y2)/(x2-x1);
        b2          = (x4*y3 - x3*y4)/(x4-x3);
        x_bdf               = (b2-b1)/(a1-a2);
        y_bdf               = a1*x_bdf+b1;
end

%==============================================================================
% Construction maillage structure autour du NACA (bug si flap / derivee non defini)
%==============================================================================
%
% input  : airfoil coordinates   : xu, xl, yu, yl, h, coeff
% output : inner box coordinates : xu_int, yu_int, xl_int, yl_int
%
% ==== Extrados ===================
% Creation du point xu_int(1) - bda
norme               = sqrt(power((xu(2)-xl(2)),2)+power((yu(2)-yl(2)),2)); 
delta_xu            = (xu(2)-xl(2))/norme;
delta_yu            = (yu(2)-yl(2))/norme;
xu_int(1)           =  xu(1) - h*(1+coeff*xu(1))*delta_yu;
yu_int(1)           =  yu(1) + h*(1+coeff*xu(1))*delta_xu;

% Creation du point xu_int(i)
n = 2;
for i = 2 : step_int : length(xu)-1
    norme   (n) = sqrt(power((xu(i+1)-xu(i-1)),2)+power((yu(i+1)-yu(i-1)),2)); 
    delta_xu(n) = (xu(i+1)-xu(i-1))/norme(n);
    delta_yu(n) = (yu(i+1)-yu(i-1))/norme(n);
    xu_int  (n) = xu(i) - h*(1+coeff*xu(i))*delta_yu(n);
    yu_int  (n) = yu(i) + h*(1+coeff*xu(i))*delta_xu(n);
    n=n+1;
end

% Creation du point xu_int(n) - bdf
norme                   = sqrt(power((xu(length(xu))-xu(length(xu)-1)),2)+power((yu(length(yu))-yu(length(yu)-1)),2)); 
delta_xu                = (xu(length(xu))-xu(length(xu)-1))/norme;
delta_yu                = (yu(length(yu))-yu(length(yu)-1))/norme;
xu_int(length(xu_int))  =  xu(length(xu)) - h*(1+coeff*xu(length(xu)))*delta_yu;
yu_int(length(yu_int))  =  yu(length(yu)) + h*(1+coeff*xu(length(xu)))*delta_xu;
% ==== Extrados end ===============

% ==== Intrados ===================
% Creation du point xl_int(i)
n=2;
for i = 2 : step_int : length(xu)-1
    norme   (n) = sqrt(power((xl(i+1)-xl(i-1)),2)+power((yl(i+1)-yl(i-1)),2)); 
    delta_xl(n) = (xl(i+1)-xl(i-1))/norme(n);
    delta_yl(n) = (yl(i+1)-yl(i-1))/norme(n);
    xl_int  (n) = xl(i) + h*(1+coeff*xu(i))*delta_yl(n);
    yl_int  (n) = yl(i) - h*(1+coeff*xu(i))*delta_xl(n);
    n=n+1;
end

% Creation du point xl_int(n) - bdf (A. Merle : passer tous les indices en xl)
norme                   = sqrt(power((xl(length(xl))-xl(length(xl)-1)),2)+power((yl(length(yl))-yl(length(yl)-1)),2)); 
delta_xl                = (xl(length(xl))-xl(length(xl)-1))/norme;
delta_yl                = (yl(length(yl))-yl(length(yl)-1))/norme;
xl_int(length(xl_int))  =  xl(length(xl)) + h*(1+coeff*xu(length(xu)))*delta_yl;
yl_int(length(yl_int))  =  yl(length(yl)) - h*(1+coeff*xu(length(xu)))*delta_xl;
% ==== Intrados end ===============

% =================================================
% Mise a l'echelle
% Z! : modif xu ,... local : ils sont modifier ici mais pas passer comme output de cette function
% =================================================
%if data_geo.scale==1  % c is the scale if scale=1 else scale is scale
%    fprintf(' szone.m : c = %g m is the scale of geometry\n',c);
%    x_bdf   = c*x_bdf;
%    y_bdf   = c*y_bdf;
%    xu      = c*xu;
%    xl      = c*xl;
%    yu      = c*yu;
%    yl      = c*yl;
%    xu_int  = c*xu_int;
%    xl_int  = c*xl_int;
%    yu_int  = c*yu_int;
%    yl_int  = c*yl_int;
%end
% =================================================
% Export fichier de points pour creation maillage
% =================================================

% ordre des vertex (Z! : jamais 2 fois le m�me point)
% 1 : extrados du bda au bdf
% 2 : intrados du point apr�s le bda jusqu'au point avant le bdf
% 3 : extrados_int du bda au bdf
% 4 : intrados_int du point apr�s le bda jusqu'au point bdf
% 5 : ajout 1 point sillage, 1 point sillage up, 1 point sillage down

points_file = 'profile_2d.dat';
fid=fopen(points_file,'w');

for i = 1 : length(xu)-1
    fprintf(fid,'%2.5f %2.5f %2.5f \n', xu(i), yu(i), 0);   % points extrados (0 < xu < 0.99?)
end
II=i;
fprintf(fid,'%2.5f %2.5f %2.5f \n', x_bdf, y_bdf, 0 );  % ajout point bdf extrados
II=II+1;
extrados_start=1;   % first point ligne extrados = bda
extrados_stop =II;  % last  point ligne extrados = bdf

for i = 2 : length(xl)-1
    fprintf(fid,'%2.5f %2.5f %2.5f \n', xl(i), yl(i), 0);   % points intrados (0.0? < xl < 0.99?)
    II=II+1;
end
intrados_start  = extrados_stop+1;      % premier point intrados = point apres bda
intrados_stop   = II;                   % dernier point intrados = point avant bdf

for i = 1 : length(xu_int)
    fprintf(fid,'%2.5f %2.5f %2.5f \n', xu_int(i), yu_int(i), 0);   % points ligne superieure, i=1,n
    II=II+1;
end
extrados_int_start  = intrados_stop + 1;    % premier point ligne superieure
extrados_int_stop   = II;                   % dernier point ligne superieure

for i = 2 : length(xl_int)
    fprintf(fid,'%2.5f %2.5f %2.5f \n', xl_int(i), yl_int(i), 0);   % points ligne inferieure, i=2,n
    II=II+1;
end
intrados_int_start  = extrados_int_stop + 1;    % premier point ligne inferieure
intrados_int_stop   = II;                       % dernier point ligne inferieure

pente1=(yu(length(yu))-yu(length(yu)-1))/(xu(length(xu))-xu(length(xu)-1));
pente2=(yl(length(yl))-yl(length(yl)-1))/(xl(length(xl))-xl(length(xl)-1));
pente = 0.5*(pente1+pente2);
x_sill=sqrt(power(coeff_sill,2)/(1+power(pente,2)));
%x_sill=sqrt(power(c*coeff_sill,2)/(1+power(pente,2)));

fprintf(fid,'%2.5f %2.5f %2.5f \n', x_bdf+x_sill, y_bdf+x_sill*pente, 0);                               % ajout point bdf
fprintf(fid,'%2.5f %2.5f %2.5f \n', xu_int(length(xu_int))+x_sill, yu_int(length(yu_int))+x_sill*pente, 0);     % ajout point bdf superieur
fprintf(fid,'%2.5f %2.5f %2.5f \n', xl_int(length(xl_int))+x_sill, yl_int(length(yl_int))+x_sill*pente, 0);     % ajout point bdf inferieur

fclose(fid);

% =====================================================
% Definition des surfaces du maillage autour du profil
% =====================================================

% Cela est fait ici et non dans journal_GAMBIT car j'ai besoin des xu, ...
% non disponible dans journal_GAMBIT (lecture du fichier vertex)

% 21/11/11 : commented to implement VLab([],45)
%[s_bda]     = sprintf('bda');
%[s_extra]   = sprintf('extrados');
%[s_intra]   = sprintf('intrados');
%[force_surface]    = sprintf('%s %s %s', s_bda, s_extra, s_intra);

bda_start           = extrados_start;
bda_stop            = i_bda;
extrados_start      = bda_stop;
intrados_start      = extrados_stop+i_bda-1;
extrados_int_start  = intrados_stop+i_bda;
intrados_int_start  = extrados_int_stop+i_bda-1;

s       = curve_length (xu(1:i_bda),yu(1:i_bda));l_bda1  = s(size(xu(1:i_bda),2));
s       = curve_length (xl(1:i_bda),yl(1:i_bda));l_bda2  = s(size(xl(1:i_bda),2));
l_bda   = l_bda1 + l_bda2;                                          % longueur curviligne bda (xu(i) i=1,4 et xl(i) i=1,4)
s       = curve_length (xu(i_bda:length(xu)),yu(i_bda:length(xu)));
l_ext   = s(size(xu(i_bda:length(xu)),2));                          % longueur curviligne extrados (xu(i) i=4,n)
s       = curve_length (xl(i_bda:length(xu)),yl(i_bda:length(xu)));
l_int   = s(size(xl(i_bda:length(xl)),2));                          % longueur curviligne intrados (xl(i) i=4,n)
l_sill  = coeff_sill;
%l_sill  = coeff_sill*c;

size_mini   = bda_size;             % used at bda - smallest mesh size in streamwise direction
size_bdf    = 2*size_mini;          % used at bdf

n_bda   =   round(l_bda  / bda_size);                   % nombre de points bda (suppl�ment de points au bda car l_ bda_int >> l_bda)
n_ext   =   round(l_ext  / bda_size / coeff_surf );     % nombre de points extrados
n_sill  =   round(l_sill / bda_size / 2);               % nombre de points sillage

switch symmetric
    case 0
n_int   =   round(0.75 * n_ext);                        % nombre de points intrados
    case 1
n_int   =   n_ext;                                      % nombre de points intrados
    otherwise
n_int   =   n_ext;                                      % nombre de points intrados
end

% data.topo pour journal GAMBIT

data_topo.i_bda                 = i_bda;
data_topo.bda_start             = bda_start;
data_topo.bda_stop              = bda_stop;
data_topo.extrados_start        = extrados_start;
data_topo.extrados_stop         = extrados_stop;
data_topo.intrados_start        = intrados_start;
data_topo.intrados_stop         = intrados_stop;
data_topo.extrados_int_start    = extrados_int_start;
data_topo.extrados_int_stop     = extrados_int_stop;
data_topo.intrados_int_start    = intrados_int_start;
data_topo.intrados_int_stop     = intrados_int_stop;
data_topo.force_surface         = force_surface;
data_topo.surf_bc               = surf_bc;

data_topo.n_bda         = n_bda;
data_topo.n_ext         = n_ext;
data_topo.n_int         = n_int;
data_topo.n_sill        = n_sill;

% Affichage for details
profil_num  =   round(n_bda+n_ext+n_int+2*n_sill);      % mesh points along the airfoil
if strcmp(data_num.Affichage,'yes')
fprintf(' Airfoil mesh         : %g x %g = %g\n',profil_num,data_mesh.n_cl,profil_num*data_mesh.n_cl);
fprintf(' Minimum mesh ratio   : %g \n',size_mini/h1);
fprintf(' BDF     mesh ratio   : %g \n',size_bdf /h1);
fprintf(' Extrados curve length: %g \n',l_ext       );
%fprintf(' Extrados curve length: %g \n',l_ext    / c);
end

% Affichage for debug
topo_file   = 'topo.txt';
fid         = fopen (topo_file,'w');

fprintf(fid,' Airfoil mesh         : %g x %g = %g\n',profil_num,data_mesh.n_cl,profil_num*data_mesh.n_cl);
fprintf(fid,' Minimum mesh ratio   : %g \n',size_mini/h1);
fprintf(fid,' BDF     mesh ratio   : %g \n',size_bdf /h1);
fprintf(fid,' Extrados curve length: %g \n',l_ext       );
%fprintf(fid,' Extrados curve length: %g \n',l_ext    / c);
fclose (fid);

%data_topo
if strcmp(data_num.debug,'yes');fprintf('szone.m : end\n');end

