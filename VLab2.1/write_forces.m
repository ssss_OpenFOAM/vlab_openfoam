function  write_forces (param)
% Compute Loads on the object in the fluid
% FLUENT    : /report/forces/wall-forces ...

% param     : parameters of the function
% output    : VLab_Fx.txt, ...

fluent_ver      = param.Fluent_ver;
dim             = param.dim;
fid             = param.fid;
inc             = param.inc;

% delete files if they exist (prevent errors)
filename = 'VLab_Cl.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_Cd.txt';if exist(filename,'file');delete (filename);end

filename = 'VLab_Fx.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_Fy.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_Fz.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_Mx.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_My.txt';if exist(filename,'file');delete (filename);end
filename = 'VLab_Mz.txt';if exist(filename,'file');delete (filename);end

all_wall_zones  = 'yes';
write_to_file   = 'yes';

switch dim  
    case {'2D'}
        Fx  = cos(inc*pi/180); Fy = sin(inc*pi/180);
        force_file      = 'VLab_Cd.txt';
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s \n',all_wall_zones,Fx,Fy,write_to_file,force_file);    % only Fluent 6.3.26 & not 6.2.16
        Fx  =-sin(inc*pi/180); Fy = cos(inc*pi/180); Fz = 0;
        force_file      = 'VLab_Cl.txt';
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s \n',all_wall_zones,Fx,Fy,write_to_file,force_file);    % only Fluent 6.3.26 & not 6.2.16
        Fx  = 1; Fy = 0; 
        force_file      = 'VLab_Fx.txt';
        fprintf(fid,'/report/forces/wall-forces %s %g %g %s %s \n',all_wall_zones,Fx,Fy,write_to_file,force_file);    % only Fluent 6.3.26 & not 6.2.16
        Fx  = 0; Fy = 1;
        force_file      = 'VLab_Fy.txt';
        fprintf(fid,'/report/forces/wall-forces %s %g %g %g %s %s \n',all_wall_zones,Fx,Fy,Fz,write_to_file,force_file);
        switch fluent_ver(1)
            case {'6'} %case '6.3.26'
                Fx  = 1; Fy = 0;
                force_file      = 'VLab_Mx.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %s %s \n',all_wall_zones,Fx,Fy,write_to_file,force_file);
            case {'1'} %case {'13.0.0','14.0.0'}
                x_moment_center = 0; y_moment_center = 0;
                x_moment_axis   = 1; y_moment_axis   = 0; z_moment_axis = 0;
                force_file      = 'VLab_Mx.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %g %g %g %s %s \n',all_wall_zones,x_moment_center,y_moment_center,x_moment_axis,y_moment_axis,z_moment_axis,write_to_file,force_file);
        end        
    case {'3D'}     
        Fx = 0;Fy = 0;Fz = 1;
        force_filename = 'VLab_Fz.txt';
        fprintf(fid,'/report/forces/wall-forces %s %g %g %g %s %s \n',all_wall_zones,Fx,Fy,Fz, write_to_file,force_filename);
        switch fluent_ver(1)
            case {'6'} %case '6.3.26'
                Fx = 0; Fy = 1; Fz = 0;
                force_filename = 'VLab_My.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %g %s %s \n',all_wall_zones,...
                    Fx,Fy,Fz,write_to_file,force_filename);
                Fx = 0; Fy = 1; Fz = 0;
                force_filename = 'VLab_Mz.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %g %s %s \n',all_wall_zones,...
                    Fx,Fy,Fz, write_to_file,force_filename);
            case {'1'} %case {'13.0.0','14.0.0'}
                x_moment_center = 0; y_moment_center = 0;
                x_moment_axis   = 0; y_moment_axis   = 0; z_moment_axis = 1;
                force_filename = 'VLab_My.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %g %g %g %s %s \n',all_wall_zones,...
                    x_moment_center,y_moment_center,x_moment_axis,y_moment_axis,z_moment_axis,write_to_file,force_filename);
                x_moment_center = 0; y_moment_center = 0;
                x_moment_axis   = 0; y_moment_axis   = 0; z_moment_axis = 1;
                force_filename = 'VLab_Mz.txt';
                fprintf(fid,'/report/forces/wall-moments %s %g %g %g %g %g %s %s \n',all_wall_zones,...
                    x_moment_center,y_moment_center,x_moment_axis,y_moment_axis,z_moment_axis,write_to_file,force_filename);
        end        
end

disp('  VLab_F?.txt files created for loads transfer to FSI');
end

