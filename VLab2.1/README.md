VLAB2 reverse engineering
===============================

Vamos a intentar ver qué hace cada fichero importante para estar seguro de que hacemos todo correctamente

VLab2.m
------------------------

Script principal, encargado de llamar al generador de la malla, a Fluent/OpenFOAM, postprocesar, etc. Empieza en la línea 230 definiendo el array Results que se utiliza en `post.m`:

    Result    = zeros(1,22);

De las líneas 408 a la 432 crea las figures donde se plotea `Figures VLab : results for comparisons (analysis, optimization)`

    if strcmp(data_sim.comparison,'yes')
        figure(3);
        data_sim.data_type = 'num'; %?
        switch data_sim.data_type
            case 'num'  % ?
            case 'file' % load file to implement
        end
        for i = 1 : size(data_sim.Cl_res,1)     % loop on the number of results
            %symbol = ['sk' 'sr' 'sg'];          % 'sk' 'sr' 'sg' for square symbol without line 
            subplot(2,2,1);
            plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),'s');hold on;
            %plot(data_sim.Cl_res(i,:),data_sim.Cd_res(i,:),symbol(i));hold on;
            title('Polar Cd = f(Cl)');
            subplot(2,2,2);
            plot(data_sim.Cl_res(i,:),data_sim.Cl_res(i,:)./data_sim.Cd_res(i,:),'s');hold on; % new  point in red
            title('Lift to drag ratio Cl/Cd = f(Cl)');
            subplot(2,2,3);
            plot(data_sim.al_res(i,:),data_sim.Cl_res(i,:),'s');hold on;
            title('Lft versus angle of attack Cl = f(i)');
            subplot(2,2,4);
            plot(data_sim.al_res(i,:),data_sim.Cm_res(i,:),'s');hold on;
            title('Moment versus lift Cm(c/4) = f(i)');

        end
    end 


En líneas 446-455 tiene los puntos del perfil con la función `airfoil_geometry`

    if strcmp(data_geo.sequence.type(1:3),'geo')
    fprintf('GEO : start \n');
    n_cut = size(data_geo.cut.var,1);                                   % nmuber of cuts
    for i = 1 : n_cut                                                   % loop   on cuts (default value 1)
        [xu,yu,xl,yl,data_geo]  = airfoil_geometry (data_geo,i);        % create or read 2D geometry (airfoil or sail cut)
    end
    %data_geo.cut.var(:,2)
    %[data_sim.str_file]     = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
    fprintf('GEO : end \n\n');
    end

En las líneas 471-493 se encarga de calcular los puntos de la malla alrededor del perfil y la zona exterior para Gambia y lo guarda en 
points file y data_topo. Estos ficheros son utilizados para generar journal_GAMBIT. 


    if strcmp(data_geo.sequence.type(5:8),'topo')
    fprintf('TOPO : start \n');
    switch n_conf
        case {0,1,4,6,10,11,12,13,14,17,20,21,22,23,38,39,40,44,45,46,47,48,49,54,55,99,100,101,102,103,107,140,143,144,145,146,200,201,499,1001,1002,1003}
            [points_file,data_topo]     = szone             (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
        case {919,9194}
            %[points_file,data_topo]     = szoneb            (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
            [points_file,data_topo]     = szoneb            (data_geo,data_topo,data_mesh,data_num,data_ac)
        case {2,3,5,7,15,30,41,51,52,53,60,61,63,71,72,73}
            [points_file,data_topo]     = szone_ac          (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);     % profile_2d.dat = f(geoJ.dat)
        case {80}   % Parapente : Louis PERROT-MINOT (CFD 3A)
            [points_file,data_topo]     = szone_para        (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);
        case {81}   % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
            [points_file,data_topo]     = szone_parafoil    (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);  
        case {82}   % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
            [points_file,data_topo]     = szone_parafoil2    (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl); 
        case {9}    % airfoil withou bda surface
            [points_file,data_topo]     = szone_bda         (data_geo,data_topo,data_mesh,data_num,data_ac,xu,yu,xl,yl);
        case {472}    % wingsail Starccm+
            szone_SCCM                                      (data_geo,data_num);
    end
    fprintf('TOPO : end \n\n');
    end

En las líneas 500-597, selecciona el mesher y crea el journal. **Aquí es donde deberíamos llamar a nuestro mesher `GMSH`**

    if strcmp(data_geo.sequence.type(10:13),'jouM') || strcmp(data_geo.sequence.type(10:13),'jouG') || strcmp(data_geo.sequence.type(10:13),'jou2') % if RANS
    fprintf('jouM : start \n');
    switch n_conf
        case {0,1,4,6,10,11,12,13,14,17,20,21,22,23,38,39,40,54,55,99,100,101,102,103,107,499,919,1001,1002,1003,9194}
        [gambit_journal_file,mesh_file] = journal_GAMBIT      (data_geo,data_mesh,data_topo,data_sim,data_num);           % (data -> *.jou)
        ...

En las líneas 602-611 establece el nombre del mesh script. Aquí añadimos el caso en de `GMSH`. La variable de elección `data_num.mesher` es 
establecida mediante el archivo default2 y simplemente es asignado a `data_num.mesher = GAMBIT`. Podríamos cambiarlo ahí o reasignarla en VLAB2. 
    switch data_num.mesher
                case 'GAMBIT'
                    data_geo.sequence.mesh_script_file  = gambit_journal_file;  % journal file computed in jouM
                case 'ICEM'
                    data_geo.sequence.mesh_script_file  = icem_replay_file;     % replay  file computed in jouM
                case 'STAR'
                    data_geo.sequence.mesh_script_file  = sccm_journal_file;    % replay  file computed in jouM
                case 'GMSH'
                    data_geo.sequence.mesh_script_file  = gmsh_journal_file; 
    end

En las líneas 680-687 se encarga de establecer diferences variables que tienen que ver con la turbulence. La función `eval_yplus` realiza una estimación de 
y_plus al final de la cuerda mediante Re_c y una estimación de Cf mediante una fórmula empírica. 

    if strcmp(data_sim.regime,'turbulent')
        data_mesh.yplus  = eval_yplus       (data_mesh.y_wall,data_num.Affichage,data_geo,data_sim,data_phy);           % evaluate wall y+ (turbulent flat plate)
        [L,lk,tL,tlk]    = flow_scales      (data_sim.Vinf,data_geo.c,data_sim.I_turb,data_phy.nu,data_num.Affichage);  % turbulent scales (L, lk, tL, tlk)
        data_mesh.scales.L      = L;
        data_mesh.scales.lk     = lk;
        data_mesh.scales.tL     = tL;
        data_mesh.scales.tlk    = tlk;
    end


En las líneas 719-726 genera el filename del fichero. Esto es bueno porque deberías llamar nuestro testcase con ese nombre. Guarda el nombre en la variable `str_file`. **Deberíamos echar un vistazo para ver realmente como construye este nombre**. **Mirar `file_name.m`**

    switch  n_conf                      % affectation des variables du cas courant
        case 80                         % Modif Parapente : Louis PERROT-MINOT (CFD 3A)
            [data_sim.str_file]     = file_name_para (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
        case {81,82}                       % Modification Parafoil PIP: F.Manara; F.Sgarbossa; A.MeyerForsting
            [data_sim.str_file]     = file_name_parapente (data_geo,data_num,data_sim,data_ac,i_cas,data_topo);
        otherwise
            [data_sim.str_file]     = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo); % create string for filenames of the current case
    end

En las líneas 747-775 genera el journal. **Aquí hay que llamar a nuestra función `openfoam_journal`**. **Atención al mesh_file que ahí tiene que estar el nombre de nuestra mesh**.

    if (data_sim.sim == 1 || data_sim.sim == 2)                 % 1:simulation, 2:?
    if strcmp(data_geo.sequence.type(20:23),'jouF')
        fprintf('jouF : start \n');
        mesh_file = data_geo.sequence.mesh_file;                   % The mesh : read in affect?.m or computed in journal_GAMBIT.m

    % Time step, Simulation time, Time for mean
    [data_sim.time_step, data_sim.time_sim, data_sim.iter_mean, data_sim.iter_sim] = The_time (data_geo.c, data_sim.Vinf, data_ac.Freq, data_sim.period_sim, data_sim.period_ndt, data_sim.period_mean);

    switch data_geo.sequence.fluent_journal
        case 'fluent_journal'
        [fluent_journal_file]           = fluent_journal     (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_cyl'
        [fluent_journal_file]           = fluent_jou_cyl     (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_cube'
        [fluent_journal_file]           = fluent_jou_cube    (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_cube2'
        [fluent_journal_file]           = fluent_jou_cube2   (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_18ft'
        [fluent_journal_file]           = fluent_jou_18ft    (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_sail_3D'
        [fluent_journal_file]           = fluent_jou_sail_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_Wing_3D'
        [fluent_journal_file]           = fluent_jou_Wing_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'fluent_jou_CROR_3D'
        [fluent_journal_file]           = fluent_jou_CROR_3D (os,mesh_file,data_geo,data_topo,data_sim,data_ac,i_cas,data_num);  % Journal   FLUENT (data -> *.jou)
        case 'SCCM_AC72_Phys'
        [sccm_journal_file]             = jou_SCCM_AC72_Sim  (data_geo,data_mesh,data_topo,data_sim,data_num); % Journal SCCM (data > *.jou)
        fluent_journal_file = sccm_journal_file;
    end
        fprintf('jouF : end \n\n');
    end



En las líneas 782-788 llama a fluent y hace postprocesado `post.m`. **Aquí habría que añadir el caso con OpenFOAM llamando a nuestro `openfoam_launch`**

    if strcmp(data_geo.sequence.type(25:28),'runF') || strcmp(data_geo.sequence.type(25:28),'run2') % if RANS
        fprintf('runF : start \n');
        [data_num]                      = fluent_launch    (os,fluent_journal_file,data_num,data_sim);              % Lancement FLUENT (*.msh -> *.cas)
        data_sim.Nit                    = data_sim.iter_trans+2*data_sim.iter_sup;
        [Resultc, Result, Err_Clmean]   = post             (os,data_geo,data_num,data_sim,data_ac);
        fprintf('runF : end \n\n');
    end


836-889 mira si converge, si no sigue corriendo. **No estoy seguro**

    if size(x,1)==0;                % if analysis mode
        if strcmp(data_num.fluent_jloads,'yes')
        [fluent_journal_file] = fluent_jloads (data_topo,data_sim,data_num);            % VLab_Drag.txt, VLab_Lift.txt
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
        if strcmp(data_num.fluent_jend,'yes')
        [fluent_journal_file] = fluent_jend   (data_geo,data_topo,data_sim,data_num);   % VLab_Cl.txt, ..., *_field.txt, *_BL.txt, *_Model.txt, fluent-interpolation.ip
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
        if strcmp(data_num.fluent_jimg,'yes')
        [fluent_journal_file] = fluent_jimg   (data_geo,data_topo,data_sim,data_num);   % *.tif
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
    end

903-916 más postprocessing

    if size(x,1)==0;                % if analysis mode
        if strcmp(data_num.fluent_jloads,'yes')
        [fluent_journal_file] = fluent_jloads (data_topo,data_sim,data_num);            % VLab_Drag.txt, VLab_Lift.txt
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
        if strcmp(data_num.fluent_jend,'yes')
        [fluent_journal_file] = fluent_jend   (data_geo,data_topo,data_sim,data_num);   % VLab_Cl.txt, ..., *_field.txt, *_BL.txt, *_Model.txt, fluent-interpolation.ip
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
        if strcmp(data_num.fluent_jimg,'yes')
        [fluent_journal_file] = fluent_jimg   (data_geo,data_topo,data_sim,data_num);   % *.tif
                   [data_num] = fluent_launch (os,fluent_journal_file,data_num,data_sim);
        end
    end

A partir de aquí es todo postprocessing.

filename.m
-----

El fichero es nombrado como `str_file = str_geo + str_sup`.
Donde  `str_geo = 'geo.dat'`si data_geo.method.str_geo no varia de su valor en defaults2.m **Por revisar**.
El string `str_sup` es algo mas elaborado:

`str_sup  = ['_v',num2str(Vinf),'_i',num2str(inc),'_',s_name,'_F',num2str(Freq),'Vjm',num2str(Vjmean),'Vjf',num2str(Vjfluct),'t',num2str(theta),'p',num2str(100*pos),'d',num2str(100*dia),'_',str_model,'_',num2str(i_cas)]`

Todas esas variables que conforman str_sup estan definidas en defaults.m. 

		function [str_file] = file_name (data_geo,data_num,data_sim,data_ac,i_cas,data_topo)
			%
			% str_file = strcat(str_geo,str_sup)    % a string for a filename

			%===================================
			%  str_geo : part 1 of the filename
			%===================================
			str_geo     = data_geo.method.str_geo;  % part 1 of the filename

			if strcmp(str_geo , '')
			    str_geo = data_geo.filename{1};    % filename without extension (.txt or .dat)
			end

			%===================================
			%  str_sup : part 2 of the filename
			%===================================
			Vinf        = data_sim.Vinf;
			inc         = data_sim.inc;
			str_URANS   = data_sim.str_URANS;
			regime      = data_sim.regime;
			str_dir     = data_sim.str_dir;
			dim         = data_sim.dim;
			Tu_model    = data_sim.Tu_model;

			theta   = data_ac.Theta_deg;
			pos     = data_ac.pos;
			dia     = data_ac.dia;
			Freq    = data_ac.Freq;
			Vjfluct = data_ac.Vjfluct;
			Vjmean  = data_ac.Vjmean;

			switch regime
			    case 'laminar'
				switch str_URANS
				    case 'RANS'
					str_model = 'LS';
				    case {'DNS','URANS'}
					str_model = 'DNS';
				end
			    case 'turbulent'
				str_model = str_URANS;
			end

			switch data_ac.ac
			    case 'no'
			%    str_sup  = ['_v',num2str(Vinf),'_i',num2str(inc),'_',str_model,'_',num2str(i_cas)];
			    str_sup  = ['_v',num2str(Vinf),'_i',num2str(round(inc)),'_',str_model,dim,'_',Tu_model,'_',num2str(i_cas)];
			    case 'yes'
				switch data_ac.type
				    case 'continuous jet'
					s_name = 'j';
				    case 'synthetic jet'
					s_name = 'js';
				    case 'pulsed jet'
					s_name = 'jp';
				end
			    str_sup  = ['_v',num2str(Vinf),'_i',num2str(inc),'_',s_name,'_F',num2str(Freq),'Vjm',num2str(Vjmean),'Vjf',num2str(Vjfluct),'t',num2str(theta),'p',num2str(100*pos),'d',num2str(100*dia),'_',str_model,'_',num2str(i_cas)];
			end

			%str_optim = num2str(x);
			%ind         = [first_intrados:last_intrados];  [intrados] = sprintf('"_x%g_%g" ',ind);     % string des 251 vertex intrados
			%==================================================================================

		str_file = strcat(str_geo,str_sup);         % filename = str_geo + str_sup
		%str_file = strcat(str_dir,'/',str_file);

write_forces.m
-----

Parece un script que se encarga de crear un journal de Fluent para escribir ficheros con nombre `VLab_F?.txt`. En la última línea del código se escribe: `  VLab_F?.txt files created for loads transfer to FSI` lo que quiere decir que no debe ser importante para nosotros.

fluent_jloads.m
-----

Script que escribe los un journal de Fluent para escribir el drag y el lift (en magnitud) en los archivos `VLab_Drag.txt` y `VLab_Lift.txt`. Puede que sea importante

In `system/controlDict`:

```
libs
(
    "libmyIncompressibleRASModels.so"
);
```

In `constant/turbulenceProperties` set

    simulationType RAS;

        RAS
        {
            RASModel        kOmegaSSTLowRe;
            turbulence      on;
            printCoeffs     on;
        }

Compilation/installation
------------------------

The current implementation does only support incompressible flow because no correction was applied to the original coded k and omega equations. This has to be basically with the way in which phi_ is calculated:

        const surfaceScalarField& phi_ = this->alphaRhoPhi_;

The model equations can be extended to a compressible formulation, although it requires rewriting the equations for k and omega, and the model might become invalid.

### Boundary conditions

|  Quantity | BC |
|----------:|:----|
| `p`     | `zeroGradient`  |
| `U`     |  `fixedValue (0 0 0)` |
| `nut`   |  `nutLowReWallFunction fixedValue uniform 0` |
| `k`     |  `fixedValue uniform 1e-12` |
| `omega` |  `omegaWallFunction` |
