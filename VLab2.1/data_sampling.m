function  data_sampling (fid, Fluent_ver)

% solve/iterate : data-sampling (URANS, DNS, ...)
        
time_statistics         = 'yes';
sampling_interval       = 1;
shear_stress_statistics = 'yes';
wall_statistics         = 'yes';

switch Fluent_ver
    case '6.2.16'
        fprintf(fid,'/solve/set/data-sampling %s\n',time_statistics);
    case '6.3.26'
        fprintf(fid,'/solve/set/data-sampling %s %g %s %s\n', time_statistics, sampling_interval, shear_stress_statistics, wall_statistics);
    case '6.4.11'
        fprintf(fid,'/solve/set/data-sampling %s %g %s %s\n', time_statistics, sampling_interval, shear_stress_statistics, wall_statistics);
end
