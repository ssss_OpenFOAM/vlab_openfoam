function [cas,var,str_var] = data0 (x,data_sim)

cas = 1;

switch cas
    case 1
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0012' '    40' '   8.3' '     1' ];
    case 2 % Yes! high camber thin airfoil
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                ' 12402' '    10' '     8' '     1' ];
    case 3 % Yes high camber
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  8412' '    10' '     8' '     1' ];
    case 4 % Comparison of 2 airfoils polars
str_var     = [ '  NACA' '  Vinf' '   inc' ' delta' ;
                '  0021' '    10' '     0' '     1' ;
                '  0021' '    10' '     2' '     1' ;
                '  0021' '    10' '     4' '     1' ;
                '  0021' '    10' '     6' '     1' ;
                '  0021' '    10' '     8' '     1' ;
                '  0021' '    10' '    10' '     1' ;
                '  0021' '    10' '    12' '     1' ;
                '  0021' '    10' '    14' '     1' ;
                '  0021' '    10' '    16' '     1' ;
                '  0021' '    10' '    18' '     1' ;
                '  0021' '    10' '    20' '     1' ;
                '  0012' '    10' '     0' '     1' ;
                '  0012' '    10' '     2' '     1' ;
                '  0012' '    10' '     4' '     1' ;
                '  0012' '    10' '     6' '     1' ;
                '  0012' '    10' '     8' '     1' ;
                '  0012' '    10' '    10' '     1' ;
                '  0012' '    10' '    12' '     1' ;
                '  0012' '    10' '    14' '     1' ;
                '  0012' '    10' '    16' '     1' ;
                '  0012' '    10' '    18' '     1' ;
                '  0012' '    10' '    20' '     1' ];
end

            
var = str2var (str_var, x, 6);