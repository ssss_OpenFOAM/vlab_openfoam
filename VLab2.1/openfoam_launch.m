function [data_num] = openfoam_launch (os,openfoam_journal_file,data_num,data_sim, data_geo, mesh_script_file)
%
% input data :
%   num     : os, parallel, nproc, fluent_jou
% FLUENT options :
% -r    version
% -ssh  ?
% -p    parallel
% -t    number of processors
% -g    without graphic
% -i    journal file

t_start_solve = clock;

parallel    = data_num.parallel;
nproc       = data_num.nproc;
str_file    = data_sim.str_file;
graphic     = data_sim.graphic;

if strcmp(data_sim.dim,'2D'); fluent_dim = '2ddp';end;
if strcmp(data_sim.dim,'3D'); fluent_dim = '3ddp';end;

%if strcmp(data_num.Affichage,'yes');fprintf(' fluent_launch : %s, FLUENT %s %s %s\n',os,fluent_ver, fluent_dim, fluent_jou);end;
fprintf(' openFOAM_launch : %s, OpenFOAM 3.0.1 \n',os);

%======================
% Run Fluent sous Linux
%======================
if strcmp(os,'Linux')
    mesh_script_file = strrep(mesh_script_file, '.geo', '.msh');
    slaunch = sprintf('gmshToFoam -case %s %s > %s/log.GMSHTOFOAM',openfoam_journal_file, mesh_script_file, openfoam_journal_file);
    [sta,str] = system  (slaunch);
    slaunch = sprintf('pyFoamChangeBoundaryType.py  %s front empty ',openfoam_journal_file);
    [sta,str] = system  (slaunch);
    slaunch = sprintf('pyFoamChangeBoundaryType.py  %s back empty ',openfoam_journal_file);
    [sta,str] = system  (slaunch);
    slaunch = sprintf('pyFoamChangeBoundaryType.py  %s airfoilWall wall ',openfoam_journal_file);
    [sta,str] = system  (slaunch);
    file    = 'finish.txt';
    if exist(file,'file')==2 ; system (sprintf('rm -f %s',file)); end
    file    = ['fluent_journal.trn'];
    if exist(file,'file')==2 ; system (sprintf('rm -f %s',file)); end
    if  (parallel == 1)
        s_nproc = num2str(nproc);
        slaunch = sprintf('potentialFoam -case %s > %s/log.pot',openfoam_journal_file,openfoam_journal_file);
        [sta,str] = system  (slaunch);
        slaunch = sprintf('simpleFoam -case %s > %s/log.simp &',openfoam_journal_file,openfoam_journal_file);
        [sta,str] = system  (slaunch);
        slaunch = sprintf('pyFoamPlotWatcher.py --non-persist --hardcopy --regexp-file=customRegexp %s/log.simp &',openfoam_journal_file);
        [sta,str] = system  (slaunch);
    else
        s_nproc = num2str(nproc);
        slaunch = sprintf('potentialFoam -case %s > %s/log.pot',openfoam_journal_file,openfoam_journal_file);
        [sta,str] = system  (slaunch);
        slaunch = sprintf('simpleFoam -case %s > %s/log.simp &',openfoam_journal_file,openfoam_journal_file);
        [sta,str] = system  (slaunch);
        slaunch = sprintf('pyFoamPlotWatcher.py --non-persist --hardcopy --regexp-file=customRegexp %s/log.simp &',openfoam_journal_file);
        [sta,str] = system  (slaunch);
    end          
end         % if Linux

%=============================================
% Mesh cells number = ?
%=============================================
advanced = 0;
if advanced==1
    sta; % defaults sta = 0
    str; % if data_sim.graphic=1 str = ''
    n_cells = 0;    % init
    string  = 'node flags';
    str_end = findstr(str,string);   % search for string in str
    if size(str_end,1) ~=0
        n_cells = str2num(str(1,str_end-9:str_end-1));
        if n_cells==0
            fprintf(' fluent_launch : Mesh cells = %i because data_sim.graphic=1 \n',n_cells);
        else
            fprintf(' fluent_launch : Mesh cells = %i \n',n_cells);
        end
    end
    if ~isnan(n_cells) || n_cells ~=0
        data_num.mesh_cells = n_cells;
    else
        data_num.mesh_cells = 1;
    end
end
%=============================================
% Detect Error in Fluent - Windows/Linux
% =============================================
% error_fatal = 1 - isempty(findstr(str,'ERROR'));
% if (sta~=0 || error_fatal~=0) % bad execution or licence pb
%     if (sta~=0)
%         fprintf('    Error during Fluent run \n');
%         fprintf('    Is FLUENT version ok ? \n');
%         fprintf('    Please STOP !!! \n');
%         fprintf('    status = %i \n',sta);
%         pause(360);
%     else
%         fprintf('    Licence FLUENT not available, still %g try \n',compteur);
%         fprintf('    result = %s \n',str);
%     end
%     sta = max(sta,error_fatal);
%     pause(10);
% else
%     fprintf(' fluent_launch : FLUENT run OK \n');
% end
%===========================================================
%
% MATLAB script wait end of Fluent - Windows/Linux
%
%===========================================================
waiting_time        = 0;
waiting_time_tot    = 0;
step                = 10;
max_cputime         = 24*3600;      % wait for 24 hours
delay               = 10;           % print delay
delay_sub           = delay/step;   % file test delay
coeff               = 2;            % coeff (may be estimated)
file                = 'finish.txt';
exitCond                = -1;

while (exitCond == -1) && (waiting_time < max_cputime)
    pause(delay_sub);
    waiting_time = waiting_time + delay_sub;
    fprintf(' %i ',waiting_time);
    fileName = strcat(openfoam_journal_file,'/log.simp');
    fid = fopen(fileName, 'r');
    fileContent=fileread(fileName)
    stopCond = strfind(fileContent,'End');
    disp('Stop');
    disp(strcat(openfoam_journal_file,'/log.simp'));
    disp(stopCond);
    if isempty(stopCond) ~= 1
        exitCond = 1
    end
    if waiting_time == delay
        waiting_time_tot = waiting_time_tot + waiting_time;
        [waiting_time_order,time_unity] = time_order (waiting_time_tot); % convert to relevant unit
        fprintf(' waited solver for %6.1f %s \n',waiting_time_order,time_unity);
        delay           = delay*coeff;  % new print delay
        delay_sub       = delay/step;   % new file test delay
        waiting_time    = 0;
    end
end
if fid==-1  % exit 1 : no finish.txt but cpu_time > 10 hours
    disp ('    Z! : finish.txt not created. Probably your FLUENT simulation was longer than 10 hours.');
    system(sprintf('cp Ok.txt finish.txt'));    % create finish.txt to continue (bug or too long run)
%else        % exit 2 : finish.txt exist and cpu_time < 10 hours
%    switch  os
%        case 'Linux'
%            system (sprintf('rm -f %s',file)); fprintf('    delete finish.txt after \n');
%        case 'Windows'
%            system (sprintf('del   %s',file)); fprintf('    delete finish.txt after \n');
%    end
end

fclose(fid);    % close finish.cas

%=============================================

if strcmp(data_num.Affichage,'yes');fprintf(' fluent_launch : end Ok \n');end;
data_num.ellapsed_time   = etime(clock,t_start_solve);
data_num.cpu_time_solve  = data_num.cpu_time_solve + etime(clock,t_start_solve);
    
    
    


