%
%==========================================
% figure 6 image
%==========================================
figure(6);

%[X_img,map] = imread('img1.tif');           % nacelle to replace ?
%image(0,0,X_img);axis off;

file_image='';
%file_image = strcat(data_sim.str_file,'_grid_z1.tif');      % 950x565 pixels
%file_image = strcat(data_sim.str_file,'_pressure.tif');     % 950x565 pixels

switch data_num.visu
    case 'analysis'
        [X_img,map] = imread('img1.tif');           % if nothing specified
        image(0,0,X_img);axis off;   % plot a predefined image
    case 'opti'
        image_file = strcat(data_sim.str_file,'_grid_z1.tif');
        if exist(image_file,'file'); [X_img,map] = imread(image_file);else [X_img,map] = imread('img1.tif');end
        image(0,0,X_img);axis off;
        % fig 4 : incrustation texte
        xx = -36; yy = 310;text(xx,yy,'\itVLab 2.0 : Analysis Design & Optimization of Navier-Stokes Flows',...
            'FontName','Broadway','FontSize',8,...
            'FontWeight','bold',...
            'HorizontalAlignment','center','Margin',4,'Color',[0.3 0. 1.],'BackgroundColor',[1. 1. 1.]);
    otherwise                                       % ni analysis ni opti 
        [X_img,map] = imread('img1.tif');           % if nothing specified
        image(0,0,X_img);axis off;   % plot a predefined image
end