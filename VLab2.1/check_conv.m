function  check_conv (fid,data_sim)
%
%   regime = {'laminar', 'turbulent'}                   2 regimes implemented in VLab
%   Tu_model = {'SA','SST','SAS','SSTtr','kklw'}        5 Turbulence model implemented in VLab
%   Fluid_model = {'incompressible','compressible'}     2 fluid models

dim         = data_sim.dim;
regime      = data_sim.regime;
Tu_model    = data_sim.Tu_model;
Fluid_model = data_sim.Fluid_model;

% Affichage criteres de convergence

Eq = 0;

switch dim
    case '2D'
        Eq = 3;
    case '3D'
        Eq = 4;
end

switch Fluid_model
    case 'incompressible'
    case 'compressible'
        Eq = Eq + 1;    % energy equation
end

switch regime
    case 'laminar'
    case 'turbulent'
    switch Tu_model
        case 'SA'
        Eq = Eq + 1;
        case {'SST','SAS'}
        Eq = Eq + 2;
        case {'kklw'}
        Eq = Eq + 3;
        case {'SSTtr'}
        Eq = Eq + 4;
    end
end

switch Eq
    case 3
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no \n');
    case 4
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no \n');
    case 5
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no \n');
    case 6
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no no \n');
    case 7
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no no no \n');
    case 8
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no no no no \n');
    case 8
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no no no no \n');
    case 9
        fprintf(fid,'/solve/monitors/residual/check-convergence no no no no no no no no no \n');
    otherwise
        disp ('check_conv.m : wrong equation number implementation');
        pause (1000);
end