function [var] = str2var (str_var, x, word_length)
%
% string conversion -> example : '  12.5' '  30' '   x1' is converted in 12.5 30 x1
%
% replace strings from str_var like x1 ou x10 by their value in the x      vector
% replace strings from str_var like vector by by their value in the vector vector

var.n_cas   = size(str_var,1)-1;                % number of lines minus one (the fist comment line)
word_number = size(str_var,2) / word_length;    % word number on a line of str_var

%var.name(:,:)   % name of the strings
for i = 1 : word_number                         % for each string
    n_first = 1 + (i-1)*word_length;            % start of string
    n_last  = n_first + word_length-1;          % end of string
    var.name(i,:) = str_var(1,n_first:n_last);  % name of each string   
end

% var.value(:,:)    % strings converted in values
for i_cas = 1 : var.n_cas   % for each cases
n_var = 0;
for i = 1 : word_number     % is there any x in the string value ?
    n_first = 1 + (i-1)*word_length;
    n_last  = n_first + word_length-1;
    pos_x   = strfind(str_var(i_cas+1,n_first:n_last),'x');                        % x      position in the string
    pos_v   = strfind(str_var(i_cas+1,n_first:n_last),'vector');                   % vector position in the string
    
    if isempty(pos_x) && isempty(pos_v)
        var.value(i_cas,i) = str2double (str_var(i_cas+1,n_first:n_last));          % var.value = a real
        if isnan(var.value(i_cas,i))    % because it was not a real
%            toto = str_var(i_cas+1,n_first:n_last);    % Ok
%            var.value(i_cas,i) = toto(1:word_length);  % No, to be ok var.value should be a cell array
            var.value(i_cas,i) =         str_var(i_cas+1,n_first:n_last);           % var.value = a string
        end
% previous version
%        [var.value(i_cas,i),status] = str2num(str_var(i_cas+1,n_first:n_last));    % var.value = a real
%        if status==0
%         var.value(i_cas,i) = str_var(i_cas+1,n_first:n_last);
%        end
    else     % if there is '    x?' or ' vector'
        if pos_x
        number = str2double(str_var(i_cas+1,n_first+pos_x:n_last));       % variable number
        n_var = n_var + 1;                                                % number of variables for optimization
        if (n_var > size(x,2))  % test vraisemblance : it was size(x,2) but it should be size(x,1) !?
            disp(' Z! : stop in str2var : number of initialized variables < to the number of declared variables !!!');
            fprintf(' Number of variables = %g',n_var);
            fprintf(' Variables = %g',x);
            pause(1000); % to replace by a stop instruction
        else
            var.value(i_cas,i) = x(1,number);
        end
        else
            var.value(i_cas,i) = scalar;
        end
    end
    %var.value(i_cas,:)
end
end

%if (n_var < size(x,1))    % test de vraisemblance : it was size(x,2) but it should be size(x,1) !?
%    disp (' Z! : Le nombre de variables initialisées est supérieur au nombre de variables déclarées dans conf !!!');
%    n_var
%    x
%    pause(1000); % how to stop the code here ?
%end
