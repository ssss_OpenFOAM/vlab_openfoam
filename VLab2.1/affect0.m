function [data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = affect0 (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x);
% Test VLab run
data_sim.graphic            = 1;        % Fluent graphic (0: non, 1: oui)

data_num.Affichage          = 'yes';    % Affichage des variables data_*
data_num.Rep_data           = 'all';    % Choix des variables to print : {'geo','mesh','topo','sim','ac','all'} 
data_num.parallel           = 1;                % 1 = calcul parallel
data_num.nproc              = 2;                % nombre de processeurs

% ======================================================================
% Ne rien toucher sous cette ligne sans une bonne connaissance de VLab
% ======================================================================
% ======================================
% Affectation du cas courant from data0
% ======================================
% 4512 -> m=0.04 p=0.5 t=0.12
m   =   fix(  var.value(i_cas,1)/1000 );
p   =   fix( (var.value(i_cas,1)-1000*m)/100 );
t   =         var.value(i_cas,1)-1000*m-100*p;

data_geo.naca4digits.m      =  m/100;
data_geo.naca4digits.p      =  p/10;
data_geo.naca4digits.t      =  t/100;
data_sim.geo    = [str_var(1,3:6) , str_var(i_cas+1,2:6)];      % 
data_sim.Vinf   = var.value(i_cas,2);
data_sim.inc    = var.value(i_cas,3);

data_sup.v1=0;

