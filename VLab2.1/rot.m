function [X_rot,Y_rot] = rot (X,Y,angle)
%
% Rotate around frame origin

npts  = size(X);

for i=1 : npts(2)    
X_rot (i) =   X (i)*cos(angle)    -   Y (i)*sin(angle); 
Y_rot (i) =   X (i)*sin(angle)    +   Y (i)*cos(angle); 
end

