function [time,time_unity] = time_order (time_sec)
%
% From a time in secs, gives the time order and its unity

% Ex :  305 secs gives 5 mins
% Ex : 7305 secs gives 2 hours

time_unity                  = 'secs';

if time_sec >           1e-9; time_unity='nanosecs ';end    % if time_sec is a vector, the condition should be true to all elements of the vector
if time_sec >           1e-6; time_unity='microsecs';end    % if time_sec is a vector, the condition should be true to all elements of the vector
if time_sec >           1e-3; time_unity='millisecs';end    % if time_sec is a vector, the condition should be true to all elements of the vector
if time_sec >              1; time_unity='secs';end         % if time_sec is a vector, the condition should be true to all elements of the vector
if time_sec >             60; time_unity='mins';end         % if time_sec is a vector, the condition should be true to all elements of the vector
if time_sec >           3600; time_unity='hours';end
if time_sec >        24*3600; time_unity='days';end
if time_sec >      7*24*3600; time_unity='weeks';end
if time_sec >    4*7*24*3600; time_unity='months';end
if time_sec > 12*4*7*24*3600; time_unity='years';end

switch time_unity
    case 'nanosecs '
        time      = 1e9*time_sec;
    case 'microsecs'
        time      = 1e6*time_sec;
    case 'millisecs'
        time      = 1e3*time_sec;
    case 'secs'
        time      = time_sec;
    case 'mins'
        time      = (time_sec/60);
    case 'hours'
        time      = (time_sec/3600);
    case 'days'
        time      = (time_sec/(3600*24));
    case 'weeks'
        time      = (time_sec/(3600*24*7));
    case 'months'
        time      = (time_sec/(3600*24*7*4));
    case 'years'
        time      = (time_sec/(3600*24*7*4*12));
end
