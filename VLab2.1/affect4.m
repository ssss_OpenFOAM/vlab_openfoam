function [data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac]   = affect4 (str_var,var,i_cas,data_geo,data_mesh,data_sim,data_num,data_sup,data_topo,data_ac,x);
% Validation NACA 0012 Re=6e6 with NASA data
%=================================================================
% Specific case definition (pr�empte defaults)
%=================================================================

%============================
% Geometry definition
%============================
% defined in data??.m
data_geo.c                  = 1;                    % chord length (in m)
data_geo.method.type        = 'naca_4digits';       % defined in affect.m           used in airfoil_geometry.m
data_geo.bdf                = 'with';               % with or without bdf

%============================
% Flow conditions
%============================
% defined in data??.m

%============================
% GAMBIT Mesh
%============================

mesh ='M1';
switch mesh
    case 'M0d'           % 2kc - y+~660 - 35x10
data_mesh.h1            = 5e-3;             % mesh size on wall in m
data_mesh.size_bda      = 1e-2;             % mesh size at bda in chord fraction
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_mesh.n_cl          = 20;               % number of gridpoints in the boundary layer - structured zone
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 800;              % Iterations of STOP in RANS/URANS
    case 'M0'           % 2kc - y+~660 - 35x10
data_mesh.h1            = 3e-3;             % mesh size on wall in m
data_mesh.size_bda      = 2e-2;             % mesh size at bda in chord fraction
data_mesh.n_cl          = 10;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.05;             % size of the structured zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 800;              % Iterations of STOP in RANS/URANS
    case 'M1'           % 14kc - 350x20 - Re=6e6 => y+~66
data_mesh.h1            = 3e-4;             % mesh size on wall             (percent of chord)
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          = 20;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.05;             % size of the structured zone   (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone         (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 1500;             % Iterations of STOP in RANS/URANS
    case 'M2'           % 22kc - y+~33 - 350x40
data_mesh.h1            = 1.5e-4;           % mesh size on wall in m
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          = 40;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     = 100;              % iterations of the transient O1
data_sim.iter_sup       = 100;              % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 3000;             % Iterations of STOP in RANS/URANS
    case 'M3'           % 55kc - y+~22 - 690x60
data_mesh.h1            = 1.0e-4;           % mesh size on wall in m
data_mesh.size_bda      = 1e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          = 60;               % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
%data_mesh.npts_edge_domain= 11;             % nombre de points edge domaine de calcul (10 � 25)
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 5000;             % Iterations of STOP in RANS/URANS
    case 'M4'           % 164kc - y+~11 - 1379x100
data_mesh.h1            = 5e-5;             % mesh size on wall in m
data_mesh.size_bda      = 5e-4;             % mesh size at bda in chord fraction
data_mesh.n_cl          = 100;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       = 10000;            % Iterations of STOP in RANS/URANS
    case 'M5'           % 76kc - y+~11
data_mesh.h1            = 5e-5;             % mesh size on wall in m
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M6'           % 76kc - y+~6
data_mesh.h1            = 3e-5;             % mesh size on wall in m
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M7'           % 76kc - y+~4
data_mesh.h1            = 2e-5;             % mesh size on wall in m
data_mesh.size_bda      = 1e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M8'           % 76kc - y+~11
data_mesh.h1            = 5e-5;             % mesh size on wall in m
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          =  60;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
    case 'M9'           % 76kc - y+~11
data_mesh.h1            = 5e-5;             % mesh size on wall in m
data_mesh.size_bda      = 2e-3;             % mesh size at bda in chord fraction
data_mesh.n_cl          =  40;              % number of gridpoints in the boundary layer - structured zone
data_mesh.h_mesh        = 0.10;             % size of the structured zone (percent of chord)
data_mesh.coeff_sill    = 0.1;              % size of the wake zone (percent of chord)
data_mesh.coeff_zone    = 1.;               % structured zone expansion toward bdf (1+coeff_zone)*h_mesh
data_mesh.npts_edge_domain_up       = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_down     = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_inlet    = 20;                   % nombre de points edge domaine de calcul
data_mesh.npts_edge_domain_outlet   = 20;                   % nombre de points edge domaine de calcul
data_sim.iter_trans     =  100;             % iterations of the transient O1
data_sim.iter_sup       =  200;             % iterations of the sliding window for Cl_mean
data_sim.Stop_Nit       =  3000;            % Iterations of STOP in RANS/URANS
end

data_mesh.y_wall    = data_mesh.h1;         % y+ evaluation

%============================
% Physical Modelization
%============================
data_sim.str_URANS          = 'RANS';           % choix RANS / URANS (utile uniquement si pas d'actionneur unsteady)
data_sim.regime             = 'turbulent';      % regime d'ecoulement (laminar, turbulent)
data_sim.Tu_model           = 'SA';             % Turbulence model
data_sim.I_turb             = 1;                % Turbulence Intensity (%) [1]
data_sim.L_turb             = 0.005;            % Turbulence Length Scale (m) [0.005]

%============================
% Numerical Modelization
%============================
data_sim.sim                = 1;            % simulation (0: non, 1: oui)
data_sim.graphic            = 1;            % Fluent graphic (0: non, 1: oui)
data_sim.scheme_order       = 'O2';         % ordre du scheme numerique spatial (O1, O2, O3)
data_sim.Stop_type          = 'Intelligent';% Stop Type : {'Intelligent','Interactive','Definite'} 
data_sim.Stop_Err           = 0.001; 

%============================
% Data numerique
%============================
data_num.parallel   = 1;
data_num.nproc      = 2;

%============================
% Comparison data
%============================
data_sim.comparison     = 'no';            % Nasa WT data
data_sim.al_res         = [10 15];          % [10 15]
data_sim.Cl_res         = [1.09 1.55];      % [1.09 1.55]
data_sim.Cd_res         = [0.0124 0.0212];  % [0.0124 0.0212]
% ======================================================================
% Ne rien toucher sous cette ligne sans une bonne connaissance de VLab
% ======================================================================

data_sim.str_dir        = strcat('BE2_',datestr(now));      % nom repertoire resultats
data_sim.str_dir(16)    = '-';                              % remplace espace par tiret
data_sim.str_dir(19)    = '';
data_sim.str_dir(21)    = '';

data_num.Affichage  = 'yes';    % Affichage des variables data_*
data_num.Rep_data   = 'all';    % Choix des variables � afficher : {'geo','mesh','topo','sim','ac','all'} 

switch data_mesh.mesh_algo
    case 'uns';
        data_mesh.y_wall    = data_mesh.size_bda;   % y+ evaluation
    case {'struct','hybrid'};
        data_mesh.y_wall    = data_mesh.h1;         % y+ evaluation
end

%===================================================================================
% Ne rien toucher sous cette ligne            
%===================================================================================

% ========================================================================
% Affectation des variables du cas courant telles que d�finies dans data1
% ========================================================================
% 4512 -> m=0.04 p=0.5 t=0.12
m   =   fix(  var.value(i_cas,1)/1000 );
p   =   fix( (var.value(i_cas,1)-1000*m)/100 );
t   =         var.value(i_cas,1)-1000*m-100*p;

data_geo.naca4digits.m      =  m/100;
data_geo.naca4digits.p      =  p/10;
data_geo.naca4digits.t      =  t/100;

data_sim.geo        = [str_var(1,3:6) , str_var(i_cas+1,2:6)];      % var  1 : NACA 0012 : 4 caracteres + 5 caracteres
data_sim.Vinf       = var.value(i_cas,2);                           % var  2 : Vent infini amont
data_sim.inc        = var.value(i_cas,3);                           % var  3 : Incidence

