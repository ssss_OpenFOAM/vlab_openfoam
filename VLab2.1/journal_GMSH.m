function [gmsh_journal_file,mesh_file] = journal_GAMBIT (data_geo,data_mesh,data_topo,data_sim,data_num)
% Create GAMBIT journal
%
% input : extrados, intrados, extrados_int, intrados_int
% input : s_bda, s_extra, s_intra, y_cl, n_cl, sill_num, npts_edge_domain,
% input : m,p,t
%
% unity : airfoil of chord c in meters
%
% liste des surfaces :
% bda               : surface bda du profil
% extrados          : surface extrados du profil
% intrados          : surface intrados du profil
% sillage           : surface sillage  du profil
%
% bda_int           : surface bda du profil
% extrados_int      : surface extrados du profil
% intrados_int      : surface intrados du profil
% sillage_int_up    : surface sillage  du profil
% sillage_int_down  : surface sillage  du profil
%
% CL_bda_up         : CL bda_up
% CL_bda_down       : CL bda_down
% CL_bdf_up         : CL bdf_up
% CL_bdf_down       : CL bdf_down
% CL_sillage_up     : CL sillage_up
% CL_sillage_down   : CL sillage_down
%
%disp('journal_gambit : debut');
gambit_ver  = data_num.Gambit_ver;
if strcmp(data_num.Affichage,'yes') fprintf(' Journal_GMSH : GAMBIT\n');end;
% affectations 
str_geo     = data_geo.method.str_geo;
%m   = data_geo.m;
%p   = data_geo.p;
%t   = data_geo.t;

mesh_algo               = data_mesh.mesh_algo;              % 

% both mesh type (non-dimensional)
npts_edge_domain        = data_mesh.npts_edge_domain;
npts_edge_domain_down   = data_mesh.npts_edge_domain_down;
N_Corde_Amont           = data_mesh.N_Corde_Amont;
N_Corde_Aval            = data_mesh.N_Corde_Aval;
N_Corde_Up              = data_mesh.N_Corde_ymax;
N_Corde_Down            = data_mesh.N_Corde_ymin;

% strutured mesh inside (prisms layer)
h1                      = data_mesh.h1;                     % first cell size on wall
n_BL                    = data_mesh.n_cl;                   % maximum number of cells in BL
g_BL                    = data_mesh.growthrate_inside;      % growthrate in BL

% strutured mesh inside (non-dimensional)
bda_size            = data_mesh.size_bda;               % cell size in chord percent
y_cl                = data_mesh.h1;                     % first cell height in chord percent
n_cl                = data_mesh.n_cl;
%bda_size            = c*data_mesh.size_bda;
%y_cl                = c*data_mesh.h1;                     % first cell height in m
%n_cl                = c*data_mesh.n_cl;
% unstructured mesh inside
growthrate_inside   = data_mesh.growthrate_inside;
growthrate_outside  = data_mesh.growthrate_outside;
size_limit          = data_mesh.size_limit;

i_bda               = data_topo.i_bda;
bda_start           = data_topo.bda_start;              % indice premier vertex extrados
bda_stop            = data_topo.bda_stop;               % indice dernier vertex extrados
extrados_start      = data_topo.extrados_start;         % indice premier vertex extrados
extrados_stop       = data_topo.extrados_stop;          % indice dernier vertex extrados
intrados_start      = data_topo.intrados_start;         % indice premier vertex intrados
intrados_stop       = data_topo.intrados_stop;          % indice avant dernier vertex intrados
extrados_int_start  = data_topo.extrados_int_start;     % indice premier vertex extrados_int
extrados_int_stop   = data_topo.extrados_int_stop;      % indice dernier vertex extrados_int
intrados_int_start  = data_topo.intrados_int_start;     % indice premier vertex intrados_int
intrados_int_stop   = data_topo.intrados_int_stop;      % indice dernier vertex intrados_int

bda_num             = data_topo.n_bda;                  % nombre de points de maillage surface bda
extra_num           = data_topo.n_ext;                  % nombre de points de maillage extrados
intra_num           = data_topo.n_int;                  % nombre de points de maillage intrados
sill_num            = data_topo.n_sill;                 % nombre de points de maillage sillage



[dummytosuppressoutput1 dummytosuppressoutput2] = system('python ../gmsh/gmshData.py'); 
%gambit_journal_file = 'naca.jou';
%geo                 = ['naca',num2str(100*m),num2str(10*p),num2str(100*t)];
gmsh_journal_file    = ['../gmsh/SD7003/SD7003toModWPoints.geo']
%gambit_journal_file = [str_geo,'.jou'];

mesh_file           = [str_geo,'.msh'];
% gmsh_journal_file = [str_geo,'.geo'];

stringFile = fileread(gmsh_journal_file)

R = 20;
xAfterProfile = 20;
pointIntrados = 1040;
heightBoxAirfoil = 0.2;
angleWake = 75;
firstPointXBox = -0.15;
thetaLeftBoxUp = 70*3.14159/180;
thetaLeftBoxDown = 75*3.14159/180;
thetaRightBox = 75*3.14159/180;
xPoint1023 = 0.12993000;
yPoint1023 = 0.04961;
xBoxLeftUp = xPoint1023 - (heightBoxAirfoil-yPoint1023)*cos(thetaLeftBoxUp)/sin(thetaLeftBoxUp);
xPoint1038 = 0.13407;
yPoint1038 = -0.02809;
xBoxLeftDown = xPoint1038 - (heightBoxAirfoil+yPoint1038)*cos(thetaLeftBoxDown)/sin(thetaLeftBoxDown);
xBoxRight = 1.0+heightBoxAirfoil/tan(thetaRightBox);
XTopDiv = 70;
XBottomDiv = 70;
YAirfoilBlockDiv = 124;
YAirfoilBlockGrading = 1.05;
YBigBlockGrading = 1.05;
XGradingTop = 1.02;
YBigBlockDiv = 96;
XBottomLEDiv = 60;
XTopLEDiv = 60;
XWakeDiv = 100;
XWakeGrading = 1.05;
BumpAirfoil = 4;

disp(stringFile)
% Modify variables in file
stringFile = strrep(stringFile, '$R', num2str(R))
disp(stringFile)
stringFile = strrep(stringFile, '$heightBoxAirfoil', num2str(heightBoxAirfoil))
stringFile = strrep(stringFile, '$firstPointXBox', num2str(firstPointXBox))
stringFile = strrep(stringFile, '$xAfterProfile', num2str(xAfterProfile))


stringFile = strrep(stringFile, '$XTopDiv',  num2str(XTopDiv))
stringFile = strrep(stringFile, '$XBottomDiv', num2str(XBottomDiv))
stringFile = strrep(stringFile, '$YAirfoilBlockDiv', num2str(YAirfoilBlockDiv))
stringFile = strrep(stringFile, '$YAirfoilBlockGrading', num2str(YAirfoilBlockGrading))
stringFile = strrep(stringFile, '$YBigBlockGrading', num2str(YBigBlockGrading))
stringFile = strrep(stringFile, '$XGradingTop', num2str(XGradingTop))
stringFile = strrep(stringFile, '$YBigBlockDiv', num2str(YBigBlockDiv))
stringFile = strrep(stringFile, '$XBottomLEDiv', num2str(XBottomLEDiv))
stringFile = strrep(stringFile, '$XTopLEDiv', num2str(XTopLEDiv))
stringFile = strrep(stringFile, '$XWakeDiv', num2str(XWakeDiv))
stringFile = strrep(stringFile, '$XWakeGrading', num2str(XWakeGrading))
stringFile = strrep(stringFile, '$BumpAirfoil', num2str(BumpAirfoil))
stringFile = strrep(stringFile, '$XBottomLEDiv', num2str(XBottomLEDiv))
disp('Aqui');
disp(stringFile);
fid=fopen(gmsh_journal_file,'w');
fprintf(fid,stringFile);    %importation des points de construction
fclose(fid);
if strcmp(data_num.Affichage,'yes') fprintf(' Journal_GMSH : end Ok \n');end;

