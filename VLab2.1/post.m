function [Resultc, Result, Err_Clmean] = post (os,data_geo,data_num,data_sim,data_ac)

%
% Post-Processing of the results => (Resultc, Result)
%
% Resultc = ['inc        ' 'Cl_mean    ' 'Cd_mean    ' 'Cl_rms     ' 'Cd_rms     ' 'Cm_mean    ' 'xcp        '...
%            'Cx         ' 'Cy         ' 'Cq         ' 'Cmu        ' 'q_jet      ' 'Err_Clmean ' 'Err_Cdmean ' 'Cl_mean2   ' 'Cd_mean2   '...
%            'Fx1        ' 'Fx2        ' 'Mx1        ' 'Mx2        ' 'eta1       ' 'eta2       '];
% Result = [inc Cl_mean Cd_mean Cl_rms Cd_rms Cm_mean xcp Cx Cy Cq Cmu q_jet Err_Clmean Err_Cdmean Cl_mean2 Cd_mean2 Fx1 Fx2 Mx1 Mx2 eta1 eta2];
%
% 1- Read 3 files in wind axes (aerodynamic frame)
%   str_file_lift_history.out   : Cl(it), Cl( t) if RANS or URANS, DES, LES, DNS
%   str_file_drag_history.out   : Cd(it), Cd( t) if RANS or URANS, DES, LES, DNS
%   str_file_moment_history.out : Cm(it), Cm( t) if RANS or URANS, DES, LES, DNS
% 2- moyennes glissantes to evaluate the mean convergence (Intelligent mode)
% 3- Convergence test + Cl plot + actuator plot
% 4- Mean coefficient Cl_mean, ... on last iter_sup
% 5- Stockage in Result
% 6- Finish.cas (Windows')
% 7- Read force & moment files : Fx.frp, Fy.frp, Fz.frp, M.frp
%
% Last debug : 23/05/14 (reporting effect on mean Cl, Cd)
%

fprintf('\n post : Begin \n');

eps         = 1e-23;
Cl_mean     = 0;
Cd_mean     = 0;
Cm_mean     = 0;
Cl_rms      = 0;
Cd_rms      = 0;
Cm_rms      = 0;
Cl_mean2    = 0;
Cd_mean2    = 0;
Cm_mean2    = 0;
xcp         = 0;
Cx          = 0;
Cy          = 0;
Cq          = 0;
Cmu         = 0;
q_jet       = 0;
Err_Cdmean  = 0;
Err_Clmean  = 0;
Err_Cdmean2 = 0;
Err_Clmean2 = 0;
Fx1         = 0;
Fx2         = 0;
Mx1         = 0;
Mx2         = 0;
eta1        = 0;
eta2        = 0;

c           = data_geo.c;                   % Hyp : data_geo.c in m

fluent_ver  = data_num.Fluent_ver;          % Choix version Fluent : {'6.2.16','6.3.26','6.4.11'} 

Vinf        = data_sim.Vinf;
inc         = data_sim.inc;
iter        = data_sim.iter;
iter_trans  = data_sim.iter_trans;
iter_sup    = data_sim.iter_sup;
str_URANS   = data_sim.str_URANS;
str_file    = data_sim.str_file;
Err_stop    = data_sim.Stop_Err;
Stop_type   = data_sim.Stop_type;
Nit         = data_sim.Nit;
reporting   = data_sim.reporting;                % reporting of lift & drag coeff.

theta       = data_ac.Theta_deg;
pos         = data_ac.pos;
dia         = data_ac.dia;
Freq        = data_ac.Freq;
Vjfluct     = data_ac.Vjfluct;
Vjmean      = data_ac.Vjmean;
duty_cycle  = data_ac.duty_cycle;

Nit_start   = iter_trans+2*iter_sup;

file_cl         = strcat(str_file,'_','lift'  ,'_history.out');
file_cd         = strcat(str_file,'_','drag'  ,'_history.out');
file_cm         = strcat(str_file,'_','moment','_history.out');
file_ac         = strcat(str_file,'_','ac'    ,'_history.out');
file_pressure   = strcat(str_file,'_pressure.tif');

%==============================================================
% Waiting for new data from the solver in history files 
%==============================================================
%size_cl = file_size(file_cl);   % size of cl file
%==============================================================
% lecture fichiers efforts, actuation,... : file > it,Cl,Cd,Cm
%==============================================================
clear it Cl Cd Cm Vj;
time = 3;
fprintf('    Pause %g sec. \n',time);pause(time);
fprintf('    Read history files : %s \n',file_cl);
fprintf('    Read history files : %s \n',file_cd);

while ~exist(file_cl,'file')==2
    fprintf('    Pause %g sec. \n',1);pause(1);
end
switch os
    case 'Linux'
        [it,Cl] = textread(file_cl,'%f %f','headerlines',2);  % ligne 2 : 0 0 0 0 sous Linux
        [it,Cd] = textread(file_cd,'%f %f','headerlines',2);
        if exist(file_cm,'file')==2;[it,Cm] = textread(file_cm,  '%f %f','headerlines',2);end
    case 'Windows'
        switch fluent_ver
        case {'6.2.16'}
        [it,Cl] = textread(file_cl,'%f %f','headerlines',3);  % ligne 3 : 0 0 0 0 sous windows !!!
        [it,Cd] = textread(file_cd,'%f %f','headerlines',3);
        if exist(file_cm,'file')==2;[it,Cm] = textread(file_cm,  '%f %f','headerlines',3);end
        case {'6.3.26'}
        [it,Cl] = textread(file_cl,'%f %f','headerlines',2);
        [it,Cd] = textread(file_cd,'%f %f','headerlines',2);
        if exist(file_cm,'file')==2;[it,Cm] = textread(file_cm,'%f %f','headerlines',2);end
        end
end        

length = size(it,1);    % default length

%===============================
% waiting method for file write
%===============================
count = 0;
if ( not(strcmp(num2str(size(it,1)),num2str(size(Cl,1)))) || not(strcmp(num2str(size(it,1)),num2str(size(Cd,1)))) ) || count<20
    count = count + 1;
    fprintf('    %i : Vector size it(%g), Cl(%g), Cd(%g) \n',count,size(it,1),size(Cl,1),size(Cd,1));
    %fprintf('    it, Cl, Cd = %i %g %g ',it(size(it,1)),Cl(size(Cl,1)),Cd(size(Cd,1)));
else
    length = min(size(it,1),size(Cl,1));
    length = min(length    ,size(Cd,1));
end

%===============================
% mean value evaluation
%===============================
%[Err_Clmean] = mean_win (it,Cl,floor(iter_sup/reporting));  % mean evaluation on iter_sup last iterations
%[Err_Cdmean] = mean_win (it,Cd,floor(iter_sup/reporting));  % mean evaluation on iter_sup last iterations
%iter_mean = iter_sup;
%[Err_Clmean] = mean_win (it,Cl,floor(0.1*it(size(it,1))/reporting));  % mean evaluation on last 10% iterations
%[Err_Cdmean] = mean_win (it,Cd,floor(0.1*it(size(it,1))/reporting));  % mean evaluation on last 10% iterations
%iter_mean = 0.1*it(size(it,1));

%zzz=0;
%if zzz==1
if strcmp(Stop_type,'Intelligent')
%iter_mean = iter_sup;
iter_mean = 0.2*it(size(it,1));
%===============================================================
% Cl, Cd, Cm : mean,rms values evaluation 
%===============================================================
switch str_URANS
    case 'RANS'
        n_window    = 2;                                    % criteria number (mean(T) + mean(T/2))
        n1          = iter_mean;                            % window size 1
        n2          = iter_mean/2;                          % window size 2
        last        = length;
    case {'DNS','URANS'}
        n_window    = 2;                                    % criteria number (mean(T) + mean(T/2))
        n1          = iter_mean;                            % window size 1
        n2          = iter_mean/2;                          % window size 2
        last        = length;
end
%==============================================================
for window = 1:n_window
    switch window
        case 1
            npts  = n2;
        case 2
            npts  = n1;
    end
    stop_2         = last;                              % indice fin   fenetre 2
    start_2        = stop_2 - npts + 1;                 % indice debut fenetre 2
    stop_1         = start_2 - 1;                       % indice fin   fenetre 1
    start_1        = stop_1 - npts + 1;                 % indice debut fenetre 1
    if start_1 < stop_1
        disp (' post : start_1 is lower than stop_1 : please increase data_sim.iter_sup or decrease data_sim.reporting ');
    end
    if start_2 < stop_2
        disp (' post : start_2 is lower than stop_2 : please increase data_sim.iter_sup or decrease data_sim.reporting ');
    end
    if exist(file_cl,'file')==2;
        Cl_mean_ev(1)  = mean(Cl(start_1:stop_1,1));        % Cl_mean on window 1
        Cl_rms_ev (1)  = std (Cl(start_1:stop_1,1),1);
        Cl_mean_ev(2)  = mean(Cl(start_2:stop_2,1));        % Cl_mean on window 2
        Cl_rms_ev (2)  = std (Cl(start_2:stop_2,1),1);
    end
    if exist(file_cd,'file')==2;
        Cd_mean_ev(1)  = mean(Cd(start_1:stop_1,1));
        Cd_rms_ev (1)  = std (Cd(start_1:stop_1,1),1);
        Cd_mean_ev(2)  = mean(Cd(start_2:stop_2,1));
        Cd_rms_ev (2)  = std (Cd(start_2:stop_2,1),1);
    end
    if exist(file_cm,'file')==2;
        Cm_mean_ev(1)  = mean(Cm(start_1:stop_1,1));
        Cm_rms_ev (1)  = std (Cm(start_1:stop_1,1),1);
        Cm_mean_ev(2)  = mean(Cm(start_2:stop_2,1));
        Cm_rms_ev (2)  = std (Cm(start_2:stop_2,1),1);
    end
    
% why not an absolute error to discard problems when Cl_mean = 0 ?
% coeff. ~ 1 and absolute error = 0.0001
% gives 4 significant numbers (to try)
 
    if exist(file_cl,'file')==2;
    ev_Cl_mean = (Cl_mean_ev(2) - Cl_mean_ev(1))/ Cl_mean_ev(2);   % variation relative Cl_mean entre fenetre 1 et 2
    ev_Cl_rms  = (Cl_rms_ev (2) - Cl_rms_ev (1))/ Cl_rms_ev (2);
    Err_Clmean(window)  = abs(ev_Cl_mean);                   % variation relative Cl_mean sur fenetre n n-1
    Err_Clrms (window)  = abs(ev_Cl_rms );
    end
    if exist(file_cd,'file')==2;
    ev_Cd_mean = (Cd_mean_ev(2) - Cd_mean_ev(1))/ Cd_mean_ev(2);
    ev_Cd_rms  = (Cd_rms_ev (2) - Cd_rms_ev (1))/ Cd_rms_ev (2);
    Err_Cdmean(window)  = abs(ev_Cd_mean);
    Err_Cdrms (window)  = abs(ev_Cd_rms );
    end
    if exist(file_cm,'file')==2;    
    ev_Cm_mean = (Cm_mean_ev(2) - Cm_mean_ev(1))/ Cm_mean_ev(2);
    ev_Cm_rms  = (Cm_rms_ev (2) - Cm_rms_ev (1))/ Cm_rms_ev (2);
    Err_Cmmean(window)  = abs(ev_Cm_mean);
    Err_Cmrms (window)  = abs(ev_Cm_rms );
    end

end % for window = 1:2

    t1 = sprintf('    Err_Clmean(%g it) = %g, Err_Clmean(%g it) = %g ',n2,Err_Clmean(1),n1,Err_Clmean(2));
    t2 = sprintf('    Err_Cdmean(%g it) = %g, Err_Cdmean(%g it) = %g ',n2,Err_Cdmean(1),n1,Err_Cdmean(2));
    t3 = sprintf('    Err_Cmmean(%g it) = %g, Err_Cmmean(%g it) = %g ',n2,Err_Cmmean(1),n1,Err_Cmmean(2));
    disp(t1);disp(t2);disp(t3)
    
    if exist(file_cl,'file')==2;
        Err_Clmean      = max (Err_Clmean(1),Err_Clmean(2));            % Err_max on T & T/2 it 
        Err_Clrms       = max (Err_Clrms (1),Err_Clrms (2));
    end
    if exist(file_cd,'file')==2;
        Err_Cdmean      = max (Err_Cdmean(1),Err_Cdmean(2));            % Err_max on T & T/2 it  
        Err_Cdrms       = max (Err_Cdrms (1),Err_Cdrms (2));
    end
    if exist(file_cm,'file')==2;    
        Err_Cmmean      = max (Err_Cdmean(1),Err_Cmmean(2));            % Err_max on T & T/2 it
        Err_Cmrms       = max (Err_Cmrms (1),Err_Cmrms (2));
    end
else
    disp('    Intelligent convergence not activated because mean value not evaluated \n');
end

%=======================
% Test convergence
%=======================
if ~strcmp(data_sim.Stop_type,'Definite')
if Err_Clmean < Err_stop
    s_Cl_mean = sprintf('    Convergence criteria : Cl_mean=%6.2f converged to %6.2f%% on %g it. : OK  ',Cl_mean_ev(1),100*Err_Clmean,iter_mean);disp(s_Cl_mean);
    s_Cl_rms  = sprintf('                           Cl_rms =%6.2f converged to %6.2f%% on %g it. : OK\n',Cl_rms_ev(1) ,100*Err_Clrms ,iter_mean);disp(s_Cl_rms);
else
    s_Cl_mean = sprintf('    Convergence criteria : Cl_mean=%6.2f converged to %6.2f%% on %g it. (criteria %g%%)  ',Cl_mean_ev(1),100*Err_Clmean,iter_mean,100*Err_stop);disp(s_Cl_mean);
    s_Cl_rms  = sprintf('                           Cl_rms =%6.2f converged to %6.2f%% on %g it. (criteria %g%%)\n',Cl_rms_ev(1) ,100*Err_Clrms ,iter_mean,999);disp(s_Cl_rms);
end
end

%============================================
% Fig(2) : Plot convergence Cl=f(t), Cd=f(t)
%============================================
figure(2);
subplot(2,1,1);plot(it,Cl);hold on;title('Cl = f(iterations or time)');
subplot(2,1,2);plot(it,Cd);hold on;title('Cd = f(iterations or time)');
% mean bar not plotted
zzz=0;
if zzz==1
stop_2         = last;                              % indice fin   fenetre 2
start_2        = stop_2 - npts + 1;                 % indice debut fenetre 2
stop_1         = start_2 - 1;                       % indice fin   fenetre 2
start_1        = stop_1 - npts + 1;                 % indice debut fenetre 2
vCl_mean    = Cl_mean_ev(1)*ones(n1,1);             % a vector at Cl_mean of the window size
plot(it(start_1:stop_1),vCl_mean,'-');              % a line of the matrix
vCl_mean    = Cl_mean_ev(2)*ones(n1,1);             % a vector at Cl_mean of the window size
plot(it(start_2:stop_2),vCl_mean,'-');              % a vector
end

%================================
% Fig(2) : Plot actuator Vj=f(t)
%================================
%try
if Nit > Nit_start
switch data_ac.ac
    case 'no'
        subplot(2,1,2);plot(it,Cd);hold on;
        title('Cd = f(iterations or time)');
    case 'yes'
        [it2,Vj] = textread(file_ac  ,'%f %f','headerlines',3);
        subplot(2,1,2);plot(it2,Vj,'+');hold on;
        title('Vj = f(iterations or time)');
end
end
%catch ME1
%   idSegLast = regexp(ME1.identifier, '(?<=:)\w+$', 'match');         % Get last segment of the error message identifier.
%   if strcmp(idSegLast, 'FileNotFound') && ~exist(file_ac, 'file')    % Did the read fail because the file could not be found? 
%        disp(' Warning message : no history file for the actuator in post.m')
%   else
%        disp(' Warning message : unknown error in post.m')
%   end
%end

%=======================================
% Fig(1) : Plot Pressure Field 950x565
%=======================================
if exist(file_pressure,'file')
    [X_img,map] = imread(file_pressure,1); % lire fichier image
    figure(1);image(X_img);
end

%===================================================================================================
% compute mean coeff. on the last iter_sup iterations from Cl, Cd, Cm
%===================================================================================================
%%ORIGINAL
%Cl_mean         = mean(Cl       (first:last,1));
%Cd_mean         = mean(Cd       (first:last,1));
%%Ma Methode - Bauerheim 2011
%Cl_mean         = coefmean(Cl(:,1) , 12,'lift');
%Cd_mean         = coefmean(Cd(:,1) , 12,'drag');

xcp     = 0;
last    = length;
number  = iter_sup/reporting;
if reporting == 1
    first   = last - iter_sup + 1;if (first < 1)first=1;end;
else
    first = length - number;
end
    if exist(file_cl,'file')==2;
        Cl_mean         = mean(Cl       (first:last,1));
        Cl_mean2        = coefmean(Cl(:,1) , 12,'lift');    % verif 1 => not convince
        Cl_rms          = std (Cl       (first:last,1),1);
    end
    if exist(file_cd,'file')==2;
        Cd_mean         = mean(Cd       (first:last,1));
        Cd_mean2        = coefmean(Cd(:,1) , 12,'drag');
        Cd_rms          = std (Cd       (first:last,1),1);
    end
    if exist(file_cm,'file')==2;
        Cm_mean         = mean(Cm       (first:last,1));
        xcp = Cm_mean/(Cl_mean+eps);    % position of the pressure center / origine en % de corde GV
    end

% ac  =                         % aerodynamic center = dCmdal

%=======================================
% coefficients in other frames
%=======================================
%beta = inc + delta;                             % apparent wind angle
%[Cr,Ch] = rot (Cd_mean,Cl_mean,beta*pi/180)     % frame of the boat (driving, heeling forces)
 [Cx,Cy] = rot (Cd_mean,Cl_mean,inc *pi/180);    % frame of the airfoil

%=======================================
% calculs coefficients Cq Cmu
%=======================================
rhoj    = 1.225;        % hypothese de masse volumique constante
Sj      = dia*c*1.;     % (d/c)*c*1 = d*1 (fente of diameter d on 1 meter of envergure)
S_ref   = c*1;
%q_jet = 0; Cq = 0; Cmu = 0;

qj_mean  = rhoj*Sj*Vjmean;                          % continuous mass flow rate of the jet in kg/s

switch data_ac.type
    case {'continuous jet'}                         % actionneur stationnaire
        qj_rms  = 0;                                % mean variable mass flow rate in kg/s
    case {'synthetic jet'}                          % actionneur jet synthetique
        qj_rms  = 0;                                % mean variable mass flow rate in kg/s
    case {'pulsed jet'}                             % actionneur jet puls�
        qj_rms  = rhoj*Sj*Vjfluct*duty_cycle;       % mean variable mass flow rate in kg/s
    otherwise
        qj_rms  = 0;                                % default value
end

q_jet    = qj_mean + qj_rms;

Cq_mean  = qj_mean / (rhoj*S_ref*Vinf);             % adimensional continuous mass flow rate for 1 meter of envergure
Cq_rms   = qj_rms  / (rhoj*S_ref*Vinf);             % adimensional mean variable mass flow rate for 1 meter of envergure
Cq       = Cq_mean + Cq_rms;

Cmu_mean = 2*Cq_mean*Vjmean/Vinf;                   % jet momentum coefficient
Cmu_rms  = 2*dia*(Vjfluct/Vinf)^2;                  % coefficient de quantit� de mvt
Cmu      = Cmu_mean + Cmu_rms;

%===============================================
% CROR processing from VLab_Fx.txt created in ?
%===============================================
if exist('VLab_Fx.txt','file')==2 && 0==1 ;
    line = 11; % standard   (3 surfaces)
    line = 12; % standard   (4 surfaces)
    %line = 17; % CROR      (? surfaces)
    [s1,Fxp1,Fxv1,Fx1,Cpxp1,Cpxv1,Cpx1] = textread('VLab_Fx.txt','%s %f %f %f %f %f %f',1,'headerlines',line)
    [s2,Fxp2,Fxv2,Fx2,Cpxp2,Cpxv2,Cpx1] = textread('VLab_Fx.txt','%s %f %f %f %f %f %f',1,'headerlines',line+1);
    [s1,Mxp1,Mxv1,Mx1,Cpxp1,Cpxv1,Cpx1] = textread('VLab_Mx.txt','%s %f %f %f %f %f %f',1,'headerlines',line);
    [s2,Mxp2,Mxv2,Mx2,Cpxp2,Cpxv2,Cpx1] = textread('VLab_Mx.txt','%s %f %f %f %f %f %f',1,'headerlines',line+1);

    Fx1 = -Fx1;
    Fx2 = -Fx2;
    Mx1 = -Mx1;
    Mx2 = -Mx2;

    N = (500/60);       % CROR rotational speed of front rotor in rps
    rho = 1.225;
    P1  = Mx1*2*pi*N;   % Power row 1
    P2  = Mx2*2*pi*N;   % Power row 2
    D1  = 4.2672;       % Diameter row 1
    D2  = 3.8404;       % Diameter row 2
    V   = 68;           % Freestream Velocity

    % Efficiency
    Cth1    = Fx1 / (rho*N^2*D1^4);
    Cth2    = Fx2 / (rho*N^2*D2^4);
    CP1     = P1  / (rho*N^3*D1^5);
    CP2     = P2  / (rho*N^3*D2^5);
    J1      = V   / (N*D1);
    J2      = V   / (N*D2);
    eta1    = J1*Cth1/CP1;
    eta2    = J2*Cth2/CP2;
end
% to debug later / CROR or not ?
Fx1  = 0;
Fx2  = 0;
Mx1  = 0;
Mx2  = 0;
eta1 = 0;
eta2 = 0;
%===================
% tableau resultats
%===================
Resultc = ['inc        ' 'Cl_mean    ' 'Cd_mean    ' 'Cl_rms     ' 'Cd_rms     ' 'Cm_mean    ' 'xcp        '...
           'Cx         ' 'Cy         ' 'Cq         ' 'Cmu        ' 'q_jet      ' 'Err_Clmean ' 'Err_Cdmean ' 'Cl_mean2   ' 'Cd_mean2   '...
           'Fx1        ' 'Fx2        ' 'Mx1        ' 'Mx2        ' 'eta1       ' 'eta2       '];
Result = [inc Cl_mean Cd_mean Cl_rms Cd_rms Cm_mean xcp Cx Cy Cq Cmu q_jet Err_Clmean Err_Cdmean Cl_mean2 Cd_mean2 Fx1 Fx2 Mx1 Mx2 eta1 eta2];

%==========================================================
% write current case in str_file_Coeff_mean.out 
%==========================================================
filename = strcat (str_file, '_Coeff_mean.out');
rw_Result (filename, 0, str_file, Result, 'open_write');
    
%=========================================
% waiting for the end of post.m (Windows)
%=========================================
% while stop.out doesn't exist wait, if stop.out exist, close it and continue
zzz = 0;
if zzz == 1
    if strcmp(os,'Windows')
        if exist('stop.out','file')
        system ('del stop.out');
        end
        fid_stop=fopen('stop.out','w');
        fprintf(fid_stop,'%s \n', str_file);
        fprintf(fid_stop,'%f \n', Cl);
        fclose(fid_stop);
        fid_stop=fopen('stop.out');
        while (fid_stop==-1)
            pause(10);
            fid_stop=fopen('stop.out');
        end
    fclose(fid_stop);
    end
end
if strcmp(data_num.Affichage,'yes');fprintf(' post : End \n');end;

%=======================================
