function [x,y] = airfoil_rw_Javafoil (str_rw,filename,x,y)
%
% ==================================
% Read  airfoil geometry in VLab format
% Write airfoil geometry in VLab format xu,yu,xl,yl
%
% input :   str_rw      ('read', 'write') 
%           filename   
%
% output :
%
% xu(i), i=1,n  : abcisse  des points extrados du bda vers le bdf
% yu(i), i=1,n  : ordinate des points extrados du bda vers le bdf
% xl(i), i=1,n  : abcisse  des points intrados du bda vers le bdf
% yl(i), i=1,n  : ordinate des points intrados du bda vers le bdf
%
% bda en (0,0)
% bdf en (1,0)
% ==================================

switch str_rw
    
    case 'read'
        
%=============================================
% read  airfoil in filename - format Javafoil
%=============================================
fid     = fopen    (filename,'r');
[x,y]   = textread (filename,'%f %f','headerlines',1);  % begin on line 2
fclose (fid);

    case 'write'
        
%=========================================
% write airfoil in filename - format VLab
%=========================================
fid         = fopen(filename,'w');

fprintf(fid,' airfoil writed by VLab2 in Javafoil format ');
for i=1:length(x)
    fprintf(fid,'%f %f %f \n', x(i), y(i), 0);
end
fclose(fid);

end     % switch string

%fprintf(' airfoil_rw_Javafoil : file %s created \n',filename);

