function LaminarFraction = Tr_model (V,c,Tu)
%
% From Langry & Menter 2006 in D:\Biblio\CFD\TransitionModel\2006_Overview_CFX_Langry_Menter.pdf p164
%
% Tests & validation done in D:\Documents\VIK\RB\CFD\CFD-Fluent.xls
%
nu = 1.5e-5;                                    % air
ReL=V*c/nu;                                     % Reynolds number
LaminarFraction = 380000*(100*Tu)^(-5/4)/ReL;    % Rextr/ReL
end