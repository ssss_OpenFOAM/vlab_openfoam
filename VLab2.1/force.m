function  force (fid,surface,data_num,data_sim,str,cas)
%
% Print, Plot, Write : efforts sur un obstacle

% 2 cas impl�ment�s :
%   cas = 'std'     : print lift and drag on surface with angle the wind angle from x axis
%   cas = 'write'   : std + write on Cl-history file

% fid       : file pointer
% surface   : string of the wall surface name to be considered
% angle     : wind angle with x-axis
% str       : 'lift' or 'drag' for lift or drag
% cas       : 'std' for print only or 'write' for print & write
        
dim         = data_sim.dim;
angle       = data_sim.inc;
frame       = data_sim.frame;
str_file    = data_sim.str_file;
%str_URANS   = data_sim.str_URANS;
Fluent_ver  = data_num.Fluent_ver;  % 6 for 6.2.16, 6.3.26, ..., 1 for 13.0.0, 14.0.0, 14.5.0, ...

monitor = 'yes';
print   = 'yes';
plot    = 'no';
pzone   = 'no';

switch frame
    case 'xy'
        Axis    = strcat('"','Z-Axis','"');     % fluent 6.3.26 only X,Y or Z-axis, on fluent13 a vector define axis (0,0,1) = Z-axis
        switch str
            case 'drag'
                x_component = cos(angle*pi/180);
                y_component = sin(angle*pi/180);
                z_component = 0;
            case 'lift'
                x_component =-sin(angle*pi/180);
                y_component = cos(angle*pi/180);
                z_component = 0;
            case 'moment'
                x_component = 0;
                y_component = 0;
                z_component = 0;
                x_axis      = 0;
                y_axis      = 0;
                z_axis      = 1;
        end
    case 'xz'
        Axis    = strcat('"','Y-Axis','"');     % fluent 6.3.26 only X,Y or Z-axis, on fluent13 a vector define axis (0,0,1) = Z-axis
        switch str
            case 'drag'
                x_component = cos(angle*pi/180);
                y_component = 0;
                z_component = sin(angle*pi/180);
            case 'lift'
                x_component =-sin(angle*pi/180);
                y_component = 0;
                z_component = cos(angle*pi/180);
            case 'moment'
                x_component = 0;
                y_component = 0;
                z_component = 0;
                x_axis      = 0;
                y_axis      = 1;
                z_axis      = 0;
        end
end



fprintf(fid,'/solve/monitors/force/%s-coefficient\n',str);

switch cas
    
    case 'std'          % usage ?
        write   = 'no';
        switch dim
            case '2D'
                fprintf(fid,'%s %s, %s %s %s %s %g %g\n', monitor, surface, print, write, plot, pzone, x_component, y_component);
            case '3D'
                fprintf(fid,'%s %s, %s %s %s %s %g %g %g\n', monitor, surface, print, write, plot, pzone, x_component, y_component, z_component);
        end
        
    case 'write'        % to write forces & moments
        write   ='yes';
        filename=strcat('"',str_file,'_',str,'_history.out"');
        
        switch dim
            case '2D'
            switch str
                case {'drag','lift'}
                fprintf(fid,'%s %s, %s %s %s %s %s %g %g %g\n\n', monitor, surface, print, write, filename, plot, pzone, x_component, y_component);
                case 'moment'
                %switch Fluent_ver
                switch Fluent_ver(1)    % only first character
                    %case {'6.3.26'}
                    case {'6'} %case {'6.2.16','6.3.26', ...}
                        fprintf(fid,'%s %s, %s %s %s %s %s %s %g %g %g\n\n', monitor, surface, print, write, filename, plot, pzone, Axis, x_component, y_component);
                    %case {'13.0.0','14.0.0','14.5.0','14.5.7'}
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
                        fprintf(fid,'%s %s, %s %s %s %s %s %s %g %g %g %g %g %g\n\n', monitor, surface, print, write, filename, plot, pzone, Axis, x_component, y_component,x_axis,y_axis,z_axis);
                    otherwise
                        fprintf(' force.m : Not yet implemented : Do it. \n\n');                        
                end
            end
            case '3D'
            switch str
                case {'drag','lift'}
                fprintf(fid,'%s %s, %s %s %s %s %s %g %g %g\n\n',monitor,surface,print,write,filename,plot,pzone,x_component,y_component,z_component);
                case 'moment'
                %switch Fluent_ver
                switch Fluent_ver(1)    % only first character
                    %case '6.3.26'
                    case {'6'} %case {'6.2.16','6.3.26', ...}
                        fprintf(fid,'%s %s, %s %s %s %s %s %s %g %g %g\n\n', monitor, surface, print, write, filename, plot, pzone, Axis, x_component, y_component,z_component);
                    %case {'13.0.0','14.0.0','14.5.0','14.5.7'}
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
                        fprintf(fid,'%s %s, %s %s %s %s %s %g %g %g %g %g %g\n\n',monitor,surface,print,write,filename,plot,pzone,x_component,y_component,z_component,x_axis,y_axis,z_axis);
                    otherwise
                        fprintf(' force.m : Not yet implemented : Do it. \n\n');                        
                end
            end
        end
end


