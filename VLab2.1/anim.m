%
% Animation of a flowfield
%
anim_type = 'anim';

param.quantity              = data_sim.anim.quantity;                                       % quantity to animate
param.images                = data_sim.anim.images;                                         % number of images
param.frequency             = data_sim.anim.frequency;                                      % time steps or iterations between images
param.min_value             = data_sim.anim.min_value;                                      % quantity min value
param.max_value             = data_sim.anim.max_value;                                      % quantity max value

data_sim.graphic = 1;

switch anim_type
    case 'anim'
        [fluent_journal_file,param] = fluent_anim (data_geo,data_sim,data_ac,data_num,param);       % create Journal anim
        [data_num]                  = fluent_launch (os,fluent_journal_file,data_num,data_sim);     % run FLUENT
        if exist(param.anim_name,'file')   % defined in anim_define.m
            delete *.hmf
            delete *.cxa
        end     % if exist(param.anim_name,'file')
    case 'tiff'
        [fluent_journal_file,param] = fluent_tiff (data_geo,data_sim,data_ac,data_num,param);
        [data_num]                  = fluent_launch (os,fluent_journal_file,data_num,data_sim);     % run FLUENT
end
