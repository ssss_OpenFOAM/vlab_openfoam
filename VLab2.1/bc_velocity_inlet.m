function  bc_velocity_inlet (param)
%
% boundary condition type : velocity_inlet

% param     : parameters of the function
% call      : fluent_jou_cube.m

% replace this :
%       fprintf(fid,'/define/boundary-conditions/velocity-inlet\n');
%       fprintf(fid,'%s yes yes no %g no %g no %g no yes %g %g\n', surf_bc_name, Vinf, Cd_ix, Cd_iy, I_turb, L_turb);

fluent_ver      = param.Fluent_ver;     % 6 for 6.2.16, 6.3.26, ..., 1 for 13.0.0, 14.0.0, 14.5.0, ...
regime          = param.regime;
Tu_model        = param.Tu_model;
dim             = param.dim;
fid             = param.fid;
zone_name       = param.surf_bc_name;
Vinf            = param.Vinf;
Vx              = param.Vx;             % X-Component of Flow Direction [0.98952579]
Vy              = param.Vy;             % Y-Component of Flow Direction [0.1443562]
Vz              = param.Vz;             % Z-Component of Flow Direction [1]
I_turb          = param.I_turb;         % Turbulence Intensity (%) [1]
L_turb          = param.L_turb;         % Turbulence Length Scale (m) [0.005]

VSM             = 'yes';                % magnitude and direction
RF              = 'yes';                % Reference Frame absolute
VM_prof         = 'no';                 % Use Profile for Velocity Magnitude? [no]
P_prof          = 'no';                 % Use Profile for Supersonic/Initial Gauge Pressure? [no]   % fluent v13
P_gauge         = 0.0;                  % Gauge pressure;
CSC             = 'yes';                % Coordinate System: Cartesian (X, Y, Z) [yes]              % semble absent sur FLUENT 6.2.16
X_prof          = 'no';                 % Use Profile for X-Component of Flow Direction? [no]
Y_prof          = 'no';                 % Use Profile for Y-Component of Flow Direction? [no]
Z_prof          = 'no';                 % Use Profile for Z-Component of Flow Direction? [no]
TSM1            = 'no';                 % Turbulence Specification Method: Modified Turbulent Viscosity [no]
TSM2            = 'yes';                % Turbulence Specification Method: Intensity and Length Scale [yes]
Int_prof        = 'no';                 % Intermittency profile
Int             = 1.0;                  % Intermittency factor

fprintf(fid,'/define/boundary-conditions/velocity-inlet\n');

switch dim
    
    case {'2D'}
        switch regime
            case 'laminar'
                switch fluent_ver(1)    % only first character
                    case {'6'} %case {'6.2.16','6.3.26', ...}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g \n',                        zone_name, VSM, RF, ...
                                                                                    VM_prof, Vinf, ...
                                                                                    X_prof, Vx, Y_prof, Vy);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g \n',                  zone_name, VSM, RF, ...
                                                                                    VM_prof,Vinf, ...
                                                                                    P_prof,P_gauge, ...
                                                                                    X_prof,Vx,Y_prof,Vy);
                otherwise
                fprintf(' bc_velocity_inlet.m : Not yet implemented. Do it. ');
                end
            case 'turbulent'
                switch fluent_ver(1)
                case {'6'} %case {'6.2.16','6.3.26', ...}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %s %g %g\n',             zone_name, VSM, RF, ...
                                                                                    VM_prof, Vinf, ...
                                                                                    X_prof, Vx, Y_prof, Vy, ...
                                                                                    TSM1, TSM2, ...
                                                                                    I_turb, L_turb);
                case {'1'} %case {'13.0.0','14.0.0','14.5.0', ...}
                    switch Tu_model
                        case {'SA','SST'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g %s %s %g %g\n',       zone_name, VSM, RF, ...
                                                                                    VM_prof, Vinf, ...
                                                                                    P_prof, P_gauge, ...
                                                                                    X_prof, Vx, Y_prof, Vy, ...
                                                                                    TSM1, TSM2, ...
                                                                                    I_turb, L_turb);
                        case 'SSTtr'
                fprintf(fid,'%s %s %s %s %g %s %g %s %g %s %g %s %s %s %g %g %g\n', zone_name, VSM, RF, ...
                                                                                    VM_prof, Vinf, ...
                                                                                    P_prof, P_gauge, ...
                                                                                    X_prof, Vx, Y_prof, Vy, ...
                                                                                    TSM1, TSM2, ...
                                                                                    Int_prof, Int, ...
                                                                                    I_turb, L_turb);
                        otherwise
                        fprintf(' bc_velocity_inlet.m : Not yet implemented. Do it. ');
                    end
                otherwise
                fprintf(' bc_velocity_inlet.m : Not yet implemented. Do it. ');
                end
        end
        
    case {'3D'}
        switch regime
            case 'laminar'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                fprintf(fid,'%s %s %s %s %g %s %s %g %s %g %s %g \n',               zone_name, VSM, RF, ...
                                                                                    VM_prof, Vinf, CSC, ...
                                                                                    X_prof, Vx, Y_prof, Vy, Z_prof, Vz);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                fprintf(fid,'%s %s %s %s %g %s %s %g %s %g %s %g %s %g \n',         zone_name,VSM,RF, ...
                                                                                    VM_prof,Vinf,CSC, ...
                                                                                    P_prof,P_gauge,   ...
                                                                                    X_prof,Vx,Y_prof,Vy,Z_prof,Vz);
                end
            case 'turbulent'
                switch fluent_ver(1)
                    case {'6'} %case {'6.2.16','6.3.26'}
                fprintf(fid,'%s %s %s %s %g %s %s %g %s %g %s %g %s %s %g %g\n',    zone_name,VSM,RF, ...
                                                                                    VM_prof,Vinf,CSC, ...
                                                                                    X_prof,Vx,Y_prof,Vy,Z_prof,Vz, ...
                                                                                    TSM1,TSM2, ...
                                                                                    I_turb,L_turb);
                    case {'1'} %case {'13.0.0','14.0.0','14.5.0'}
                        switch Tu_model
                        case {'SA','SST'}
                fprintf(fid,'%s %s %s %s %g %s %g %s %s %g %s %g %s %g %s %s %g %g\n',  zone_name,VSM,RF, ...
                                                                                        VM_prof,Vinf, ...
                                                                                        P_prof,P_gauge,CSC, ...
                                                                                        X_prof,Vx,Y_prof,Vy,Z_prof,Vz, ...
                                                                                        TSM1,TSM2, ...
                                                                                        I_turb,L_turb);
                        case 'SSTtr'
                fprintf(fid,'%s %s %s %s %g %s %g %s %s %g %s %g %s %g %s %s %s %g %g %g\n',    zone_name,VSM,RF, ...
                                                                                                VM_prof,Vinf, ...
                                                                                                P_prof,P_gauge,CSC, ...
                                                                                                X_prof,Vx,Y_prof,Vy,Z_prof,Vz, ...
                                                                                                TSM1,TSM2, ...
                                                                                                Int_prof,Int, ...
                                                                                                I_turb,L_turb);
                        otherwise
                        fprintf(' bc_velocity_inlet.m : Not yet implemented. Do it. ');
                        end
                otherwise
                fprintf(' bc_velocity_inlet.m : Not yet implemented. Do it. ');
                end
        end
end


