function Fig3p (number,i_cas,xR,yR,xX,yX,EFD_title,mode1,mode2,mode3)
% Plot Aero Coeff. (Lift, Drag, ...)
%
% Define Fig 3.number

% input :
%       number      subplot number
%       i_cas       case number
%       xR,yR       object coordinates in RANS    
%       xX,yX       object coordinates in XFOIL
%       EFD_title   title of the Figure
%       mode1       = size(x,1)                         0:analysis, n:optimisation
%       mode2       = data_geo.sequence.type(25:28)     'runR', 'runX', 'run2'
%       mode3       = data_sim.comparison               'yes', 'no'

subplot(2,2,number);

% mode2='runR' & mode3='no'  => RANS alone
% mode2='runX' & mode3='no'  => XFOIL alone
% mode2='runR' & mode3='yes' => RANS, Exp
% mode2='runX' & mode3='yes' => XFOIL, Exp
% mode2='run2' & mode3='no'  => RANS, XFOIL
% mode2='run2' & mode3='yes' => RANS, XFOIL, Exp

if strcmp(mode2,'runF') && strcmp(mode3,'no')     % RANS alone
    plot(xR,yR,'-+b');hold on;
    legend('RANS');
end
if strcmp(mode2,'runX') && strcmp(mode3,'no')     % XFOIL alone
    plot(xX,yX,'-+b');hold on;
    legend('XFOIL');
end
if strcmp(mode2,'runF') && strcmp(mode3,'yes')    % RANS, Exp
    plot(xR,yR,'-+b');hold on;
    legend(EFD_title,'RANS');
end
if strcmp(mode2,'runX') && strcmp(mode3,'yes')    % XFOIL, Exp
    plot(xX,yX,'-+b');hold on;
    legend(EFD_title,'XFOIL');
end
if strcmp(mode2,'run2') && strcmp(mode3,'no')    % RANS, XFOIL
    plot(xR,yR,'-+b');hold on;
    plot(xX,yX,'-or');
    legend('RANS','XFOIL');
end
if strcmp(mode2,'run2') && strcmp(mode3,'yes')    % RANS, XFOIL, Exp
    plot(xR,yR,'-+b');hold on;
    plot(xX,yX,'-or');
    legend(EFD_title,'RANS','XFOIL');
end


% if analysis mode : print i_cas number
switch mode1;
    case 0
    str = num2str(i_cas);
    text(xR,yR,str);
    otherwise
end


