function  yplus = eval_yplus (y_cl,affichage,data_geo,data_sim,data_phy)

%compute y+ (hyp: flate plate turbulent regime)

% c     reference length in m
% Vinf  reference velocity in m/s
% y_cl  height of the first grid point from the surface in m

% Biblio : see FLUENT documentation
%          see http://www.ansys.com/staticassets/ANSYS/Conference/Confidence/Detroit/Downloads/the-most-accurate-and-advanced-turbulence-capabilities.pdf
%
% G. Iaccarino  : RANS      y+<  5 without wall-function turbulence modeling
%               : RANS      y+=100 with    wall-function
%               : LES, DNS  y+<1
%
c           = data_geo.c;
Vinf        = data_sim.Vinf;
Tu_Model    = data_sim.Tu_model;      % Turbulence model (SA, SST, SSTtr)
nu          = data_phy.nu;
Re          = Vinf*c/nu;

flow = 'external_flows';
switch flow
    case 'external_flows'
        Cfdiv2  = 0.0359*Re^-0.2;   % external flows
    case 'internal_flows'
        Cfdiv2  = 0.0395*Re^-0.25   % internal flows
end
Utau    = Vinf*sqrt(Cfdiv2);
y_size  = nu/Utau;
yplus   = round(10*y_cl*c/y_size)/10;

switch Tu_Model
    case 'SA'
        recommended_std_yplus   = 30;
        recommended_LowRe_yplus = 5;
    case 'ke'
        recommended_std_yplus   = 30;
        recommended_LowRe_yplus = 5;
    case 'SST'
        recommended_std_yplus   = 30;
        recommended_LowRe_yplus = 5;
    otherwise
        recommended_std_yplus   = 30;
        recommended_LowRe_yplus = 5;
end

% on screen
if strcmp(affichage,'yes')
    fprintf(' eval_yplus \n');
    fprintf('     Chord Reynolds number : %g  \n',Re);
    fprintf('     Wall y+ estimated     : %g  (hyp : turbulent flat plate)\n',yplus);
    fprintf('     Wall y+ recommended   > %g  \n',recommended_std_yplus);
    if yplus < recommended_LowRe_yplus
        fprintf('     Wall mesh adapted for viscous sublayer prediction)\n');
    end
    if yplus > recommended_std_yplus
        fprintf('     Wall mesh adapted for standard wall treatment)\n');
    end
    if (yplus < recommended_std_yplus) || (yplus > recommended_LowRe_yplus)
        fprintf('     Wall mesh may be not well chosen)\n');
    end
end

% in file
ai_file = data_sim.ai_file;
fid=fopen(ai_file,'a+');
fprintf(fid,'==================================================\n');
fprintf(fid,' eval_yplus \n');
fprintf(fid,'     Chord Reynolds number : %g  \n',Re);
fprintf(fid,'     Wall y+ estimated     : %g  (hyp : turbulent flat plate)\n',yplus);
fprintf(fid,'     Wall y+ recommended   > %g  \n',recommended_std_yplus);
if yplus < recommended_LowRe_yplus
    fprintf(fid,'     Wall mesh adapted for viscous sublayer prediction)\n');
end
if yplus > recommended_std_yplus
    fprintf(fid,'     Wall mesh adapted for standard wall treatment)\n');
end
if (yplus < recommended_std_yplus) || (yplus > recommended_LowRe_yplus)
    fprintf(fid,'     Wall mesh may be not well chosen)\n');
end

