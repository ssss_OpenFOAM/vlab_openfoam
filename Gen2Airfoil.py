from BezierN import BezierN
from xfoil_dat import *
import numpy as np
import matplotlib.pyplot as plt


def gen2airfoil(gen,name):
    upx = [0, 0.01,  0.25,0.5,0.75,1]
    downx = upx
    upy =   [0]*6
    downy = [0]*6
              # Leading edge
    upy   [1] = gen[0]
    downy [1] = - gen[1]
              # camber + thickness
    upy   [2] = gen[2] + gen[5]
    upy   [3] = gen[3] + gen[6]
    upy   [4] = gen[4] + gen[7]
    factor=1.3
    downy [2] = gen[2] - gen[5]*factor
    downy [3] = gen[3] - gen[6]*factor
    downy [4] = gen[4] - gen[7]*factor
    """
    N=0.5
    upy =   [0,  0.05,    0.2,  0.15,  0.1 ,       0]
    upy=[x*N for x,y in upy,gen]
    downy = [0, -0.05,   -0.1,  0.10,  0.05,       0]

    """
    #downy=[x*0.5 for x in downy]
    #generate foil
    #n=50;
    """print upx
    print upy

    print ""

    print downx
    print downy"""

    n=100;
    MyBezier = BezierN(6)
    pupx  = MyBezier.interpolate(upx   ,n)
    pupy  = MyBezier.interpolate(upy   ,n)
    pdownx= MyBezier.interpolate(downx ,n)
    pdowny= MyBezier.interpolate(downy ,n)

    # save foil
    foilfile = open(name+".dat",'w')
    foilfile.write(name+"\n")
    for i in range (n,0,-1):
         foilfile.write(  " %1.6f    %1.6f\n" %(pupx[i],pupy[i]))
    for i in range (0,n+1):
         foilfile.write(  " %1.6f    %1.6f\n" %(pdownx[i],pdowny[i]))
    foilfile.close()

    foilfile = open(name+".datBezier",'w')
    foilfile.write(name+"\n")
    for i in range (len(upx)):
         foilfile.write(  " %1.6f    %1.6f\n" %(upx[i],upy[i]))
    for i in range (len(upx)):
         foilfile.write(  " %1.6f    %1.6f\n" %(downx[i],downy[i]))
    foilfile.close()
    dataAirfoil=np.loadtxt(name+".dat",delimiter="    ",skiprows=1)

    dataAirfoilBezier=np.loadtxt(name+".datBezier",delimiter="    ",skiprows=1)
    plot=False
    if plot == True:
        fig,ax=plt.subplots(1)

        ax.set_title('Airfoil')
        ax.plot(dataAirfoil[:,0],dataAirfoil[:,1])
        ax.plot(dataAirfoilBezier[:,0],dataAirfoilBezier[:,1],'go')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.ylim([-0.5,0.5])
        fig.tight_layout()  # make layout as tight as possible
        plt.show()


Re = 160000
Ncrit = 9.0
"""
    upy   [1] = gen[0]
    downy [1] = - gen[1]
    # camber + thickness
    upy   [2] = gen[2] + gen[5]
    upy   [3] = gen[3] + gen[6]
    upy   [4] = gen[4] + gen[7]

    downy [2] = gen[2] - gen[5]
    downy [3] = gen[3] - gen[6]
    downy [4] = gen[4] - gen[7]
"""
#testgen = [ 0.05,  0.025,    0.1,  0.1,   0.05,     0.1,  0.2,  0.025   ]
# First parameter and second define the height of the nodes which are at 0
# Maybe we want them to be symmetrical to ensure that the airfoil is rounded at the leading edge?



#               0    1     2     3      4    5     6     7
testgen= [    0.08,  0.08,  0.08,  0.08,   0.08, 0.13,  0.13,  0.08   ]
#testgen = [ 0.0376,  0.0121,  0.0845, 0.1290,  0.0496,  0.0295,  0.1326,  0.0277   ]

name = "myfoil_1111"
gen2airfoil(testgen,name)
#Xfoil(name,Ncrit,Re)
#print name,getLDmax(name)
